/** @file Element.h
    @brief Interface for the Element class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Dmytro Skrypnyk.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_ELEMENT_H
#define UIPF_ELEMENT_H

#include <QString>
#include <QPainterPath>

struct Element {
  int href;
  QPainterPath path;
  QString classname;
}; // struct Element

#endif // UIPF_ELEMENT_H
