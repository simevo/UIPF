/** @file WidgetTableDouble.h
    @brief Interface for the WidgetTableDouble class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera, Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_WIDGETTABLEDOUBLE_H
#define UIPF_WIDGETTABLEDOUBLE_H

#include <QWidget>
#include <QStandardItemModel>

#include "UnitEngine.h" // for Units

// forward declarations
class ModelQuantityEditable;
class DelegateUnit;
class ViewTable;
class QUndoStack;

class WidgetTableDouble : public QWidget {
  Q_OBJECT

public:
  WidgetTableDouble(const char* filter, QUndoStack* undoStack, QWidget* parent = 0);
  void clear(void);
  void setRw(bool b);
  /// Ensure the displayed values match the displayed Uom
  void resetUom();
  /// Traverse the table and set the Uom to match the selected global setting
  void setUom(Units u);
  ViewTable* view;
  bool editStatus_;

  QString getTbviewSelectedItem(void);
  void setTbviewSelectAll(void);

private:
  ModelQuantityEditable* model_;
  //  ViewTable* view;
  QStandardItemModel* model1_;
  QStringList listHeader_;
  DelegateUnit *ufDelegate;

signals:
  void forceGo(int node);

public slots:
  void enteredItem(const QModelIndex & index);
  /// Ensure the value displayed is updated on changing of the Uom combo box
  void indexChanged(int indx, QObject *objPointer);
  void go(int node = 0);
  void setChild(bool child);
  void emitForceGo(int node, int row);
}; // class WidgetTableDouble

#endif // UIPF_WIDGETTABLEDOUBLE_H
