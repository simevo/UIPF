/** @file strtopath.h
    @brief strToPath function declaration

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from Dmytro Skrypnyk.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_STRTOPATH_H
#define UIPF_STRTOPATH_H

#include <QString>
#include <QPainterPath>

QPainterPath strToPath(QString str);

#endif // UIPF_STRTOPATH_H
