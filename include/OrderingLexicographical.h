/** @file OrderingLexicographical.h
    @brief OrderingLexicographical class declaration

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#ifndef UIPF_ORDERINGLEXICOGRAPHICAL_H
#define UIPF_ORDERINGLEXICOGRAPHICAL_H

#include <cstdio>
#include <cassert>
#include <vector>
#include <map>
#include <vector>
#include <set>

#include "Ordering.h"

class OrderingLexicographical : public Ordering {
private:
  static const char name_ [];
  mutable int id_;

public:
  explicit OrderingLexicographical(const std::vector<int> &m);

  // implement Ordering pure virtual functions
  int idFromRound(int round) const;
  int roundFromId(int id) const;
}; // class OrderingLexicographical

#endif // UIPF_ORDERINGLEXICOGRAPHICAL_H
