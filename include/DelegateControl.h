/** @file DelegateControl.h
    @brief Interface for the DelegateControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DELEGATECONTROL_H
#define UIPF_DELEGATECONTROL_H

#include <QStyledItemDelegate>
#include "UnitEngine.h"

class QSqlQueryModel;

/// A delegate class for ControlTableWidget.
class DelegateControl : public QStyledItemDelegate {
  Q_OBJECT
public:
  DelegateControl(int caseId, Units *u, QObject* parent = 0);
  /// Reimplemented from QStyledItemDelegate::createEditor
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
  /// Reimplemented from QStyledItemDelegate::setEditorData
  void setEditorData(QWidget* editor, const QModelIndex &index) const;
  /// Reimplemented from QStyledItemDelegate::setModelData
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex &index) const;

  /// Returns the index of the specified fullTag in the comboModel_, otherwise returns -1.
  int hasFullTag(const QString & fullTag) const;
  
private slots:
  /// Commits the data into the model and closes the combo displayed in the cell of the second column
  void commitAndCloseCombo(void);
  /// Commits the data into the model and closes the line edit displayed in the cell of the label, start and end columns.
  void commitAndCloseLineEdit(void);
  /// Commits the data into the model and closes the spinbox editor displayed in the points column.
  void commitAndCloseSpinBox(void);

private:
  QSqlQueryModel* comboModel_;
  /// Returns the siValue for the new entered value. newValue is the entered value
  /// @param newUnit is the displayed Uom
  QString setNewValue(double newValue, QString newUnit) const;

  // Used to update the units of measurement
  Units *u_;

signals:
  /// signal emitted when the value was changed
  void currentIndexChanged();
}; // class DelegateControl

#endif // UIPF_DELEGATECONTROL_H
