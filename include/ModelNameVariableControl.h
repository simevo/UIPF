/** @file ModelNameVariableControl.h
    @brief Interface for the ModelNameVariableControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_MODELNAMEVARIABLECONTROL_H
#define UIPF_MODELNAMEVARIABLECONTROL_H

#include <QStringListModel>

class QSqlQueryModel;

/// A model class for a QComboBox that disables some choices.
class ModelNameVariableControl : public QStringListModel {
  Q_OBJECT
public:
  ModelNameVariableControl(const QString & currentVal, const QSqlQueryModel* dbModel, int dbNameCol, const QAbstractItemModel* tblModel, QObject* parent = 0);

  virtual Qt::ItemFlags flags(const QModelIndex & index) const;

private:
  QList<bool> enabled_;
};

#endif // UIPF_MODELNAMEVARIABLECONTROL_H
