/** @file VariableControlled.h
    @brief Interface for the VariableControlled class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_VARIABLECONTROLLED_H
#define UIPF_VARIABLECONTROLLED_H

#include <string>

#include <QMetaType> // for Q_DECLARE_METATYPE

class VariableControlled {
private:
  double start_;
  double end_;
  int points_;
  std::string tag_;
  std::string units_;
  int id_;

public:
  VariableControlled(double start, double end, int points, const std::string &tag, const std::string &units, int id);
  VariableControlled(void);
  double start(void) const;
  double end(void) const;
  int points(void) const;
  const std::string &tag(void) const;
  const std::string &units(void) const;
  int id(void) const;
}; // class VariableControlled

#endif // UIPF_VARIABLECONTROLLED_H
