/** @file ModelQuantityEditable.h
    @brief Interface for the ModelQuantityEditable class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_MODELQUANTITYEDITABLE_H
#define UIPF_MODELQUANTITYEDITABLE_H

#include <QWidget>
#include <QModelIndex>
#include <QHash>

#include "ModelEditable.h"

class QUndoStack;

class ModelQuantityEditable : public ModelEditable {
  Q_OBJECT
public:
  ModelQuantityEditable(const char* filter, QWidget* parent = 0);

  void go(int node);
  QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex &index, const QVariant &value, int role);
  bool setQ(int dbid, double value);
  void setHeaders(void);
  QStringList getHeaders(void);

protected:
  // ModelEditable member functions implemented
  int editableColumn(void) const { return Qtbl_Q; }
  int idColumn(void) const { return Qtbl_Id; }
  int tagColumn(void) const { return Qtbl_Tag; }
  int descriptionColumn(void) const { return Qtbl_Description; }

private:
  QStringList lHeader_;
  QHash<int, QVariant> qValue_;
  QHash<int, QVariant> uValue_;
}; // class ModelQuantityEditable

#endif // UIPF_MODELQUANTITYEDITABLE_H
