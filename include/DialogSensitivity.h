/** @file DialogSensitivity.h
    @brief Interface for the DialogSensitivity class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian, Yasantha Samarasekera and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DIALOGSENSITIVITY_H
#define UIPF_DIALOGSENSITIVITY_H

#include <QDialog>
#include <QMetaType> // for Q_DECLARE_METATYPE

#include "ActionBase.h"
#include "UnitEngine.h"

class WidgetTableControl;
class QTabWidget;
class QProgressBar;
class WidgetTableMonitor;
class QTableWidget;
class QAbstractTableModel;
class ActionLibpfSensitivity;
class QLineEdit;
class QComboBox;

/// The class DialogSensitivity implements the dialog for a multi-dimensional sensitivity study.
class DialogSensitivity : public QDialog {
  Q_OBJECT
public:
  /// Constructor
  /// @param caseId the current case ID
  /// @param type the case type?
  /// @param goAction action to perform when Go button is pressed.
  /// @param parent the parent widget
  DialogSensitivity(int caseId, const std::string &type, ActionLibpfSensitivity* goAction, Units u, QWidget* parent = 0);
  bool uomFlag_;

  /// Loads a given SensitivityInput file
  /// @param fileName The absolute file path
  /// @param name The sensitivity name
  /// @param type The sensitivity type
  bool loadSensitivity(const QString& fileName, const QString& name, const QString &type);

signals:
  void controlIncomplete(bool enable);
  void controlComplete(bool enable);
  void monitorIncomplete(bool enable);
  void monitorComplete(bool enable);

private slots:
  /// Sets the current tab to the page at index
  void setCurrentPage(int index);
  /// Activates the control tab
  void selectControlPage(void);
  /// Activates the monitor tab
  void selectMonitorPage(void);
  /// Activates the run tab
  void selectRunPage(void);

  /// Sets up the results table and buttons on the run tab
  void onCurrentPageChanged(int index);
  /// Sets up the dialog in control_incomplete configuration
  void controlIncompleteSetup(void);
  /// Sets up the dialog in control_complete configuration
  void controlCompleteSetup(void);
  /// Sets up the dialog in monitor_incomplete configuration
  void monitorIncompleteSetup(void);
  /// Sets up the dialog in monitor_complete configuration
  void monitorCompleteSetup(void);
  /// Saves the current analysis to a XML
  void monitorSaveAnalysis();
  /// Sets up the dialog in run_empty configuration
  void runEmptySetup(void);
  /// Sets up the dialog in run_running configuration
  void runRunningSetup(void);
  /// Sets up the dialog in run_stopped_partial configuration
  void runStoppedPartialSetup(void);
  /// Sets up the dialog in run_complete configuration
  void runCompleteSetup(void);

  /// Cancels the dialog
  void cancel(void);
  /// Restarts the dialog, called if user clicks "Edit sensitivity"
  void restart(void);
  /// Starts the go action
  void go(void);
  /// Stops the go action
  void stop(void);
  /// Copies the results to the clipboard.
  void copyResults(void);
  /// Updates the progress bar and color codes the result table row.
  void rowFinished(int ID, enum RowStatus rs);

private:
  /// Tests the control page completion.
  bool testMonitorCompletion(void);
  /// Tests the monitor page completion
  bool testControlCompletion(void);

protected:
  /// Reimplements QDialog::closeEvent
  void closeEvent(QCloseEvent* event);
  /// Reimplements QDialog::keyPressEvent
  void keyPressEvent(QKeyEvent* event);

private:
  /// Return new display value from changing of Uom. siUnit is original si value and
  /// @param newUnit is the new Uom
  QString getNewUnit(double siUnit, QString newUnit);
  QTabWidget* tabWidget_;

  WidgetTableControl* controlTableWidget_;
  QPushButton* controlAddButton_;
  QPushButton* controlCancelButton_;
  QPushButton* controlNextButton_;
  QLineEdit *controlTimeOut_;
  QComboBox *controlOrdering_;
  // QPushButton* controlPreviousButton_;

  WidgetTableMonitor* monitorTableWidget_;
  QPushButton* monitorAddButton_;
  QPushButton* monitorCancelButton_;
  QPushButton* monitorSaveButton_;
  QPushButton* monitorNextButton_;
  QPushButton* monitorPreviousButton_;
  QString lastSavedAnalysisName_;

  QTableWidget* runTableWidget_;
  QAbstractTableModel* runModel_;
  QProgressBar* runProgressBar_;
  QPushButton* runCancelButton_;
  QPushButton* runGoButton_;
  QPushButton* runStopButton_;
  QPushButton* runCopyButton_;
  QPushButton* runEditButton_;

  int caseId_;
  std::string type_;
  bool resultsCopied_;
  bool someResultsAvailable_;
  ActionLibpfSensitivity* goAction_;
}; // class DialogSensitivity

#endif // UIPF_DIALOGSENSITIVITY_H
