/** @file LineEditValidated.h
    @brief Interface for the LineEditValidated class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Harish Surana, Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_LINEEDITVALIDATED_H
#define UIPF_LINEEDITVALIDATED_H

#include <QLineEdit>

class QLabel;

class LineEditValidated : public QLineEdit {
  Q_OBJECT

public:
  LineEditValidated(QWidget* parent, QLabel* label, QString acceptedChars);
  ~LineEditValidated(void);

private slots:
  void onShowToolTip(const QString &);
  void onHideToolTip(void);
  void onTextChanged(void);

private:
};

#endif // UIPF_LINEEDITVALIDATED_H
