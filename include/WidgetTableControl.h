/** @file WidgetTableControl.h
    @brief Interface for the WidgetTableControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera, Shane Shields and Luiz A. Buhnemann (la3280@gmail.com).

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_WIDGETTABLECONTROL_H
#define UIPF_WIDGETTABLECONTROL_H

#include <QTableView>

#include "UnitEngine.h" // for Unit

class DelegateUnit;

/// Widget for showing a table of control variables.
class WidgetTableControl : public QTableView {
  Q_OBJECT
private:
  Units u_;
  DelegateUnit *ufDelegate;

public:
  /// Constructor
  explicit WidgetTableControl(int caseId, QWidget* parent = 0);
  /// If the data model contains valid data or is empty returns true, otherwise returns false
  bool hasValidData(void) const;
  /// If the data model is not empty returns true, otherwise returns false
  bool hasData(void) const;
  /// set the displayed values to match the Uom for the global setting
  void setUom(Units u);

signals:
  void edited(void);

public slots:
  /// Removes r from the model
  void removeRow(int r);
  /// Appends an empty row to the model
  void appendRow(void);

private slots:
  /// Resizes table column widths to fill al available space.
  void resizeToContents(void);
  void updateUom(void);
  /// Ensure the displayed values immediately match the chosen Uom in the dropdown combobox
  void indexChanged(int indx, QObject *objPointer);
protected:
  void resizeEvent(QResizeEvent* event);
}; // class WidgetTableControl

#endif // UIPF_WIDGETTABLECONTROL_H
