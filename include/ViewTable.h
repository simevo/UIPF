/** @file ViewTable.h
    @brief Interface for the ViewTable class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_VIEWTABLE_H
#define UIPF_VIEWTABLE_H

#include <QTableView>
#include <QStandardItemModel>
#include <QModelIndex>

class QTableView;

class ViewTable : public QTableView {
private:
  int columnToHide_;

public:
  explicit ViewTable(int cth);
  void dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight, const QVector<int> &);
};

#endif // UIPF_VIEWTABLE_H
