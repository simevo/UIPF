/** @file ViewSvgNative.h
    @brief Interface for the ViewSvgNative class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Dmytro Skrypnyk.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_VIEWSVGNATIVE_H
#define UIPF_VIEWSVGNATIVE_H

#include <QWidget>

class QPaintEvent;
class QSvgRenderer;
class QWheelEvent;

class ViewSvgNative : public QWidget {
  Q_OBJECT

public:
  ViewSvgNative(QString &file, QWidget* parent = 0);

  virtual QSize sizeHint(void) const;

protected:
  virtual void paintEvent(QPaintEvent* event);
  virtual void wheelEvent(QWheelEvent* event);

private:
  QSvgRenderer* doc_;

  friend class ScrollAreaSvg;
}; // class ViewSvgNative

#endif // UIPF_VIEWSVGNATIVE_H
