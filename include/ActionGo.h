/** @file ActionGo.h
    @brief Interface for the ActionGo class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_ACTIONGO_H
#define UIPF_ACTIONGO_H

/** @file ActionGo.h

   The class ActionGo represents the base class of for the actions triggered by the Go button in the Run tab.
   The programmer should reimplement the method compute(int). The default implementation just fills the model
   with random data
 */

#include <QThread>
#include <QMutex>

#include "ActionBase.h"
#include "VariableControlled.h"

class ActionGo;

class ActionGo : public QThread, public ActionBase {
  Q_OBJECT
public:
  /// Constructor
  ActionGo(void);
  /// The method stops the action.
  void stop(void);
  void setControlledVariable(const VariableControlled *controlledVariable);
  const VariableControlled* controlledVariable(void) const;

protected:
  /// This method contains the code which generates results for the row r.
  virtual void compute(int row);
  virtual void kill(void) = 0;
  /// This method contains the code which generates results for the row r.
  void run(void);
  QMutex mutex;
  // ActionBase baseAction_;

signals:
  /// This is emited if the action is stopped by the user
  void stopped(void);
  /// This signal should be emitted during the various stages and statuses ActionGo::compute method
  void computed(int ID, RowStatus rs);
  /// This is emited if the action is completed
  void complete(void);

private:
  const VariableControlled* control_;
}; // class ActionGo

#endif // UIPF_ACTIONGO_H
