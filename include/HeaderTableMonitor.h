/** @file HeaderTableMonitor.h
    @brief Interface for the HeaderTableMonitor class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_HEADERTABLEMONITOR_H
#define UIPF_HEADERTABLEMONITOR_H

#include <QWidget>
#include <QHeaderView>

class HeaderTableMonitor : public QHeaderView {
  Q_OBJECT
public:
  /// Constructor
  explicit HeaderTableMonitor(QWidget* parent = 0);

protected:
  /// Repaints the section such that a remove button is displayed
  void paintSection(QPainter* painter, const QRect & rect, int logicalIndex) const;
}; // class HeaderTableMonitor

#endif // UIPF_HEADERTABLEMONITOR_H
