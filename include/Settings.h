/** @file Settings.h
    @brief Interface for the Settings class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_SETTINGS_H
#define UIPF_SETTINGS_H

#include <QSettings>

class Settings : public QSettings {
public:
  Settings(void);

  QString homePath(void);
  QString home(void);
  QString kernelPath(void);
  QString fullKernelName(void);
  QString svgFile(int id);
  QString kernelName(void);
  QString units(void);
  void setUnits(QString unit);
  bool child(void);
  void toggleChild();
}; // class Settings

#endif // UIPF_SETTINGS_H
