/** @file Hierarchy.h
    @brief Interfaces to query the class Hierarchy for reflection

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_HIERARCHY_H
#define UIPF_HIERARCHY_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>

#include "Type.h"

/// LIBPF reflection Hierarchy
class Hierarchy {
private:
  int lineCount_;

  std::vector<Type> types_;
  /// maps names to ids
  std::map<std::string, int> typesmap_;
  /// maps ids to names
  std::map<int, std::string> rtypesmap_;
  /// maps base to derived ids
  std::map<int, int> dependencies_;
  /// transitive closure for maps base to derived ids
  std::set<std::pair<int, int> > tc_;

  /// maps generic name to templated ids
  std::map<std::string, std::list<int> > generics_;

  std::vector<Type>::iterator m_;

  /// adds a new type with id=1, name=n, description=d
  int insertType(std::string n, std::string d = "");
  /// introduces a dependency relationship in the database
  void insertDependency(int ibase, int iderived);
  /// introduces a dependency relationship in the database, based on class names
  /// returns the number of actually inserted pais in tc_
  void insertDependency(std::string base, std::string derived);
  /// used for typedefs
  void insertSynonym(std::string newname, std::string source);

  /// compute the transitive closure
  /// returns the number of actually inserted pais in tc_
  int transitiveClosure(int ibase, int iderived);
  /// auxiliary function for computing transitive closure
  /// returns the number of actually inserted pais in tc_
  int closeDown(int ibase, int iderived);

  /// returns id for type name n; if not found, return -1
  int id_(std::string n);
  /// returns name for type id i; if not found, return ""
  const std::string &name_(int i);

  /// isA and isAGeneric utility function
  bool isA_(int ibase, int iderived);

public:
  explicit Hierarchy(std::string typesFile);

  /// returns true if the class c is derived from any class based on exactly the specified template of s
  bool isA(std::string c, std::string s);
  /// returns true if the class c is derived from any class based on the template class s
  bool isAGeneric(std::string c, std::string s);

  /// start enumerating all types
  void start(void);
  /// continue enumerating all types by returning the next one; when over, returns NULL
  const Type* next(void);
  /// enumeration of types is over
  bool end(void);
}; // class Hierarchy

#endif // UIPF_HIERARCHY_H
