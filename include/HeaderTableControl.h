/** @file HeaderTableControl.h
    @brief Interface for the HeaderTableControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_HEADERTABLECONTROL_H
#define UIPF_HEADERTABLECONTROL_H

#include <QStringListModel>
#include "HeaderTableMonitor.h"

/// Row header for the rows in ControlTableWidget.
class HeaderTableControl : public HeaderTableMonitor {
  Q_OBJECT
public:
  /// Constructor
  explicit HeaderTableControl(QWidget* parent = 0);
}; // class HeaderTableControl

#endif // UIPF_HEADERTABLECONTROL_H
