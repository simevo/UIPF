/** @file ModelDataControl.h
    @brief Interface for the ModelDataControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_MODELDATACONTROL_H
#define UIPF_MODELDATACONTROL_H

#include "RecordControl.h"

/// A model class for WidgetTableControl
class ModelDataControl : public QAbstractTableModel {
  Q_OBJECT
public:
  /// Constructor
  explicit ModelDataControl(QObject* parent = 0);
  virtual Qt::ItemFlags flags(const QModelIndex & index) const;
  /// Returns the row count of the model
  virtual int rowCount(const QModelIndex & index = QModelIndex()) const;
  /// Returns the colum count of the model
  virtual int columnCount(const QModelIndex & index = QModelIndex()) const;
  virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
  virtual bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
  /// Return the displayed value matching the selected Uom. siUnis is the original unit
  /// @param newUnit the Uom to be converted to
  QString getNewUnit(double siUnit, QString newUnit) const;
  virtual bool insertRows(int row, int count, const QModelIndex & parent = QModelIndex());
  virtual bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex());
  virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
  bool rowValid(int row) const;

private:
  QList<RecordControl> data_;
  QHash<int, QVariant> sValue_;
  QHash<int, QVariant> eValue_;
  QHash<int, QVariant> uValue_;
}; // class ModelDataControl

#endif // UIPF_MODELDATACONTROL_H
