/** @file ValidatorText.h
    @brief Interface for the ValidatorText class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_VALIDATORTEXT_H
#define UIPF_VALIDATORTEXT_H

#include <QValidator>

class QLabel;

/// Validator class for validation of the text.
class ValidatorText : public QValidator {
  Q_OBJECT

public:
  /// The Constructor
  /// @param parent Ideally it should point to line edit itself because tool tip will be shown reference to parent
  /// @param label It is the label whose text will be used while showing tooltip
  /// @param acceptedChars It is the string containing allowed characters
  ValidatorText(QObject* parent, QLabel* label, QString acceptedChars);

  /// The Destructor
  ~ValidatorText(void);

  /// Function to set label
  /// @param label It is used while showing tooltip
  void setLabel(QLabel* label);

  /// Function to set accepted characters
  /// @param acceptedChars It is the string containing allowed characters
  void setAcceptedChars(QString acceptedChars);

  /// Function which has validation logic and tool tip functionality
  /// @param input It is the string which need to be validated
  /// @param pos It is the position of the cursor
  virtual QValidator::State validate(QString & input, int & pos) const;

signals:
  void showToolTip(const QString &) const;
  void hideToolTip(void) const;

private:
  QLabel* label_;              //Tag Label
  QString acceptedChars_;      //Accepted Characters
}; // class ValidatorText

#endif // UIPF_VALIDATORTEXT_H
