/** @file DialogNew.h
    @brief Interface for the DialogNew class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DIALOGNEW_H
#define UIPF_DIALOGNEW_H

#include <QDialog>

class QRadioButton;
class QPushButton;
class QGridLayout;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class LineEditValidated;
class QGroupBox;

class DialogNew : public QDialog {
  Q_OBJECT

public:
  explicit DialogNew(QWidget* parent = 0);
  void addType(const char* type, const char* description);
  void addOption(const char* name, const char* description);
  int typesCount(void);
  void lockType(void);

signals:
  void open(const QString &type, const QString &tag, const QString &description);

private slots:
  void okClicked(void);

private:
  QLabel* tagLabel_;
  LineEditValidated* tagEdit_;
  QLabel* descriptionLabel_;
  LineEditValidated* descriptionEdit_;

  std::list<QRadioButton *> sx_;
  std::list<QLabel *> dx_;
  int typesCount_;
  int optionsCount_;

  QHBoxLayout* bottom_;
  QGridLayout* types_;
  QGroupBox* typesGroup_;
  QGridLayout* options_;
  QGroupBox* optionsGroup_;
  QHBoxLayout* titles_;
  QVBoxLayout* top_;
  QVBoxLayout* all_;

  QPushButton* ok_;
  QPushButton* cancel_;
}; // class DialogNew

#endif // UIPF_DIALOGNEW_H
