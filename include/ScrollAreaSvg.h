/** @file ScrollAreaSvg.h
    @brief Interface for the ScrollAreaSvg class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Dmytro Skrypnyk.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_SCROLLAREASVG_H
#define UIPF_SCROLLAREASVG_H

#include <QPoint>
#include <QScrollArea>
#include <QString>
#include <QSvgRenderer>

#include "DataXml.h"

class QMouseEvent;
class QWidget;

class ScrollAreaSvg : public QScrollArea {
  Q_OBJECT

public:
  ScrollAreaSvg(void);
  void clear(void);

  void mousePressEvent(QMouseEvent* event);
  void mouseMoveEvent(QMouseEvent* event);
  void mouseReleaseEvent(QMouseEvent* event);
  void wheelEvent(QWheelEvent*);
  void openFile(const QString &file);
  void setHighQualityAntialiasing(bool highQualityAntialiasing);

signals:
  void select(int);

private:
  QWidget* view_;

  QPoint mousePressPos_;
  QPoint scrollBarValuesOnMousePress_;
  QString currentPath_;

  bool highQualityAntialiasing_;

  DataXml data_;
  QMap<QString, QRect> rects_;
  QMap<QString, QPainterPath> paths_;
  void calcRects(QSvgRenderer* doc);
}; // class ScrollAreaSvg

#endif // UIPF_SCROLLAREASVG_H
