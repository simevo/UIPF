/** @file UnitEngine.h
    @brief Contains the interface to the units of measurement classes.

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2004-2021 Paolo Greppi simevo s.r.l.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_UNITENGINE_H
#define UIPF_UNITENGINE_H

#include <string>
#include <map>
#include <list>

#include "Dimension.h"

class UnitEngine {
public:
  typedef std::map<std::string, Dimension*> dictionary_t;

private:
  UnitEngine(const UnitEngine &);              ///< not implemented, making class non copyable
  UnitEngine &operator=(const UnitEngine &);   ///< not implemented, making class non copyable

  std::list<Dimension> dimensions_;
  dictionary_t descLookup_;
  dictionary_t unitLookup_;
  mutable dictionary_t::const_iterator i_;

public:
  UnitEngine(void);

  void addDimension(std::string desc, std::string SIu, int kmol, int rad, int A, int m, int cd, int kg, int K, int s, int eur = 0);
  void addUnit(Units u, std::string desc, std::string unit, double factor, double offset = 0);
  void addUnit(std::string desc, std::string unit, double factor, double offset = 0);

  double fromSI(double v, const std::string &destination_unit) const;
  double toSI(double v, const std::string &source_unit, UnitArray &unit) const;

  /// return a pointer to the Dimension based on the unit
  const Dimension* pdimension(std::string unit);

  // read-only iterate through supported dimensions
  dictionary_t::const_iterator begin(void) const;
  dictionary_t::const_iterator next(void) const;
  dictionary_t::const_iterator end(void) const;

  /// diagnostic print
  void printUnits(void) const;
  /// output conversion in JSON format
  void printJson(void) const;
}; // class UnitEngine

extern UnitEngine unitEngine_;

#endif // UIPF_UNITENGINE_H
