/** @file ModelEditable.h
    @brief Interface for the ModelEditable class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_MODELEDITABLE_H
#define UIPF_MODELEDITABLE_H

#include <QWidget>
#include <QSqlQueryModel>
#include <QModelIndex>

class QUndoStack;

enum {
  Qtbl_Tag = 0,
  Qtbl_Q = 1,
  Qtbl_Uom = 2,
  Qtbl_Description = 3,
  Qtbl_Id = 4
};

enum {
  Itbl_Tag = 0,
  Itbl_I = 1,
  Itbl_Description = 2,
  Itbl_Id = 3
};

class ModelEditable : public QSqlQueryModel {
  Q_OBJECT
public:
  explicit ModelEditable(QWidget* parent);

  Qt::ItemFlags flags(const QModelIndex &index) const;
  void go(int node = 0);
  void setChild(bool child);
  void rwStatus(bool b);
  void clear(void);
  int id(int row);
  QString tag(int row);
  QString description(int row);
  QString completeTag(int row);
  void setUndoStack(QUndoStack* undoStack);  ///< Function to set undo stack

  // Functions to emit changes
  void emitDataChanged(const QModelIndex &index);
  void emitForceGo(int node, int row);

signals:
  void forceGo(int node, int row);

protected:
  int currentNode_;
  std::string extraFilter_;
  //Pointer to undo stack
  QUndoStack* undoStack_;
  // if true, only show current node else include all descendants
  bool child_;
  bool rwStatus_;
  // number of characters to strip_ from completetag
  int strip_;
  // range of ids to show
  int range_;
  // fulltag of parent object
  QString prefix_;

  // functions to be implemented by derivate classes
  virtual int editableColumn(void) const = 0;
  virtual int idColumn(void) const = 0;
  virtual int tagColumn(void) const = 0;
  virtual int descriptionColumn(void) const = 0;
}; // class ModelEditable

#endif // UIPF_MODELEDITABLE_H
