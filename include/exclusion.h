/** @file exclusion.h
    @brief Interface to the Windows-specific code to avoid multiple instances of the application

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based on code by Joseph M. Newcomer from: http://www.flounder.com/nomultiples.htm

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_EXCLUSION_H
#define UIPF_EXCLUSION_H

/****************************************************************************
*                         testUniqueness
* Result:
*       Returns true if window exists, false if not.
*       Multiple instances are checked that are started for the same user account,
*       no matter how many desktops or sessions running under this account
*       exist, but allowing multiple instances to run concurrently for
*       sessions running under a different user account.
*
* Effect:
*       Attempts to create an interlock with a properly crafted ID.
****************************************************************************/
bool testUniqueness(void);

#endif // UIPF_EXCLUSION_H
