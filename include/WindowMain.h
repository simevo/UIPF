/** @file WindowMain.h
    @brief Interface for the WindowMain class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera, Harish Surana, Shane Shields
    and Luiz A. Buhnemann (la3280@gmail.com).

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_WINDOWMAIN_H
#define UIPF_WINDOWMAIN_H

#include <QMainWindow>
#include <QProcess>
#include <QTableView>
#include <QActionGroup>

#include "Dimension.h" // for Units

class WidgetTableDouble;
class WidgetTree;
class QTextEdit;
class QSplitter;
class QTabWidget;
class ScrollAreaSvg;
class DialogNew;
class DialogOpen;
class DialogDuplicate;
class QProcess;
class WidgetTableMessages;
class QToolBar;
class QLabel;
class QPixmap;
class QUndoStack;
class Settings;

class WindowMain : public QMainWindow {
  Q_OBJECT

public:
  WindowMain(void);
  ~WindowMain(void);

  QString kernelName(void);
  void setChild(bool child);

public slots:
  void toggleChild(void);

private slots:
  void tablewInputCustomContextMenu(const QPoint &);
  void tablewOutputCustomContextMenu(const QPoint &);
  void checkInContextMenu(const QModelIndex &index);
  void checkOutContextMenu(const QModelIndex &index);
  void editShowMenuClicked(void);
  void editHideMenuClicked(void);

  void acopy_(void);
  void aselectAll_(void);

  void new_(void);
  void open_(void);
  void save_(void);
  void dumpTxt_(void);
  void openTxtDump_(void);
  void dumpHtml_(void);
  void openHtmlDump_(void);
  void delete_(void);
  void homotopy(void);
  void purge_(void);

  void reset_(void);
  void calculate_(void);
  void stop_(void);

  /// Shows the Dialog that opens a previously saved sensitivity
  void sensitivityOpen(void);
  /// Restores last ran sensitivity, if called without the sensitivityFilePath paramenter
  /// it will assume you are trying to restore the last ran analysis, if you don't provide
  /// the sensitivityName or the sensitivityType params the function will try to get them
  /// from the sensitivityFilePath
  /// @param sensitivityFilePath Absolute file path for the sensitivity xml file
  /// @param sensitivityName The sensitivity name
  /// @param sensitivityType The sensitivity type
  void sensitivityRestore(const QString& sensitivityFilePath = "", const QString& sensitivityName = "", const QString &sensitivityType = "");

  void sensitivity_(void);
  void activate_(void);

  void clear_(void);

  void updateLog(void);
  void updateLogErr(void);
  void finished(int, QProcess::ExitStatus exitStatus);
  void started(void);
  void errorProcess(QProcess::ProcessError e);
  
  void openNew(const QString &type, const QString &tag, const QString &description);
  void openExisting(const QString &type, int cid);
  void duplicate(const QString &tag, const QString &description);

  void showPfd(int id);
  
  void about(void);
  void openOds(void);
#ifdef Q_OS_WIN
  void openXls(void);
#endif
  void selectkernel(void);
  Q_INVOKABLE void setLoggingProcess(QProcess *p);

  void setLanguageDefault(void);
  void setLanguageAr(void);
  void setLanguageDe(void);
  void setLanguageEs(void);
  void setLanguageEn(void);
  void setLanguageFr(void);
  void setLanguageHe(void);
  void setLanguageIt(void);
  void setLanguageJp(void);
  void setLanguageKo(void);
  void setLanguagePt(void);
  void setLanguageRu(void);
  void setLanguageZh(void);

  void setUnit_(int node);
  void setUnit_(QAction *);

  void undoTextChanged(const QString &);
  void redoTextChanged(const QString &);
  // user has modified any value in tablewInput_ or in intTablewInput_
  void dataChanged(bool status);
  // jump to node, open tablewInput_ tab
  void forceGoTable(int node);

  void clearStatusMessage(void);
  void showMessage(int);
private:
  void run(QString command, QString arg);
  void run(QString command, QString arg1, QString arg2);
  void run(QString command, QString arg1, QString arg2, QString arg3);

  void clear(bool clearmess = true);
  Units unitSelection(void) const;

  std::string currentCase_;
  std::string tentativeCase_;
  Settings* mySettings_;
  bool highlight_;

  WidgetTableDouble* tablewInput_;
  WidgetTableDouble* tablewOutput_;
  WidgetTableMessages* tablewMessages_;
  WidgetTree* treew_;
  ScrollAreaSvg* svgw_;

  QTabWidget* tabs_;
  /// QTabWidget tab indexes
  struct Tab {
    enum index {
      inputs=0, // Inputs
      outputs, // Outputs
      messages, // Messages
      pfd // PFD
    }; // enum index
  };

  QTextEdit* messages_;

  QSplitter* vsplitter_;
  QSplitter* hsplitter_;

  QAction* newAction_;
  QAction* openAction_;
  QAction* saveAction_;
  QAction* dumpTxtAction_;
  QAction* dumpHtmlAction_;
  QAction* deleteAction_;
  QAction* purgeAction_;
  QAction* exitAction_;

  QUndoStack* undoStack_;
  QAction* undoAction_;
  QAction* redoAction_;

  QAction* acopyAction_;
  QAction* aselectallAction_;

  QAction* resetAction_;
  QAction* calculateAction_;
  QAction* homotopyAction_;
  QAction* stopAction_;

  QAction* expandAction_;
  QAction* collapseAction_;
  QAction* rootAction_;
  QAction* upAction_;
  QAction* toggleTC_;

  QAction* sensitivityRestoreAction_;
  QAction* sensitivityOpenAction_;
  QAction* sensitivityAnalysisAction_;
  QAction* activateAction_;

  QAction* siUnitAction_;
  QAction* enUnitAction_;
  QAction* engUnitAction_;

  QAction* clearmsgAction_;

  QAction* aboutAction_;
  QAction* aboutQtAction_;

  QAction* odsAction_;
  QAction* xlsAction_;

  QAction * selectKernelAction_;

  QAction* languageDefault_;
  QAction* languageAr_;
  QAction* languageDe_;
  QAction* languageEs_;
  QAction* languageEn_;
  QAction* languageFr_;
  QAction* languageHe_;
  QAction* languageIt_;
  QAction* languageJp_;
  QAction* languageKo_;
  QAction* languagePt_;
  QAction* languageRu_;
  QAction* languageZh_;

  /// actions that should be enabled when a case is loaded
  std::vector<QAction *> enableLoadedActions_;
  /// actions that are normally always enabled but that should be disabled when a case is running
  std::vector<QAction *> disableRunningActions_;
  /// actions that are normally always disabled but that should be enabled when a case is running
  std::vector<QAction *> enableRunningActions_;
  
  QActionGroup* unitGroup_;

  QMenu* menuCase_;
  QMenu* menuEdit_;
  QMenu* menuView_;
  QMenu* menuRun_;
  QMenu* menuTools_;
  QMenu* menuSettings_;
  QMenu* subMenuUnits_;
  QMenu* subMenuLanguages_;
  QMenu* menuHelp_;

  QToolBar* toolbarMain_;

  QProcess* process_;
  QProcess* loggingProcess_; // can be different from process_ when MDLibpfAction is running

  DialogOpen* od_;
  DialogNew* nd_;
  DialogDuplicate* dd_;

  /// UI states
  struct State {
    enum value { 
      undefined=0,  ///< undetermined
      empty,        ///< no case loaded
      ok,           ///< case loaded without errors and without warnings
      warnings,     ///< case loaded without errors but with warnings 
      errors,       ///< case loaded with errors
      changed,      ///< input values changed
      invalid,      ///< invalid return code from kernel
      running       ///< kernel running
    } v_; // enum value
    State(enum value v) : v_(v) { }
  }; // struct State

  State previousState_;
  State state_;
  
  /// sets the UI state and enables / disables actions as required
  void setState(State state);
  /// sets the UI state to the previous state
  void resetState(void);

  /// action to perform when the kernel process finishes
  struct FinishAction { 
    enum value {
      none=0,       ///< perform no action
      load,         ///< load possibly new case based on kernel return code
      openTxt,      ///< open the text file matching the current object
      openHtml,     ///< open the HTML file matching the current object
      success,      ///< report successful operation
      cleanup       ///< clear all data
    } v_; // enum value
    FinishAction(enum value v) : v_(v) { }
  }; // class FinishAction 
  
  FinishAction finishAction_;
  
  /// starts asynchronously the kernel process and sets the exit action
  void startKernel(QStringList args, FinishAction finishAction);

  void setLanguage(const char* lang);

  QString typesFile(void);
  void buildNewDialog(void);

  // statusbar related
  QPixmap* pix_;
  QLabel* flag_;
  /// volatile messages
  QLabel* statusMessage_;
  /// permanent state message, @see State
  QLabel* stateMessage_;
  void showMessage(const QString &text);

  bool boolGetItemTableView(QTableView *table);
  void editActionStatus(bool value);
  QHeaderView* tblInHorzHeader_;
  QHeaderView* tblOutHorzHeader_;
  QHeaderView* tblInVertHeader_;
  QHeaderView* tblOutVertHeader_;
  QString tempFileName_;
}; // class WindowMain

#endif // UIPF_WINDOWMAIN_H
