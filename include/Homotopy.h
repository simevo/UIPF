/** @file Homotopy.h
    @brief Homotopy class declaration

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_HOMOTOPY_H
#define UIPF_HOMOTOPY_H

#include <map>
#include <string>

class QUndoStack;

class Homotopy {
private:
  int caseId_;
  int points_;
  std::string type_;
  typedef std::map<QString, std::pair<double, double> > VariableMapType;
  VariableMapType variables_;

public:
  Homotopy(const QUndoStack *undoStack, int caseId, std::string type);
  int size(void); ///< return number of controlled variables
  void writeXmlFile(std::string fileName); ///< output homotopy in XML format according to Homotopy_schema.xsd
}; // class Homotopy

#endif // UIPF_HOMOTOPY_H
