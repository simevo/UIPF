/** @file ActionLibpfSensitivity.h
    @brief Interface for the ActionLibpfSensitivity class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_ACTIONLIBPFSENSITIVITY_H
#define UIPF_ACTIONLIBPFSENSITIVITY_H

#include <vector>
#include <QThread>
#include <QXmlStreamReader>
#include <QFileSystemWatcher>
#include <QXmlSchema>
#include <QXmlSchemaValidator>

#include "ActionBase.h"
#include "Ordering.h"
#include "Settings.h"

class QXmlSchema;
class QXmlSchemaValidator;
class QProcess;
class QTimer;
class QMutex;

class ActionLibpfSensitivity : public QObject, public ActionBase {
  Q_OBJECT
private:
  QObject * parent_;
  QProcess* libpfProc_;
  const QAbstractItemModel* ctlDataModel_;
  std::vector< bool > rowComputed_;
  QFileSystemWatcher* fsw_;
#if (QT_VERSION >= 0x040600)
  QXmlSchema* schema_;
  QXmlSchemaValidator* schemaValidator_;
#endif
  bool resume_;
  bool stoprun_;
  Settings* settings_;
  Ordering* ordering_;
  QString libpfInputFilename_;
  QString libpfOutputFilename_;
  bool skipSignals_;
  QTimer* timer_;
  QMutex *mutex_;

  // Timeout functionality
  QTimer* timeOutTimer_;  // Timer
  bool timedOut_;         // Indicates whether the process timed out or not

  void watchOutputFile_(void);
  void dontWatchOutputFile_(void);
public:
  /// Constructor
  explicit ActionLibpfSensitivity(QObject* parent = 0);
  /// Destructor
  ~ActionLibpfSensitivity(void);

  void setControlledVariableModel(const QAbstractItemModel* model);
  Ordering* ordering(void);
  /// 0: Lexicographical
  /// 1: Boustrophedon
  /// 2: Quasi-spiral
  void setOrdering(int o);

  /// Starts the calculation engine to compute results; replaces QThread::start
  void start();

  /// Save to XML
  /// @param fileName The fileName to be written
  /// @param round Round the last analysis stopped for resuming
  void saveToXml(const QString &fileName, unsigned int round = 0);

  /// Reimplements ActionGo::kill
  void kill(void);

  /// Stop the currently running calculations.
  void stop(void);

  /// Sets the resume flag.
  void setResumeFlag(bool value);

private slots:
  /// Reads the results file and updates the results table.
  void onFileChanged(const QString & filename);
  /// Adds or removes the results file from the filesystem watcher.
  void onDirChanged(const QString & dirname);
  void processFinished(int exitCode);
  /// Called if the process has timedout during execution
  void processTimedOut();
  void dontSkipSignals(void);

protected:
  /// Moves the xml reader to the given element or end of the parent element.
  QXmlStreamReader::TokenType readToElement(QXmlStreamReader & rdr, const QString & elemname, const QString & parentelem);

signals:
  /// This is emited if the action is stopped by the user
  void stopped(void);
  /// This signal should be emitted during the various stages and statuses of the start method
  void computed(int ID, RowStatus rs);
  /// This is emited if the action is completed
  void complete(void);
  /// This is emited as soon as the action is started
  void started(void);
}; // class ActionLibpfSensitivity

#endif // UIPF_ACTIONLIBPFSENSITIVITY_H
