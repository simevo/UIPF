/** @file WidgetTableMessages.h
    @brief Interface for the WidgetTableMessages class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_WIDGETTABLEMESSAGES_H
#define UIPF_WIDGETTABLEMESSAGES_H

#include <QWidget>
#include <QSqlQueryModel>
#include <QTableView>

class QTableView;

class WidgetTableMessages : public QWidget {
  Q_OBJECT

public:
  explicit WidgetTableMessages(QWidget* parent = 0);
  void clear(void);

private:
  QSqlQueryModel* model_;
  QTableView* view_;

public slots:
  void go(int node = 0);
}; // class WidgetTableMessages

#endif // UIPF_WIDGETTABLEMESSAGES_H
