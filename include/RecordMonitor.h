/** @file RecordMonitor.h
    @brief Interface for the RecordMonitor struct

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_RECORDMONITOR_H
#define UIPF_RECORDMONITOR_H

#include <QWidget>
#include <QTableWidget>

struct RecordMonitor {
  RecordMonitor(int id = -1, QString fulltag = QString(), QString uom = QString(), QString description = QString(), QString label = QString(), int dbid = -1) :
    ID(id), FULLTAG(fulltag), UOM(uom), DESCRIPTION(description), LABEL(label), DBID(dbid) {} // RecordMonitor

  int ID;
  QString FULLTAG;
  QString UOM;
  QString DESCRIPTION;
  QString LABEL;
  int DBID;
}; // struct RecordMonitor

#endif // UIPF_RECORDMONITOR_H
