/** @file CommandDataSet.h
    @brief Interface for the CommandDataSet class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_COMMANDDATASET_H
#define UIPF_COMMANDDATASET_H

#include <QUndoCommand>
#include <QVariant>

class CommandDataSet : public QUndoCommand {
protected:
  QVariant newValue_;
  QVariant oldValue_;
  int role_;
  int row_;
  int col_;
  int id_;
  int currentNode_;
  QString fullTag_;
  QString description_;

public:
  CommandDataSet(const QVariant & newValue, int role, int currentNode);
  double oldValue(void) const;
  double newValue(void) const;
  QString fullTag(void) const;
}; // class CommandDataSet

#endif // UIPF_COMMANDDATASET_H
