/** @file DialogSensitivityOpen.h
    @brief Interface for the DialogSensitivityOpen class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#ifndef UIPF_DIALOGSENSITIVITYOPEN_H
#define UIPF_DIALOGSENSITIVITYOPEN_H

#include <QDialog>

class QGridLayout;
class QTableView;
class QHeaderView;
class QHBoxLayout;
class QSpacerItem;
class Settings;
class ModelSensitivityList;
class QItemSelection;
class QModelIndex;

/// Lists and opens a previously saved sensitivity
class DialogSensitivityOpen : public QDialog {
  Q_OBJECT
public:
  /// Constructor
  /// @param parent Our parent widget
  DialogSensitivityOpen(const QString & currentType, QWidget* parent = 0);

  /// Destructor
  ~DialogSensitivityOpen();

  /// Returns the selected sensitivity file name
  const QString& sensitivitiyFileName() const;
  /// Returns the selected sensitivity name
  const QString& sensitivityName() const;
  /// Returns the selected sensitivity type
  const QString& sensitivityType() const;

private slots:
  /// Called when row selection was changed
  void selectionChanged(const QItemSelection & selected, const QItemSelection & deselected);
  /// Set the initial size of the view header
  void resizeViewSections();
  /// Double-click accept, make sure we have an item selected before accepting
  void tableDoubleClicked(const QModelIndex & index);

private:
  // Dialog controls
  QGridLayout* gridLayout_;
  QTableView*  viewSensitivity_;
  QHeaderView* viewSensitivityHeader_;
  QHBoxLayout* horizontalLayout_;
  QPushButton* buttonOK_;
  QPushButton* buttonCancel_;
  QSpacerItem* horizontalSpacer_;

  // Model for view sensitivity
  ModelSensitivityList* modelSensitivity_;

  // Settings used to retrieve search path
  Settings*    settings_;

  // The file name selected
  QString sensitivityFileName_;
  QString sensitivityName_;
  QString sensitivityType_;

  // Copy constructor and assingment operator not supported by this class so make them private
  DialogSensitivityOpen(const DialogSensitivityOpen &);
  DialogSensitivityOpen & operator=(const DialogSensitivityOpen &);
}; // class DialogSensitivityOpen

#endif // UIPF_DIALOGSENSITIVITYOPEN_H
