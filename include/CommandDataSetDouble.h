/** @file CommandDataSetDouble.h
    @brief Interface for the CommandDataSetDouble class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_COMMANDDATASETDOUBLE_H
#define UIPF_COMMANDDATASETDOUBLE_H

#include <QModelIndex>

#include "CommandDataSet.h"

class ModelQuantityEditable;

class CommandDataSetDouble : public CommandDataSet {
public:
  CommandDataSetDouble(
    ModelQuantityEditable* model,
    const QModelIndex & index,
    const QVariant & newValue,
    int role,
    int currentNode);
  void undo(void);
  void redo(void);

private:
  bool setData(const QModelIndex & index, const QVariant & value, int role);

private:
  ModelQuantityEditable* model_;
}; // class CommandDataSetDouble

#endif // UIPF_COMMANDDATASETDOUBLE_H
