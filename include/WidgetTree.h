/** @file WidgetTree.h
    @brief Interface for the WidgetTree class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_WIDGETTREE_H
#define UIPF_WIDGETTREE_H

#include <QWidget>

#include "Hierarchy.h"

class QTreeWidget;
class QTreeWidgetItem;

extern Hierarchy* h_;

class WidgetTree : public QWidget {
  Q_OBJECT

private:
  static unsigned int maxTreeLeaves_;
  unsigned int leaves_;
  int currentNode_;
  int rootNode_;
  char type_[256];
  QString completeTag_;
  std::list<std::pair<bool, bool> > states_;

  void expandAll(QTreeWidgetItem* parentItem);
  void collapseAll(QTreeWidgetItem* parentItem);
  void colorize_(QTreeWidgetItem* root, int errors, int warnings);
  bool expandThis(QTreeWidgetItem* parentItem, int node);

  QTreeWidget* tree_;

  bool isStream(std::string s);
  bool isUnit(std::string s);
  bool isOther(std::string s);
  bool isAny(std::string);
  bool isPhase(std::string s);
  bool isFlowsheet(std::string s);
  bool isReaction(std::string s);
  bool isMultireaction(std::string s);

  int warnings_;
  int errors_;
  QTreeWidgetItem* createChild(QString tag, QString type, QString id, QString description, QString parent, QString range, int errs, int warns);
public:
  explicit WidgetTree(QWidget* parent = 0);

  bool getChildData(const int id, QString &tag, QString &type, QString &description, QString &parent, QString &range, QString &fulltag, int &errs, int &warns);

  bool addChildren(QTreeWidgetItem* root, int node, int range);

  int currentNode(void);
  int rootNode(void);
  const QString &completeTag(void);
  const char* type(void);
  void setCurrentNode(int n);
  void setRootnode(int n);
  void expandThis(int node);
  void clear(void);
  /// @return the number of top-level errors during the last computation, or zero
  int warnings(void) const;
  /// @return the number of top-level warnings during the last computation, or zero
  int errors(void) const;
  
  void saveTreeState(void);
  void restoreTreeState(void);
signals:
  void clickedNode(int node);
  void gone(int node = 0);
  void showPfd(int id);

public slots:
  void go(int node = -1);
  void refresh(void);
  void forceGo(int node);
  void goUp(void);

private slots:
  void expandAll(void);
  void collapseAll(void);
  void clickedOn(QTreeWidgetItem* item, int column);
}; // class WidgetTree

#endif // UIPF_WIDGETTREE_H
