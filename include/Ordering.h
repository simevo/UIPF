/** @file OrderingReflectedGray.h
    @brief OrderingReflectedGray class declaration

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#ifndef UIPF_ORDERING_H
#define UIPF_ORDERING_H

#include <vector>

class Ordering {
protected:
  int k_;
  std::vector<int> m_; ///< the maximum for each digit
  mutable char digits_[256];
  mutable std::vector<int> g_; ///< the encoded format
  mutable std::vector<int> h_; ///< the integer in mixed-radix representation

public:
  explicit Ordering(const std::vector<int> &m);
  virtual ~Ordering(void);

  virtual int idFromRound(int round) const = 0;
  virtual int roundFromId(int id) const = 0;
  virtual int initial(void) const;

  int localIdFromId(int id, int j) const;
  void toInternalRepresentation_(int i, std::vector<int> &g) const;
  int fromInternalRepresentation_(const std::vector<int> &g) const;
  int maxRound(void) const;
  int distance(int i, int j) const;
}; // class Ordering

#endif // UIPF_ORDERING_H
