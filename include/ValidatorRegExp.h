/** @file ValidatorRegExp.h
    @brief Implementation for the ValidatorRegExp class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#ifndef UIPF_VALIDATORREGEXP_H
#define UIPF_VALIDATORREGEXP_H

#include <QRegExpValidator>

class QLabel;

/// This class validates input based on a given Regular Expression pattern.
class ValidatorRegExp : public QRegExpValidator {
  Q_OBJECT
public:
  /// Constructor
  /// @param parent   Object's parent
  /// @param label    Label of the control that is being validated
  /// @param rx       Regular expression used to validate the input
  ValidatorRegExp(QLabel* label, const QRegExp & rx, QObject* parent = 0);

  /// Validates input and shows tooltips when necessary
  /// Obs.: It strips : at the end of labels
  /// @param input Input to be validated
  /// @param pos   Cursor pos when validate was called
  virtual QValidator::State validate(QString & input, int & pos) const;

private:
  QLabel* label_;
}; // class ValidatorRegExp

#endif // UIPF_VALIDATORREGEXP_H
