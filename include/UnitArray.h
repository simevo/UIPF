/** @file UnitArray.h
    @brief Contains the interface to the UOM array class.

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2004-2021 Paolo Greppi simevo s.r.l.
 */

#ifndef LIBPF_UNITARRAY_H
#define LIBPF_UNITARRAY_H

#include <iostream>

#define UNICODESTRING(s) (sizeof(wchar_t) == 4 ? s : UtfConverter::ToUtf8(L ## s))

// required to avoid
// warning: friend declaration `GenericQuantity<valuetype> pow(GenericQuantity<valuetype>, valuetype)'
// declares a non-template function
// warning: (if this is not what you intended, make sure the function template
// has already been declared and add <> after the function name here)
// -Wno-non-template-friend disables this warning

template<class T> class UnitArrayGen;

template<class T> std::ostream & operator<<(std::ostream &os, const UnitArrayGen<T> &x);

/// contains the units of measurements encoded as an array of integer exponents as follows:
/// - amount (kmol) 0
/// - angle (rad) 1
/// - current (A) 2
/// - length (m) 3
/// - luminous intensity (cd) 4
/// - mass (kg) 5
/// - temperature (K) 6
/// - time (s) 7
/// - money (EUR) 8

template<class T> class UnitArrayGen {
private:
  static std::string uomTags_[9];
  T elems_[9];
  static std::string buffer_;

public:
  /// set all elements to val
  explicit UnitArrayGen(T val = 0);
  UnitArrayGen(T val0, T val1, T val2, T val3, T val4, T val5, T val6, T val7, T val8);
  UnitArrayGen(const UnitArrayGen & source);
  ~UnitArrayGen(void);

  T operator[](int i) const;
  T &operator[](int i);

  UnitArrayGen<T> & operator+=(const UnitArrayGen & rhs);
  UnitArrayGen<T> & operator-=(const UnitArrayGen & rhs);
  UnitArrayGen<T> & operator*=(const UnitArrayGen & rhs);
  UnitArrayGen<T> & operator/=(T factor);

  bool operator==(const UnitArrayGen & rhs) const;
  bool operator==(T x) const;

  friend std::ostream & operator<<<>(std::ostream & os, const UnitArrayGen<T> &x);
  std::ostream & print(std::ostream &os) const;

  void tostring(std::string &) const;
  const std::string &tostring(void) const;
}; // class UnitArrayGen

/// binary +
template<class T> UnitArrayGen<T> operator+(const UnitArrayGen<T> & x, const UnitArrayGen<T> & y);
// binary -
template<class T> UnitArrayGen<T> operator-(const UnitArrayGen<T> & x, const UnitArrayGen<T> & y);
/// binary *
template<class T> UnitArrayGen<T> operator*(const UnitArrayGen<T> & x, const UnitArrayGen<T> & y);
/// binary /
template<class T> UnitArrayGen<T> operator/(const UnitArrayGen<T> & x, T factor);
/// unary +
template<class T> UnitArrayGen<T> operator+(const UnitArrayGen<T> & x);
/// unary -
template<class T> UnitArrayGen<T> operator-(const UnitArrayGen<T> & x);

typedef UnitArrayGen<int> UnitArray;

#endif // LIBPF_UNITARRAY_H
