/** @file persistency.h
    @brief common initialization for persistency connection

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef PERSISTENCY_H
#define PERSISTENCY_H

// use this code to force SQLITE connection for all architectures
// #define SQLITE

// use this code to use the appropriate connection
#if 1
#if defined Q_OS_MAC
#define SQLITE
#elif defined Q_OS_WIN
#define ODBC
#else
// #define SQLITE
#define POSTGRESQL
// #define MYSQL
// #define ODBC
#endif
#endif

enum {
  ID_COL = 0, FULLTAG_COL, UOM_COL, DESCR_COL, LABEL_COL,
  START_COL, END_COL, NPOINTS_COL, DBID_COL, NUM_COLS
};

#endif // PERSISTENCY_H
