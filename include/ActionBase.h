/** @file ActionBase.h
    @brief Interface for the ActionBase class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_ACTIONBASE_H
#define UIPF_ACTIONBASE_H

#include <string>

#include <QMetaType> // for Q_DECLARE_METATYPE

class QAbstractItemModel;

#ifndef ROWSTATUS
enum RowStatus {
  toDo = 0,
  executing,
  doneOk,
  doneWarnings,
  doneErrors,
  doneSevereError,
  doneCrash,
  doneNotFinished
}; // enum RowStatus
Q_DECLARE_METATYPE(RowStatus);
#define ROWSTATUS
#endif

class ActionBase {
public:
  ActionBase(void);

  /// Sets the data model where results will be saved
  void setResultsModel(QAbstractItemModel* model);
  /// Sets the data model where the monitored variables are found
  void setMonitorModel(const QAbstractItemModel* model);
  void setCaseId(int caseId);
  void setType(const std::string &type);
  void setStopped(bool s);
  void setTimeOut(int to);

  void incrementCurrentRow(void);

  /// @brief Returns the model where the results are saved
  QAbstractItemModel* resultsModel(void) const;
  /// Returns the model where the monitored variables are found
  const QAbstractItemModel* monitorModel(void) const;
  int currentRow(void);
  bool isStopped(void);

  int caseId(void) const;
  const std::string &type(void);
  int timeOut(void) const;

private:
  int caseId_;
  std::string type_;
  QAbstractItemModel* resultsModel_;
  const QAbstractItemModel* monitorModel_;
  volatile bool stopped_;
  volatile int currentRow_;
  int timeOut_;
}; // class ActionBase

#endif // UIPF_ACTIONBASE_H
