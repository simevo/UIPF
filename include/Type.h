/** @file Type.h
    @brief Interface for the Type class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_TYPE_H
#define UIPF_TYPE_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>

class Type {
  static int idgen_;
  int id_;
  std::string name_;
  std::string description_;

public:
  Type(std::string n, std::string d);

  int id(void) const;
  const std::string &name(void) const;
  const std::string &description(void) const;
}; // class Type

#endif // UIPF_TYPE_H
