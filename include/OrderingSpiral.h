/** @file OrderingSpiral.h
    @brief OrderingSpiral class declaration

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#ifndef UIPF_ORDERINGSPIRAL_H
#define UIPF_ORDERINGSPIRAL_H

#include <vector>
#include <map>
#include <set>

#include "Ordering.h"

class OrderingSpiral : public Ordering {
  int initial_;
  std::map<int,int> direct_;
  std::map<int,int> reverse_;
  int acceptable_;
  std::vector<std::pair<int, int> > suitable_;
  std::set<int> visited_;
  int begin_(int from, int acceptable) const;
  int end_(void) const;
  int next_(void) const;
  mutable std::vector<int> min_;
  mutable std::vector<int> l_;
  mutable std::vector<int> max_;
public:
  explicit OrderingSpiral(const std::vector<int> &m, int initial);

  // implement Ordering pure virtual functions
  int idFromRound(int round) const;
  int roundFromId(int id) const;

  // override
  int initial(void) const;
}; // class OrderingSpiral

#endif // UIPF_ORDERINGSPIRAL_H
