/** @file OrderingReflectedGray.h
    @brief OrderingReflectedGray class declaration

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#ifndef UIPF_ORDERINGREFLECTEDGRAY_H
#define UIPF_ORDERINGREFLECTEDGRAY_H

#include <vector>

#include "Ordering.h"

class OrderingReflectedGray : public Ordering {
  static const char name_ [];
  mutable std::vector<int> u_; ///< +1 or -1
  void grayInternalRepresentation_(void) const;

public:
  explicit OrderingReflectedGray(const std::vector<int> &m);

  // implement Ordering pure virtual functions
  int idFromRound(int round) const;
  int roundFromId(int id) const;
}; // class OrderingReflectedGray

#endif // UIPF_ORDERINGREFLECTEDGRAY_H
