/** @file DialogSensitivitySave.h
    @brief Interface for the DialogSaveSensitivityAnalysis class

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#ifndef UIPF_DIALOGSAVESENSITIVITYANALYSIS_H
#define UIPF_DIALOGSAVESENSITIVITYANALYSIS_H

#include <QDialog>

class QGridLayout;
class QLabel;
class QLineEdit;
class QPushButton;
class Settings;
class ActionLibpfSensitivity;

/// Saves the Sensitivity Analysis into a XML file
class DialogSensitivitySave : public QDialog {
  Q_OBJECT
public:
  /// Constructor
  /// @param goAction The action we use to saveToXml
  /// @param suggestedName A suggested sensitivity name for the user
  /// @param parent Dialog parent widget
  explicit DialogSensitivitySave(ActionLibpfSensitivity* goAction, const QString & suggestedName = "", QWidget *parent = 0);

  /// Get saved analysisName name
  const QString & analysisName() const;

private slots:
  /// Called when OK button is clicked, will call saveToXml
  void save();
  /// Called when analysisNameEdit has changed, used for enabling the Ok button
  void onAnalysisNameEditChange(const QString & text);

private:
  ActionLibpfSensitivity* goAction_;
  QGridLayout*      gridLayout_;
  QLabel*           descriptionLabel_;
  QLineEdit*        analysisNameEdit_;
  QPushButton*      okButton_;
  QPushButton*      cancelButton_;
  Settings*         settings_; // Used to get the working directory
  QString analysisName_;

  // Copy constructor and assingment operator not supported by this class so make them private
  DialogSensitivitySave(const DialogSensitivitySave &);
  DialogSensitivitySave & operator=(const DialogSensitivitySave &);
}; // class DialogSensitivitySave

#endif // UIPF_DIALOGSAVESENSITIVITYANALYSIS_H
