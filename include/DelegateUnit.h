/** @file DelegateUnit.h
    @brief Interface for the DelegateUnit class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_UNITFIELDCBDELEGATE_H
#define UIPF_UNITFIELDCBDELEGATE_H

#include <QStyledItemDelegate>
#include <QComboBox>
#include "UnitEngine.h"

class DelegateUnit : public QStyledItemDelegate {
  Q_OBJECT

public:
  DelegateUnit(QList<int> valColumns, QWidget* parent = 0);
  virtual ~DelegateUnit();
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;

  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model,
                    const QModelIndex &index) const;

  void updateEditorGeometry(QWidget *editor,
                            const QStyleOptionViewItem &option, const QModelIndex &index) const;
  void setCBModel(QStringList *model);
  QString getNewUnit(double siUnit, QString newUnit) const;
  //virtual QString displayText(const QVariant & value, const QLocale & locale ) const;

signals:
  /// signal emitted when the value was changed
  void currentIndexChanged(int, QObject *objPointer);

private slots:
  void currentIndexChngd(int index);

private:
  mutable QStringList *editorModel;
  QList<int> valueColumns;
};

#endif // UIPF_UNITFIELDCBDELEGATE_H
