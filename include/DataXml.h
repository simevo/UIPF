/** @file DataXml.h
    @brief Interface for the DataXml class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Dmytro Skrypnyk.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DATAXML_H
#define UIPF_DATAXML_H

#include <QString>
#include <QPainterPath>
#include <QDomDocument>
#include <QMap>

#include "Element.h"

class DataXml {
public:
  DataXml(void);
  bool openXml(const QString &filename);
  void read(void);
  void clear(void);

  int href(const QString & id) const;
  QPainterPath path(const QString & id) const;
  QString classname(const QString & id) const;
  QStringList elementList(void) const;

private:
  QDomDocument document_;
  QMap<QString, Element> hrefMap_;
}; // class DataXml

#endif // UIPF_DATAXML_H
