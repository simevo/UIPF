/** @file UtfConverter.h
    @brief UNICODE conversion helper function interfaces

    This file is part of UIPF, the User Interface for Process Flowsheeting

    Based in part on code by rh_:
    http://www.codeproject.com/KB/string/UtfConverter.aspx
    Convert between std::string and std::wstring, UTF-8 and UTF-16

    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_UTFCONVERTER_H
#define UIPF_UTFCONVERTER_H

#include <string>

#include "ConvertUTF.h"

namespace UtfConverter {
/// return true if the UTF32 code point c is alphabetic according to Annex E of ISO/IEC 10176
bool unicode_isalpha(UTF32 c);
/// render the return code from ConvertUTFxxtoUTFyy to a string
std::string ConversionResult_tostring(int res);
/// convert one character at a time from source
/// After the conversion, the source pointer is updated to point to the end of last text successfully converted
UTF32 FromUtf8(const char **source);
std::wstring FromUtf8(const std::string & utf8string);
std::string ToUtf8(const std::wstring & widestring);
std::string ToUtf8(const wchar_t*);
void test_unicode_alpha_range(void);
} // namespace UtfConverter

#endif // UIPF_UTFCONVERTER_H
