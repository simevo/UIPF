/** @file ModelSensitivityList.h
    @brief Interface for the ModelSensitivityList class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#ifndef UIPF_MODELSENSITIVITYLIST_H
#define UIPF_MODELSENSITIVITYLIST_H

#include <QSortFilterProxyModel>

class QStandardItemModel;
class QItemSelectionModel;
class QDir;

/// Stores all SensitivityInput_*_*.xml file names found on a given directory
class ModelSensitivityList : public QSortFilterProxyModel {
  Q_OBJECT

public:
  enum { COL_RADIO, COL_NAME, COL_TYPE }; // Our columns

  /// Constructor
  /// @param parent Our parent object
  ModelSensitivityList(QObject* parent = 0);

  /// Destructor
  ~ModelSensitivityList();

  /// Sets the directory and loads a list of all XML files found at the given
  /// directory, if we do not have permissions on the directory it will return false
  /// @param sensitivityDirectory Path to the directory where the XML files are.
  bool setSensitivityDirectory(const QString & sensitivityDirectory);

  /// The typeName specified by this function will be highlighted on the list
  /// @param typeName The type name to be highlighted
  void setHighlightedTypeName(const QString & typeName);

  /// Reimplementation of QSortFilterProxyModel::data for showing colors
  virtual QVariant data(const QModelIndex &index, int role) const;
  /// Reimplementation of QSortFilterProxyModel::headerData for setting up our headers text
  virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
  /// Reimplementation of QSortFilterProxyModel::flags to avoid editing restricted columns
  virtual Qt::ItemFlags flags(const QModelIndex &index) const;

private:
  QStandardItemModel* modelSensitivity_;  // Our internal model for storing the found rows
  QDir*               searchXmlPath_;     // Directory where we are going to scan for saved SensitivityInput*.xml files
  QString highlightTypeName_;             // Type name to highlight on light blue color

  /// Returns whether the given typeName is supported by the currently selected kernel
  /// @param typeName The type name to be checked
  bool isTypeSupported(const QString & typeName) const;

  /// Loads the sensitivity list based on xml files found at the searchXmlPath_ directory
  /// This function can be called anytime to refresh the list case new files are added to the directory
  void loadSensitivityList();
}; // class ModelSensitivityList

#endif // UIPF_MODELSENSITIVITYLIST_H
