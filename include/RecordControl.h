/** @file RecordControl.h
    @brief Interface for the RecordControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_RECORDCONTROL_H
#define UIPF_RECORDCONTROL_H

#include <QStringListModel>

/// A structure holding all the values of a row in the WidgetTableControl
struct RecordControl {
  RecordControl(int id = -1, QString fulltag = QString(), QString uom = QString(), QString description = QString(), QString label = QString(), double start = 0, double end = 0, int npoints = 1, int dbid = -1) :
    ID(id), NPOINTS(npoints), DBID(dbid), FULLTAG(fulltag), UOM(uom), DESCRIPTION(description), LABEL(label), START(start), END(end) {} // RecordControl

  int ID;
  int NPOINTS;
  int DBID;
  QString FULLTAG;
  QString UOM;
  QString DESCRIPTION;
  QString LABEL;
  double START, END;
}; // struct RecordControl

#endif // UIPF_RECORDCONTROL_H
