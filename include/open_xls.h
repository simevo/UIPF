/** @file open_xls.h
    @brief Interfaces to the Windows-specific code for opening Excel application

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_OPEN_XLS_H
#define UIPF_OPEN_XLS_H

/// returns true on success
bool openXlsMain(const wchar_t* fileAndPath, int node);

#endif // UIPF_OPEN_XLS_H
