/** @file DelegateDouble.h
    @brief Interface for the DelegateDouble class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DELEGATEDOUBLE_H
#define UIPF_DELEGATEDOUBLE_H

#include <QStyledItemDelegate>

#include "persistency.h"

class DelegateDouble : public QStyledItemDelegate {
private:
  /// Returns the si unit for the displayed Uom
  QString setNewValue(double newUnit, QString newValue) const;
public:
  DelegateDouble(QObject* parent = 0);
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
  void setEditorData(QWidget* editor, const QModelIndex &index) const;
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex &index) const;
  void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
}; // class DelegateDouble

#endif // UIPF_DELEGATEDOUBLE_H
