/** @file DelegateRadioButton.h
    @brief Interface for the DelegateRadioButton class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */
#ifndef UIPF_DELEGATERADIOBUTTON_H
#define UIPF_DELEGATERADIOBUTTON_H

#include <QStyledItemDelegate>

/// Draws a radio button on a table view column where this delegate is set
class DelegateRadioButton : public QStyledItemDelegate {
  Q_OBJECT
public:
  /// Constructor
  /// @param parent Our parent object
  DelegateRadioButton(QObject* parent = 0);

  /// Destructor
  ~DelegateRadioButton();

  /// Paints a QRadioButton ON or OFF depending on the index bool data
  void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
}; // class DelegateRadioButton

#endif // UIPF_DELEGATERADIOBUTTON_H
