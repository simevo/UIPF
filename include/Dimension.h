/** @file Dimension.h
    @brief Contains the interface to the Dimension class.

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2004-2021 Paolo Greppi simevo s.r.l.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DIMENSION_H
#define UIPF_DIMENSION_H

#include <map>

#include "UnitArray.h"

typedef enum {
  UnitsUndefined = 0xFF,
  UnitsSI = 0x1,  // Systeme international
  UnitsEN = 0x2,  // U.S. Customary Units
  UnitsEng = 0x4  // Engineering Units
} Units;

/// physical Dimension with units of measurements and their conversions
class Dimension {
public:
  typedef std::map<std::string, std::pair<double, double> > conversion_t;

private:
  UnitArray unit_;            ///< unit of measurement encoded as array of integers
  std::string description_;   ///< description of physical Dimension
  std::string SIunit_;        ///< unit in Systeme international
  std::string ENunits_;       ///< unit in U.S. Customary Units
  std::string Engunits_;      ///< unit in Engineering Units

  /// dictionary of units and (factor, offset) pairs
  std::map<std::string, std::pair<double, double> > units_;
  mutable conversion_t::const_iterator i_;
  const std::pair<double, double> &findUnit(std::string destination_unit) const;

public:
  /// @param desc description of the Dimension such as "power"
  /// @param SIu base unit in SI for the Dimension
  /// @param kmol exponent of the amount of substance SI fundamental unit
  /// @param rad exponent of the angular measure SI fundamental unit
  /// @param A exponent of the electric current SI fundamental unit
  /// @param m exponent of the length SI fundamental unit
  /// @param cd exponent of the  luminous intensity SI fundamental unit
  /// @param kg exponent of the mass SI fundamental unit
  /// @param K exponent of the temperature SI fundamental units
  /// @param s exponent of the time SI fundamental units
  /// @param eur exponent of the economic value unit
  Dimension(std::string desc, std::string SIu, int kmol, int rad, int A, int m, int cd, int kg, int K, int s, int eur);
  Dimension(const Dimension &);

  /// add a new unit to the Dimensions
  /// @param unit unit name
  /// @param factor first multiply by factor ...
  /// @param offset then add offset to convert to base SI unit: SI_value = unit_value*factor + offset;
  /// examples of usage:
  /// addUnit("power", "kW", 1000.0);
  /// addUnit("pressure", "barg", 100000.0, 101325.0);
  void addUnit(Units u, std::string unit, double factor, double offset);

  const std::string &description(void) const; // returns "power"
  const std::string &unit(Units u) const;
  const UnitArray &unitArray(void) const;

  // read-only iterate through units supported by this Dimension
  conversion_t::const_iterator begin(void) const;
  conversion_t::const_iterator next(void) const;
  conversion_t::const_iterator end(void) const;

  /// diagnostic print
  void printUnits(void) const;

  /// convert v from SI base unit to destination_unit
  /// i.e. fromSI(1.0, "kW") = 0.001
  /// fromSI = (v - offset)/factor
  double fromSI(double v, std::string destinationUnit) const;
  /// convert v from source_unit to SI base unit
  /// i.e. toSI(1.0, "kW") = 1000.0
  /// toSI = v*factor + offset;
  double toSI(double v, std::string sourceUnit) const;
  /// return number of units
  int size(void) const;
}; // class Dimension

#endif // UIPF_DIMENSION_H
