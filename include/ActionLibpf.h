/** @file ActionLibpf.h
    @brief Interface for the ActionLibpf class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_ACTIONLIBPF_H
#define UIPF_ACTIONLIBPF_H

#include "ActionGo.h"
#include "Settings.h"
#include "persistency.h"

class QProcess;

/// get the number of errors and warnings for case id
/// @param[in] id the case id
/// @param[out] errors the nubmer of errors for the case
/// @param[out] warnings the nubmer of errors for the case
void getErrorsWarnings(int id, int &errors, int &warnings);

class ActionLibpf : public ActionGo {
private:
  Settings* settings_;
  QProcess* kernelProcess_;

public:
  ActionLibpf(void);
  ~ActionLibpf(void);

  // override ActionGo::compute
  void compute(int row);
  // implement ActionGo::kill
  void kill(void);

  /// set the value in the database for the variable identified by dbid
  bool setValue(int dbid, double value);
  /// return the current value in the database for the variable identified by dbid, or FLT_MAX in case or error
  double getValue(int dbid);
}; // class ActionLibpf

#endif // UIPF_ACTIONLIBPF_H
