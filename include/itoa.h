/** @file itoa.h
    @brief Interface for the itoa function

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef ITOA_H
#define ITOA_H

#include <string>

/// C++ version std::string style "itoa"
/// converts an integer value to a string in base with maximum/padded width
/// @param value number to be output as string with minus sign if negative
/// @param base 2 to 16 inclusive
/// @param width in characters apart from sign with zero padding; no crop or padding if zero
std::string itoa(int value, int base = 10, int width = 0);

#endif // ITOA_H
