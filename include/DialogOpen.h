/** @file DialogOpen.h
    @brief Interface for the DialogOpen class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DIALOGOPEN_H
#define UIPF_DIALOGOPEN_H

#include <QDialog>

class QPushButton;
class QHBoxLayout;
class QVBoxLayout;
class QTableWidget;

class DialogOpen : public QDialog {
  Q_OBJECT

public:
  explicit DialogOpen(QWidget* parent = NULL);
  void addItem(); //const QString &desc, const QString &type);
  void clear(void);

signals:
  void openExisting(const QString &type, int cid);

private slots:
  void okClicked(void);
  void doubleClicked(int row, int);

private:
  QTableWidget* tableExisting_;

  QPushButton* ok_;
  QPushButton* cancel_;

  QVBoxLayout* all_;
  QHBoxLayout* bottom_;

  int addRow(void);
}; // class DialogOpen

#endif // UIPF_DIALOGOPEN_H
