/** @file DelegateMessage.h
    @brief Interface for the DelegateMessage class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#ifndef UIPF_DELEGATEMESSAGE_H
#define UIPF_DELEGATEMESSAGE_H

#include <QStyledItemDelegate>

class DelegateMessage : public QStyledItemDelegate {
public:
  DelegateMessage(void);
  void paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
}; // class DelegateMessage

#endif // UIPF_DELEGATEMESSAGE_H
