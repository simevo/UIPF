/** @file CommandDataSet.cc
    @brief Implementation for the CommandDataSet class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "CommandDataSet.h"
#include <QDebug>

CommandDataSet::CommandDataSet(const QVariant & newValue, int role, int currentNode) :
  newValue_(newValue),
  role_(role),
  currentNode_(currentNode) { qDebug() << "Entering "<< Q_FUNC_INFO; } // CommandDataSet::CommandDataSet

double CommandDataSet::oldValue(void) const {
  return oldValue_.toDouble();
} // CommandDataSet::oldValue

double CommandDataSet::newValue(void) const {
  return newValue_.toDouble();
} // CommandDataSet::newValue

QString CommandDataSet::fullTag(void) const {
  return fullTag_;
} // CommandDataSet::fullTag
