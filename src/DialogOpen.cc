/** @file DialogOpen.cc
    @brief Implementation for the DialogOpen class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QTableWidget>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

#include "DialogOpen.h"

int DialogOpen::addRow(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int row = tableExisting_->rowCount();
  tableExisting_->insertRow(row);

  // Tag
  QTableWidgetItem* item0 = new QTableWidgetItem;
  item0->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
  tableExisting_->setItem(row, 0, item0);
  // Type
  QTableWidgetItem* item1 = new QTableWidgetItem;
  item1->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
  tableExisting_->setItem(row, 1, item1);
  // Description
  QTableWidgetItem* item2 = new QTableWidgetItem;
  item2->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
  tableExisting_->setItem(row, 2, item2);
  // ID
  QTableWidgetItem* item3 = new QTableWidgetItem;
  item3->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
  tableExisting_->setItem(row, 3, item3);
  // RANGE
  QTableWidgetItem* item4 = new QTableWidgetItem;
  item4->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  tableExisting_->setItem(row, 4, item4);

//  QTableWidgetItem* item5 = new QTableWidgetItem;
//  item4->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
//  tableExisting_->setItem(row, 5, item5);

  return row;
} // DialogOpen::addRow

void DialogOpen::clear(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  //tableExisting_->clear();
  for (int i = tableExisting_->rowCount() + 1; i >= 0; --i)
    tableExisting_->removeRow(i);
//  tableExisting_->setColumnHidden(5, true);
// Table heading
//  tableExisting_->setHorizontalHeaderLabels(QStringList() << tr("Tag") << tr("Description") << tr("Type") << tr("Type description") << tr("id"));
  tableExisting_->setHorizontalHeaderLabels(QStringList() << tr("Tag") << tr("Type") << tr("Description") << tr("ID") << tr("RANGE"));

  QHeaderView* headerView = tableExisting_->horizontalHeader();
#if (QT_VERSION >= 0x050000)
  headerView->setSectionResizeMode(QHeaderView::ResizeToContents);
#elif (QT_VERSION >= 0x040200)
  headerView->setResizeMode(QHeaderView::ResizeToContents);
#else
  headerView->setResizeMode(QHeaderView::Stretch);
#endif
} // DialogOpen::clear

DialogOpen::DialogOpen(QWidget* parent) : QDialog(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  tableExisting_ = new QTableWidget(0, 5, this);
  tableExisting_->setSelectionBehavior(QAbstractItemView::SelectRows);
  tableExisting_->setEditTriggers(QAbstractItemView::NoEditTriggers);
  tableExisting_->setMinimumWidth(600);
  tableExisting_->setColumnHidden(4, true); // range

  connect(tableExisting_, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(doubleClicked(int, int)));

  ok_ = new QPushButton(tr("Ok"));
  cancel_ = new QPushButton(tr("Cancel"));

  connect(ok_, SIGNAL(clicked()), this, SLOT(okClicked()));
  connect(cancel_, SIGNAL(clicked()), this, SLOT(close()));

  bottom_ = new QHBoxLayout;
  bottom_->addWidget(ok_);
  bottom_->addWidget(cancel_);
  bottom_->addStretch();

  all_ = new QVBoxLayout;
  all_->addWidget(tableExisting_);
  all_->addLayout(bottom_);
  setLayout(all_);
  setWindowTitle(tr("Open case"));
  setMinimumWidth(1200);
  setAttribute(Qt::WA_DeleteOnClose);
} // DialogOpen::DialogOpen

void DialogOpen::doubleClicked(int row, int) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString type = tableExisting_->item(row, 1)->text();
  int id = tableExisting_->item(row, 3)->text().toInt();
  openExisting(type, id);
  close();
} // DialogOpen::doubleClicked

void DialogOpen::okClicked(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (tableExisting_->rowCount() > 0) {
    if (tableExisting_->selectedRanges().count() > 0) {
      int row = (*tableExisting_->selectedRanges().begin()).topRow();
      doubleClicked(row, 0);
    }
  }
  close();
} // DialogOpen::okClicked

void DialogOpen::addItem() { //const QString &desc, const QString &type) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);
  // since the new persistency refactoring the opening dialog population should be based on the N table:
  // select tag, description, id from N where id=root
  // this should open also non-flowsheet<zerozero> models
  QString querystring = QString("select tag, type, description, id, range from N where id=root"); //type = \'%1\'").arg(type);
  query.exec(querystring);
  if (query.lastError().isValid()) {
    qCritical() << query.lastError().type() << query.lastError().text();
    qCritical() << querystring;
  }
  while (query.next()) {
    int row = addRow();
    // tag
    tableExisting_->item(row, 0)->setText(query.value(0).toString().trimmed());
    // type
    tableExisting_->item(row, 1)->setText(query.value(1).toString().trimmed());
    // description
    tableExisting_->item(row, 2)->setText(query.value(2).toString().trimmed());
    // id
    tableExisting_->item(row, 3)->setText(query.value(3).toString().trimmed());
    // range
    tableExisting_->item(row, 4)->setText(query.value(4).toString().trimmed());
  } // for each fitting saved case
} // DialogOpen::addItem
