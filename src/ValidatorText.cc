/** @file ValidatorText.cc
    @brief Implementation for the ValidatorText class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QLabel>
#include  <QDebug>

#include "ValidatorText.h"
#include "UtfConverter.h"

ValidatorText::ValidatorText(QObject* parent, QLabel* label, QString acceptedChars)
  : QValidator(parent), label_(label), acceptedChars_(acceptedChars) {  qDebug() << "Entering "<< Q_FUNC_INFO; } // ValidatorText::ValidatorText

ValidatorText::~ValidatorText()
{ } // ValidatorText::~ValidatorText

void ValidatorText::setLabel(QLabel* label) {
  label_ = label;
} // ValidatorText::setLabel

void ValidatorText::setAcceptedChars(QString acceptedChars) {
  acceptedChars_ = acceptedChars;
} // ValidatorText::setAcceptedChars

QValidator::State ValidatorText::validate(QString & input, int & pos) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  Q_UNUSED(pos);
  if (input.length() > 0) {
    QString::const_iterator it = input.begin();
    QString::const_iterator itEnd = input.end();
    emit hideToolTip();
    bool isAlpha = UtfConverter::unicode_isalpha(it->unicode());
    if (!isAlpha) {
      if (label_) {
        if (it->isDigit()) {
          emit showToolTip(tr("Character '%1' is numeric therefore it is not allowed as first character in the %2 field").arg(*it).arg(label_->text()));
        } else {
          emit showToolTip(tr("Character '%1' is not alphabetic therefore it is not allowed as first character in the %2 field").arg(*it).arg(label_->text()));
        }
      }
      return QValidator::Invalid;
    }
    for (++it; it != itEnd; ++it) {
      emit hideToolTip();
      isAlpha = UtfConverter::unicode_isalpha(it->unicode());
      if (isAlpha || it->isDigit() || it->isSpace() || acceptedChars_.contains(*it)) {
        continue;
      } else {
        if (label_) {
          emit showToolTip(tr("Character '%1' is not allowed in the %2 field").arg(*it).arg(label_->text()));
        }
        return QValidator::Invalid;
      }
    }
  }
  return QValidator::Acceptable;
} // ValidatorText::validate
