/** @file UnitArray.cc
    @brief Contains the implementations of the UOM array class.

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2004-2021 Paolo Greppi simevo s.r.l.
 */

#include <sstream> // for std::basic_ostringstream
#include <cassert>
#include <climits> // for INT_MAX

#include "UnitArray.h"

const int ndimensions = 9;

template<class T> std::string UnitArrayGen<T>::buffer_;

template<class T> UnitArrayGen<T>::UnitArrayGen(T val) {
  for (int i = 0; i < ndimensions; ++i) elems_[i] = val;
} // UnitArrayGen<T>::UnitArrayGen

template<class T> UnitArrayGen<T>::UnitArrayGen(T val0, T val1, T val2, T val3, T val4, T val5, T val6, T val7, T val8) {
  elems_[0] = val0;
  elems_[1] = val1;
  elems_[2] = val2;
  elems_[3] = val3;
  elems_[4] = val4;
  elems_[5] = val5;
  elems_[6] = val6;
  elems_[7] = val7;
  elems_[8] = val8;
} // UnitArrayGen<T>::UnitArrayGen

template<class T> UnitArrayGen<T>::UnitArrayGen(const UnitArrayGen &source) {
  for (int i = 0; i < ndimensions; i += 1) elems_[i] = source.elems_[i];
} // UnitArrayGen<T>::UnitArrayGen

template<class T> UnitArrayGen<T>::~UnitArrayGen(void) { } // UnitArrayGen<T>::~UnitArrayGen

template<class T> UnitArrayGen<T> &UnitArrayGen<T>::operator+=(const UnitArrayGen & rhs) {
  for (int i = 0; i < ndimensions; i += 1) elems_[i] += rhs.elems_[i];
  return *this;
} // UnitArrayGen<T>::operator+=

template<class T> UnitArrayGen<T> &UnitArrayGen<T>::operator-=(const UnitArrayGen & rhs) {
  for (int i = 0; i < ndimensions; i += 1) elems_[i] -= rhs.elems_[i];
  return *this;
} // UnitArrayGen<T>::operator-=

template<class T> UnitArrayGen<T> &UnitArrayGen<T>::operator*=(const UnitArrayGen & rhs) {
  for (int i = 0; i < ndimensions; i += 1) elems_[i] *= rhs.elems_[i];
  return *this;
} // UnitArrayGen<T>::operator*=

template<class T> T UnitArrayGen<T>::operator[](int i) const {
  assert(i >= 0);
  assert(i < ndimensions);
  return elems_[i];
} // UnitArrayGen<T>::operator[]

template<class T> T & UnitArrayGen<T>::operator[](int i) {
  assert(i >= 0);
  assert(i < ndimensions);
  return elems_[i];
} // UnitArrayGen<T>::operator[]

template<class T> bool UnitArrayGen<T>::operator==(const UnitArrayGen & rhs) const {
  for (int i = 0; i < ndimensions; i += 1)
    if (elems_[i] != rhs.elems_[i]) return false;
  return true;
} // UnitArrayGen<T>::operator==

template<class T> bool UnitArrayGen<T>::operator==(T x) const {
  for (int i = 0; i < ndimensions; i += 1)
    if (elems_[i] != x) return false;
  return true;
} // UnitArrayGen<T>::operator==

template<class T> UnitArrayGen<T> &UnitArrayGen<T>::operator/=(T factor) {
  for (int i = 0; i < ndimensions; i += 1) elems_[i] /= factor;
  return *this;
} // UnitArrayGen<T>::operator/=

template<class T> std::ostream & operator<<(std::ostream &os, const UnitArrayGen<T> &x) {
  return x.print(os);
} // UnitArrayGen<T>::operator<<

template<class T> void UnitArrayGen<T>::tostring(std::string & s) const {
  static std::basic_ostringstream<char> buffer;
  buffer.str("");
  bool written = false;
  if (elems_[0] == INT_MAX) {
    buffer << "[invalid unit]";
  } else {
    for (int i = 0; i < ndimensions; i++) {
      if (elems_[i] == 0) {
        // do nothing
      } else {
        if (written)
          buffer << ' ';
        if (elems_[i] == 1)
          buffer << uomTags_[i];
        else
          buffer << uomTags_[i] << '^' << elems_[i];
        written = true;
      }
    } // for each dimension
  } // if INT_MAX
  s = buffer.str();
  return;
} // UnitArrayGen<T>::tostring

template<class T> const std::string &UnitArrayGen<T>::tostring(void) const {
  tostring(buffer_);
  return buffer_;
} // UnitArrayGen<T>::tostring

template<class T> std::ostream & UnitArrayGen<T>::print(std::ostream &os) const {
  std::string s;
  tostring(s);
  os << s;
  return os;
} // UnitArrayGen<T>::print

// binary +
template<class T> UnitArrayGen<T> operator+(const UnitArrayGen<T> & x, const UnitArrayGen<T> & y) {
  UnitArrayGen<T> r(x);
  return r += y;
} // UnitArrayGen<T>::operator+

// binary -
template<class T> UnitArrayGen<T> operator-(const UnitArrayGen<T> & x, const UnitArrayGen<T> & y) {
  UnitArrayGen<T> r(x);
  return r -= y;
} // UnitArrayGen<T>::operator-

// binary *
template<class T> UnitArrayGen<T> operator*(const UnitArrayGen<T> & x, const UnitArrayGen<T> & y) {
  UnitArrayGen<T> r(x);
  return r *= y;
} // UnitArrayGen<T>::operator*

// binary /
template<class T> UnitArrayGen<T> operator/(const UnitArrayGen<T> & x, T factor) {
  UnitArrayGen<T> r(x);
  return r /= factor;
} // UnitArrayGen<T>::operator/

// unary +
template<class T> UnitArrayGen<T> operator+(const UnitArrayGen<T> & x) {
  UnitArrayGen<T> r(x);
  return r;
} // UnitArrayGen<T>::operator+

// unary -
template<class T> UnitArrayGen<T> operator-(const UnitArrayGen<T> & x) {
  UnitArrayGen<T> r(0);
  return r -= x;
} // UnitArrayGen<T>::operator-

template class UnitArrayGen<int>;
template std::ostream & operator<<(std::ostream &os, const UnitArrayGen<int> &x);
template UnitArrayGen<int> operator-(const UnitArrayGen<int> & x);
