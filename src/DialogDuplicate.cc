/** @file DialogDuplicate.cc
    @brief Implementation for the DialogDuplicate class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QRadioButton>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QGroupBox>
#include <QDebug>

#include "DialogDuplicate.h"
#include "LineEditValidated.h"

DialogDuplicate::DialogDuplicate(QWidget* parent) : QDialog(parent), index_(0) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  tagLabel_ = new QLabel(tr("Tag:"));
  tagEdit_ = new LineEditValidated(this, tagLabel_, ",-_{ }<>");
  tagLabel_->setBuddy(tagEdit_);
  descriptionLabel_ = new QLabel(tr("Description:"));
  descriptionEdit_ = new LineEditValidated(this, descriptionLabel_, ".,:-_{ }<>[]");
  descriptionLabel_->setBuddy(descriptionEdit_);
  top_ = new QHBoxLayout;
  top_->addWidget(tagLabel_);
  top_->addWidget(tagEdit_);
  top_->addWidget(descriptionLabel_);
  top_->addWidget(descriptionEdit_);

  ok_ = new QPushButton(tr("Ok"));
  cancel_ = new QPushButton(tr("Cancel"));
  bottom_ = new QHBoxLayout;
  bottom_->addWidget(ok_);
  bottom_->addWidget(cancel_);
  bottom_->addStretch();

  all_ = new QVBoxLayout;
  all_->addLayout(top_);
  all_->addSpacing(10);
  all_->addLayout(bottom_);

  setLayout(all_);

  connect(ok_, SIGNAL(clicked()), this, SLOT(okClicked()));
  connect(cancel_, SIGNAL(clicked()), this, SLOT(close()));
  setWindowTitle(tr("New case"));
} // DialogDuplicate::DialogDuplicate

void DialogDuplicate::okClicked(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  duplicate(tagEdit_->text(), descriptionEdit_->text());
  close();
} // DialogDuplicate::okClicked
