/** @file ActionLibpfSensitivity.cc
    @brief Implementation for the ActionLibpfSensitivity class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian, Yasantha Samarasekera and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <cassert> // for assert
#include <algorithm>

#include <QDebug>
#include <QProcess>
#include <QItemDelegate>
#include <QFile>
#include <QTimer>
#if (QT_VERSION >= 0x040600)
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include <QMutex>
#endif

#include "ActionLibpfSensitivity.h"
#include "OrderingLexicographical.h"
#include "OrderingReflectedGray.h"
#include "OrderingSpiral.h"
#include "persistency.h"
#include "Dimension.h"
#include "UnitEngine.h"
extern UnitEngine unitEngine_;

ActionLibpfSensitivity::ActionLibpfSensitivity(QObject *parent) : parent_(parent), libpfProc_(NULL), ctlDataModel_(NULL),
  fsw_(NULL),
#if (QT_VERSION >= 0x040600)
  schema_(NULL), schemaValidator_(NULL),
#endif
  resume_(false),
  stoprun_(false), ordering_(NULL), skipSignals_(true), timer_(NULL), mutex_(NULL) {

  qDebug() << "Entering "<< Q_FUNC_INFO;
  settings_ = new Settings();
  mutex_ = new QMutex();

  libpfInputFilename_ = settings_->homePath();
  libpfInputFilename_ += "/SensitivityInput.xml";
  libpfOutputFilename_ = settings_->homePath();
  libpfOutputFilename_ += "/SensitivityResults.xml";
} // ActionLibpfSensitivity::ActionLibpfSensitivity

ActionLibpfSensitivity::~ActionLibpfSensitivity(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  dontWatchOutputFile_();
  delete settings_;
  delete mutex_;
  if (ordering_ != NULL)
    delete ordering_;
} // ActionLibpfSensitivity::~ActionLibpfSensitivity

void ActionLibpfSensitivity::watchOutputFile_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;

  dontWatchOutputFile_();

#if (QT_VERSION >= 0x040600)
  schema_ = new QXmlSchema();
  QString schemaFileName(settings_->homePath());
  schemaFileName += "SensitivityResult_schema.xsd";
  qDebug() << "Trying to open schema: " << schemaFileName;
  QUrl schemaUrl = QUrl::fromLocalFile(schemaFileName);
  schema_->load(schemaUrl);

  if (schema_->isValid())
    schemaValidator_ = new QXmlSchemaValidator(*schema_);
#endif

  // create the file system watcher in a different thread w.r.t. gui
  fsw_ = new QFileSystemWatcher(this);

  connect(fsw_, SIGNAL(fileChanged(const QString &)), this, SLOT(onFileChanged(const QString &)));
  connect(fsw_, SIGNAL(directoryChanged(const QString &)), this, SLOT(onDirChanged(const QString &)));
  if (QFile::exists(libpfOutputFilename_))
    fsw_->addPath(libpfOutputFilename_);
  // watch the home directory in case the results file is created/deleted.
  fsw_->addPath(settings_->homePath());

  timer_ = new QTimer(this);
  connect(timer_, SIGNAL(timeout()), this, SLOT(dontSkipSignals()));
  timer_->start(1000);
} // ActionLibpfSensitivity::watchOutputFile

void ActionLibpfSensitivity::dontWatchOutputFile_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;

  mutex_->lock();
  if (timer_ != NULL) {
    delete timer_;
    timer_ = NULL;
  }
  if (fsw_ != NULL) {
    delete fsw_;
    fsw_ = NULL;
  }
#if (QT_VERSION >= 0x040600)
  if (schemaValidator_ != NULL) {
    delete schemaValidator_;
    schemaValidator_ = NULL;
  }
  if (schema_ != NULL) {
    delete schema_;
    schema_ = NULL;
  }
#endif
  mutex_->unlock();
} // ActionLibpfSensitivity::dontWatchOutputFile

/// Note this does not override GoAction::stop() as that is not virtual.
void ActionLibpfSensitivity::stop(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  stoprun_ = true;
  kill();
  emit stopped();
} // ActionLibpfSensitivity::stop

void ActionLibpfSensitivity::saveToXml(const QString & fileName, unsigned int round) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // Extract the analysis name from the fileName
  QRegExp rx("SensitivityInput_.+_(.+).xml", Qt::CaseInsensitive);
  QString analysisName;
  if (rx.indexIn(fileName.section("/", -1)) != -1)
    analysisName = rx.cap(1);
  else
    analysisName = "test name";
  // write input file to LibPF
  QFile libpfinput(fileName);
  libpfinput.open(QIODevice::WriteOnly);

  QXmlStreamWriter xml(&libpfinput);
  xml.setAutoFormatting(true);
  xml.writeStartDocument();

  xml.writeStartElement("sensitivity");
  xml.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
  xml.writeAttribute("xsi:noNamespaceSchemaLocation", "SensitivityInput_schema.xsd");

  xml.writeTextElement("name", analysisName);
  xml.writeTextElement("type", type().c_str());
  xml.writeTextElement("id", QString().setNum(caseId()));
  if (dynamic_cast<OrderingLexicographical *>(ordering()) != NULL)
    xml.writeTextElement("order", "lexicographical");
  if (dynamic_cast<OrderingReflectedGray *>(ordering()) != NULL)
    xml.writeTextElement("order", "boustrophedon");
  if (dynamic_cast<OrderingSpiral *>(ordering()) != NULL)
    xml.writeTextElement("order", "quasi-spiral");
  xml.writeTextElement("timeout", QString().setNum(timeOut()));
  xml.writeTextElement("startinground", QString().setNum(round));

  xml.writeStartElement("controlled");

  for (int cv = 0; cv < ctlDataModel_->rowCount(); ++cv) {
    xml.writeStartElement("variable");

    QModelIndex index;

    index = ctlDataModel_->index(cv, UOM_COL);
    std::string uom(ctlDataModel_->data(index, Qt::EditRole).toString().toUtf8());
    const Dimension *dim = unitEngine_.pdimension(uom);
    if (dim) {
      // we need to compare only to the default SI unit because the sensitivity input file
      // has to talk to the kernel which knows nothing about UnitsEng etc.
      if (dim->unit(UnitsSI).c_str() != uom) {
        xml.writeAttribute("uom", ctlDataModel_->data(index, Qt::DisplayRole).toString());
        // qDebug() << "Model unit " << monitorModel()->data(index, Qt::EditRole).toString() << " differs from default unit " << dim->unit(UnitsSI).c_str();
      }
    } else { 
      qDebug() << "Unit " << ctlDataModel_->data(index, Qt::EditRole).toString() << " has no associated dimension !";
    }

    index = ctlDataModel_->index(cv, START_COL);
    xml.writeAttribute("start", ctlDataModel_->data(index, Qt::EditRole).toString());

    index = ctlDataModel_->index(cv, END_COL);
    xml.writeAttribute("end", ctlDataModel_->data(index, Qt::EditRole).toString());

    index = ctlDataModel_->index(cv, NPOINTS_COL);
    xml.writeAttribute("points", ctlDataModel_->data(index).toString());

    index = ctlDataModel_->index(cv, LABEL_COL);
    QString label = ctlDataModel_->data(index).toString();

    index = ctlDataModel_->index(cv, FULLTAG_COL);
    QString fulltag = ctlDataModel_->data(index).toString();

    // workaround for incorrect FullTag
    int firstComma = fulltag.indexOf(":");
    int firstDot = fulltag.indexOf(".");
    QString tag;
    if (firstDot >= 0)
      tag = fulltag.right(fulltag.size() - firstDot - 1);
    else
      fulltag = tag;
    if ((firstDot >= 0) && ((firstComma < 0) || (firstDot < firstComma)))
      fulltag = fulltag.right(fulltag.size() - firstDot - 1);
    else if (firstComma >= 0)
      fulltag = fulltag.right(fulltag.size() - firstComma - 1);

    if (tag != label)
      xml.writeAttribute("label", label);
    
    xml.writeCharacters(fulltag);

    xml.writeEndElement(); // </variable>
  }
  xml.writeEndElement(); // </controlled>

  xml.writeStartElement("monitored");

  for (int mv = 0; mv < monitorModel()->rowCount(); ++mv) {
    xml.writeStartElement("variable");

    QModelIndex index;

    index = monitorModel()->index(mv, UOM_COL);
    std::string uom(monitorModel()->data(index, Qt::EditRole).toString().toUtf8());
    const Dimension *dim = unitEngine_.pdimension(uom);
    if (dim) {
      // we need to compare only to the default SI unit because the sensitivity input file
      // has to talk to the kernel which knows nothing about UnitsEng etc.
      if (dim->unit(UnitsSI).c_str() != uom) {
        xml.writeAttribute("uom", monitorModel()->data(index, Qt::DisplayRole).toString());
        // qDebug() << "Model unit " << monitorModel()->data(index, Qt::EditRole).toString() << " differs from default unit " << dim->unit(UnitsSI).c_str();
      }
    } else { 
      qDebug() << "Unit " << monitorModel()->data(index, Qt::EditRole).toString() << " has no associated dimension !";
    }

    index = monitorModel()->index(mv, LABEL_COL);
    QString label = monitorModel()->data(index).toString();
    
    index = monitorModel()->index(mv, FULLTAG_COL);
    QString fulltag = monitorModel()->data(index).toString();

    // workaround for incorrect FullTag
    int firstComma = fulltag.indexOf(":");
    int firstDot = fulltag.indexOf(".");
    QString tag;
    if (firstDot >= 0)
      tag = fulltag.right(fulltag.size() - firstDot - 1);
    else
      fulltag = tag;
    if ((firstDot >= 0) && ((firstComma < 0) || (firstDot < firstComma)))
      fulltag = fulltag.right(fulltag.size() - firstDot - 1);
    else if (firstComma >= 0)
      fulltag = fulltag.right(fulltag.size() - firstComma - 1);

    if (tag != label)
      xml.writeAttribute("label", label);

    xml.writeCharacters(fulltag);

    xml.writeEndElement(); // </variable>
  }
  xml.writeEndElement(); // </monitored>
  xml.writeEndElement(); // </sensitivity>

  xml.writeEndDocument();
  libpfinput.close();
} // ActionLibpfSensitivity::saveToXml

void ActionLibpfSensitivity::start() {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // if we are resuming a stopped run figure out the
  // round number to pass to the engine.
  unsigned int totalCVCombos = resultsModel()->rowCount();
  unsigned int round;
  if (rowComputed_.size() == totalCVCombos && resume_) {
    // this may be a restart of a stopped run.  see if any row
    // was not computed.

    if (std::find(rowComputed_.begin(), rowComputed_.end(), false) ==
        rowComputed_.end()) {
      // all rows were computed.  this must be a new run.
      // reinitialize rowComputed_.
      std::fill(rowComputed_.begin(), rowComputed_.end(), false);
      qDebug() << "restart but actually new run";
      round = 0;
    } else {
      // figure out the round number we should resume at.  rounds
      // may not be the same as the id (results table row number),
      // the results may be scattered in the table.

      for (round = 0; round < totalCVCombos; ++round) {
        int row = ordering_->idFromRound(round);
        if (!rowComputed_[row])
          break;
      }
      qDebug() << "resume at round " << round;
    }
  } else {
    // this is a new run.
    rowComputed_.resize(totalCVCombos);
    std::fill(rowComputed_.begin(), rowComputed_.end(), false);
    qDebug() << "new run";
    round = 0;
  }
  // write input file to LibPF
  saveToXml(libpfInputFilename_, round);

  skipSignals_ = false;

  for (unsigned int i = 0; i < totalCVCombos; ++i)
    if (!rowComputed_[i])
      emit computed(i, executing);
  // start LibPF in another process

  // write input file to LibPF
  QFile libpfoutput(libpfOutputFilename_);
  libpfoutput.remove();

  qDebug() << "start LibPF process";

  stoprun_ = false;
  libpfProc_ = new QProcess(this);
  libpfProc_->setWorkingDirectory(settings_->homePath());
  connect(libpfProc_, SIGNAL(finished(int)), this, SLOT(processFinished(int)));
  if (parent_ != 0) {
    QMetaObject::invokeMethod(parent_, "setLoggingProcess", Qt::DirectConnection, Q_ARG(QProcess *, libpfProc_));
    connect(libpfProc_, SIGNAL(readyReadStandardOutput()), parent_, SLOT(updateLog()));
    connect(libpfProc_, SIGNAL(readyReadStandardError()), parent_, SLOT(updateLogErr()));
  }
  watchOutputFile_();

  QString kernel = settings_->fullKernelName();
  QStringList args;
  args << "sensitivity" << QString().setNum(caseId());
  qDebug() << "starting " << kernel << " " << args;

  // Start the timeout timer
  timedOut_ = false;
  if (timeOut() > 0) {
    timeOutTimer_ = new QTimer(this);
    timeOutTimer_->setSingleShot(true);
    timeOutTimer_->start(timeOut() * 1000);
    connect(timeOutTimer_, SIGNAL(timeout()), SLOT(processTimedOut()));
    qDebug() << "Timeout timer started" << timeOut() << "seconds.";
  } else {
    timeOutTimer_ = NULL;
  }
  libpfProc_->start(kernel, args); // , QIODevice::NotOpen);

  emit started();
} // ActionLibpfSensitivity::start

/// If set to true, the next calculation run will try to restart
/// from the closest possible place to the last valid result.  If
/// set to false, it will restart the calculation from the beginning.
void ActionLibpfSensitivity::setResumeFlag(bool value) {
  resume_ = value;
} // ActionLibpfSensitivity::setResumeFlag

/// Sets the controlled variable data model.
void ActionLibpfSensitivity::setControlledVariableModel(const QAbstractItemModel * model) {
  ctlDataModel_ = model;
} // ActionLibpfSensitivity::setControlledVariableModel

Ordering *ActionLibpfSensitivity::ordering(void) {
  assert(ordering_ != NULL);
  return ordering_;
} // ActionLibpfSensitivity::ordering

void ActionLibpfSensitivity::processTimedOut() {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  qCritical() << "Process run timed out...";
  kill();
  timedOut_ = true;
} // ActionLibpfSensitivity::processTimedOut()

void ActionLibpfSensitivity::processFinished(int exitCode) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  dontSkipSignals();
  qDebug() << "call onFileChanged now !";
  onFileChanged(libpfOutputFilename_);
  skipSignals_ = true;
  dontWatchOutputFile_();

  // The process has timed out set all rows to doneNotFinished status
  if (timedOut_) {
    unsigned int totalCVCombos = resultsModel()->rowCount();
    for (unsigned int i = 0; i < totalCVCombos; ++i) {
      if (!rowComputed_[i]) {
        emit computed(i, doneNotFinished);
        rowComputed_[i] = true;
      }
    }
  }
  // process finished.  check for invalid exit code.
  else if (!stoprun_ && (exitCode != -1)) {
    qDebug() << "LibPF process exit code is: " << libpfProc_->exitCode();
    if (parent_)
      QMetaObject::invokeMethod(parent_, "setLoggingProcess", Qt::DirectConnection, Q_ARG(QProcess *, NULL));
    unsigned int totalCVCombos = resultsModel()->rowCount();
    for (unsigned int i = 0; i < totalCVCombos; ++i) {
      if (!rowComputed_[i]) {
        emit computed(i, doneSevereError);
        rowComputed_[i] = true;
      }
    }
  }
  // The process has exited no need for the timer loaded anymore
  if (timeOutTimer_) {
    timeOutTimer_->stop();
    timeOutTimer_->deleteLater();
  }
  if (stoprun_) {
    stoprun_ = false;
  } else {
    qCritical() << "processFinished() sending complete signal";
    emit complete();
  }
} // ActionLibpfSensitivity::processFinished

void ActionLibpfSensitivity::dontSkipSignals(void) {
  skipSignals_ = false;
} // ActionLibpfSensitivity::dontSkipSignals

/// Kills the calculation engine process if it is still running.
void ActionLibpfSensitivity::kill(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  qCritical() << "In kill()";

  if (libpfProc_ != NULL && libpfProc_->state() == QProcess::Running) {
    qCritical() << "killing LibPF process";
    libpfProc_->kill();
    if (parent_)
      QMetaObject::invokeMethod(parent_, "setLoggingProcess", Qt::DirectConnection, Q_ARG(QProcess *, NULL));
  }
} // ActionLibpfSensitivity::kill

void ActionLibpfSensitivity::onDirChanged(const QString & /* dirname */) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (skipSignals_)
    return;
  static int count = 0;
  qCritical() << "In onDirChanged()" << count++;
  if (QFile::exists(libpfOutputFilename_) && fsw_->files().size() == 0) {
    fsw_->addPath(libpfOutputFilename_);
    emit onFileChanged(libpfOutputFilename_);
  } else if (!QFile::exists(libpfOutputFilename_) && fsw_->files().size() == 1) {
    fsw_->removePath(libpfOutputFilename_);
  }
  qCritical() << "Out onDirChanged()" << count++;
} // ActionLibpfSensitivity::onDirChanged

void ActionLibpfSensitivity::onFileChanged(const QString & /* filename */) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (skipSignals_)
    return;
  static int count = 0;
  if (mutex_->tryLock()) {
    qCritical() << "In onFileChanged()" << count++;
    if (ctlDataModel_ == NULL) {
      // if control variable data model is not set we're not running;
      qCritical() << "ignore this signal because we're not running";
      mutex_->unlock();
      qCritical() << "Out 1 onFileChanged()" << count++;
      return;
    }
    QFile libpfoutput(libpfOutputFilename_);
    libpfoutput.open(QIODevice::ReadOnly);

    QByteArray libpfoutputbuffer(libpfoutput.readAll());
    libpfoutput.close();

#if (QT_VERSION >= 0x040600)
    if (schemaValidator_ != NULL) {
      if (!schemaValidator_->validate(libpfoutputbuffer)) {
        qCritical() << "document is invalid";
        skipSignals_ = true;
        timer_->start(1000);
        mutex_->unlock();
        qCritical() << "Out 2 onFileChanged()" << count++;
        return;
      }
    }
#endif

    QXmlStreamReader xml(libpfoutputbuffer);

    int monVarStartCol = 1 + ctlDataModel_->rowCount();

    try {
      // find next round of results or end

      while (true) {
        std::vector< double > mvValues;

        QXmlStreamReader::TokenType tt = readToElement(xml, "round", "result");

        if (tt != QXmlStreamReader::StartElement) {
          // no <round> found, bad xml file?  keep looking.
          continue;
        }
        xml.readNext();  // get to text of round

        int round = xml.text().string()->toInt();
        assert(ordering_ != NULL);

        double time(0.0);
        int errors(0), warnings(0), iterations(0);

        if (readToElement(xml, "time", "result") == QXmlStreamReader::StartElement) {
          xml.readNext();  // get to text of time
          time = xml.text().string()->toDouble();
        }
        if (readToElement(xml, "errors", "result") == QXmlStreamReader::StartElement) {
          xml.readNext();  // get to text of errors
          errors = xml.text().string()->toInt();
        }
        if (readToElement(xml, "warnings", "result") == QXmlStreamReader::StartElement) {
          xml.readNext();  // get to text of warnings
          warnings = xml.text().string()->toInt();
        }
        if (readToElement(xml, "iterations", "result") == QXmlStreamReader::StartElement) {
          xml.readNext();  // get to text of iterations
          iterations = xml.text().string()->toInt();
        }
        RowStatus rs = doneOk;
        if (errors > 0) {
          rs = doneErrors;
        } else {
          if (warnings > 0)
            rs = doneWarnings;
        }
        int row = ordering_->idFromRound(round);
        qCritical() << "onFileChanged() processing " << row;
        if (!rowComputed_[row]) {
          // get to monitored variables
          tt = readToElement(xml, "monitored", "result");
          if (tt != QXmlStreamReader::StartElement) {
            // no <monitored> found, keep looking.
            continue;
          }
          while (readToElement(xml, "variable", "monitored") == QXmlStreamReader::StartElement) {
            xml.readNext();  // get to text of variable
            mvValues.push_back(xml.text().string()->toDouble());
          }
          for (unsigned int i = 0; i < mvValues.size(); ++i) {
            resultsModel()->setData(resultsModel()->index(row, monVarStartCol + i), mvValues[i]);
          }
          resultsModel()->setData(resultsModel()->index(row, monVarStartCol + mvValues.size()), time);
          resultsModel()->setData(resultsModel()->index(row, monVarStartCol + mvValues.size() + 1), errors);
          resultsModel()->setData(resultsModel()->index(row, monVarStartCol + mvValues.size() + 2), warnings);
          resultsModel()->setData(resultsModel()->index(row, monVarStartCol + mvValues.size() + 3), iterations);

          qCritical() << "onFileChanged() completed row " << row;
          emit computed(row, rs);
          rowComputed_[row] = true;
        } // if row not computed
      } // while true
      skipSignals_ = true;
      if (std::find(rowComputed_.begin(), rowComputed_.end(), false) ==
          rowComputed_.end()) {
        static int count1 = 0;
        qCritical() << "onFileChanged() sending complete signal " << count1++;
        emit complete();
      } else {
        timer_->start(1000);
      }
    }
    catch (QXmlStreamReader::TokenType tt) {
      qCritical() << "we got an error or reached end of document";
      skipSignals_ = true;
      timer_->start(1000);
    }
    mutex_->unlock();
  } else {
    qCritical() << "onFileChanged() is locked";
  } // mutex_
  qCritical() << "Out 3 onFileChanged()" << count++;
} // ActionLibpfSensitivity::onFileChanged

/// @param rdr the xml reader
/// @param elemname the name of the element to stop at.
/// @param parentelem the name of elemname's parent element.
/// @return Returns StartElement if the start of elemname was found.
/// @return Returns EndElement if the end of parentelem was found.
/// @exception TokenType Thrown if an Invalid or EndDocument token is found.
QXmlStreamReader::TokenType ActionLibpfSensitivity::readToElement(QXmlStreamReader & rdr, const QString & elemname,
                                                                  const QString & parentelem) {

  qDebug() << "Entering "<< Q_FUNC_INFO;

  QXmlStreamReader::TokenType tt;
  for (tt = rdr.readNext(); tt != QXmlStreamReader::StartElement ||
       rdr.name() != elemname; tt = rdr.readNext()) {
    if (tt == QXmlStreamReader::Invalid || tt == QXmlStreamReader::EndDocument)
      throw tt;
    if (tt == QXmlStreamReader::EndElement && rdr.name() == parentelem)
      break;
  }
  return tt;
} // ActionLibpfSensitivity::readToElement

void ActionLibpfSensitivity::setOrdering(int o) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // number of points for each control variable
  std::vector<int> variablePoints;
  int center = 0;
  for (int cv = 0; cv < ctlDataModel_->rowCount(); ++cv) {
    QModelIndex index = ctlDataModel_->index(cv, NPOINTS_COL);
    variablePoints.push_back(ctlDataModel_->data(index).toInt());
    center *= variablePoints.back();
    center += variablePoints.back() / 2;
  } // for each control veriable
  std::reverse(variablePoints.begin(), variablePoints.end());
  if (ordering_ != NULL)
    delete ordering_;
  switch (o) {
  case 0:
    ordering_ = new OrderingLexicographical(variablePoints);
    break;
  case 1:
    ordering_ = new OrderingReflectedGray(variablePoints);
    break;
  case 2:
    ordering_ = new OrderingSpiral(variablePoints, center);
    break;
  default:
    ordering_ = NULL;
  } // switch o
} // ActionLibpfSensitivity::setOrdering
