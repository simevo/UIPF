/** @file OrderingLexicographical.cc
    @brief OrderingLexicographical class implementation

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#include "OrderingLexicographical.h"

OrderingLexicographical::OrderingLexicographical(const std::vector<int> &m) : Ordering(m), id_(0) {} // OrderingLexicographical::OrderingLexicographical

int OrderingLexicographical::idFromRound(int round) const {
  return round;
} // OrderingLexicographical::idFromRound

int OrderingLexicographical::roundFromId(int id) const {
  return id;
} // OrderingLexicographical::roundFromId
