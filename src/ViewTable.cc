/** @file ViewTable.cc
    @brief Implementation for the ViewTable class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDebug>

#include "persistency.h"
#include "ViewTable.h"

ViewTable::ViewTable(int cth) : columnToHide_(cth) {
  // setSortingEnabled(true);
} // ViewTable::ViewTable

void ViewTable::dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight, const QVector<int> & /* roles */) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // QAbstractItemModel::
  QAbstractItemView::dataChanged(topLeft, bottomRight);
  setColumnHidden(columnToHide_, true);
  scrollTo(topLeft);
} // ViewTable::dataChanged
