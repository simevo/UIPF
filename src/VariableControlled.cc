/** @file VariableControlled.cc
    @brief Implementation for the VariableControlled class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "VariableControlled.h"

VariableControlled::VariableControlled(double start, double end, int points, const std::string &tag, const std::string &units, int id) : start_(start), end_(end), points_(points), tag_(tag), units_(units), id_(id) { } // VariableControlled::VariableControlled

VariableControlled::VariableControlled(void) : start_(0.0), end_(0.0), points_(1), tag_("") { } // VariableControlled::VariableControlled

double VariableControlled::start(void) const {
  return start_;
} // VariableControlled::start

double VariableControlled::end(void) const {
  return end_;
} // VariableControlled::end

int VariableControlled::points(void) const {
  return points_;
} // VariableControlled::points

const std::string &VariableControlled::tag(void) const {
  return tag_;
} // VariableControlled::tag

const std::string &VariableControlled::units(void) const {
  return units_;
} // VariableControlled::units

int VariableControlled::id(void) const {
  return id_;
} // VariableControlled::id
