/** @file DelegateMessage.cc
    @brief Implementation for the DelegateMessage class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QPainter>
#include <QDebug>

#include "DelegateMessage.h"

DelegateMessage::DelegateMessage(void) { }

void DelegateMessage::paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString val = index.data().toString();
  if (val.left(5) == "error")
    painter->fillRect(option.rect, Qt::red);
  else
    painter->fillRect(option.rect, Qt::yellow);
  QStyledItemDelegate::paint(painter, option, index);
} // DelegateMessage::paint
