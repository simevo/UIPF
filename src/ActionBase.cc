/** @file ActionBase.cc
    @brief Implementation for the ActionBase class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "persistency.h"
#include "ActionBase.h"

ActionBase::ActionBase(void) : resultsModel_(0), stopped_(false) {} // ActionBase::ActionBase

QAbstractItemModel* ActionBase::resultsModel(void) const {
  return resultsModel_;
} // ActionBase::resultsModel

void ActionBase::setResultsModel(QAbstractItemModel* model) {
  resultsModel_ = model;
  currentRow_ = 0;
  stopped_ = false;
} // ActionBase::setResultsModel

const QAbstractItemModel* ActionBase::monitorModel(void) const {
  return monitorModel_;
} // ActionBase::monitorModel

void ActionBase::setMonitorModel(const QAbstractItemModel* model) {
  monitorModel_ = model;
} // ActionBase::setMonitorModel

void ActionBase::incrementCurrentRow(void) {
  ++currentRow_;
} // ActionBase::incrementCurrentRow

void ActionBase::setCaseId(int caseId) {
  caseId_ = caseId;
} // ActionBase::setCaseId

int ActionBase::caseId(void) const {
  return caseId_;
} // ActionBase::caseId

void ActionBase::setType(const std::string &type) {
  type_ = type;
} // ActionBase::setType

void ActionBase::setStopped(bool s) {
  stopped_ = s;
} // ActionBase::setStopped

int ActionBase::currentRow(void) {
  return currentRow_;
} // ActionBase::currentRow

bool ActionBase::isStopped(void) {
  return stopped_;
} // ActionBase::isStopped

const std::string &ActionBase::type(void) {
  return type_;
} // ActionBase::type

void ActionBase::setTimeOut(int to) {
  timeOut_ = to;
} // ActionBase::setTimeOut

int ActionBase::timeOut(void) const {
  return timeOut_;
} // ActionBase::timeOut
