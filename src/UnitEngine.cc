/** @file UnitEngine.cc
    @brief Contains the implementations of the units of measurement classes.

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2004-2021 Paolo Greppi simevo s.r.l.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDebug>

#include "UnitEngine.h"
#include "UtfConverter.h"

template<> std::string UnitArrayGen<int>::uomTags_[9] = {
  "kmol", "rad", "A", "m", "cd", "kg", "K", "s", UNICODESTRING("\u20AC")
};

UnitEngine unitEngine_;

// modified from:
// http://idlebox.net/2007/0530-StdString-Trim.blog

/// in-place trim all spaces to the right
void ltrim(std::string &str) {
  str.erase(0, str.find_first_not_of(' '));
} // ltrim

/// in-place trim all spaces to the left
void rtrim(std::string &str) {
  str.erase(str.find_last_not_of(' ') + 1, std::string::npos);
} // rtrim

/// in-place trim all spaces to the right and to the left
void trim(std::string &str) {
  ltrim(str);
  rtrim(str);
} // trim

UnitEngine::UnitEngine(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (dimensions_.size() == 0) {
    addDimension("adimensional", "", 0, 0, 0, 0, 0, 0, 0, 0);

    // fundamental units:
    addDimension("amount", "kmol", 1, 0, 0, 0, 0, 0, 0, 0);
    addDimension("angle", "rad", 0, 1, 0, 0, 0, 0, 0, 0);
    addDimension("current", "A", 0, 0, 1, 0, 0, 0, 0, 0);
    addDimension("length", "m", 0, 0, 0, 1, 0, 0, 0, 0);
    addDimension("luminous intensity", "cd", 0, 0, 0, 0, 1, 0, 0, 0);
    addDimension("mass", "kg", 0, 0, 0, 0, 0, 1, 0, 0);
    addDimension("temperature", "K", 0, 0, 0, 0, 0, 0, 1, 0);
    addDimension("time", "s", 0, 0, 0, 0, 0, 0, 0, 1);
    addDimension("money", UNICODESTRING("\u20AC"), 0, 0, 0, 0, 0, 0, 0, 0, 1);

    addDimension("acceleration", "m/s2", 0, 0, 0, 1, 0, 0, 0, -2);
    addDimension("amount flow", "kmol/s", 1, 0, 0, 0, 0, 0, 0, -1);
    addDimension("amount specific electric charge", "C/kmol", -1, 0, 1, 0, 0, 0, 0, 1);
    addDimension("amount specific energy", "J/kmol", -1, 0, 0, 2, 0, 1, 0, -2);
    addDimension("amount specific entropy", "J/(kmol*K)", -1, 0, 0, 2, 0, 1, -1, -2);
    addDimension("area", "m2", 0, 0, 0, 2, 0, 0, 0, 0);
    addDimension("areic current", "A/m2", 0, 0, 1, -2, 0, 0, 0, 0);
    addDimension("areic electric resistance", "ohm*m2", 0, 0, -2, 4, 0, 1, 0, -3);
    addDimension("areic power density", "W/m2", 0, 0, 0, 0, 0, 1, 0, -3);
    addDimension("capacitance", "F", 0, 0, 2, -2, 0, -1, 0, 4);
    addDimension("conductance", "S", 0, 0, 2, -2, 0, -1, 0, 3);
    addDimension("density", "kg/m3", 0, 0, 0, -3, 0, 1, 0, 0);
    addDimension("diffusivity", "m2/s", 0, 0, 0, 2, 0, 0, 0, -1);
    addDimension("dynamical viscosity", "Pa*s", 0, 0, 0, -1, 0, 1, 0, -1);
    addDimension("electric charge", "C", 0, 0, 1, 0, 0, 0, 0, 1);
    addDimension("electric potential", "V", 0, 0, -1, 2, 0, 1, 0, -3);
    addDimension("electric resistance", "ohm", 0, 0, -2, 2, 0, 1, 0, -3);
    addDimension("entropy", "J/K", 0, 0, 0, 2, 0, 1, -1, -2);
    addDimension("entropy power", "W/K", 0, 0, 0, 2, 0, 1, -1, -3);
    addDimension("flow coefficient", "kg/m7", 0, 0, 0, -7, 0, 1, 0, 0);
    addDimension("force", "N", 0, 0, 0, 1, 0, 1, 0, -2);
    addDimension("frequency", "Hz", 0, 0, 0, 0, 0, 0, 0, -1);
    addDimension("heat transfer coefficient", "W/(m2*K)", 0, 0, 0, 0, 0, 1, -1, -3);
    addDimension("heat transfer resistance", "m2*K/W", 0, 0, 0, 0, 0, -1, 1, 3);
    addDimension("inductance", "H", 0, 0, -2, 2, 0, 1, 0, -2);
    addDimension("inverse of length", "1/m", 0, 0, 0, -1, 0, 0, 0, 0);
    addDimension("inverse of molecular weight", "kmol/kg", 1, 0, 0, 0, 0, -1, 0, 0);
    addDimension("inverse of temperature", "1/K", 0, 0, 0, 0, 0, 0, -1, 0);
    addDimension("length*time", "m*s", 0, 0, 0, 1, 0, 0, 0, 1);
    addDimension("magnetic flux", "Wb", 0, 0, -1, 2, 0, 1, 0, -2);
    addDimension("magnetic flux density", "T", 0, 0, -1, 0, 0, 1, 0, -2);
    addDimension("mass flow", "kg/s", 0, 0, 0, 0, 0, 1, 0, -1);
    addDimension("mass per unit work", "kg/J", 0, 0, 0, -2, 0, 0, 0, 2);
    addDimension("mass specific energy", "J/kg", 0, 0, 0, 2, 0, 0, 0, -2);
    addDimension("mass specific power","W/kg",0,0,0,2,0,0,0,-3,0);
    addDimension("mass specific heat capacity", "J/(kg*K)", 0, 0, 0, 2, 0, 0, -1, -2);
    addDimension("mass specific volume", "m3/kg", 0, 0, 0, 3, 0, -1, 0, 0);
    addDimension("molar concentration", "kmol/m3", 1, 0, 0, -3, 0, 0, 0, 0);
    addDimension("molar volume", "m3/kmol", -1, 0, 0, 3, 0, 0, 0, 0);
    addDimension("molecular weight", "kg/kmol", -1, 0, 0, 0, 0, 1, 0, 0);
    addDimension("permeance", "kmol/kg/m", 1, 0, 0, -1, 0, -1, 0, 1);
    addDimension("power", "W", 0, 0, 0, 2, 0, 1, 0, -3);
    addDimension("pressure", "Pa", 0, 0, 0, -1, 0, 1, 0, -2);
    addDimension("pressure*molar volume squared", "Pa*m6/kmol2", -2, 0, 0, 5, 0, 1, 0, -2);
    addDimension("temperature derivative of pressure*molar volume squared", "Pa*m6/kmol2/K", -2, 0, 0, 5, 0, 1, -1, -2);
    addDimension("second temperature derivative of pressure*molar volume squared", "Pa*m6/kmol2/K2", -2, 0, 0, 5, 0, 1, -2, -2);
    addDimension("resistivity", "ohm*m", 0, 0, -2, 3, 0, 1, 0, -3);
    addDimension("second temperature derivative of density", "kg/(m3*K2)", 0, 0, 0, -3, 0, 1, -2, 0);
    addDimension("second temperature derivative of pressure", "Pa/K2", 0, 0, 0, -1, 0, 1, -2, -2);
    addDimension("second temperature derivative of specific heat", "J/(kg*K3)", 0, 0, 0, 2, 0, 0, -3, -2);
    addDimension("surface tension", "N/m", 0, 0, 0, 0, 0, 1, 0, -2);
    addDimension("temperature derivative of density", "kg/(m3*K)", 0, 0, 0, -3, 0, 1, -1, 0);
    addDimension("temperature derivative of pressure", "Pa/K", 0, 0, 0, -1, 0, 1, -1, -2);
    addDimension("temperature derivative of specific heat", "J/(kg*K2)", 0, 0, 0, 2, 0, 0, -2, -2);
    addDimension("temperature squared/pressure", "K2/Pa", 0, 0, 0, 1, 0, -1, 2, 2);
    addDimension("temperature/pressure", "K/Pa", 0, 0, 0, 1, 0, -1, 1, 2);
    addDimension("thermal conductivity", "W/(m*K)", 0, 0, 0, 1, 0, 1, -1, -3);
    addDimension("thermal resistivity", "m*K/W", 0, 0, 0, -1, 0, -1, 1, 3);
    addDimension("thermal resistivity squared", "m2*K2/W2", 0, 0, 0, -2, 0, -2, 2, 6);
    addDimension("VdW a", "N*m4*kmol-2", -2, 0, 0, 5, 0, 1, 0, -2);
    addDimension("VdW a*n*n", "N*m4", 0, 0, 0, 5, 0, 1, 0, -2);
    addDimension("velocity", "m/s", 0, 0, 0, 1, 0, 0, 0, -1);
    addDimension("volume", "m3", 0, 0, 0, 3, 0, 0, 0, 0);
    addDimension("volume flow", "m3/s", 0, 0, 0, 3, 0, 0, 0, -1);
    addDimension("energy", "J", 0, 0, 0, 2, 0, 1, 0, -2);
    addDimension("energy price", UNICODESTRING("\u20AC/J"), 0, 0, 0, -2, 0, -1, 0, 2, 1);

    addUnit("amount", "mol", 1.0 / 1000.0);
    addUnit(UnitsEng, "amount flow", "kmol/h", 1.0 / 3600.0);
    addUnit(UnitsEN, "amount flow", "lbmol/h", 0.4535924 / 3600.0);
    addUnit("amount flow", "mol/h", 1.0 / 3600000.0);
    addUnit("amount flow", "mol/s", 1.0 / 1000.0);
    addUnit("amount flow", "Nm3/h", (101325.0 / 273.15 / 8314.4) * 1.0 / 3600.0);
    addUnit("amount flow", "Nm3/d", (101325.0 / 273.15 / 8314.4) * 1.0 / 3600.0 / 24.0);
    addUnit("amount flow", "STPccm", (101325.0 / 298.15 / 8314.4) * 1.0 / 60.0 / 1000000.0);
    addUnit("amount flow", "Nl/min", (101325.0 / 273.15 / 8314.4) / 1000.0 / 60.0);
    addUnit("amount specific energy", "kJ/kmol", 1000);
    addUnit(UnitsEN, "amount specific energy", "BTU/lbmol", 1055.05585262 / 0.4535924);
    addUnit(UnitsEng, "amount specific energy", "kcal/kmol", 4186.8);
    addUnit("amount specific entropy", "J/kmol/K", 1.0);
    addUnit("amount specific entropy", "kJ/kmol/K", 1000.0);
    addUnit("amount specific entropy", "kcal/kmol/K", 4186.8);
    addUnit("area", "dm2", 1.0 / 100.0);
    addUnit(UnitsEN, "area", "ft2", 0.3048 * 0.3048);
    addUnit("area", "cm2", 1.0 / 100.0 / 100.0);
    addUnit("area", "mm2", 1.0 / 100.0 / 100.0 / 100.0);
    addUnit("areic current", "A/dm2", 100.0);
    addUnit(UnitsEN, "areic current", "A/ft2", 1.0 / (0.3048 * 0.3048));
    addUnit(UnitsEng, "areic current", "A/cm2", 100.0 * 100.0);
    addUnit("areic current", "A/mm2", 100.0 * 100.0 * 100.0);
    addUnit("areic current", "mA/cm2", 10.0);
    addUnit("areic electric resistance", "ohm*cm2", 1.0 / 100.0 / 100.0);
    addUnit("density", "mg/m3", 1.0 / 1000.0 / 1000.0);
    addUnit("density", "g/m3", 1.0 / 1000.0);
    addUnit(UnitsEN, "density", "lb/ft3", 0.4535924 / (0.3048 * 0.3048 * 0.3048));
    addUnit(UnitsEng, "dynamical viscosity", "cP", 1.0 / 1000.0);
    addUnit("dynamical viscosity", "mPa*s", 1.0 / 1000.0);
    addUnit("dynamical viscosity", "uPa*s", 1.0 / 1000000.0);
    addUnit("electric potential", "mV", 1.0 / 1000.0);
    addUnit(UnitsEN, "entropy power", UNICODESTRING("BTU/h/\u00B0F"), 1055.05585262 / 3600.0 / (5.0 / 9.0));
    addUnit("frequency", "rpm", 1.0 / 60.0);
    addUnit(UnitsEng, "heat transfer coefficient", "kcal/(h*m2*K)", 4186.8 / 3600.0);
    addUnit(UnitsEN, "heat transfer coefficient", "BTU/(h*ft2*F)", 9.0 * 1055.05585262 / 3600.0 / 0.3048 / 0.3048 / 5.0);
    addUnit("heat transfer coefficient", "W/m2/K", 1.0);
    addUnit("inverse of molecular weight", "mol/kg", 1.0 / 1000.0);
    addUnit("length", "in", 0.0254);
    addUnit(UnitsEN, "length", "ft", 0.3048);
    addUnit("length", "km", 1000.0);
    addUnit("length", "dm", 0.1);
    addUnit("length", "cm", 0.01);
    addUnit("length", "mm", 0.001);
    addUnit("length", "um", 0.000001);
    addUnit("mass", "mg", 1.0 / 1000.0 / 1000.0);
    addUnit("mass", "t", 1000.0);
    addUnit(UnitsEN, "mass", "lb", 0.4535924);
    addUnit("mass", "g", 1.0 / 1000.0);
    addUnit(UnitsEng, "mass flow", "kg/h", 1.0 / 3600);
    addUnit("mass flow", "kg/d", 1.0 / 3600 / 24.0);
    addUnit("mass flow", "g/d", 1.0 / 3600 / 24.0 / 1000.0);
    addUnit("mass flow", "t/h", 1000.0 / 3600.0);
    addUnit("mass flow", "t/d", 1000.0 / 3600.0 / 24.0);
    addUnit("mass flow", "t/yr", 1000.0 / 3600.0 / 24.0 / 365.2421);
    addUnit(UnitsEN, "mass flow", "lb/h", 0.4535924 / 3600.0);
    addUnit(UnitsEng, "mass specific energy", "kcal/kg", 4186.8);
    addUnit(UnitsEN, "mass specific energy", "BTU/lb", 1055.05585262 / 0.4535924);
    addUnit("mass specific energy", "MJ/kg", 1000000.0);
    addUnit("mass specific energy", "kJ/g", 1000000.0);
    addUnit("mass specific energy", "kJ/kg", 1000.0);
    addUnit("mass specific energy", "m2/s2", 1.0);
    addUnit("mass specific power", "kW/kg", 1000.0);
    addUnit(UnitsEng, "mass specific heat capacity", "kcal/(kg*K)", 4186.8);
    addUnit(UnitsEN, "mass specific heat capacity", "BTU/lb/\u00B0F", 1055.05585262 / 0.4535924 / (5.0 / 9.0));
    addUnit("mass specific heat capacity", "kcal/kg/K", 4186.8);
    addUnit("mass specific heat capacity", "J/kg/K", 1.0);
    addUnit("mass specific heat capacity", "kJ/kg/K", 1000.0);
    addUnit("molar concentration", "mol/m3", 1.0 / 1000.0);
    addUnit(UnitsEN, "molar concentration", "lbmol/ft3", 0.4535924 / (0.3048 * 0.3048 * 0.3048));
    addUnit("molar concentration", "mmol/dm3", 1.0 / 1000.0);
    addUnit("molar concentration", "mol/dm3", 1.0);
    addUnit("pressure","J/m3",1.0); // volume specific energy
    addUnit("pressure","kJ/m3",1000.0); // volume specific energy
    addUnit("pressure","J/l", 1.0 / 0.001); // volume specific energy
    addUnit("pressure","kWh/m3",3600.0 * 1000.0); // volume specific energy
    addUnit("pressure","kcal/m3",4186.8); // volume specific energy
    addUnit("pressure","kcal/l",4186.8 / 0.001); // volume specific energy
    addUnit("pressure","BTU/ft3",1055.05585262 / (0.3048 * 0.3048 * 0.3048)); // volume specific energy
    addUnit("molecular weight", "mg/Nm3", 1.0 / (101325.0 / 273.15 / 8314.4) / 1000000.0);
    addUnit("molecular weight", "g/Nm3", 1.0 / (101325.0 / 273.15 / 8314.4) / 1000.0);
    addUnit(UnitsEN, "molecular weight", "lb/lbmol", 1.0);
    addUnit(UnitsEng, "permeance", "GPU", 76.0 * 1E-8 / 273.15 / 8314.4);
    addUnit(UnitsEN, "power", "BTU/h", 1055.05585262 / 3600.0);
    addUnit(UnitsEng, "power", "kW", 1000.0);
    addUnit("power", "kcal/s", 4186.8);
    addUnit("power", "kcal/h", 4186.8 / 3600.0);
    addUnit("power", "TW", 1000000000000.0);
    addUnit("power", "GW", 1000000000.0);
    addUnit("power", "MW", 1000000.0);
    addUnit("power", "mW", 0.001);
    addUnit("pressure", "MPa", 1000000.0);
    addUnit("pressure", "atm", 101325.0);
    addUnit(UnitsEng, "pressure", "bar", 100000.0);
    addUnit("pressure", "barg", 100000.0, 101325.0);
    addUnit("pressure", "mbar", 100.0);
    addUnit("pressure", "hPa", 100.0);
    addUnit("pressure", "kPa", 1000.0);
    addUnit("pressure", "mmHg", 101325.0 / 760.0);
    addUnit("pressure", "Torr", 101325.0 / 760.0);
    addUnit("pressure", "mmH2O", 9.806);
    addUnit("pressure", "kg/cm2", 98060.0);
    addUnit(UnitsEN, "pressure", "psi", 101325.0 / 14.6959487755);
    addUnit("resistivity", "ohm*cm", 0.01);
    addUnit(UnitsEng, "temperature", UNICODESTRING("\u00B0C"), 1.0, 273.15); // Celsius degrees
    addUnit(UnitsEN, "temperature", UNICODESTRING("\u00B0F"), 5.0 / 9.0, 273.15 - 32.0 * 5.0 / 9.0); // Fahrenheit degrees
    addUnit("thermal conductivity", "W/m/K", 1.0);
    addUnit("time", "min", 60.0);
    addUnit(static_cast<Units>(UnitsEng & UnitsEN), "time", "h", 3600.0);
    addUnit("time", "d", 3600.0 * 24.0);
    addUnit("time", "yr", 3600.0 * 24.0 * 365.2421);
    addUnit(UnitsEN, "volume", "ft3", 0.3048 * 0.3048 * 0.3048);
    addUnit("volume", "dm3", 1.0 / 1000.0);
    addUnit("volume", "cm3", 1.0 / 1000.0 / 1000.0);
    addUnit("volume", "mm3", 1.0 / 1000.0 / 1000.0 / 1000.0);
    addUnit("volume", "ul", 1.0 / 1000.0 / 1000.0 / 1000.0);
    addUnit("volume", "ml", 1.0 / 1000.0 / 1000.0);
    addUnit("volume", "l", 1.0 / 1000.0);
    addUnit("volume", "bbl", 42.0 * 231.0 * 0.0254 * 0.0254 * 0.0254);
    addUnit(UnitsEng, "volume flow", "m3/h", 1.0 / 3600.0);
    addUnit(UnitsEN, "volume flow", "ft3/h", 0.3048 * 0.3048 * 0.3048 / 3600.0);
    addUnit("volume flow", "l/h", 1.0 / 3600.0 / 1000.0);
    addUnit("volume flow", "ccm", 1.0 / 60.0 / 1000000.0);
    addUnit(UnitsEN, "energy", "BTU", 1055.05585262);
    addUnit("energy", "mmBTU", 1055.05585262E6);
    addUnit("energy", "MWh", 3600.0 * 1000000.0);
    addUnit("energy", "kcal", 4186.8);
    addUnit(UnitsEng, "energy", "kWh", 3600.0 * 1000.0);
    addUnit("energy", "kJ", 1000);
    addUnit("energy price", UNICODESTRING("\u20AC/mmBTU"), 1.0 / 1055.05585262E6);
    addUnit(UnitsEN, "energy price", "$/mmBTU", 1.0 / 1.3677 / 1055.05585262E6);
    addUnit("energy price", "$/J", 1.0 / 1.3677);
    addUnit("money", "$", 1.0 / 1.3677);
    addUnit(UnitsEng, "energy price", UNICODESTRING("\u20AC/kWh"), 1.0 / 3600000.0);
    addUnit("energy price", UNICODESTRING("\u20AC/MWh"), 1.0 / 3600000000.0);
    addUnit("energy price", UNICODESTRING("\u20AC/kcal"), 1.0 / 4186.8);

    printUnits();
  } // factorlookup_.size == 0
} // UnitEngine::UnitEngine

void UnitEngine::addDimension(std::string desc, std::string SIu, int kmol, int rad, int A, int m, int cd, int kg, int K, int s, int eur) {
//  qDebug() << "Entering "<< Q_FUNC_INFO;
  trim(desc);
  dictionary_t::iterator p(descLookup_.find(desc));
  if (p != descLookup_.end()) {
    qCritical() << "duplicate dimension";
  } else {
    dimensions_.push_back(Dimension(desc, SIu, kmol, rad, A, m, cd, kg, K, s, eur));
    descLookup_.insert(std::pair<std::string, Dimension *>(desc, &dimensions_.back()));
    addUnit(static_cast<Units>(UnitsSI & UnitsEN & UnitsEng), desc, SIu, 1.0, 0.0);
    std::string new_object = "";
    dimensions_.back().unitArray().tostring(new_object);
    if (new_object != SIu)
      addUnit(UnitsUndefined, desc, new_object, 1.0, 0.0);
  }
} // UnitEngine::addDimension

void UnitEngine::addUnit(Units u, std::string desc, std::string unit, double factor, double offset) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  trim(desc);
  dictionary_t::iterator p(descLookup_.find(desc));
#ifdef VERBOSE
  std::cout << "UnitEngine::addUnit * Adding <" << unit << ">" << " to dimension " << desc << std::endl;
#endif
  if (p == descLookup_.end()) {
    qCritical() << "dimension " << desc.c_str() << " not found";
  } else {
    p->second->addUnit(u, unit, factor, offset);
    unitLookup_.insert(std::pair<std::string, Dimension *>(unit, p->second));
  }
} // UnitEngine::addUnit

void UnitEngine::addUnit(std::string desc, std::string unit, double factor, double offset) {
  addUnit(UnitsUndefined, desc, unit, factor, offset);
} // UnitEngine::addUnit

double UnitEngine::toSI(double v, const std::string &destination_unit, UnitArray &unit) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  dictionary_t::const_iterator p(unitLookup_.find(destination_unit));
  if (p == unitLookup_.end()) {
    qCritical() << "toSI failure: " << destination_unit.c_str();
    return 0.0;
  } else {
    unit = p->second->unitArray();
    return p->second->toSI(v, destination_unit);
  }
} // UnitEngine::toSI

double UnitEngine::fromSI(double v, const std::string &source_unit) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // std::cout << "Looking for unit " << source_unit << std::endl;
  dictionary_t::const_iterator p(unitLookup_.find(source_unit));
  if (p == unitLookup_.end()) {
    qCritical() << "fromSI failure: " << source_unit.c_str();
    return 0.0;
  } else {
    return p->second->fromSI(v, source_unit);
  }
} // UnitEngine::fromSI

void UnitEngine::printUnits(void) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  std::list<Dimension>::const_iterator p;

  std::cout << "[" << std::endl;
  for (p = dimensions_.begin(); p != dimensions_.end(); ++p)
    if (p->size() > 1)
      p->printUnits();
  std::cout << "]" << std::endl;
}; // UnitEngine::printUnits

UnitEngine::dictionary_t::const_iterator UnitEngine::begin(void) const {
  return (i_ = descLookup_.begin());
} // UnitEngine::begin

UnitEngine::dictionary_t::const_iterator UnitEngine::next(void) const {
  return ++i_;
} // UnitEngine::next

UnitEngine::dictionary_t::const_iterator UnitEngine::end(void) const {
  return descLookup_.end();
} // UnitEngine::end

const Dimension *UnitEngine::pdimension(std::string unit) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  trim(unit);
  // std::cout << "Looking for unit " << unit << std::endl;
  std::map<std::string, Dimension *>::const_iterator p(unitLookup_.find(unit));
  if (p == unitLookup_.end()) {
    qDebug() << unit.c_str();
    return NULL;
  } else {
    return p->second;
  }
} // UnitEngine::pdimension
