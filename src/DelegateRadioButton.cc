/** @file DelegateRadioButton.cc
    @brief Implementation for the DelegateRadioButton class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#include "DelegateRadioButton.h"
#include <QObject>
#include <QStylePainter>
#include <QTableView>
#include <QRadioButton>
#include <QApplication>
#include <QDebug>

DelegateRadioButton::DelegateRadioButton(QObject *parent) :
  QStyledItemDelegate(parent) {} // DelegateRadioButton::DelegateRadioButton

DelegateRadioButton::~DelegateRadioButton() {} // DelegateRadioButton::~DelegateRadioButton

void DelegateRadioButton::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // Draw background first
  if (option.state & QStyle::State_Selected) {
    painter->fillRect(option.rect, option.palette.highlight());
  } else {
    QColor backgroundColor = qvariant_cast<QColor>(index.data(Qt::BackgroundColorRole));
    painter->fillRect(option.rect, backgroundColor);
  }

  // Draw the control
  QStyleOptionButton button;
  button.rect = option.rect;
  button.rect.setX(button.rect.width()/2 - 8);
  button.state = QStyle::State_Raised | QStyle::State_Enabled | (index.data(Qt::EditRole).toBool() ? QStyle::State_On : QStyle::State_Off);
  QApplication::style()->drawControl(QStyle::CE_RadioButton, &button, painter);
} // DelegateRadioButton::paint
