/** @file OrderingSpiral.cc
    @brief OrderingSpiral class implementation

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#include <algorithm> // for sort
#include <iostream> // for sort
#include <cassert>

#include "OrderingSpiral.h"

// Return whether first point is closer than the second
bool closer(const std::pair<int, int> &p1, const std::pair<int, int> &p2) {
  return (p1.second < p2.second);
} // closer

OrderingSpiral::OrderingSpiral(const std::vector<int> &m, int initial) : Ordering(m), initial_(initial) {
  min_.resize(k_);
  l_.resize(k_);
  max_.resize(k_);
  // fill in direct_ and reverse_ maps
  // start
  int round_ = 0;
  int current_ = initial;
  visited_.insert(current_);
  direct_[round_] = current_;
  reverse_[current_] = round_;
  round_++;
  // while there is any available points left, start
  while (round_ < maxRound()) {
    // set the acceptable distance to 1, i.e. we start looking for points adjacent to the current point in the sense of the dense grid
    acceptable_ = 1;
    // while the resulting suitable list is empty, start
    suitable_.clear();
    while (suitable_.size() == 0) {
      // create the list of suitable points
      int neighbor = begin_(current_, acceptable_);
      while (neighbor != end_()) {
        // std::cout << "Neighbor = " << neighbor << std::endl;
        if (visited_.find(neighbor) == visited_.end())
          suitable_.push_back(std::pair<int, int>(neighbor, distance(initial_, neighbor)));
        neighbor = next_();
      } // for all points which have distance lower than the acceptable distance
        // increase by 1 the acceptable distance
      acceptable_++;
    } // suitable found
    std::sort(suitable_.begin(), suitable_.end(), closer);
    // std::cout << "Suitable points: ";
    // print_(suitable_);
    // set the first suitable point as the new current point
    current_ = suitable_[0].first;
    // std::cout << "At round " << round_ << " with " << suitable_.size() << " suitable points, set current to " << current_ << std::endl;
    // delete the current point from the available list and append it to the path
    visited_.insert(current_);
    direct_[round_] = current_;
    reverse_[current_] = round_;
    round_++;
    // printVisited_();
  } // available points left
  assert(visited_.size() == (unsigned int)maxRound());
  toInternalRepresentation_(initial_, g_);
} // OrderingSpiral::OrderingSpiral

int OrderingSpiral::idFromRound(int round) const {
  std::map<int,int>::const_iterator id = direct_.find(round);
  assert(id != direct_.end());
  return id->second;
} // OrderingSpiral::idFromRound

int OrderingSpiral::roundFromId(int id) const {
  std::map<int,int>::const_iterator round = reverse_.find(id);
  assert(round != reverse_.end());
  return round->second;
} // OrderingSpiral::roundFromId

int OrderingSpiral::initial(void) const {
  return initial_;
} // OrderingSpiral::initial

int OrderingSpiral::begin_(int from, int acceptable) const {
  // std::cout << "Beginning from " << from << std::endl;
  toInternalRepresentation_(from, g_);
  for (int i = 0; i < k_; ++i) {
    l_[i] = min_[i] = std::max(0, g_[i] - acceptable);
    max_[i] = std::min(m_[i], g_[i] + acceptable + 1);
    // std::cout << i << " = [ " << 0 << " [" << min_[i] << " [ " << g_[i] << " ] " << max_[i] << "] " << m_[i] << " ]" << std::endl;
  }
  return fromInternalRepresentation_(l_);
} // OrderingSpiral::begin_

int OrderingSpiral::end_(void) const {
  return -1;
} // OrderingSpiral::end_

int OrderingSpiral::next_(void) const {
  int i = 0;
  int k = l_[i] + 1;
  while ((k >= max_[i]) && (i < k_)) {
    l_[i] = min_[i];
    i++;
    k = l_[i] + 1;
  }
  if (i == k_) {
    return end_();
  } else {
    l_[i] = k;
    return fromInternalRepresentation_(l_);
  }
} // OrderingSpiral::next_
