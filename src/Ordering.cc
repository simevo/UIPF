/** @file Ordering.cc
    @brief Ordering class implementation

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#include <cstdlib> // for abs
#include <cassert>
#include <algorithm> // for min/max

#include "Ordering.h"

Ordering::Ordering(const std::vector<int> &m) {
  k_ = m.size();
  assert(k_ > 0);
  m_.resize(k_);
  g_.resize(k_);
  h_.resize(k_);
  for (int i = 0; i < k_; ++i) {
    m_[i] = i < k_ ? m[i] : m[std::max(i - 1, 0)];
    g_[i] = 0;
    assert(m_[i] > 0);
  }
} // Ordering::Ordering

Ordering::~Ordering(void) { }

int Ordering::maxRound(void) const {
  if (k_ <= 0) {
    return 0;
  } else {
    int mr = 1;
    for (int i = 0; i < k_; ++i)
      mr *= m_[i];
    return mr;
  }
} // Ordering::maxRound

void Ordering::toInternalRepresentation_(int i, std::vector<int> &g) const {
  // MSD is k_ - 1
  // LSD is 0
  for (int j = 0; j < k_; ++j) {
    g[j] = i % m_[j];
    i /= m_[j];
  }
} // Ordering::toInternalRepresentation_

int Ordering::fromInternalRepresentation_(const std::vector<int> &g) const {
  int i = g[k_ - 1];
  for (int j = k_ - 2; j >= 0; --j) {
    i *= m_[j];
    i += g[j];
  }
  return i;
} // Ordering::fromInternalRepresentation_

int Ordering::localIdFromId(int id, int j) const {
  toInternalRepresentation_(id, g_);
  return g_[k_ - j - 1];
} // Ordering::localIdFromId

int Ordering::initial(void) const {
  return 0;
} // Ordering::initial

int Ordering::distance(int i, int j) const {
  toInternalRepresentation_(i, g_);
  toInternalRepresentation_(j, h_);
  int d(0);
  for (unsigned int k = 0; k < m_.size(); ++k)
    d = std::max(d, abs(g_[k] - h_[k]));
  return d;
} // Ordering::distance
