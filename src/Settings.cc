/** @file Settings.cc
    @brief Implementation for the Settings class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDir>
#include <QCoreApplication>

#include "Settings.h"

Settings::Settings() : QSettings("libpf.com", "LIBPF") {
  beginGroup("1.1");
} // Settings::Settings

QString Settings::homePath(void) {
#if defined(Q_OS_MAC)
  return value("home_path", QDir::homePath() + "/Library/LIBPF 1.0").toString();

#else
  return value("home_path", QDir::homePath() + "/LIBPF 1.0").toString();

#endif
} // Settings::homePath

QString Settings::kernelPath(void) {
#if defined(Q_OS_MAC)
  return value("kernel_path", QCoreApplication::applicationDirPath() + "/../../../uipf.app/Contents/Resources").toString();

#else
  return value("kernel_path", QDir::currentPath()).toString();

#endif
} // Settings::kernelPath

QString Settings::home(void) {
#if defined(Q_OS_MAC)
  return value("home", QCoreApplication::applicationDirPath() + "/../../../uipf.app/Contents/Resources").toString();

#else
  return value("home", ".").toString();

#endif
} // Settings::home

QString Settings::fullKernelName(void) {
  QString kernel = kernelPath();
  kernel += "/";
  if (value("active_kernel", "").toString() != "") {
    kernel += value("active_kernel").toString();
    kernel += "/";
  }
  kernel += kernelName();
  return kernel;
} // Settings::fullKernelName

QString Settings::svgFile(int id) {
  QString svgfile = homePath();
  svgfile += QString("/%1.svg").arg(id);
  return svgfile;
} // Settings::svgFile

QString Settings::kernelName(void) {
  return value("kernel_name", "kernel").toString();
} // Settings::kernelName

QString Settings::units(void) {
  return value("units", "SI").toString();
} // Settings::units

void Settings::setUnits(QString unit) {
  setValue("units", unit);
} // Settings::setUnits

bool Settings::child(void) {
  return value("child", false).toBool();
}

void Settings::toggleChild() {
  if (child())
    setValue("child", false);
  else
    setValue("child", true);
}
