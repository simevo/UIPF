/** @file itoa.cc
    @brief Implementation for the itoa function

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "itoa.h"

#include <cstdlib> // for abs
#include <algorithm> // for reverse

std::string itoa(int value, int base, int width) {
  enum {
    kMaxDigits = 35
  };

  std::string buffer;
  buffer.reserve(kMaxDigits);   // Pre-allocate enough space.

  // check that the base if valid
  if (base < 2 || base > 16) return buffer;
  int quotient = value;

  // Translating number to string with base:
  do {
    buffer += "0123456789ABCDEF"[std::abs(quotient % base)];
    quotient /= base;
  } while (quotient);
  if (width > 0) {
    int crop = buffer.size() - width;
    if (crop > 0)
      buffer = buffer.substr(buffer.size() - width);
    else if (crop < 0)
      for (int i = 0; i < -crop; i++) buffer.push_back('0');
  }
  // Append the negative sign
  if (value < 0) buffer += '-';
  std::reverse(buffer.begin(), buffer.end());
  return buffer;
} // itoa
