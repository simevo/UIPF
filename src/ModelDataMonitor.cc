/** @file ModelDataMonitor.cc
    @brief Implementation for the ModelDataMonitor class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian, Yasantha Samarasekera and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDebug>

#include "persistency.h"
#include "ModelDataMonitor.h"

/// @param parent the parent object
ModelDataMonitor::ModelDataMonitor(QObject* parent) :
  QAbstractTableModel(parent)
{   qDebug() << "Entering "<< Q_FUNC_INFO; } // ModelDataMonitor::ModelDataMonitor

/// Reimplemented from QAbstractTableModel::rowCount()
int ModelDataMonitor::rowCount(const QModelIndex & /*index*/) const {
  return data_.count();
} // ModelDataMonitor::rowCount

/// Reimplemented from QAbstractTableModel::columCount()
int ModelDataMonitor::columnCount(const QModelIndex & /*index*/) const {
  return 6;
} // ModelDataMonitor::columnCount

/// Reimplemented from QAbstractTableModel::data()
QVariant ModelDataMonitor::data(const QModelIndex & index, int role) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!index.isValid())
    return QVariant();
  RecordMonitor record = data_[index.row()];
  if (role == Qt::TextAlignmentRole) {
    return static_cast<int>(Qt::AlignLeft | Qt::AlignVCenter);
  } else if (role == Qt::EditRole) {
    if (index.column() == 0) {
      return record.ID;
    }
    if (index.column() == 1) {
      return record.FULLTAG;
    }
    if (index.column() == 2) {
      return record.UOM;
    }
    if (index.column() == 3) {
      return record.DESCRIPTION;
    }
    if (index.column() == 4) {
      return record.LABEL;
    }
    if (index.column() == 5) {
      return record.DBID;
    }
  } else if (role == Qt::DisplayRole) {
    if (index.column() == 0) {
      return record.ID;
    }
    if (index.column() == 1) {
      return record.FULLTAG;
    }
    if (index.column() == 2) {
      if (uValue_.contains(index.row()))
        return uValue_.value(index.row());
      else
        return record.UOM;
    }
    if (index.column() == 3) {
      return record.DESCRIPTION;
    }
    if (index.column() == 4) {
      return record.LABEL;
    }
    if (index.column() == 5) {
      return record.DBID;
    }
  }
  return QVariant();
} // ModelDataMonitor::data

/// Reimplemented from QAbstractTableModel::flags()
Qt::ItemFlags ModelDataMonitor::flags(const QModelIndex & index) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if ((index.column() == 1) || (index.column() == 4) || (index.column() == UOM_COL))
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
  return QAbstractItemModel::flags(index);
} // ModelDataMonitor::flags

/// Reimplemented from QAbstractTableModel::setData()
bool ModelDataMonitor::setData(const QModelIndex & index, const QVariant & value, int role) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid() && (role == Qt::EditRole)) {
    int row = index.row();
    if (index.column() == 0) {
      data_[row].ID = value.toInt();
      emit dataChanged(index, index);
      return true;
    }
    if (index.column() == 1) {
      data_[row].FULLTAG = value.toString().trimmed();
      emit dataChanged(index, index);
      return true;
    }
    if (index.column() == 2) {
      data_[row].UOM = value.toString().trimmed();
      emit dataChanged(index, index);
      return true;
    }
    if (index.column() == 3) {
      data_[row].DESCRIPTION = value.toString().trimmed();
      emit dataChanged(index, index);
      return true;
    }
    if (index.column() == 4) {
      data_[row].LABEL = value.toString().trimmed();
      emit dataChanged(index, index);
      return true;
    }
    if (index.column() == 5) {
      data_[row].DBID = value.toInt();
      emit dataChanged(index, index);
      return true;
    }
  } else if (index.isValid() && role == Qt::DisplayRole) {
    if (index.column() == UOM_COL) {
      uValue_.insert(index.row(), value);
      emit dataChanged(index, index);
    }
  }
  return false;
} // ModelDataMonitor::setData

/// Reimplemented from QAbstractTableModel::inserRows()
bool ModelDataMonitor::insertRows(int row, int count, const QModelIndex & parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (count <= 0)
    return false;
  beginInsertRows(parent, row, row + count - 1);
  for (int i = 0; i < count; ++i) {
    data_.insert(row + i, RecordMonitor(row + i + 1));
  }
  for (int i = row + count; i < rowCount(); ++i) {
    data_[i].ID = i + 1;
  }
  endInsertRows();
  return true;
} // ModelDataMonitor::insertRows

/// Reimplemented from QAbstractTableModel::removeRows()
bool ModelDataMonitor::removeRows(int row, int count, const QModelIndex & parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!rowCount())
    return false;
  if (count <= 0)
    return false;
  beginRemoveRows(parent, row, row + count - 1);
  for (int i = 0; i < count; ++i) {
    data_.removeAt(row);
  }
  for (int i = row; i < rowCount(); ++i) {
    data_[i].ID = i + 1;
  }
  endRemoveRows();
  return true;
} // ModelDataMonitor::removeRows

/// Reimplemented from QAbstractTableModel::headerData()
QVariant ModelDataMonitor::headerData(int section, Qt::Orientation orientation, int role) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if ((role == Qt::DisplayRole) && (orientation == Qt::Horizontal)) {
    if (section == 0)
      return tr("ID");
    if (section == 1)
      return tr("Monitored variables");
    if (section == 2)
      return tr("Units");
    if (section == 3)
      return tr("Description");
    if (section == 4)
      return tr("Label");
  }
  if ((role == Qt::TextAlignmentRole) && (orientation == Qt::Horizontal)) {
    return static_cast<int>(Qt::AlignLeft | Qt::AlignVCenter);
  }
  return QVariant();
} // ModelDataMonitor::headerData
