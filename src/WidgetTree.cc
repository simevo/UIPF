/** @file WidgetTree.cc
    @brief Implementation for the WidgetTree class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QTreeWidgetItem>
#include <QHeaderView>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>

#include "persistency.h"
#include "WidgetTree.h"
#include "ActionLibpf.h" // for getErrorsWarnings

unsigned int WidgetTree::maxTreeLeaves_ = 3000;

#if defined (_MSC_VER)
// disable annoying MSVC 2005 "This function or variable may be unsafe" warning
#pragma warning(disable : 4996)
#endif

bool WidgetTree::isStream(std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (h_->isA(s, "Stream"));
} // WidgetTree::isStream

bool WidgetTree::isUnit(std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (h_->isA(s, "VertexBase"));
} // WidgetTree::isUnit

bool WidgetTree::isOther(std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (!h_->isA(s, "ModelInterface") || (!(isStream(s)) && !(isUnit(s))));
} // WidgetTree::isOther

bool WidgetTree::isAny(std::string) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return true;
} // WidgetTree::isAny

bool WidgetTree::isPhase(std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (h_->isA(s, "Phase"));
} // WidgetTree::isPhase

bool WidgetTree::isFlowsheet(std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (h_->isAGeneric(s, "FlowSheet"));
} // WidgetTree::isFlowsheet

bool WidgetTree::isReaction(std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (h_->isA(s, "Reaction"));
} // WidgetTree::isReaction

bool WidgetTree::isMultireaction(std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (h_->isAGeneric(s, "MultiReaction"));
} // WidgetTree::isMultireaction

WidgetTree::WidgetTree(QWidget* parent) : QWidget(parent), currentNode_(-1), rootNode_(-1) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  tree_ = new QTreeWidget();

  tree_->setColumnCount(5);
  tree_->setIconSize(QSize(20, 20));
  tree_->setHeaderLabels(
    QStringList() << tr("Tag") << tr("Description") << tr("Type") << tr("ID") << tr("Parent ID"));
#if (QT_VERSION >= 0x050000)
  tree_->header()->setSectionResizeMode(0, QHeaderView::Stretch);
  tree_->header()->setSectionResizeMode(1, QHeaderView::Stretch);
  tree_->header()->setSectionResizeMode(2, QHeaderView::Stretch);
#else
  tree_->header()->setResizeMode(0, QHeaderView::Stretch);
  tree_->header()->setResizeMode(1, QHeaderView::Stretch);
  tree_->header()->setResizeMode(2, QHeaderView::Stretch);
#endif  

  tree_->hideColumn(3);
  tree_->hideColumn(4);

  tree_->clear();
  leaves_ = 0;

  connect(tree_, SIGNAL(itemActivated(QTreeWidgetItem *, int)), this, SLOT(clickedOn(QTreeWidgetItem *, int)));

  QVBoxLayout* mainlayout = new QVBoxLayout;

  mainlayout->addWidget(tree_);
  setLayout(mainlayout);
} // WidgetTree::WidgetTree

void WidgetTree::expandAll(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (leaves_ >= maxTreeLeaves_) {
#if (QT_VERSION >= 0x040200)
    QMessageBox::StandardButton button;
    button = QMessageBox::warning(this, "Warning",
                                  tr("Document has large number of elements (>= %1). "
                                     "Expanding all may take lots of time. \n"
                                     "Application could hang.\n"
                                     "Are you sure you want to expand them all ?")
                                  .arg(maxTreeLeaves_),
                                  QMessageBox::Yes | QMessageBox::No);
#else
    int button;
    button = QMessageBox::warning(this, "Warning",
                                  tr("Document has large number of elements (>= %1). "
                                     "Expanding all may take lots of time. \n"
                                     "Application could hang.\n"
                                     "Are you sure you want to expand them all ?")
                                  .arg(maxTreeLeaves_),
                                  QMessageBox::Yes,
                                  QMessageBox::No);

#endif
    if (button == QMessageBox::No)
      return;
  }
  expandAll(tree_->topLevelItem(0));
} // WidgetTree::expandAll

void WidgetTree::expandAll(QTreeWidgetItem *parentItem) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (parentItem == NULL)
    return;
  QTreeWidgetItem* child = NULL;
  tree_->expandItem(parentItem);
  int childCount = parentItem->childCount();
  for (int x = 0; x < childCount; x++) {
    child = parentItem->child(x);
    if (child->childCount()) {
      expandAll(child);
    }
  }
} // WidgetTree::expandAll

bool WidgetTree::expandThis(QTreeWidgetItem* parentItem, int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QTreeWidgetItem* child = NULL;
  // std::cout << "Entered for " << parentItem->text(0).toStdString() << std::endl;
  int childCount = parentItem->childCount();
  for (int x = 0; x < childCount; x++) {
    child = parentItem->child(x);
    // std::cout << "Looking at " << child->text(0).toStdString() << std::endl;
    if (child->text(3).toInt() == node) {
#if (QT_VERSION >= 0x040200)
      child->setSelected(true);
#else
      tree_->setItemSelected(child, true);
#endif
      tree_->expandItem(parentItem);
      // std::cout << "This is it, returning true" << std::endl;
      return true;
    } else {
      if (child->childCount())
        if (expandThis(child, node)) {
          // std::cout << "Then expanding " << parentItem->text(0).toStdString() << std::endl;
          tree_->expandItem(parentItem);
          return true;
        }
    } // if is a node
  } // for each child
  return false;
} // WidgetTree::expandThis

void WidgetTree::expandThis(int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  expandThis(tree_->topLevelItem(0), node);
} // WidgetTree::expandThis

void WidgetTree::collapseAll(QTreeWidgetItem *parentItem) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (parentItem == NULL)
    return;
  QTreeWidgetItem* child = NULL;
  tree_->collapseItem(parentItem);
  int childCount = parentItem->childCount();
  for (int x = 0; x < childCount; x++) {
    child = parentItem->child(x);
    if (child->childCount()) {
      collapseAll(child);
    }
  }
} // WidgetTree::collapseAll

void WidgetTree::collapseAll(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  collapseAll(tree_->topLevelItem(0));
}  // WidgetTree::collapseAll

int WidgetTree::currentNode(void) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return currentNode_;
} // WidgetTree::currentNode

void WidgetTree::setCurrentNode(int n) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  currentNode_ = n;
} // WidgetTree::setCurrentNode

const char* WidgetTree::type(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (tree_->topLevelItem(0) == 0)
    type_[0] = '\0';
  else
    strcpy(type_, qPrintable(tree_->topLevelItem(0)->text(2)));
  return type_;
} // WidgetTree::type

void WidgetTree::clear(void) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  tree_->clear();
} // WidgetTree::clear

QTreeWidgetItem* WidgetTree::createChild(QString tag, QString type, QString id, QString description, QString parent, QString range, int errors, int warnings) {
  qDebug() << "Entering "<< Q_FUNC_INFO;

  QTreeWidgetItem* child = new QTreeWidgetItem();

  child->setText(0, tag);
  child->setText(1, description);
  child->setText(2, type);
  child->setText(3, id);
  child->setText(4, parent);
  child->setText(5, range);

  colorize_(child, errors, warnings);

  if (isFlowsheet(type.toStdString())) {
    qDebug() << "Setting flowsheet icon for " << type;
    child->setIcon(0, QIcon(":/images/flowsheet.png"));
    QTreeWidgetItem* streams;
    streams = new QTreeWidgetItem(child);
    streams->setText(0, "Streams");
    streams->setIcon(0, QIcon(":/images/streams.png"));
    streams->setText(1, "");
    streams->setText(2, "");
    streams->setText(3, id + "s");
    streams->setText(4, "-1");
    QTreeWidgetItem* units;
    units = new QTreeWidgetItem(child);
    units->setText(0, "Units");
    units->setIcon(0, QIcon(":/images/units.png"));
    units->setText(1, "");
    units->setText(2, "");
    units->setText(3, id + "u");
    units->setText(4, "-1");
  } else if (isStream(type.toStdString())) {
    qDebug() << "Setting stream icon for " << type;
    child->setIcon(0, QIcon(":/images/stream.png"));
  } else if (isUnit(type.toStdString())) {
    qDebug() << "Setting unit icon for " << type;
    child->setIcon(0, QIcon(":/images/unit.png"));
  } else {
    if (isPhase(type.toStdString())) {
      qDebug() << "Setting phase icon for " << type;
      child->setIcon(0, QIcon(":/images/phase.png"));
    } else if (isReaction(type.toStdString())) {
      qDebug() << "Setting reaction icon for " << type;
      child->setIcon(0, QIcon(":/images/reaction.png"));
    } else if (isMultireaction(type.toStdString())) {
      qDebug() << "Setting multireaction icon for " << type;
      child->setIcon(0, QIcon(":/images/multireaction.png"));
    } else {
      qDebug() << "Setting other icon for " << type;
      child->setIcon(0, QIcon(":/images/other.png"));
    }
  }  // end check type for icon
  qDebug() << "Finish with "<< Q_FUNC_INFO;
  return child;
} // WidgetTree::createChild

void WidgetTree::forceGo(int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (node == -1)
    return;
  tree_->clear();
  currentNode_ = node;
  leaves_ = 0;

  QString tag, type, description, parent, range;
  getChildData(node, tag, type, description, parent, range, completeTag_, errors_, warnings_);
  QString id = QVariant(node).toString();

  qDebug() << "tag = " << tag;
  qDebug() << "type = " << type;
  qDebug() << "description = " << description;
  qDebug() << "parent = " << parent;
  qDebug() << "id = " << id;
  qDebug() << "range = " << range;
  qDebug() << "completetag = " << completeTag_;
  qDebug() << "warnings = " << warnings_;
  qDebug() << "errors = " << errors_;

  QTreeWidgetItem* root = createChild(tag, type, id, description, parent, range, errors_, warnings_);
  tree_->insertTopLevelItem(0, root);

  addChildren(root, node, range.toInt());

  // tree_->sortByColumn(0);
  tree_->setFocus();

  emit gone(node);
  if (isFlowsheet(type.toStdString())) emit showPfd(node);
  else emit showPfd(-1);

  qDebug() << "Finish with "<< Q_FUNC_INFO;
} // WidgetTree::forceGo

void WidgetTree::colorize_(QTreeWidgetItem* item, int errors, int warnings) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // qDebug() << "colorize " << item->text(0) << " with " << errors << " errors, " << warnings << " warnings";
  if (errors > 0) {
#if (QT_VERSION >= 0x040200)
    item->setBackground(0, QBrush(Qt::red));
    item->setBackground(1, QBrush(Qt::red));
    item->setBackground(2, QBrush(Qt::red));
    item->setBackground(3, QBrush(Qt::red));
#else
    item->setBackgroundColor(0, Qt::red);
    item->setBackgroundColor(1, Qt::red);
    item->setBackgroundColor(2, Qt::red);
    item->setBackgroundColor(3, Qt::red);
#endif
  } else if (warnings > 0) {
#if (QT_VERSION >= 0x040200)
    item->setBackground(0, QBrush(Qt::yellow));
    item->setBackground(1, QBrush(Qt::yellow));
    item->setBackground(2, QBrush(Qt::yellow));
    item->setBackground(3, QBrush(Qt::yellow));
#else
    item->setBackgroundColor(0, Qt::yellow);
    item->setBackgroundColor(1, Qt::yellow);
    item->setBackgroundColor(2, Qt::yellow);
    item->setBackgroundColor(3, Qt::yellow);
#endif
  }
} // WidgetTree::colorize_

int WidgetTree::warnings(void) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return warnings_;
} // WidgetTree::warnings

int WidgetTree::errors(void) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return errors_;
} // WidgetTree::errors
  
void WidgetTree::go(int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (node == currentNode_) return;
  if (node == -1)
    node = rootNode_;
  forceGo(node);
} // WidgetTree::go

void WidgetTree::refresh(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  saveTreeState();
  forceGo(currentNode_);
  restoreTreeState();
} // WidgetTree::refresh

void WidgetTree::goUp(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int previousCurrentNode = currentNode_;
  if (tree_->topLevelItem(0) == NULL)
    return;
  int parentNode = tree_->topLevelItem(0)->text(4).toInt();
  go(parentNode);
  if (parentNode != previousCurrentNode)
    expandThis(tree_->topLevelItem(0), previousCurrentNode);
} // WidgetTree::goUp

bool WidgetTree::addChildren(QTreeWidgetItem* root, int node, int range) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString rootType = root->text(2);
  qDebug() << "Type  "<< rootType << ", node " << node << ", range " << range;

  bool chkChild=true;
  for (int id=node+1;id<node+range;++id) {

    QString tag("");
    QString type("");
    QString description("");
    QString parent("");
    QString range("");
    QString fulltag("");
    int errs(0);
    int warns(0);
    chkChild = getChildData(id, tag, type, description, parent, range, fulltag, errs, warns);
    qDebug() << "id "<< QVariant(id).toString() 
      << ", tag " << tag
      << ", type " << type
      << ", description " << description
      << ", parent " << parent;

    QTreeWidgetItem* child = createChild(tag, type, QVariant(id).toString(), description, parent, range, errs, warns);

    if (isStream(type.toStdString())) {
      parent += "s";
    } else if (isUnit(type.toStdString())) {
      parent += "u";
    }
    QList<QTreeWidgetItem *> itemlist = tree_->findItems(parent, Qt::MatchFixedString | Qt::MatchRecursive, 3);

    // id of the parent must exist once
    QTreeWidgetItem* parentItem;
    if (itemlist.count() == 1) {
      parentItem = itemlist.at(0);
      qDebug() << "parent: id " << parentItem->text(3)
        << ", tag " << parentItem->text(0)
        << ", type " << parentItem->text(2)
        << ", description " << parentItem->text(1)
        << ", parent " << parentItem->text(4);
      parentItem->addChild(child);
      leaves_++;
    }  // if (itemlist.count()==1)
    else if (itemlist.count()<=0) {
      qWarning() << "Really the item " << parent << " does not exist";
    } else if (itemlist.count()>1) {
      qWarning() << "There are too many items " << parent << ": database ERROR!";
      //parentItem = root;
    }
  } // for (int i=node;i<node+range;++i)
  return chkChild;
} // WidgetTree::addChildren

bool WidgetTree::getChildData(const int id, QString &tag, QString &type, QString &description, QString &parent, QString &range, QString &fulltag, int &errs, int &warns) {
  qDebug() << "Entering "<< Q_FUNC_INFO << " with id = " << id;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  if (id >= 0) {
    QSqlQuery query(db);
    QString queryString;
#if defined Q_OS_WIN
    queryString = "SELECT tag, type, description, parent, [range], fulltag FROM N WHERE id = " + QVariant(id).toString();
#else
    queryString = "SELECT tag, type, description, parent, range, fulltag FROM N WHERE id = " + QVariant(id).toString();
#endif
    qDebug() << "Query in WidgetTree::getChildData = " << queryString;
    query.exec(queryString);
    if (!query.isActive()) qCritical() << "Error while executing query in WidgetTree::getChildData: " << query.lastError().type() << query.lastError().text();
    if (!query.next()) return false;
    if (query.lastError().isValid()) qCritical() << "Error while nexting in WidgetTree::getChildData: " << query.lastError().type() << query.lastError().text();
    tag = query.value(0).toString().trimmed();
    type = query.value(1).toString().trimmed();
    description = query.value(2).toString().trimmed();
    parent = query.value(3).toString().trimmed();
    range = query.value(4).toString().trimmed();
    fulltag = query.value(5).toString().trimmed();
    getErrorsWarnings(id, errs, warns);
  } // if (id > 0)
  //qDebug() << "id "<<id<<", tag "<<tag<<", type "<<type<<", description "<<description<<", parent "<<QVariant(parent).toString();
  return true;
} // WidgetTree::getChildData

void WidgetTree::clickedOn(QTreeWidgetItem* item, int) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int i = item->text(3).toInt();
  go(i);
} // WidgetTree::clickedOn

void WidgetTree::setRootnode(int n) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  qWarning() << "Set root node = " << n;
  rootNode_ = n;
} // WidgetTree::setRootnode

int WidgetTree::rootNode(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  return rootNode_;
} // WidgetTree::rootnode

const QString &WidgetTree::completeTag(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  return completeTag_;
} // WidgetTree::completeTag

// saveTreeState and restoreTreeState code from:
// http://www.qtcentre.org/threads/13826-QTreeView-restore-Expanded-node-after-reload-model

void WidgetTree::saveTreeState(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  states_.clear();
  QTreeWidgetItemIterator it(tree_);
  while (*it) {
    states_.push_back(std::pair<bool,bool>((*it)->isExpanded(), (*it)->isSelected()));
    ++it;
  } // while there are leaves
} // WidgetTree::saveTreeState

void WidgetTree::restoreTreeState(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QTreeWidgetItemIterator it(tree_);
  while (*it && !states_.empty()) {
    (*it)->setExpanded(states_.front().first);
    (*it)->setSelected(states_.front().second);
    states_.pop_front();
    ++it;
  } // while there are states left
} // WidgetTree::restoreTreeState
