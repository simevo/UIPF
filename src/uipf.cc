/** @file uipf.cc
    @brief Main function for UIPF

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QMessageBox>
#include <QApplication>
#include <QSqlDatabase>
#include <QDir>
#include <QSqlError>
#include <QDebug>
#include <QTranslator>
#include <QSqlQuery>

#include "persistency.h"
#include "Settings.h"
#include "WindowMain.h"
#ifdef Q_OS_WIN
#include "exclusion.h"
#endif

// @param[in] homePath home path
bool createConnection(QString hp) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db;

  QStringList list = QSqlDatabase::drivers();
  for (int i = 0; i < list.size(); i++)
    qCritical() << list.at(i).toLatin1().data();
#if defined(Q_OS_MAC) || defined(SQLITE)
  db = QSqlDatabase::addDatabase("QSQLITE", "persistency");
  db.setDatabaseName(hp + "/persistency.db");

  qCritical() << "now attempting to open file " << hp + "/persistency.db";
#elif defined Q_OS_WIN
  db = QSqlDatabase::addDatabase("QODBC", "persistency");
  db.setDatabaseName("persistency");
#else
  // Postgres
  db = QSqlDatabase::addDatabase("QPSQL", "persistency");
  db.setUserName("libpf");
  db.setPassword("libpf");
  db.setDatabaseName("persistency");

  // MySQL
  //db = QSqlDatabase::addDatabase("QMYSQL", "persistency");
  //db.setUserName("libpf");
  //db.setDatabaseName("libpf");
#endif

  if (!db.open()) {
    QMessageBox::warning(0, QObject::tr("Database Error"),
                         db.lastError().text());
    return false;
  }
#if defined(Q_OS_MAC) || defined(SQLITE)
  db.exec("PRAGMA full_column_names=1;");
  db.exec("PRAGMA short_column_names=0;");
#endif

  // qDebug() << db.driverName() << (db.driver()->hasFeature(QSqlDriver::LowPrecisionNumbers) ? " allows" : " does not allow") << " fetching numerical values with low precision.";
  return true;
} // createConnection

/// provide full path to files
void copyFile(QString source, QString destination) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // QMessageBox::information(0, "copyFile", "Now copying " + source + " to " + destination);
  QFile file(source);
  if (!file.copy(destination)) {
    qCritical() << "Error copying " << source << " to " << destination << ": " << file.errorString();
    // QMessageBox::warning(0, "Error copying " + source + " to " + destination, file.errorString());
  }
} // copyFile

/// paths should not be slash-terminated
void copyDir(QString sourcedir, QString destdir) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QDir dir;
  dir.mkpath(QDir::toNativeSeparators(destdir));
  dir.setPath(QDir::toNativeSeparators(sourcedir));
  qCritical() << "Copying " << sourcedir << " to " << destdir;
  // QMessageBox::information(0, "copyDir", "Now copying " + sourcedir + " to " + destdir);
  foreach(QString filename, dir.entryList(QDir::Files)) {
    QString source = QDir::toNativeSeparators(sourcedir + "/" + filename);
    QString destination = QDir::toNativeSeparators(destdir + "/" + filename);
    copyFile(source, destination);
  }
} // copyDir

/// paths should not be slash-terminated
void listDir(QString thedir) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QDir dir(QDir::toNativeSeparators(thedir));
  // qDebug() << "Listing " << thedir << " contents";
  QString contents;
  foreach(QString filename, dir.entryList(QDir::Files)) {
    contents += filename + "\n";
  }
  // QMessageBox::information(0, "listDir", "Listing " + thedir + " contents:\n" + contents);
} // listDir

int main(int argc, char* argv []) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  Q_INIT_RESOURCE(uipf);

#ifdef Q_OS_WIN
  if (testUniqueness())
    return 1;
#endif

  QApplication app(argc, argv);

  QTranslator appTranslator;
  QTranslator qtTranslator;

  QString locale(""), homePath(""), home("");
  bool child;
  {
    Settings mySettings;

    locale = mySettings.value("locale", "").toString();

    homePath = mySettings.homePath();
    home = mySettings.home();
    child = mySettings.child();

    // QMessageBox::information(0, "setting", "homePath = " + homePath);
    // QMessageBox::information(0, "setting", "home = " + home);
  }

  QDir dir;
  if (!QDir().exists(homePath)) {
    copyDir(home + "/LIBPF 1.0", homePath);
    copyDir(home + "/LIBPF 1.0/META-INF", homePath + "/META-INF");
  } else {
    listDir(homePath);
  } // copy files to LIBPF application directory

  if (argc == 2) {
    qCritical() << "Assuming " << argv[1] << " as locale";
    locale = argv[1];
  } else if (locale.isEmpty()) {
    locale = QLocale::system().name();
    // qDebug() << "QLocale::system().name() = " << locale;
  }
  
  bool appLoaded(false);
#if defined(Q_OS_MAC)
  appLoaded = appTranslator.load(home + "/uipf_" + locale);
#elif defined(Q_OS_UNIX)
  appLoaded = appTranslator.load("/usr/share/libpf-ui/uipf_" + locale);
#else
  appLoaded = appTranslator.load("uipf_" + locale);
#endif
  if (!appLoaded)
    qCritical() << "Error loading translation uipf_" << locale;
  
  qtTranslator.load("qt_" + locale);
  app.installTranslator(&appTranslator);
  app.installTranslator(&qtTranslator);
  if ((locale.left(2) == "he") || (locale.left(2) == "ar"))
    QApplication::setLayoutDirection(Qt::RightToLeft);
  if (!createConnection(homePath))
    return 1;
  //TreeWindow *treewin = new TreeWindow;
  //treewin->show();
  //return app.exec();

  //QMessageBox::information(0, "setting", "locale = " + locale);

  WindowMain* mainw = new WindowMain();
  mainw->setChild(child);
  mainw->show();
  int rc = app.exec();
  QSqlDatabase::removeDatabase("persistency");

  return rc;

  //TableWindow window;
  //// window.resize(600, 500);
  //window.show();
  //return app.exec();
} // main
