/** @file Dimension.cc
    @brief Contains the implementations of the Dimension class.

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2004-2021 Paolo Greppi simevo s.r.l.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <cstdio>
#include <iostream>
#include <string>
#include <QDebug>

#include "Dimension.h"
#include "UtfConverter.h"

// #define VERBOSE

Dimension::Dimension(std::string desc, std::string SIu, int kmol, int rad, int A, int m, int cd, int kg, int K, int s, int eur)
  : unit_(kmol, rad, A, m, cd, kg, K, s, eur), description_(desc), SIunit_("undefined"), ENunits_("undefined"), Engunits_("undefined") {

  //qDebug() << "Entering "<< Q_FUNC_INFO;
#ifdef VERBOSE
  std::cout << "Dimension::Dimension * Adding Dimension " << desc << std::endl;
#endif
  addUnit(UnitsSI, SIu, 0.0, 1.0);
} // Dimension::Dimension

Dimension::Dimension(const Dimension &source)
  : unit_(source.unit_), description_(source.description_), SIunit_(source.SIunit_) {} // Dimension::Dimension

void Dimension::addUnit(Units u, std::string unit, double factor, double offset) {
//  qDebug() << "Entering "<< Q_FUNC_INFO;
#ifdef VERBOSE
  std::cout << "Dimension::addUnit * Adding unit <" << unit << ">" << std::endl;
#endif
  //   trim(unit);
  i_ = units_.find(unit);
  if (i_ != units_.end())
    qCritical() << "Duplicate unit" << unit.c_str();
  units_[unit] = std::pair<double, double>(offset, factor);
  if ((u & UnitsSI) == u) {
#ifdef VERBOSE
    std::cout << "Dimension::addUnit * unit <" << unit << ">" << " is SI" << std::endl;
#endif
    SIunit_ = unit;
  }
  if ((u & UnitsEN) == u) {
    ENunits_ = unit;
#ifdef VERBOSE
    std::cout << "Dimension::addUnit * unit <" << unit << ">" << " is EN" << std::endl;
#endif
  }
  if ((u & UnitsEng) == u) {
    Engunits_ = unit;
#ifdef VERBOSE
    std::cout << "Dimension::addUnit * unit <" << unit << ">" << " is Eng" << std::endl;
#endif
  }
} // Dimension::addUnit

const std::string &Dimension::description(void) const {
  return description_;
} // Dimension::description

const std::string &Dimension::unit(Units u) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if ((u & UnitsSI) == u) {
    return SIunit_;
  } else if ((u & UnitsEN) == u) {
    return ENunits_;
  } else if ((u & UnitsEng) == u) {
    return Engunits_;
  } else {
    return SIunit_;
  }
} // Dimension::unit

const UnitArray &Dimension::unitArray(void) const {
  return unit_;
} // Dimension::unit

Dimension::conversion_t::const_iterator Dimension::begin(void) const {
  return (i_ = units_.begin());
} // Dimension::begin

Dimension::conversion_t::const_iterator Dimension::next(void) const {
  return ++i_;
} // Dimension::next

Dimension::conversion_t::const_iterator Dimension::end(void) const {
  return units_.end();
} // Dimension::end

/// convert v from SI to destinationUnit
/// i.e. fromSI(1, "kW") = 0.001
/// i.e. fromSI(100, "C") = -173.15
double Dimension::fromSI(double v, std::string destinationUnit) const {
  const std::pair<double, double> &fo = findUnit(destinationUnit);
  return (v - fo.first) / fo.second;
} // Dimension::fromSI

/// convert v source_unit to SI
/// i.e. toSI(1, "kW") = 1000.0
/// i.e. toSI(100, "C") = 373.15
double Dimension::toSI(double v, std::string sourceUnit) const {
  const std::pair<double, double> &fo = findUnit(sourceUnit);
  return v * fo.second + fo.first;
} // Dimension::toSI

const std::pair<double, double> &Dimension::findUnit(std::string unit) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  //   trim(unit);
  static std::pair<double, double> nullPair(0.0, 0.0);
  i_ = units_.find(unit);
  if (i_ == units_.end()) {
    qCritical() << "Unit not found: [" << unit.c_str() << "]";
    return nullPair;
  } else {
    return i_->second;
  }
} // Dimension::findUnit

void Dimension::printUnits(void) const {
  std::map<std::string, std::pair<double, double> >::const_iterator p;

  std::cout << "  { ";
  for (p = units_.begin(); p != units_.end(); ++p)
    std::cout << "\"" << p->first << "\": [" << p->second.first << ", " << p->second.second << "], ";
  std::cout << "}" << std::endl;
} // Dimension::printUnits

int Dimension::size(void) const {
  return units_.size();
} // Dimension::size
