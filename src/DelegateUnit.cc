/** @file DelegateUnit.cc
    @brief Implementation of the DelegateUnit class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "DelegateUnit.h"
#include <QDebug>

DelegateUnit::DelegateUnit(QList<int> valColumns, QWidget* parent)
  : QStyledItemDelegate(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  editorModel = new QStringList;
  valueColumns = valColumns;
}

DelegateUnit::~DelegateUnit() {}

QWidget *DelegateUnit::createEditor(QWidget *parent,
                                    const QStyleOptionViewItem & /* option */,
                                    const QModelIndex & /* index */) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QComboBox *editor = new QComboBox(parent);
  connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChngd(int)));
  editor->addItems(*editorModel);
  return editor;
}

void DelegateUnit::setEditorData(QWidget *editor,
                                 const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString value = index.model()->data(index, Qt::EditRole).toString();
  QComboBox *comboBox = qobject_cast<QComboBox*>(editor);

  QStringList units;
  const Dimension *myDimension_ = unitEngine_.pdimension(std::string(value.toUtf8()));
  if (myDimension_ != NULL) {
    for (Dimension::conversion_t::const_iterator i = myDimension_->begin(); i != myDimension_->end(); ++i)
      units.append(QString::fromUtf8(i->first.c_str()));
    comboBox->addItems(units);
    value = index.model()->data(index, Qt::DisplayRole).toString();
    comboBox->setCurrentIndex(comboBox->findText(value));
  } else {
    qCritical() << value.toUtf8() << " has no associated dimension !";
    comboBox->addItem(value);
    comboBox->setCurrentIndex(comboBox->findText(value));
  }
}

void DelegateUnit::setModelData(QWidget *editor, QAbstractItemModel *model,
                                const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QComboBox *comboBox = qobject_cast<QComboBox*>(editor);
  QString value = model->data(index, Qt::EditRole).toString();

  for (int indx = 0; indx < valueColumns.size(); ++indx) {
    QModelIndex sibIndex = index.sibling(index.row(), valueColumns.at(indx));
    double siUnit = model->data(sibIndex, Qt::EditRole).toDouble();
    //qDebug() << "DelegateUnit::setModelData" << getNewUnit(siUnit,comboBox->currentText());
    model->setData(sibIndex, getNewUnit(siUnit, comboBox->currentText()), Qt::DisplayRole);
  }
  model->setData(index, comboBox->currentText(), Qt::DisplayRole);
}

// Return new display value from changing of Uom. siUnit is original si value and
// newUnit is the new Uom.
QString DelegateUnit::getNewUnit(double siUnit, QString newUnit) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  double newDisplay = unitEngine_.fromSI(siUnit, std::string(newUnit.toUtf8()));
  return QString::number(newDisplay);
}

void DelegateUnit::updateEditorGeometry(QWidget *editor,
                                        const QStyleOptionViewItem &option, const QModelIndex & /* index */) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  editor->setGeometry(option.rect);
}

void DelegateUnit::currentIndexChngd(int index) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  emit currentIndexChanged(index, sender());
}

void DelegateUnit::setCBModel(QStringList *model) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  editorModel = model;
}
