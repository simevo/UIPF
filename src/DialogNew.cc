/** @file DialogNew.cc
    @brief Implementation for the DialogNew class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QRadioButton>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QGroupBox>
#include <QDebug>

#include "DialogNew.h"
#include "LineEditValidated.h"

void DialogNew::addType(const char* type, const char* description) {
  qDebug() << "Entering "<< Q_FUNC_INFO << " with type = " << type;
  sx_.push_back(new QRadioButton(type));
  dx_.push_back(new QLabel(description));
  types_->addWidget(sx_.back(), typesCount_, 0);
  types_->addWidget(dx_.back(), typesCount_, 1);
  dx_.back()->setBuddy(sx_.back());
  (*sx_.begin())->setChecked(true);
  typesCount_++;
} // DialogNew::addType

void DialogNew::addOption(const char* name, const char* description) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  options_->addWidget(new QLabel(name), optionsCount_, 0);
  options_->addWidget(new QLabel(description), optionsCount_, 1);
  optionsCount_++;
} // DialogNew::addOption

int DialogNew::typesCount(void) {
  return typesCount_;
} // DialogNew::typesCount

void DialogNew::lockType(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  (*sx_.begin())->setEnabled(false);
} // DialogNew::lockType

DialogNew::DialogNew(QWidget* parent) : QDialog(parent), typesCount_(0), optionsCount_(0) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  tagLabel_ = new QLabel(tr("Tag:"));
  tagEdit_ = new LineEditValidated(this, tagLabel_, ",-_{ }<>");
  tagLabel_->setBuddy(tagEdit_);
  descriptionLabel_ = new QLabel(tr("Description:"));
  descriptionEdit_ = new LineEditValidated(this, descriptionLabel_, ".,:-_{ }<>[]");
  descriptionLabel_->setBuddy(descriptionEdit_);

  bottom_ = new QHBoxLayout;

  typesGroup_ = new QGroupBox(tr("types"));
  types_ = new QGridLayout;
  typesGroup_->setLayout(types_);

  // optionsGroup_ = new QGroupBox(tr("options"));
  // options_ = new QGridLayout;
  // optionsGroup_->setLayout(options_);
  // addOption("aaaaaa", "aaaaaaaaaaaaaaaaaa");
  // addOption("bbbbbb", "bbbbbbbbbbbbbbbbbb");
  // addOption("cccccc", "cccccccccccccccccc");
  // addOption("dddddd", "dddddddddddddddddd");

  titles_ = new QHBoxLayout;
  titles_->addWidget(tagLabel_);
  titles_->addWidget(tagEdit_);
  titles_->addWidget(descriptionLabel_);
  titles_->addWidget(descriptionEdit_);

  top_ = new QVBoxLayout;
  top_->addLayout(titles_);
  top_->addWidget(typesGroup_);
  // top_->addWidget(optionsGroup_);

  all_ = new QVBoxLayout;
  all_->addLayout(top_);
  all_->addLayout(bottom_);

  setLayout(all_);

  ok_ = new QPushButton(tr("Ok"));
  cancel_ = new QPushButton(tr("Cancel"));

  bottom_->addWidget(ok_);
  bottom_->addWidget(cancel_);
  bottom_->addStretch();

  connect(ok_, SIGNAL(clicked()), this, SLOT(okClicked()));
  connect(cancel_, SIGNAL(clicked()), this, SLOT(close()));
  setWindowTitle(tr("New case"));
} // DialogNew::DialogNew

void DialogNew::okClicked(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString type;
  for (std::list<QRadioButton *>::const_iterator i = sx_.begin(); i != sx_.end(); ++i) {
    if ((*i)->isChecked()) {
      type = (*i)->text();
      break;
    }
  }
  open(type, tagEdit_->text(), descriptionEdit_->text());

  close();
} // DialogNew::okClicked
