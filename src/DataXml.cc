/** @file DataXml.cc
    @brief Implementation for the DataXml class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Dmytro Skrypnyk.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QFile>
#include <QDebug>
#include <QStringList>

#include "DataXml.h"
#include "strtopath.h"

DataXml::DataXml(void) { } // DataXml::DataXml

bool DataXml::openXml(const QString &filename) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    qCritical("Failed to open xml-file for reading.");
    return false;
  }
  if (!document_.setContent(&file)) {
    qCritical("Failed to parse the xml-file into a DOM tree.");
    file.close();
    return false;
  }
  file.close();
  return true;
} // DataXml::openXml

void DataXml::read(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QDomNodeList nodeList = document_.elementsByTagName("a");
  // qDebug() << "size=" << nodeList.size();
  
  for (int i = 0; i < nodeList.size(); ++i) {
    QDomNode a = nodeList.at(i);
    QDomNamedNodeMap attr = a.attributes();

    QString classname("");
    QDomElement path = a.firstChildElement("path");
    if (!path.isNull()) {
      classname = "edge";
    } else {
      QDomElement image = a.firstChildElement("image");
      if (!image.isNull()) {
        classname = "node";
      }
    }
    
    if (classname != "") {
      // remove trailing .html before converting to int
      QString href = attr.namedItem("xlink:href").nodeValue();
      href.chop(5);
      int hrefInt = href.toInt();
      
      QString id = attr.namedItem("id").nodeValue();

      hrefMap_[id].href = hrefInt;
      hrefMap_[id].classname = classname;
      QDomElement path = a.firstChildElement("path");
      if (!path.isNull()) {
        QString pathStr = path.attribute("d");
        // qDebug() << "pathStr=" << pathStr;
        hrefMap_[id].path = strToPath(pathStr);
      }
      
      // qDebug() << "classname = " << classname << " href = " << hrefInt << " id = " << id;
    } // edge or node anchor
  }
} // DataXml::read

void DataXml::clear(void) {
  hrefMap_.clear();
} // DataXml::clear

int DataXml::href(const QString & id) const {
  return hrefMap_[id].href;
} // DataXml::href

QPainterPath DataXml::path(const QString & id) const {
  return hrefMap_[id].path;
} // DataXml::path

QString DataXml::classname(const QString & id) const {
  return hrefMap_[id].classname;
} // DataXml::classname

QStringList DataXml::elementList(void) const {
  return hrefMap_.keys();
} // DataXml::elementList
