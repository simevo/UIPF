/** @file LineEditValidated.cc
    @brief Implementation for the LineEditValidated class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Harish Surana, Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QToolTip>
#include <QDebug>

#include "LineEditValidated.h"
#include "ValidatorText.h"

LineEditValidated::LineEditValidated(QWidget* parent, QLabel* label, QString acceptedChars)
  : QLineEdit(parent) {

  qDebug() << "Entering "<< Q_FUNC_INFO;
  setValidator(new ValidatorText(this, label, acceptedChars));

  connect(validator(), SIGNAL(hideToolTip()), SLOT(onHideToolTip()));
  connect(validator(), SIGNAL(showToolTip(const QString &)), SLOT(onShowToolTip(const QString &)));

  connect(this, SIGNAL(cursorPositionChanged(int, int)), SLOT(onHideToolTip()));
  connect(this, SIGNAL(selectionChanged()), SLOT(onHideToolTip()));
  connect(this, SIGNAL(textChanged(const QString &)), SLOT(onTextChanged()));
} // LineEditValidated::LineEditValidated

LineEditValidated::~LineEditValidated(void) { } // LineEditValidated::~LineEditValidated

void LineEditValidated::onShowToolTip(const QString & toolTip) {
  QToolTip::showText(mapToGlobal(QPoint(0, 0)), toolTip, this);
} // LineEditValidated::onShowToolTip

void LineEditValidated::onHideToolTip(void) {
  QToolTip::hideText();
} // LineEditValidated::onHideToolTip

void LineEditValidated::onTextChanged(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QPalette p = palette();
  if (!hasAcceptableInput()) {
    p.setColor(QPalette::Base, QColor(255, 0, 0));    //red color
    setPalette(p);
  } else {
    p.setColor(QPalette::Base, QColor(255, 255, 255));    //white color
    setPalette(p);
  }
} // LineEditValidated::onTextChanged
