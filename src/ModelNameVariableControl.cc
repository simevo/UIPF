/** @file ModelNameVariableControl.cc
    @brief Implementation for the ModelNameVariableControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QSqlTableModel>
#include <QSqlQuery>
#if (QT_VERSION >= 0x040600)
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#endif

#include <QDebug>


#include "ModelNameVariableControl.h"
#include "persistency.h"

/// Constructor.
/// Constructs a QStringListModel of control variable names.
/// Variables that are already in the control variable data table
/// are disabled.
/// @param currentVal the current value of the ComboBox.
/// @param dbModel the database model from which to get data.
/// @param dbNameCol the column in dbmodel that contains the variable names.
/// @param tblModel a ControlDataModel instance.
/// @param parent parent object.
ModelNameVariableControl::ModelNameVariableControl(const QString & currentVal, const QSqlQueryModel* dbModel, int dbNameCol, const QAbstractItemModel* tblModel, QObject* parent) :
  QStringListModel(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int tRows = tblModel->rowCount();
  int rows = dbModel->rowCount();

  QStringList cvNameList;

  for (int r = 0; r < rows; ++r) {
    QModelIndex srcIdx = dbModel->index(r, dbNameCol);
    QString dbVal = dbModel->data(srcIdx).toString();
    cvNameList.append(dbVal);

    int tr;
    for (tr = 0; tr < tRows; ++tr) {
      QModelIndex tblidx = tblModel->index(tr, FULLTAG_COL);
      QString tblVal = tblModel->data(tblidx).toString();
      if (tblVal == dbVal && tblVal != currentVal)
        break;
    }
    if (tr < tRows) {
      // already in table
      enabled_.append(false);
    } else {
      enabled_.append(true);
    }
  }
  setStringList(cvNameList);
} // ModelNameVariableControl::ModelNameVariableControl

/// Reimplements QStringListModel::flags().
Qt::ItemFlags ModelNameVariableControl::flags(const QModelIndex & index) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!enabled_[index.row()])
    return QStringListModel::flags(index) & ~(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
  else
    return QStringListModel::flags(index);
} // ModelNameVariableControl::flags
