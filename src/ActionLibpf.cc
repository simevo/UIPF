/** @file ActionLibpf.cc
    @brief Implementation for the ActionLibpf class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <cassert> // for assert
#include <cfloat> // for FLT_MAX

#include <QSqlQuery>
#include <QDebug>
#include <QtDebug>
#include <QProcess>
#include <QSqlError> // for QSqlQuery::lastError
#include <QItemDelegate>

#include "ActionLibpf.h"
#include "itoa.h"

ActionLibpf::ActionLibpf(void) : kernelProcess_(NULL) {
  settings_ = new Settings();
} // ActionLibpf::ActionLibpf

ActionLibpf::~ActionLibpf(void) {
  delete settings_;
} // ActionLibpf::~ActionLibpf

void ActionLibpf::compute(int r) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  assert(r >= 0);
  assert(r < controlledVariable()->points());

  emit computed(r, executing);

  double cv = resultsModel()->index(r, 1).data().toDouble();
  qWarning() << "set input [" << controlledVariable()->tag().c_str() << "," << controlledVariable()->id() << "] =" << cv;
  setValue(controlledVariable()->id(), cv);

  double cv_check = getValue(controlledVariable()->id());
  qWarning() << "read input [" << controlledVariable()->tag().c_str() << "," << controlledVariable()->id() << "] =" << cv_check;

  kernelProcess_ = new QProcess;
  kernelProcess_->setWorkingDirectory(settings_->homePath());

  QString kernel = settings_->fullKernelName();
  QStringList args;
  args << "calculate" << itoa(caseId()).c_str();
  qCritical() << "starting kernel calculate" << args;
  kernelProcess_->start(kernel, args);
  // attempt to run kernel for 60 s
  if (!kernelProcess_->waitForFinished(60000)) {
    // process did not finish execution within 60 s
    emit computed(r, doneNotFinished);
    return;
  } else {
    QProcess::ExitStatus exitStatus = kernelProcess_->exitStatus();
    if (exitStatus == QProcess::CrashExit) {
      // process crashed
      emit computed(r, doneCrash);
      return;
    } else {
      int exitCode;
#if defined(Q_OS_MAC) || defined(Q_OS_LINUX)
      FILE *idfile(NULL);
      QString idfilepath = settings_->homePath();
      idfilepath += "/id.txt";
      idfile = fopen(idfilepath.toStdString().c_str(), "rt");
      int rt = fscanf(idfile, "%d\n", &exitCode);
      if ((rt == EOF) || (rt != 1))
        exitCode = -10;
      fclose(idfile);
#else
      exitCode = kernelProcess_->exitCode();
#endif
      qCritical() << "The process has exited with return code" << exitCode;
      if (exitCode < 0) {
        // process finished with severe error
        emit computed(r, doneSevereError);
        return;
      } else {
        for (int c = 2; c < resultsModel()->columnCount(); ++c) {
          double mv = getValue(monitorModel()->index(c - 2, 5).data().toInt());
          qWarning() << "read output [" << monitorModel()->index(c - 2, 1).data().toString() << "," << monitorModel()->index(c - 2, 5).data().toInt() << "] =" << mv;
          resultsModel()->setData(resultsModel()->index(r, c), mv);
        }
        int errors, warnings;
        getErrorsWarnings(caseId(), errors, warnings);
        if (errors > 0)
          emit computed(r, doneErrors);
        else if (warnings > 0)
          emit computed(r, doneWarnings);
        else
          emit computed(r, doneOk);
      } // exitCode
    } // exitStatus
  } // waitForFinished
} // ActionLibpf::compute

void ActionLibpf::kill(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (kernelProcess_ != NULL)
    kernelProcess_->kill();
  // kernelProcess_->waitForFinished(1000);
  // delete kernelProcess_;
} // ActionLibpf::kill

bool ActionLibpf::setValue(int dbid, double value) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);
#if defined(Q_OS_WIN) && (!defined(SQLITE))
  query.prepare("update Q set [value] = ? where id = ?");
#else
  query.prepare("update Q set value = ? where id = ?");
#endif
  query.addBindValue(value);
  query.addBindValue(dbid);

  bool test = query.exec();

  if (query.lastError().isValid())
    qCritical() << "Error while executing query " << query.executedQuery() << " in "<< Q_FUNC_INFO << ": " << query.lastError().type() << query.lastError().text();
  if (!test)
    qCritical() << "Unknown error in " << Q_FUNC_INFO << ": update Q set value = " << value << " where id = " << dbid;
  return test;
} // ActionLibpf::setValue

double ActionLibpf::getValue(int dbid) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);
#if defined(Q_OS_WIN) && (!defined(SQLITE))
  query.prepare("select [value] from Q where nid = ?");
#else
  query.prepare("select value from Q where nid = ?");
#endif
  query.addBindValue(dbid);

  bool test = query.exec();

  if (query.lastError().isValid())
    qCritical() << "Error while executing query " << query.executedQuery() << " in "<< Q_FUNC_INFO << ": " << query.lastError().type() << query.lastError().text();
  if (!test)
    qCritical() << "Unknown error in " << Q_FUNC_INFO << ": select value from Q where id =" << dbid;
  double value;
  if (query.next()) {
    value = query.value(0).toDouble();
  } else {
    value = FLT_MAX;
  }
  return value;
} // ActionLibpf::getValue

void getErrorsWarnings(int id, int &errors, int &warnings) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QString querystring;
  {
    QSqlQuery query(db);
    querystring = "select count(*) from svv inner join sv on sv.id = svv.vid where sv.nid=? and sv.tag=\'errors\'";
    query.prepare(querystring);
    query.addBindValue(id);
    query.exec();
    if (!query.isActive())
      qCritical() << "Error while executing query in ActionLibpf::getErrorsWarnings: " << query.lastError().type() << query.lastError().text();
    if (!query.next()) {
      errors = 0;
    } else {
      if (query.lastError().isValid()) {
        qCritical() << "Error while nexting query in ActionLibpf::getErrorsWarnings: " << query.lastError().type() << query.lastError().text();
        qCritical() << "querystring = " << querystring;
      }
      errors = query.value(0).toInt();
    }
  }
  {
    QSqlQuery query(db);
    querystring = "select count(*) from svv inner join sv on sv.id = svv.vid where sv.nid=? and sv.tag=\'warnings\'";
    query.prepare(querystring);
    query.addBindValue(id);
    query.exec();
    if (!query.isActive())
      qCritical() << "Error while executing query in ActionLibpf::getErrorsWarnings: " << query.lastError().type() << query.lastError().text();
    if (!query.next()) {
      errors = 0;
    } else {
      if (query.lastError().isValid()) {
        qCritical() << "Error while nexting query in ActionLibpf::getErrorsWarnings: " << query.lastError().type() << query.lastError().text();
        qCritical() << "querystring = " << querystring;
      }
      warnings = query.value(0).toInt();
    }
  }
} // getErrorsWarnings
