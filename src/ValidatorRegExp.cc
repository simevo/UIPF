/** @file ValidatorRegExp.cc
    @brief Implementation for the ValidatorRegExp class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#include "ValidatorRegExp.h"
#include <QToolTip>
#include <QLabel>

ValidatorRegExp::ValidatorRegExp(QLabel *label, const QRegExp & rx, QObject *parent) :
  QRegExpValidator(rx, parent),
  label_(label)
{} // ValidatorRegExp::ValidatorRegExp

QValidator::State ValidatorRegExp::validate(QString &input, int &pos) const {
  QToolTip::hideText();

  QValidator::State r = QRegExpValidator::validate(input, pos);
  if (r == QValidator::Invalid) {
    QWidget* widget = label_->buddy();
    QPoint positionToolTip;
    if (!widget) {
      positionToolTip = label_->mapToGlobal(QPoint(0, 0));
      widget = label_;
    } else {
      positionToolTip = widget->mapToGlobal(QPoint(0, 0));
    }
    QToolTip::showText(positionToolTip, QString(tr("The character '%1' is not allowed in the '%2' field.")).arg(input.at(pos - 1)).arg(label_->text().trimmed().remove(QRegExp(":\\s*$"))), widget);
  }
  return r;
} // ValidatorRegExp::validate
