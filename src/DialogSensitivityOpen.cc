/** @file DialogSensitivityOpen.h
    @brief Implementation for the DialogSensitivityOpen class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#include "DialogSensitivityOpen.h"
#include "ModelSensitivityList.h"
#include "DelegateRadioButton.h"
#include "Settings.h"
#include <QGridLayout>
#include <QTableView>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QPushButton>
#include <QHeaderView>
#include <QItemSelection>
#include <QMessageBox>
#include <QTimer>
#include <QDebug>

DialogSensitivityOpen::DialogSensitivityOpen(const QString &currentType, QWidget *parent) :
  QDialog(parent),
  modelSensitivity_(new ModelSensitivityList(this)),
  settings_(new Settings()) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // If directory couldnt be loaded
  if (!modelSensitivity_->setSensitivityDirectory(settings_->homePath())) {
    QMessageBox::critical(this, tr("Error"), QString(tr("The directory '%1' couldn't be opened for reading.")).arg(settings_->homePath()));
    QTimer::singleShot(0, this, SLOT(reject()));
    return;
  }
  modelSensitivity_->setHighlightedTypeName(currentType);

  // Setup our dialog
  setWindowModality(Qt::WindowModal);
  setWindowTitle(tr("Open existing multi-dimensional sensitivity study"));
  resize(600, 400);

  // Setup the grid layout
  gridLayout_ = new QGridLayout(this);

  // Setup our table view
  viewSensitivity_ = new QTableView(this);
  viewSensitivityHeader_ = viewSensitivity_->horizontalHeader();
#if (QT_VERSION >= 0x050000)
  viewSensitivityHeader_->setSectionResizeMode(QHeaderView::Interactive);
#else
  viewSensitivityHeader_->setResizeMode(QHeaderView::Interactive);
#endif
  viewSensitivityHeader_->setStretchLastSection(true);

  viewSensitivity_->setModel(modelSensitivity_);
  viewSensitivity_->setSortingEnabled(true);
  viewSensitivity_->sortByColumn(ModelSensitivityList::COL_TYPE, Qt::AscendingOrder);
  viewSensitivity_->setSelectionBehavior(QAbstractItemView::SelectRows);
  viewSensitivity_->setSelectionMode(QAbstractItemView::SingleSelection);
  viewSensitivity_->verticalHeader()->hide();
  viewSensitivity_->setItemDelegateForColumn(0, new DelegateRadioButton(viewSensitivity_));
  QTimer::singleShot(0, this, SLOT(resizeViewSections()));

  gridLayout_->addWidget(viewSensitivity_, 0, 0, 1, 1);
  // Setup the horizontal layout box for the buttons
  horizontalLayout_ = new QHBoxLayout();
  buttonCancel_ = new QPushButton(this);
  buttonCancel_->setText(tr("Cancel"));
  buttonOK_ = new QPushButton(this);
  buttonOK_->setText(tr("OK"));
  buttonOK_->setEnabled(false);
  buttonOK_->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
  buttonOK_->setIcon(QIcon(":/images/edit-redo.png"));
  buttonOK_->setFixedHeight(buttonCancel_->height() - 3);
  horizontalSpacer_ = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  horizontalLayout_->addWidget(buttonOK_);
  horizontalLayout_->addWidget(buttonCancel_);
  horizontalLayout_->addItem(horizontalSpacer_);
  gridLayout_->addLayout(horizontalLayout_, 3, 0, 1, 1);

  connect(buttonOK_, SIGNAL(clicked()), SLOT(accept()));
  connect(buttonCancel_, SIGNAL(clicked()), SLOT(reject()));
  connect(viewSensitivity_->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), SLOT(selectionChanged(QItemSelection,QItemSelection)));
  connect(viewSensitivity_, SIGNAL(doubleClicked(QModelIndex)), SLOT(tableDoubleClicked(QModelIndex)));
} // DialogSensitivityOpen::DialogSensitivityOpen

DialogSensitivityOpen::~DialogSensitivityOpen() {
  delete settings_;
} // DialogSensitivityOpen::~DialogSensitivityOpen

const QString& DialogSensitivityOpen::sensitivitiyFileName() const {
  return sensitivityFileName_;
} // DialogSensitivityOpen::sensitivitiyFileName

const QString& DialogSensitivityOpen::sensitivityName() const {
  return sensitivityName_;
} // DialogSensitivityOpen::sensitivityName

const QString& DialogSensitivityOpen::sensitivityType() const {
  return sensitivityType_;
} // DialogSensitivityOpen::sensitivityType

void DialogSensitivityOpen::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // Unmark deselected row
  if (deselected.indexes().size()) {
    QModelIndex deselectedIndex = deselected.indexes().at(0);
    if (deselectedIndex.isValid())
      modelSensitivity_->setData(modelSensitivity_->index(deselectedIndex.row(), 0), false);
  }
  // Mark selected row
  if (!selected.indexes().size()) {
    sensitivityFileName_.clear();
    buttonOK_->setEnabled(false);
    return;
  }
  QModelIndex selectedIndex = selected.indexes().at(0);
  if (!selectedIndex.isValid()) {
    buttonOK_->setEnabled(false);
    sensitivityFileName_.clear();
    return;
  }
  modelSensitivity_->setData(modelSensitivity_->index(selectedIndex.row(), ModelSensitivityList::COL_RADIO), true);
  sensitivityFileName_ = modelSensitivity_->data(modelSensitivity_->index(selectedIndex.row(), ModelSensitivityList::COL_NAME), Qt::UserRole + 1).toString();
  sensitivityName_ = modelSensitivity_->data(modelSensitivity_->index(selectedIndex.row(), ModelSensitivityList::COL_NAME), Qt::DisplayRole).toString();
  sensitivityType_ = modelSensitivity_->data(modelSensitivity_->index(selectedIndex.row(), ModelSensitivityList::COL_TYPE), Qt::DisplayRole).toString();
  buttonOK_->setEnabled(true);
} // DialogSensitivityOpen::selectionChanged

void DialogSensitivityOpen::resizeViewSections() {
  viewSensitivityHeader_->resizeSection(0, 30);
  viewSensitivityHeader_->resizeSection(1, 200);
} // DialogSensitivityOpen::resizeViewSections

void DialogSensitivityOpen::tableDoubleClicked(const QModelIndex &index) {
  if (!index.isValid())
    return;
  accept();
} // DialogSensitivityOpen::tableDoubleClicked
