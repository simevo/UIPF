/** @file ViewSvgNative.cc
    @brief Implementation for the ViewSvgNative class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Dmytro Skrypnyk.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QSvgRenderer>
#include <QPainter>
#include <QWheelEvent>
#include <QDebug>

#include "ViewSvgNative.h"

ViewSvgNative::ViewSvgNative(QString &file, QWidget* parent) : QWidget(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  doc_ = new QSvgRenderer(file, this);
  if (!doc_->isValid()) {
    qCritical() << "Opening " << file << " failed.";
    QStringList list;
    list = file.split('[');
    file = *list.begin() + ".svg";
    qCritical() << "Now trying " << file;
    doc_ = new QSvgRenderer(file, this);
    if (!doc_->isValid())
      qCritical() << "Invalid svg file";
  }
  connect(doc_, SIGNAL(repaintNeeded()),
          this, SLOT(update()));
} // ViewSvgNative::ViewSvgNative

void ViewSvgNative::paintEvent(QPaintEvent *) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QPainter p(this);
  p.setViewport(0, 0, width(), height());
  doc_->render(&p);
} // ViewSvgNative::paintEvent

QSize ViewSvgNative::sizeHint(void) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (doc_)
    return doc_->defaultSize();
  return QWidget::sizeHint();
} // ViewSvgNative::sizeHint

void ViewSvgNative::wheelEvent(QWheelEvent *e) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  const double diff = 0.1;
  QSize size = doc_->defaultSize();
  int width = size.width();
  int height = size.height();
  if (e->delta() > 0) {
    width = int (this->width() + this->width() * diff);
    height = int (this->height() + this->height() * diff);
  } else {
    width = int (this->width() - this->width() * diff);
    height = int (this->height() - this->height() * diff);
  }
  resize(width, height);
  QWidget::wheelEvent(e);
} // void ViewSvgNative::wheelEvent
