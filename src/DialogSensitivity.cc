/** @file DialogSensitivity.cc
    @brief Implementation for the DialogSensitivity class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian, Yasantha Samarasekera, Shane Shields
    and Luiz A. Buhnemann (la3280@gmail.com).

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QTabWidget>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QProgressBar>
#include <QDebug>
#include <QClipboard>
#include <QApplication>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QComboBox>
#include <QLineEdit>
#include <QIntValidator>
#include <QXmlStreamReader>
#include <QFile>
#include <QToolTip>
 
#include "DialogSensitivity.h"
#include "WidgetTableControl.h"
#include "ActionLibpfSensitivity.h"
#include "WidgetTableMonitor.h"
#include "DialogSensitivitySave.h"
#include "ModelDataControl.h"
#include "ModelDataMonitor.h"
#include "DelegateControl.h"
#include "DelegateMonitor.h"
#include "Settings.h"
#include "persistency.h"

DialogSensitivity::DialogSensitivity(int caseId, const std::string &type, ActionLibpfSensitivity* goAction, Units u, QWidget* parent)
  : QDialog(parent), caseId_(caseId), type_(type), resultsCopied_(false), someResultsAvailable_(false), goAction_(goAction) {

  qDebug() << "Entering "<< Q_FUNC_INFO;
  setWindowTitle(tr("Sensitivity analysis"));
  Qt::WindowFlags flags;
#if (QT_VERSION >= 0x040500)
  flags = Qt::Dialog | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint;
#else
  flags = Qt::Window;
#endif
  setWindowFlags(flags);

  tabWidget_ = new QTabWidget(this);

  /// Control Page
  QFrame* controlFrame = new QFrame(this);
  controlFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QLabel* controlInfoLabel = new QLabel(tr("Select the variables to manipulate and their ranges"), controlFrame);
  controlInfoLabel->setFont(QFont("", -1, QFont::Bold));
  controlInfoLabel->setFrameStyle(QFrame::StyledPanel);
  controlInfoLabel->setFrameShadow(QFrame::Sunken);
  controlInfoLabel->setMinimumHeight(50);
  controlInfoLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

  QFrame* controlFormFrame = new QFrame(controlFrame);
  controlTableWidget_ = new WidgetTableControl(caseId, controlFormFrame);
  controlTableWidget_->setUom(u);
  controlAddButton_ = new QPushButton(controlFormFrame);
  controlAddButton_->setIcon(QIcon(":/images/list-add.png"));
  controlAddButton_->setMaximumWidth(24);
  controlAddButton_->setAutoDefault(false);

  QFrame* controlOptionsFrame = new QFrame(controlFormFrame);
  QLabel* controlOrderingLabel = new QLabel(tr("Multi-dimensional sensitivity ordering"), controlOptionsFrame);
  controlOrdering_ = new QComboBox(controlOptionsFrame);
  controlOrdering_->addItem(tr("Lexicographical"));
  controlOrdering_->setItemData(0, "Conventional left-to-right and carriage return scanning order", Qt::ToolTipRole);
  controlOrdering_->addItem(tr("Boustrophedon"));
  controlOrdering_->setItemData(1, "Alternating left-to-right and right-to-left scanning order", Qt::ToolTipRole);
  controlOrdering_->addItem(tr("Quasi-spiral"));
  controlOrdering_->setItemData(2, "Spiralling outwards from the center of the box", Qt::ToolTipRole);
  controlOrdering_->setCurrentIndex(1);
  controlOrdering_->setEnabled(true); // user-set option currently disabled
  controlOrderingLabel->setBuddy(controlOrdering_);
  QLabel* controlTimeOutLabel = new QLabel(tr("Maximum running time (s)"), controlOptionsFrame);
  controlTimeOut_ = new QLineEdit(controlOptionsFrame);
  controlTimeOut_->setText("0");
  QIntValidator *controlTimeOutValidator = new QIntValidator(0, 99999, controlTimeOut_);
  controlTimeOut_->setValidator(controlTimeOutValidator);
  controlTimeOut_->setToolTip(tr("Enter 0 to run indefinitely"));
  controlTimeOut_->setEnabled(true); // user-set option currently enabled
  controlTimeOutLabel->setBuddy(controlTimeOut_);
  QGridLayout* controlOptionsLayout = new QGridLayout(controlOptionsFrame);
  controlOptionsLayout->addWidget(controlOrderingLabel, 0, 0);
  controlOptionsLayout->addWidget(controlOrdering_, 0, 1);
  controlOptionsLayout->addWidget(controlTimeOutLabel, 1, 0);
  controlOptionsLayout->addWidget(controlTimeOut_, 1, 1);
  controlOptionsFrame->setLayout(controlOptionsLayout);
  controlOptionsFrame->setFrameStyle(QFrame::StyledPanel);
  controlOptionsFrame->setFrameShadow(QFrame::Sunken);
  controlOptionsLayout->setColumnStretch(0, 1);

  QBoxLayout* controlFormLayout = new QVBoxLayout(controlFormFrame);
  controlFormLayout->addWidget(controlTableWidget_);
  controlFormLayout->addWidget(controlAddButton_);
  controlFormLayout->addWidget(controlOptionsFrame);
  controlFormFrame->setLayout(controlFormLayout);

  QFrame* controlButtonsFrame = new QFrame(controlFrame);
  controlCancelButton_ = new QPushButton(controlButtonsFrame);
  controlCancelButton_->setText(tr("Cancel"));
  controlCancelButton_->setAutoDefault(false);
  controlNextButton_ = new QPushButton(controlButtonsFrame);
  controlNextButton_->setText(tr("Next"));
  controlNextButton_->setAutoDefault(false);
  QBoxLayout* controlButtonsLayout = new QHBoxLayout(controlButtonsFrame);
  controlButtonsLayout->addWidget(controlCancelButton_);
  controlButtonsLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum));
  controlButtonsLayout->addWidget(controlNextButton_);
  controlButtonsFrame->setLayout(controlButtonsLayout);

  QBoxLayout* controlLayout = new QVBoxLayout(controlFrame);
  controlLayout->addWidget(controlInfoLabel);
  controlLayout->addWidget(controlFormFrame);
  controlLayout->addWidget(controlButtonsFrame);
  controlFrame->setLayout(controlLayout);

  // qDebug() << controlTableWidget_->model()->rowCount();
  // qDebug() << controlTableWidget_->model()->columnCount();
  // qDebug() << controlTableWidget_->indexWidget(controlTableWidget_->model()->index(controlTableWidget_->model()->rowCount() - 1,
  //   controlTableWidget_->model()->columnCount() - 1));
  // qDebug() << controlTableWidget_->indexWidget(controlTableWidget_->model()->index(0,0));
  // qDebug() << controlTableWidget_->childAt(0,0);

  setTabOrder(controlTableWidget_->indexWidget(
                controlTableWidget_->model()->index(controlTableWidget_->model()->rowCount() - 1,
                                                    controlTableWidget_->model()->columnCount() - 1)), // does not currently work because indexWidget and childAt return 0x0
              controlAddButton_);
  setTabOrder(controlAddButton_, controlOptionsFrame);
  setTabOrder(controlOptionsFrame, controlButtonsFrame);
  setTabOrder(controlButtonsFrame,
              // controlTableWidget_->indexWidget(controlTableWidget_->model()->index(0,0)));
              controlTableWidget_->childAt(0, 0)); // does not currently work because indexWidget and childAt return 0x0

  tabWidget_->addTab(controlFrame, tr("Control"));

  /// Monitor Page
  QFrame* monitorFrame = new QFrame(this);
  monitorFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QLabel* monitorInfoLabel = new QLabel(monitorFrame);
  monitorInfoLabel->setText(tr("Select the variables that will be reported for each value of the controlled variable"));
  monitorInfoLabel->setFont(QFont("", -1, QFont::Bold));
  monitorInfoLabel->setFrameStyle(QFrame::StyledPanel);
  monitorInfoLabel->setFrameShadow(QFrame::Sunken);
  monitorInfoLabel->setMinimumHeight(50);

  QFrame* monitorFormFrame = new QFrame(monitorFrame);
  monitorTableWidget_ = new WidgetTableMonitor(caseId, monitorFormFrame);
  monitorTableWidget_->setUom(u);
  monitorAddButton_ = new QPushButton(monitorFormFrame);
  monitorAddButton_->setIcon(QIcon(":/images/list-add.png"));
  monitorAddButton_->setMaximumWidth(24);
  QBoxLayout* monitorFormLayout = new QVBoxLayout(monitorFormFrame);
  monitorFormLayout->addWidget(monitorTableWidget_);
  monitorFormLayout->addWidget(monitorAddButton_);
  monitorFormFrame->setLayout(monitorFormLayout);

  QFrame* monitorButtonsFrame = new QFrame(monitorFrame);
  monitorCancelButton_ = new QPushButton(monitorButtonsFrame);
  monitorCancelButton_->setText(tr("Cancel"));
  monitorSaveButton_ = new QPushButton(monitorButtonsFrame);
  monitorSaveButton_->setText(tr("Save"));
  monitorSaveButton_->setIcon(QIcon(":/images/document-save-as.png"));
  monitorSaveButton_->setMaximumHeight(monitorCancelButton_->height() - 3);
  monitorPreviousButton_ = new QPushButton(monitorButtonsFrame);
  monitorPreviousButton_->setText(tr("Previous"));
  monitorNextButton_ = new QPushButton(monitorButtonsFrame);
  monitorNextButton_->setText(tr("Next"));
  QBoxLayout* monitorButtonsLayout = new QHBoxLayout(monitorButtonsFrame);
  monitorButtonsLayout->addWidget(monitorCancelButton_);
  monitorButtonsLayout->addWidget(monitorSaveButton_);
  monitorButtonsLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum));
  monitorButtonsLayout->addWidget(monitorPreviousButton_);
  monitorButtonsLayout->addWidget(monitorNextButton_);
  monitorButtonsFrame->setLayout(monitorButtonsLayout);

  QBoxLayout* monitorLayout = new QVBoxLayout(monitorFrame);
  monitorLayout->addWidget(monitorInfoLabel);
  monitorLayout->addWidget(monitorFormFrame);
  monitorLayout->addWidget(monitorButtonsFrame);
  monitorFrame->setLayout(monitorLayout);

  tabWidget_->addTab(monitorFrame, tr("Monitor"));

  /// Run Page
  QFrame* runFrame = new QFrame(this);
  runFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  runTableWidget_ = new QTableWidget(this);
  //Making table widget "read-only"
  runTableWidget_->setEditTriggers(QAbstractItemView::NoEditTriggers);
  runTableWidget_->verticalHeader()->hide();
  runTableWidget_->setColumnWidth(1, 200);
  QFrame * runButtonsFrame = new QFrame(runFrame);
  runCancelButton_ = new QPushButton(runButtonsFrame);
  runCancelButton_->setText(tr("Cancel"));
  runGoButton_ = new QPushButton(runButtonsFrame);
  runGoButton_->setText(tr("Go"));
  runGoButton_->setIcon(QIcon(":images/media-playback-start.png"));
  runStopButton_ = new QPushButton(runButtonsFrame);
  runStopButton_->setText(tr("Stop"));
  runStopButton_->setIcon(QIcon(":images/media-playback-stop.png"));
  runCopyButton_ = new QPushButton(runButtonsFrame);
  runCopyButton_->setText(tr("Copy results to clipboard"));
  runEditButton_ = new QPushButton(runButtonsFrame);
  runEditButton_->setText(tr("Edit sensitivity"));
  QBoxLayout* runButtonsLayout = new QHBoxLayout(runButtonsFrame);
  runButtonsLayout->addWidget(runCancelButton_);
  runButtonsLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum));
  runButtonsLayout->addWidget(runGoButton_);
  runButtonsLayout->addWidget(runStopButton_);
  runButtonsLayout->addWidget(runCopyButton_);
  runButtonsLayout->addWidget(runEditButton_);
  runButtonsFrame->setLayout(runButtonsLayout);

  runProgressBar_ = new QProgressBar(runFrame);

  QBoxLayout* runLayout = new QVBoxLayout(this);
  runLayout->addWidget(runTableWidget_);
  runLayout->addWidget(runProgressBar_);
  runLayout->addWidget(runButtonsFrame);
  runFrame->setLayout(runLayout);

  tabWidget_->addTab(runFrame, tr("Results"));

  QBoxLayout* layout = new QVBoxLayout(this);
  layout->addWidget(tabWidget_);
  setLayout(layout);

  /// Connections
  connect(tabWidget_, SIGNAL(currentChanged(int)), this, SLOT(onCurrentPageChanged(int)));
  connect(controlAddButton_, SIGNAL(clicked()), controlTableWidget_, SLOT(appendRow()));
  connect(controlNextButton_, SIGNAL(clicked()), this, SLOT(selectMonitorPage()));
  connect(controlCancelButton_, SIGNAL(clicked()), this, SLOT(cancel()));

  connect(monitorAddButton_, SIGNAL(clicked()), monitorTableWidget_, SLOT(appendRow()));
  connect(monitorNextButton_, SIGNAL(clicked()), this, SLOT(selectRunPage()));
  connect(monitorPreviousButton_, SIGNAL(clicked()), this, SLOT(selectControlPage()));
  connect(monitorCancelButton_, SIGNAL(clicked()), this, SLOT(cancel()));
  connect(monitorSaveButton_, SIGNAL(clicked()), this, SLOT(monitorSaveAnalysis()));

  connect(runCancelButton_, SIGNAL(clicked()), this, SLOT(cancel()));
  connect(runGoButton_, SIGNAL(clicked()), this, SLOT(go()));
  connect(runStopButton_, SIGNAL(clicked()), this, SLOT(stop()));
  connect(runCopyButton_, SIGNAL(clicked()), this, SLOT(copyResults()));
  connect(runEditButton_, SIGNAL(clicked()), this, SLOT(restart()));

  connect(controlTableWidget_, SIGNAL(edited()), this, SLOT(controlCompleteSetup()));
  connect(monitorTableWidget_, SIGNAL(edited()), this, SLOT(monitorCompleteSetup()));
  connect(tabWidget_, SIGNAL(currentChanged(int)), this, SLOT(onCurrentPageChanged(int)));

  if (goAction_) {
    qRegisterMetaType<RowStatus>("RowStatus");

    connect(goAction_, SIGNAL(computed(int, RowStatus)), this, SLOT(rowFinished(int, RowStatus)));
    connect(goAction_, SIGNAL(complete()), this, SLOT(runCompleteSetup()));
    connect(goAction_, SIGNAL(started()), this, SLOT(runRunningSetup()));
    connect(goAction_, SIGNAL(stopped()), this, SLOT(runStoppedPartialSetup()));
  }
  controlIncompleteSetup();
} // DialogSensitivity::DialogSensitivity

void DialogSensitivity::controlIncompleteSetup(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  tabWidget_->setTabEnabled(1, false);
  tabWidget_->setTabEnabled(2, false);
  controlNextButton_->setEnabled(false);
  resultsCopied_ = false;
  controlTableWidget_->setFocus();
} // DialogSensitivity::controlIncompleteSetup

void DialogSensitivity::controlCompleteSetup(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (testControlCompletion()) {
    tabWidget_->setTabEnabled(1, true);
    tabWidget_->setTabEnabled(2, false);
    controlNextButton_->setEnabled(true);
    controlTableWidget_->setFocus();
  } else {
    controlIncompleteSetup();
  }
} // DialogSensitivity::controlCompleteSetup

void DialogSensitivity::monitorIncompleteSetup(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  tabWidget_->setTabEnabled(0, true);
  tabWidget_->setTabEnabled(2, false);
  monitorNextButton_->setEnabled(false);
} // DialogSensitivity::monitorIncompleteSetup

void DialogSensitivity::monitorCompleteSetup(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (testMonitorCompletion()) {
    tabWidget_->setTabEnabled(0, true);
    tabWidget_->setTabEnabled(2, true);
    monitorNextButton_->setEnabled(true);
  } else {
    monitorIncompleteSetup();
  }
} // DialogSensitivity::monitorCompleteSetup

void DialogSensitivity::monitorSaveAnalysis() {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // connect and set up ActionLibpfSensitivity
  // required for xml saving
  if (goAction_) {
    //goAction_->setResultsModel(runTableWidget_->model());
    goAction_->setMonitorModel(monitorTableWidget_->model());
    goAction_->setControlledVariableModel(controlTableWidget_->model());
    goAction_->setCaseId(caseId_);
    goAction_->setType(type_);
    goAction_->setTimeOut(controlTimeOut_->text().toInt());
    goAction_->setOrdering(controlOrdering_->currentIndex());
    goAction_->setResumeFlag(false);
  }
  DialogSensitivitySave dlg(goAction_, lastSavedAnalysisName_, this);
  int r = dlg.exec();
  if (r == QDialog::Rejected)
    return;
  lastSavedAnalysisName_ = dlg.analysisName();
} // DialogSensitivity::monitorSaveAnalysis

void DialogSensitivity::runEmptySetup(void) {
  const QAbstractItemModel* monitorDataModel = monitorTableWidget_->model();
  const QAbstractItemModel* controlDataModel = controlTableWidget_->model();
  int totalRows = 1;
  std::vector<int> points;

  for (int r = 0; r < controlDataModel->rowCount(); ++r) {
    int np = controlDataModel->data(controlDataModel->index(r, NPOINTS_COL)).toInt();
    points.push_back(np);
    totalRows *= np;
  }
  tabWidget_->setTabEnabled(0, false);
  tabWidget_->setTabEnabled(1, false);
  runStopButton_->setEnabled(false);
  runCopyButton_->setEnabled(false);

  // resize table
  runTableWidget_->clear();
  runTableWidget_->setRowCount(totalRows);
  // time, errors, warnings, iterations
  runTableWidget_->setColumnCount(1 + controlDataModel->rowCount() + monitorDataModel->rowCount() + 4);
  runTableWidget_->setColumnWidth(0, 40);
  // set headers
  qCritical() << "Setting headers";
  QTableWidgetItem *newItem = new QTableWidgetItem(tr("ID"));
  newItem->setToolTip("Row identifier in sequence");
  runTableWidget_->setHorizontalHeaderItem(0, newItem);
  for (int r = 0; r < controlDataModel->rowCount(); ++r) {
    QString fulltag = controlDataModel->data(controlDataModel->index(r, FULLTAG_COL)).toString();
    QString units = controlDataModel->data(controlDataModel->index(r, UOM_COL)).toString();
    QString longLabel(QString("%1\n[%2]").arg(fulltag).arg(units));
    QString label = controlDataModel->data(controlDataModel->index(r, LABEL_COL)).toString();
    QString shortLabel(QString("%1\n[%2]").arg(label).arg(units));
    newItem = new QTableWidgetItem(shortLabel);
    newItem->setToolTip(longLabel);
    runTableWidget_->setHorizontalHeaderItem(1 + r, newItem);
  }
  for (int i = 0; i < monitorDataModel->rowCount(); ++i) {
    QString fulltag = monitorDataModel->data(monitorDataModel->index(i, FULLTAG_COL)).toString();
    QString units = monitorDataModel->data(monitorDataModel->index(i, UOM_COL)).toString();
    QString longLabel(QString("%1\n[%2]").arg(fulltag).arg(units));
    QString label = monitorDataModel->data(monitorDataModel->index(i, LABEL_COL)).toString();
    QString shortLabel(QString("%1\n[%2]").arg(label).arg(units));
    newItem = new QTableWidgetItem(shortLabel);
    newItem->setToolTip(longLabel);
    runTableWidget_->setHorizontalHeaderItem(1 + controlDataModel->rowCount() + i, newItem);
  }
  {
    QString shortLabel("Time\n[s]");
    QString longLabel("Calculation time\n[s]");
    newItem = new QTableWidgetItem(shortLabel);
    newItem->setToolTip(longLabel);
    runTableWidget_->setHorizontalHeaderItem(
      1 + controlDataModel->rowCount() + monitorDataModel->rowCount(),
      newItem);
  }
  {
    QString label("Errors");
    newItem = new QTableWidgetItem(label);
    newItem->setToolTip(label);
    runTableWidget_->setHorizontalHeaderItem(
      1 + controlDataModel->rowCount() + monitorDataModel->rowCount() + 1,
      newItem);
  }
  {
    QString label("Warnings");
    newItem = new QTableWidgetItem(label);
    newItem->setToolTip(label);
    runTableWidget_->setHorizontalHeaderItem(
      1 + controlDataModel->rowCount() + monitorDataModel->rowCount() + 2,
      newItem);
  }
  {
    QString label("Iterations");
    newItem = new QTableWidgetItem(label);
    newItem->setToolTip(label);
    runTableWidget_->setHorizontalHeaderItem(
      1 + controlDataModel->rowCount() + monitorDataModel->rowCount() + 3,
      newItem);
  }
  // connect and set up ActionLibpfSensitivity
  if (goAction_) {
    goAction_->setResultsModel(runTableWidget_->model());
    goAction_->setMonitorModel(monitorTableWidget_->model());
    goAction_->setControlledVariableModel(controlTableWidget_->model());
    goAction_->setCaseId(caseId_);
    goAction_->setType(type_);
    goAction_->setTimeOut(controlTimeOut_->text().toInt());
    goAction_->setOrdering(controlOrdering_->currentIndex());
    goAction_->setResumeFlag(false);
  }
  // fill in ID numbers
  QAbstractItemModel *runTableModel = runTableWidget_->model();
  for (int r = 0; r < totalRows; ++r)
    runTableModel->setData(runTableModel->index(r, 0), r + 1);
  // fill in controlled variable values and show in boldface
  QFont boldFont;
  boldFont.setBold(true);
  for (int cv = 0; cv < controlDataModel->rowCount(); ++cv)
    runTableWidget_->horizontalHeaderItem(cv + 1)->setFont(boldFont);
  for (int id = 0; id < totalRows; ++id) {
    for (int cv = 0; cv < controlDataModel->rowCount(); ++cv) {
      runTableWidget_->horizontalHeaderItem(cv + 1)->setFont(boldFont);

      double start = controlDataModel->data(controlDataModel->index(cv, START_COL)).toDouble();
      double end = controlDataModel->data(controlDataModel->index(cv, END_COL)).toDouble();
      double increment = (end - start) / (static_cast<double>(std::max(1, points[cv] - 1)));
      int localround = goAction_->ordering()->localIdFromId(id, cv);
      double value = start + (static_cast<double>(localround)) * increment;
      runTableModel->setData(runTableModel->index(id, cv + 1), value);
      runTableModel->setData(runTableModel->index(id, cv + 1), boldFont, Qt::FontRole);
    } // for each controlled variable
  } // for each row

  runProgressBar_->setMinimum(0);
  runProgressBar_->setMaximum(2 * (totalRows - 1) + 1);
  runProgressBar_->setValue(0);

  resultsCopied_ = false;
  someResultsAvailable_ = false;

  runCancelButton_->setEnabled(true);
  runGoButton_->setEnabled(true);
  runStopButton_->setEnabled(false);
  runEditButton_->setEnabled(true);
} // DialogSensitivity::runEmptySetup

void DialogSensitivity::runRunningSetup(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  runCancelButton_->setEnabled(false);
  runCopyButton_->setEnabled(true);
  runGoButton_->setEnabled(false);
  runStopButton_->setEnabled(true);
  runEditButton_->setEnabled(false);
} // DialogSensitivity::runRunningSetup

void DialogSensitivity::runStoppedPartialSetup(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  resultsCopied_ = false;
  someResultsAvailable_ = true;
  runCancelButton_->setEnabled(true);
  runGoButton_->setEnabled(true);
  runStopButton_->setEnabled(false);
  runEditButton_->setEnabled(true);
  if (goAction_)
    goAction_->setResumeFlag(true);
} // DialogSensitivity::runStoppedPartialSetup

void DialogSensitivity::runCompleteSetup(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  resultsCopied_ = false;
  someResultsAvailable_ = true;
  runCancelButton_->setEnabled(true);
  runGoButton_->setEnabled(false);
  runStopButton_->setEnabled(false);
  runEditButton_->setEnabled(true);
} // DialogSensitivity::runCompleteSetup

/// If the results are not copied, a confirmation box is displayed. Otherwise the dialog is closed.
void DialogSensitivity::cancel(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (testControlCompletion()) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("Dismiss?"));
    msgBox.setText(tr("Are you sure you want to dismiss the sensitivity?"));
    if (someResultsAvailable_ && !resultsCopied_)
      msgBox.setInformativeText(tr("All sensitivity settings and results will be lost"));
    else
      msgBox.setInformativeText(tr("All sensitivity settings will be lost"));
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    if (msgBox.exec() == QMessageBox::Ok) {
      stop();
      reject();
    }
  } else {
    stop();
    accept();
  }
} // DialogSensitivity::cancel

void DialogSensitivity::restart(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  bool restart_ok = true;

  if (someResultsAvailable_ && !resultsCopied_) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("Dismiss?"));
    msgBox.setText(tr("Are you sure you want to dismiss the sensitivity?"));
    msgBox.setInformativeText(tr("All results will be lost"));
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);

    if (msgBox.exec() == QMessageBox::Cancel) {
      restart_ok = false;
    }
  }
  if (restart_ok) {
    runTableWidget_->clear();
    runProgressBar_->setValue(0);
    resultsCopied_ = false;
    someResultsAvailable_ = false;
    monitorCompleteSetup();
    controlCompleteSetup();
    runEmptySetup();
    tabWidget_->setTabEnabled(0, true);
    tabWidget_->setTabEnabled(1, true);
    setCurrentPage(0);
    if (goAction_)
      goAction_->setResumeFlag(true);
  }
} // DialogSensitivity::restart

void DialogSensitivity::closeEvent(QCloseEvent * event) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  event->ignore();
  cancel();
} // DialogSensitivity::closeEvent

void DialogSensitivity::keyPressEvent(QKeyEvent* event) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (event->key() == Qt::Key_Escape) {
    event->ignore();
    cancel();
  } else {
    QDialog::keyPressEvent(event);
  }
} // DialogSensitivity::keyPressEvent

void DialogSensitivity::onCurrentPageChanged(int index) {
  if (index == 2) {
    runEmptySetup();
  }
} // DialogSensitivity::onCurrentPageChanged

void DialogSensitivity::setCurrentPage(int index) {
  tabWidget_->setCurrentIndex(index);
} // DialogSensitivity::setCurrentPage

void DialogSensitivity::selectControlPage(void) {
  setCurrentPage(0);
} // DialogSensitivity::selectControlPage

void DialogSensitivity::selectMonitorPage(void) {
  setCurrentPage(1);
  monitorCompleteSetup();
} // DialogSensitivity::selectMonitorPage

void DialogSensitivity::selectRunPage(void) {
  setCurrentPage(2);
} // DialogSensitivity::selectMonitorPage

void DialogSensitivity::go(void) {
  if (goAction_) {
    goAction_->start();
  }
} // DialogSensitivity::go

void DialogSensitivity::stop(void) {
  if (goAction_) {
    goAction_->stop();
  }
} // DialogSensitivity::stop

void DialogSensitivity::copyResults(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString copiedText;
  QAbstractItemModel* model = runTableWidget_->model();
  QLocale syslocale = QLocale::system();
  qWarning() << "Using locale " << QLocale::countryToString(syslocale.country());

  int cvs = controlTableWidget_->model()->rowCount();
  for (int c = 0; c < model->columnCount(); ++c) {
    if (c == 0)
      copiedText += "Counter";
    else if (c <= cvs)
      copiedText += tr("Controlled variable %1").arg(c - 1);
    else
      copiedText += tr("Monitored variable %1").arg(c - cvs - 1);  // c > cvs
    copiedText += "\t";
  }
  copiedText = copiedText + "\n";
  for (int c = 0; c < model->columnCount(); ++c) {
    copiedText += runTableWidget_->horizontalHeaderItem(c)->toolTip().replace(QString("\n"), QString(" "));
    copiedText += "\t";
  }
  copiedText = copiedText.trimmed() + "\n";
  for (int r = 0; r < model->rowCount(); ++r) {
    copiedText += model->data(model->index(r, 0)).toString();

    for (int c = 1; c < model->columnCount(); ++c) {
      copiedText += "\t";
      copiedText += syslocale.toString(model->data(model->index(r, c)).toDouble());
    }
    copiedText = copiedText + "\n";
  }
  QApplication::clipboard()->setText(copiedText);
  QToolTip::showText(
    runCopyButton_->mapToGlobal(QPoint()) + QPoint(runCopyButton_->width()/2, runCopyButton_->height()/2), 
    tr("All results have been copied to the clipboard"), runCopyButton_);
  resultsCopied_ = true;
} // DialogSensitivity::copyResults

void DialogSensitivity::rowFinished(int ID, enum RowStatus rs) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int count = runTableWidget_->model()->columnCount();
  if (rs >= doneOk) {
    int round = goAction_->ordering()->roundFromId(ID);
    runProgressBar_->setValue(2 * round + 1);
  }
  QColor color;
  switch (rs) {
  case (executing):
    color = Qt::gray;
    break;
  case (doneOk):
    color = Qt::green;
    break;
  case (doneWarnings):
    color = Qt::yellow;
    break;
  case (doneErrors):
    color = Qt::red;
    break;
  case (doneSevereError):
    color = Qt::darkRed;
    break;
  case (doneCrash):
    color = Qt::darkMagenta;
    break;
  case (doneNotFinished):
    color = Qt::darkCyan;
    break;
  default:
    color = Qt::white;
  }
  const QAbstractItemModel* controlDataModel = controlTableWidget_->model();
  const QAbstractItemModel* monitorDataModel = monitorTableWidget_->model();
  int startConvertColumn = controlDataModel->rowCount() + 1;

  for (int i = 0; i < count; ++i) {
    QModelIndex index = runTableWidget_->model()->index(ID, i);
    runTableWidget_->model()->setData(index, color, Qt::BackgroundColorRole);
    if (i > startConvertColumn - 1) {
      runTableWidget_->model()->setData(index, getNewUnit(runTableWidget_->model()->data(index).toDouble(),
                                                          monitorDataModel->data(monitorDataModel->index(i - startConvertColumn, 2)).toString().trimmed()));
    }
  }
} // DialogSensitivity::rowFinished

QString DialogSensitivity::getNewUnit(double siUnit, QString newUnit) {
  double newDisplay = unitEngine_.fromSI(siUnit, std::string(newUnit.toUtf8()));
  return QString::number(newDisplay);
}

bool DialogSensitivity::testControlCompletion(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (controlTableWidget_->hasData()) {
    if (controlTableWidget_->hasValidData() && controlOrdering_->currentIndex() != -1) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
} // DialogSensitivity::testControlCompletion

bool DialogSensitivity::testMonitorCompletion(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (monitorTableWidget_->hasData()) {
    if (monitorTableWidget_->hasValidData()) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
} // DialogSensitivity::testMonitorCompletion

/// Structs used for reading SensitivityInput only
struct ControlledVariable {
  QString uom;
  double start;
  double end;
  int points;
  QString label;
  QString name;
};

struct MonitoredVariable {
  QString uom;
  QString name;
  QString label;
};

bool DialogSensitivity::loadSensitivity(const QString& fileName, const QString &name, const QString & /* type */) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // Fetch data from the XML file
  QFile xmlFile(fileName);
  if (!xmlFile.open(QFile::ReadOnly)) {
    qCritical() << "Abort sensitivity loading: Impossible to open in read only mode the file " << fileName;
    return false;
  }
  bool restoringLastRanAnalysis = fileName.endsWith("SensitivityInput.xml", Qt::CaseInsensitive);
  QXmlStreamReader xml(&xmlFile);
  QString sensitivityName;
  QString sensitivityType;
  QString sensitivityId;
  QString sensitivityOrder;
  QString sensitivityTimeOut;
  QString sensitivityStartingRound;
  QList<ControlledVariable> controlledVariables;
  QList<MonitoredVariable> monitoredVariables;

  while (xml.readNextStartElement()) {
    if (xml.name() == "sensitivity") {
      while (xml.readNextStartElement()) {
        if (xml.name() == "name") {
          sensitivityName = xml.readElementText();
          // If sensitivityName is different from the element name in the xml file.
          // Case we are loading a last ran analysis, it's forgivable
          if (sensitivityName != name && !restoringLastRanAnalysis) {
            qCritical("Warning: File name different from XML name.");
            sensitivityName = name; // keep the name from the file
          }
        } else if (xml.name() == "type") {
          sensitivityType = xml.readElementText();
          // Checking the type of sensitivity in order to generate only valid forms when restoring
          // a previous ran analysis
          if (restoringLastRanAnalysis && (sensitivityType != QString(type_.c_str()))) {
            qCritical("Abort sensitivity loading, different types while restoring last ran analysis.");
            return false;
          }
        } else if (xml.name() == "id") {
          sensitivityId = xml.readElementText();
        } else if (xml.name() == "order") {
          sensitivityOrder = xml.readElementText();
        } else if (xml.name() == "timeout") {
          sensitivityTimeOut = xml.readElementText();
        } else if (xml.name() == "startinground") {
          sensitivityStartingRound = xml.readElementText();
        } else if (xml.name() == "controlled") {
          while (xml.readNextStartElement()) {
            if (xml.name() != "variable") {
              qCritical() << "Abort sensitivity loading: unexpected element within controlled block";
              return false;
            }
            ControlledVariable var;
            QXmlStreamAttributes attributes = xml.attributes();
            var.uom = attributes.value("uom").toString();
            var.start = attributes.value("start").toString().toDouble();
            var.end = attributes.value("end").toString().toDouble();
            var.points = std::min(999, std::max(1, attributes.value("points").toString().toInt()));
            var.label = attributes.value("label").toString();
            var.name = xml.readElementText();
            // qDebug() << "Read controlled variable " << var.name;
            controlledVariables.push_back(var);
          }
        } else if (xml.name() == "monitored") {
          MonitoredVariable var;
          while (xml.readNextStartElement()) {
            if (xml.name() != "variable") {
              qCritical() << "Abort sensitivity loading: unexpected element within monitored block";
              return false;
            }
            QXmlStreamAttributes attributes = xml.attributes();
            var.uom = attributes.value("uom").toString();
            var.label = attributes.value("label").toString();
            var.name = xml.readElementText();
            // qDebug() << "Read monitored variable " << var.name;
            monitoredVariables.push_back(var);
          }
        } else {
          qCritical() << "Abort sensitivity loading: unexpected element within sensitivity block";
          return false;
        }
      }
    } else {
      qCritical() << "Abort sensitivity loading: unexpected element at top level";
      return false;
    }
  }
  // Setup the Dialog with the data

  // Last Analysis name is set for hinting when saving again
  lastSavedAnalysisName_ = sensitivityName;

  // TimeOut (guard against invalid values coming from the xml)
  controlTimeOut_->setText(sensitivityTimeOut);
  int size = sensitivityTimeOut.size();
  QValidator::State state = controlTimeOut_->validator()->validate(sensitivityTimeOut, size);
  if (controlTimeOut_->text().isEmpty() || state == QValidator::Invalid) {
    qCritical() << "Validation failed for sensitivity time-out " << sensitivityTimeOut;
    controlTimeOut_->setText("0");
  } else {
    // qDebug() << "Validation succeeded for sensitivity time-out " << controlTimeOut_->text();
  }
  // Ordering
  int orderingIndex = controlOrdering_->findText(sensitivityOrder, Qt::MatchFixedString);
  if (orderingIndex == -1) {
    qCritical() << "Validation failed for sensitivity ordering " << sensitivityOrder;
  } else {
    controlOrdering_->setCurrentIndex(orderingIndex);
    // qDebug() << "Validation succeeded for sensitivity ordering " << controlOrdering_->currentText();
  }
  QModelIndex index;
  int row;

  // Fill control table
  if (controlledVariables.size()) {
    ControlledVariable cv;
    ModelDataControl*  modelControl = qobject_cast<ModelDataControl*>(controlTableWidget_->model());
    DelegateControl*   delegateControl = qobject_cast<DelegateControl*>(controlTableWidget_->itemDelegate());

    modelControl->insertRows(1, controlledVariables.size() - 1);
    row = 0;
    foreach(cv, controlledVariables) {
      if (row == modelControl->rowCount())
        return false;
      // Check fulltag before setting it
      index = modelControl->index(row, FULLTAG_COL);
      QString fullTag;
      fullTag = cv.name;
      int indexCv = delegateControl->hasFullTag(fullTag);
      if (indexCv >= 0) {
        QComboBox* comboBox;

        // The FullTag column
        controlTableWidget_->openPersistentEditor(index);
        comboBox = qobject_cast<QComboBox*>(controlTableWidget_->indexWidget(index));
        comboBox->setCurrentIndex(indexCv);
        controlTableWidget_->closePersistentEditor(index);
        // qDebug() << "Validation succeeded for controlled variable " << fullTag;
      } else {
        qCritical() << "Validation failed for controlled variable " << fullTag;
      }
      if (!modelControl->index(row, FULLTAG_COL).data().isNull()) {
        index = modelControl->index(row, UOM_COL);
        controlTableWidget_->openPersistentEditor(index);
        QComboBox* comboBox = qobject_cast<QComboBox*>(controlTableWidget_->indexWidget(index));
        int comboIndex = comboBox->findText(cv.uom);
        if (comboIndex > -1) {
          modelControl->setData(index, cv.uom, Qt::DisplayRole);
          comboBox->setCurrentIndex(comboIndex);
        } else {
          qCritical() << "Validation failed for unit " << cv.uom << " of controlled variable " << fullTag;
        } // units validates
        controlTableWidget_->closePersistentEditor(index);
      }

      index = modelControl->index(row, START_COL);
      modelControl->setData(index, cv.start, Qt::EditRole);

      index = modelControl->index(row, END_COL);
      modelControl->setData(index, cv.end, Qt::EditRole);

      index = modelControl->index(row, NPOINTS_COL);
      modelControl->setData(index, cv.points, Qt::EditRole);

      // Set the label from the XML if we have it
      if (!cv.label.isEmpty()) {
        index = modelControl->index(row, LABEL_COL);
        modelControl->setData(index, cv.label, Qt::EditRole);
      }
      row++;
    } // for each controlled variable
  } // controlledVariables.size > 0

  // Fill monitored table
  if (monitoredVariables.size()) {
    MonitoredVariable mv;
    ModelDataMonitor* modelMonitor = qobject_cast<ModelDataMonitor*>(monitorTableWidget_->model());
    DelegateMonitor*  delegateMonitor = qobject_cast<DelegateMonitor*>(monitorTableWidget_->itemDelegate());

    modelMonitor->insertRows(1, monitoredVariables.size() - 1);
    row = 0;
    foreach(mv, monitoredVariables) {
      if (row == modelMonitor->rowCount())
        return false;
      // Check fulltag before setting it
      QString fullTag;
      fullTag = mv.name;
      int indexMv = delegateMonitor->hasFullTag(fullTag);
      if (indexMv >= 0) {
        // Set fulltag
        index = modelMonitor->index(row, FULLTAG_COL);
        monitorTableWidget_->openPersistentEditor(index);
        QComboBox* comboBox = qobject_cast<QComboBox*>(monitorTableWidget_->indexWidget(index));
        comboBox->setCurrentIndex(indexMv);
        monitorTableWidget_->closePersistentEditor(index);
        // qDebug() << "Validation succeeded for monitored variable " << fullTag;
      } else {
        qCritical() << "Validation failed for monitored variable " << fullTag;
      }

      // Set UOM
      if (!modelMonitor->index(row, FULLTAG_COL).data().isNull()) {
        index = modelMonitor->index(row, UOM_COL);
        monitorTableWidget_->openPersistentEditor(index);
        QComboBox* comboBox = qobject_cast<QComboBox*>(monitorTableWidget_->indexWidget(index));
        int comboIndex = comboBox->findText(mv.uom);
        if (comboIndex > -1) {
          modelMonitor->setData(index, mv.uom, Qt::DisplayRole);
          comboBox->setCurrentIndex(comboBox->findText(mv.uom));
        } else {
          qCritical() << "Validation failed for unit " << mv.uom << " of controlled variable " << fullTag;
        } // units validates

        monitorTableWidget_->closePersistentEditor(index);
      }

      // Set the label from XML if we have it
      if (!mv.label.isEmpty()) {
        index = modelMonitor->index(row, LABEL_COL);
        modelMonitor->setData(index, mv.label, Qt::EditRole);
      }
      row++;
    } // for each monitored variable
  } // monitoredVariables.size

  // Setup the dialog completeness
  controlCompleteSetup();
  monitorCompleteSetup();

  return true;
} // DialogSensitivity::loadSensitivity
