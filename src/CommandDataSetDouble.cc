/** @file CommandDataSetDouble.cc
    @brief Implementation for the CommandDataSetDouble class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "CommandDataSetDouble.h"
#include "ModelQuantityEditable.h"
#include <QDebug>

CommandDataSetDouble::CommandDataSetDouble(
  ModelQuantityEditable* model,
  const QModelIndex & index,
  const QVariant & newValue,
  int role,
  int currentNode) : CommandDataSet(newValue, role, currentNode), model_(model) {

  qDebug() << "Entering "<< Q_FUNC_INFO;
  row_ = index.row();
  col_ = index.column();
  oldValue_ = index.data(role_);
  id_ = model_->id(row_);
  fullTag_ = model_->completeTag(row_);
  description_ = model_->description(row_);
} // CommandDataSetDouble::CommandDataSetDouble

void CommandDataSetDouble::undo(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QModelIndex index = model_->index(row_, col_);
  setData(index, oldValue_, role_);
  
  QString cmd_description =
    QObject::tr("Will set again the value of %1 (%2) to %3").arg(fullTag_).arg(description_).arg(newValue_.toDouble());
  setText(cmd_description);
} // CommandDataSetDouble::undo

void CommandDataSetDouble::redo(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QModelIndex index = model_->index(row_, col_);
  setData(index, newValue_, role_);

  QString cmd_description =
    QObject::tr("Will set the value of %1 (%2) back to %3").arg(fullTag_).arg(description_).arg(oldValue_.toDouble());
  setText(cmd_description);
} // CommandDataSetDouble::redo

bool CommandDataSetDouble::setData(const QModelIndex & index, const QVariant & value, int role) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  bool ok(false);

  if (role == Qt::EditRole) {
    if (col_ == Qtbl_Q) {
      ok = model_->setQ(id_, value.toDouble());
    }
  }
  model_->emitDataChanged(index);
  model_->emitForceGo(currentNode_, row_);
  return ok;
} // CommandDataSetDouble::setData
