/** @file ModelDataControl.cc
    @brief Implementation for the ModelDataControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#if (QT_VERSION >= 0x040600)
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#endif
#include <QColor>
#include <QDebug>

#include "ModelDataControl.h"
#include "persistency.h"
#include "UnitEngine.h"

/// @param parent the parent object
ModelDataControl::ModelDataControl(QObject* parent) :
  QAbstractTableModel(parent)
{   qDebug() << "Entering "<< Q_FUNC_INFO; } // ModelDataControl::ModelDataControl

/// Reimplemented from QAbstractTableModel::rowCount()
int ModelDataControl::rowCount(const QModelIndex & /*index*/) const {
  return data_.count();
} // ModelDataControl::rowCount

/// Reimplemented from QAbstractTableModel::columCount()
int ModelDataControl::columnCount(const QModelIndex & /*index*/) const {
  return NUM_COLS;
} // ModelDataControl::columnCount

/// Reimplemented from QAbstractTableModel::data()
QVariant ModelDataControl::data(const QModelIndex & index, int role) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!index.isValid())
    return QVariant();
  RecordControl record = data_[index.row()];
  if (role == Qt::TextAlignmentRole) {
    return static_cast<int>(Qt::AlignLeft | Qt::AlignVCenter);
  } else if (role == Qt::BackgroundColorRole) {
    if (!record.FULLTAG.isEmpty() && !rowValid(index.row()) &&
        (index.column() >= START_COL && index.column() <= NPOINTS_COL)) {
      QColor red = Qt::red;
      return red;
    } else {
      QColor white = Qt::white;
      return white;
    }
  } else if (role == Qt::EditRole) {
    if (index.column() == ID_COL) {
      return record.ID;
    }
    if (index.column() == FULLTAG_COL) {
      return record.FULLTAG;
    }
    if (index.column() == UOM_COL) {
      return record.UOM;
    }
    if (index.column() == DESCR_COL) {
      return record.DESCRIPTION;
    }
    if (index.column() == LABEL_COL) {
      return record.LABEL;
    }
    if (index.column() == START_COL) {
      return record.START;
    }
    if (index.column() == END_COL) {
      return record.END;
    }
    if (index.column() == NPOINTS_COL) {
      return record.NPOINTS;
    }
    if (index.column() == DBID_COL) {
      return record.DBID;
    }
  } else if (role == Qt::DisplayRole) {
    if (index.column() == ID_COL) {
      return record.ID;
    }
    if (index.column() == FULLTAG_COL) {
      return record.FULLTAG;
    }
    if (index.column() == DESCR_COL) {
      return record.DESCRIPTION;
    }
    if (index.column() == LABEL_COL) {
      return record.LABEL;
    }
    if (index.column() == NPOINTS_COL) {
      return record.NPOINTS;
    }
    if (index.column() == DBID_COL) {
      return record.DBID;
    }
    if (index.column() == START_COL) {
      if (sValue_.contains(index.row()))
        return sValue_.value(index.row());
      else
        return record.START;
    }
    if (index.column() == END_COL) {
      if (eValue_.contains(index.row()))
        return eValue_.value(index.row());
      else
        return record.END;
    }
    if (index.column() == UOM_COL) {
      if (uValue_.contains(index.row()))
        return uValue_.value(index.row());
      else
        return record.UOM;
    }
  }
  return QVariant();
} // ModelDataControl::data

/// Reimplemented from QAbstractTableModel::flags()
Qt::ItemFlags ModelDataControl::flags(const QModelIndex & index) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if ((index.column() == ID_COL) || (index.column() == DESCR_COL))
    return QAbstractItemModel::flags(index);
  return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
} // ModelDataControl::flags

/// Reimplemented from QAbstractTableModel::setData()
bool ModelDataControl::setData(const QModelIndex & index, const QVariant & value, int role) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid() && (role == Qt::EditRole)) {
    int row = index.row();
    if (index.column() == ID_COL) {
      data_[row].ID = value.toInt();
      emit dataChanged(index, index);
    }
    if (index.column() == FULLTAG_COL) {
      data_[row].FULLTAG = value.toString().trimmed();
      emit dataChanged(index, index);
    }
    if (index.column() == UOM_COL) {
      data_[row].UOM = value.toString().trimmed();
      emit dataChanged(index, index);
    }
    if (index.column() == DESCR_COL) {
      data_[row].DESCRIPTION = value.toString().trimmed();
      emit dataChanged(index, index);
    }
    if (index.column() == LABEL_COL) {
      data_[row].LABEL = value.toString().trimmed();
      emit dataChanged(index, index);
    }
    if (index.column() == START_COL) {
      data_[row].START = value.toDouble();
      if (sValue_.contains(row))
        sValue_.insert(row,getNewUnit(value.toDouble(), uValue_.value(row).toString().trimmed()));
      emit dataChanged(index, index);
    }
    if (index.column() == END_COL) {
      data_[row].END = value.toDouble();
      if (eValue_.contains(row))
        eValue_.insert(row,getNewUnit(value.toDouble(), uValue_.value(row).toString().trimmed()));
      emit dataChanged(index, index);
    }
    if (index.column() == NPOINTS_COL) {
      data_[row].NPOINTS = value.toInt();
      emit dataChanged(index, index);
    }
    if (index.column() == DBID_COL) {
      data_[row].DBID = value.toInt();
      emit dataChanged(index, index);
    }
    return true;
  } else if (index.isValid() && role == Qt::DisplayRole) {
    if (index.column() == START_COL) {
      sValue_.insert(index.row(),value);
      emit dataChanged(index, index);
    }
    if (index.column() == END_COL) {
      eValue_.insert(index.row(),value);
      emit dataChanged(index, index);
    }
    if (index.column() == UOM_COL) {
      uValue_.insert(index.row(),value);
      emit dataChanged(index, index);
    }
  }
  return QAbstractTableModel::setData(index, value, role);
} // ModelDataControl::setData

QString ModelDataControl::getNewUnit(double siUnit,QString newUnit) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  double newDisplay = unitEngine_.fromSI(siUnit,std::string(newUnit.toUtf8()));
  return QString::number(newDisplay);
}

/// Reimplemented from QAbstractTableModel::inserRows()
bool ModelDataControl::insertRows(int row, int count, const QModelIndex & parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (count <= 0)
    return false;
  beginInsertRows(parent, row, row + count - 1);
  for (int i = 0; i < count; ++i) {
    data_.insert(row + i, RecordControl(row + i + 1));
  }
  for (int i = row + count; i < rowCount(); ++i) {
    data_[i].ID = i + 1;
  }
  endInsertRows();
  return true;
} // ModelDataControl::insertRows

/// Reimplemented from QAbstractTableModel::removeRows()
bool ModelDataControl::removeRows(int row, int count, const QModelIndex & parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!rowCount())
    return false;
  if (count <= 0)
    return false;
  beginRemoveRows(parent, row, row + count - 1);
  for (int i = 0; i < count; ++i) {
    data_.removeAt(row);
  }
  for (int i = row; i < rowCount(); ++i) {
    data_[i].ID = i + 1;
  }
  endRemoveRows();
  return true;
} // ModelDataControl::removeRows

/// Reimplemented from QAbstractTableModel::headerData()
QVariant ModelDataControl::headerData(int section, Qt::Orientation orientation, int role) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if ((role == Qt::DisplayRole) && (orientation == Qt::Horizontal)) {
    if (section == ID_COL)
      return tr("ID");
    if (section == FULLTAG_COL)
      return tr("Controlled variables");
    if (section == UOM_COL)
      return tr("Units");
    if (section == DESCR_COL)
      return tr("Description");
    if (section == LABEL_COL)
      return tr("Label");
    if (section == START_COL)
      return tr("Start");
    if (section == END_COL)
      return tr("End");
    if (section == NPOINTS_COL)
      return tr("Points");
  }
  if ((role == Qt::TextAlignmentRole) && (orientation == Qt::Horizontal)) {
    return static_cast<int>(Qt::AlignLeft | Qt::AlignVCenter);
  }
  return QVariant();
} // ModelDataControl::headerData

/// Checks validity of the given row.
/// Returns true if the given row has valid start, end and number of
/// points data.
bool ModelDataControl::rowValid(int row) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  double start = data_[row].START;
  double end = data_[row].END;
  int points = data_[row].NPOINTS;

  if (points == 0 || (end == start && points != 1))
    return false;
  return true;
} // ModelDataControl::rowValid
