/** @file WindowMain.cc
    @brief Implementation for the WindowMain class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera, Harish Surana, Shane Shields
    and Luiz A. Buhnemann (la3280@gmail.com).

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <algorithm>

#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QUndoStack>
#include <QMenuBar>
#include <QToolBar>
#include <QTextEdit>
#include <QLabel>
#include <QSplitter>
#include <QMessageBox>
#include <QStatusBar>
#include <QFileDialog>
#include <QHeaderView>
#include <QDesktopServices>
#include <QApplication>
#include <QClipboard>
#include <QInputDialog>
#include <QLineEdit>
#include <QTemporaryFile>
#include <QFontDatabase>

#include "persistency.h"
#include "WindowMain.h"
#include "itoa.h"
#include "version.h"
#include "Homotopy.h"
#include "ActionLibpf.h"
#ifdef Q_OS_WIN
#include "open_xls.h"
#include <windows.h>
#include <shellapi.h> // for ShellExecuteEx
#endif
#include "Hierarchy.h"
#include "Settings.h"
#include "WidgetTableDouble.h"
#include "WidgetTableMessages.h"
#include "WidgetTree.h"
#include "ScrollAreaSvg.h"
#include "DialogOpen.h"
#include "DialogNew.h"
#include "DialogDuplicate.h"
#include "ActionLibpfSensitivity.h"
#include "DialogSensitivity.h"
#include "DialogSensitivityOpen.h"
#include "ViewTable.h"

Hierarchy* h_;

WindowMain::WindowMain() : 
  currentCase_(""), 
  highlight_(false),
  previousState_(State::undefined),
  state_(State::undefined), 
  finishAction_(FinishAction::none)
{
  qDebug() << "Entering "<< Q_FUNC_INFO;

  setWindowTitle(tr("User Interface for Process Flowsheeting"));

  mySettings_ = new Settings;

#ifdef Q_OS_MAC
  // to make sure the PNG files are found by the SVG renderer
  QDir::setCurrent(mySettings_->homePath());
#endif

  newAction_ = new QAction(tr("&New"), this);
  newAction_->setStatusTip(tr("Create a new case"));
  newAction_->setIcon(QIcon(":/images/document-new.png"));
  connect(newAction_, SIGNAL(triggered()), this, SLOT(new_()));

  openAction_ = new QAction(tr("&Open"), this);
  openAction_->setStatusTip(tr("Open an existing case"));
  openAction_->setIcon(QIcon(":/images/document-open.png"));
  connect(openAction_, SIGNAL(triggered()), this, SLOT(open_()));

  saveAction_ = new QAction(tr("&Save as"), this);
  saveAction_->setStatusTip(tr("Save the current case with a different description"));
  saveAction_->setIcon(QIcon(":/images/document-save-as.png"));
  saveAction_->setEnabled(false);
  connect(saveAction_, SIGNAL(triggered()), this, SLOT(save_()));

  deleteAction_ = new QAction(tr("&Delete"), this);
  deleteAction_->setStatusTip(tr("Delete an existing case"));
  deleteAction_->setIcon(QIcon(":/images/edit-delete.png"));
  deleteAction_->setEnabled(false);
  connect(deleteAction_, SIGNAL(triggered()), this, SLOT(delete_()));

  purgeAction_ = new QAction(tr("&Purge"), this);
  purgeAction_->setStatusTip(tr("Delete all existing cases"));
  purgeAction_->setIcon(QIcon(":/images/edit-clear.png"));
  connect(purgeAction_, SIGNAL(triggered()), this, SLOT(purge_()));

  exitAction_ = new QAction(tr("&Exit"), this);
  exitAction_->setStatusTip(tr("Quit the program"));
  exitAction_->setIcon(QIcon(":/images/exit.png"));
  exitAction_->setIconVisibleInMenu(true);
  connect(exitAction_, SIGNAL(triggered()), this, SLOT(close()));

  //undo redo actions and stack
  undoStack_ = new QUndoStack();

  //undoAction_ = undoStack_->createUndoAction(this, tr("&Undo"));
  undoAction_ = new QAction(tr("&Undo"), this);
  connect(undoAction_, SIGNAL(triggered()), undoStack_, SLOT(undo()));
  connect(undoStack_, SIGNAL(undoTextChanged(const QString &)), this, SLOT(undoTextChanged(const QString &)));
  connect(undoStack_, SIGNAL(canUndoChanged(bool)), undoAction_, SLOT(setEnabled(bool)));
  connect(undoStack_, SIGNAL(canUndoChanged(bool)), this, SLOT(dataChanged(bool)));
  undoAction_->setStatusTip(tr("Undo last change"));
  undoAction_->setIcon(QIcon(":/images/edit-undo.png"));
  undoAction_->setShortcut(QString("Ctrl+Z"));
  undoAction_->setIconVisibleInMenu(true);
  undoAction_->setEnabled(false);

  //redoAction_ = undoStack_->createRedoAction(this, tr("&Redo"));
  redoAction_ = new QAction(tr("&Redo"), this);
  connect(redoAction_, SIGNAL(triggered()), undoStack_, SLOT(redo()));
  connect(undoStack_, SIGNAL(redoTextChanged(const QString &)), this, SLOT(redoTextChanged(const QString &)));
  connect(undoStack_, SIGNAL(canRedoChanged(bool)), redoAction_, SLOT(setEnabled(bool)));
  redoAction_->setStatusTip(tr("Redo last change"));
  redoAction_->setIcon(QIcon(":/images/edit-redo.png"));
  redoAction_->setShortcut(QString("Ctrl+Y"));
  redoAction_->setIconVisibleInMenu(true);
  redoAction_->setEnabled(false);

  //create contextmenu
  acopyAction_ = new QAction(tr("&Copy"), this);
  acopyAction_->setStatusTip(tr("Copy table region"));
  acopyAction_->setIcon(QIcon(":/images/edit-copy.png"));
  acopyAction_->setShortcut(QString("Ctrl+C"));
  acopyAction_->setIconVisibleInMenu(true);
  acopyAction_->setEnabled(true);
  connect(acopyAction_, SIGNAL(triggered()), this, SLOT(acopy_()));

  aselectallAction_ = new QAction(tr("&Select All"), this);
  aselectallAction_->setStatusTip(tr("Select entire table"));
  aselectallAction_->setIcon(QIcon(":/images/edit-select-all.png"));
  aselectallAction_->setShortcut(QString("Ctrl+A"));
  aselectallAction_->setIconVisibleInMenu(true);
  aselectallAction_->setEnabled(true);
  connect(aselectallAction_, SIGNAL(triggered()), this, SLOT(aselectAll_()));

  resetAction_ = new QAction(tr("&Reset"), this);
  resetAction_->setStatusTip(tr("Reset the case with its original results"));
  resetAction_->setIcon(QIcon(":/images/media-skip-backward.png"));
  resetAction_->setEnabled(false);
  connect(resetAction_, SIGNAL(triggered()), this, SLOT(reset_()));

  calculateAction_ = new QAction(tr("&Calculate"), this);
  calculateAction_->setStatusTip(tr("Launch calculation"));
  calculateAction_->setIcon(QIcon(":/images/media-playback-start.png"));
  calculateAction_->setEnabled(false);
  connect(calculateAction_, SIGNAL(triggered()), this, SLOT(calculate_()));

  homotopyAction_ = new QAction(tr("&Homotopy"), this);
  homotopyAction_->setStatusTip(tr("Move smoothly to a different solution"));
  homotopyAction_->setIcon(QIcon(":/images/go-homotopy.png"));
  homotopyAction_->setEnabled(false);
  connect(homotopyAction_, SIGNAL(triggered()), this, SLOT(homotopy()));

  stopAction_ = new QAction(tr("&Stop"), this);
  stopAction_->setStatusTip(tr("Stop the calculation"));
  stopAction_->setIcon(QIcon(":/images/media-playback-stop.png"));
  stopAction_->setEnabled(false);
  connect(stopAction_, SIGNAL(triggered()), this, SLOT(stop_()));

  aboutAction_ = new QAction(tr("&About"), this);
  aboutAction_->setStatusTip(tr("Show the application's About box"));
  aboutAction_->setIcon(QIcon(":data/128x128/uipf.png"));
  connect(aboutAction_, SIGNAL(triggered()), this, SLOT(about()));

  aboutQtAction_ = new QAction(tr("About &Qt"), this);
  aboutQtAction_->setStatusTip(tr("Show the Qt library's About box"));
  aboutQtAction_->setIcon(QIcon(":/images/QT.png"));
  connect(aboutQtAction_, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

  odsAction_ = new QAction(tr("&ODS stream results"), this);
  odsAction_->setStatusTip(tr("Open stream results for last run in OpenDocument Spreadsheet format"));
  odsAction_->setIcon(QIcon(":/images/ods.png"));
  connect(odsAction_, SIGNAL(triggered()), this, SLOT(openOds()));
  odsAction_->setEnabled(false);

  xlsAction_ = new QAction(tr("&XLS stream results"), this);
  xlsAction_->setStatusTip(tr("Open stream results for last run in Microsoft Excel Spreadsheet format"));
  xlsAction_->setIcon(QIcon(":/images/xls.png"));
#ifdef Q_OS_WIN
  connect(xlsAction_, SIGNAL(triggered()), this, SLOT(openXls()));
#endif
  xlsAction_->setEnabled(false);

  dumpTxtAction_ = new QAction(tr("&TXT results"), this);
  dumpTxtAction_->setStatusTip(tr("Open a text file with all results for the current node"));
  dumpTxtAction_->setIcon(QIcon(":/images/text-x-generic.png"));
  dumpTxtAction_->setEnabled(false);
  connect(dumpTxtAction_, SIGNAL(triggered()), this, SLOT(dumpTxt_()));

  dumpHtmlAction_ = new QAction(tr("&HTML results"), this);
  dumpHtmlAction_->setStatusTip(tr("Export all results for the current node in HTML format"));
  dumpHtmlAction_->setIcon(QIcon(":/images/text-html.png"));
  dumpHtmlAction_->setEnabled(false);
  connect(dumpHtmlAction_, SIGNAL(triggered()), this, SLOT(dumpHtml_()));

  selectKernelAction_ = new QAction(tr("&Select kernel"), this);
  selectKernelAction_->setStatusTip(tr("Select active kernel"));
  selectKernelAction_->setIcon(QIcon(":/images/emblem-system.png"));
  selectKernelAction_->setEnabled(true);

  expandAction_ = new QAction(tr("&Expand all"), this);
  expandAction_->setStatusTip(tr("Expand all objects"));
  expandAction_->setIcon(QIcon(":/images/expand_all.png"));
  expandAction_->setEnabled(false);

  collapseAction_ = new QAction(tr("&Collapse all"), this);
  collapseAction_->setStatusTip(tr("Collapse all objects"));
  collapseAction_->setIcon(QIcon(":/images/collapse_all.png"));
  collapseAction_->setEnabled(false);

  rootAction_ = new QAction(tr("&Root"), this);
  rootAction_->setStatusTip(tr("Go to root object"));
  rootAction_->setIcon(QIcon(":/images/go-home.png"));
  rootAction_->setEnabled(false);

  upAction_ = new QAction(tr("&Up"), this);
  upAction_->setStatusTip(tr("Go up one level"));
  upAction_->setIcon(QIcon(":/images/go-up.png"));
  upAction_->setEnabled(false);

  toggleTC_ = new QAction(tr("&Toggle child"), this);
  toggleTC_->setStatusTip(tr("Toggle visualization of child items"));
  toggleTC_->setIcon(QIcon(":/images/lens.png"));
  toggleTC_->setEnabled(false);

  sensitivityAnalysisAction_ = new QAction(tr("&Sensitivity analysis"), this);
  sensitivityAnalysisAction_->setStatusTip(tr("Launch multi-dimensional sensitivity analysis"));
  sensitivityAnalysisAction_->setIcon(QIcon(":/images/applications-games.png"));
  sensitivityAnalysisAction_->setEnabled(false);
  connect(sensitivityAnalysisAction_, SIGNAL(triggered()), this, SLOT(sensitivity_()));

  sensitivityOpenAction_ = new QAction(tr("&Open multi-dimensional sensitivity"), this);
  sensitivityOpenAction_->setStatusTip(tr("Opens an existing multidimensional sensitivity analysis"));
  sensitivityOpenAction_->setIcon(QIcon(":/images/document-open.png"));
  sensitivityOpenAction_->setEnabled(false);
  connect(sensitivityOpenAction_, SIGNAL(triggered()), this, SLOT(sensitivityOpen()));

  sensitivityRestoreAction_ = new QAction(tr("&Restore last run multi-dimensional sensitivity"), this);
  sensitivityRestoreAction_->setStatusTip(tr("Restores the last sensitivity that was executed"));
  sensitivityRestoreAction_->setIcon(QIcon(":/images/edit-redo.png"));
  sensitivityRestoreAction_->setEnabled(false);
  connect(sensitivityRestoreAction_, SIGNAL(triggered()), this, SLOT(sensitivityRestore()));

  activateAction_ = new QAction(tr("&Activate"), this);
  activateAction_->setStatusTip(tr("Activate LIBPF"));
  activateAction_->setIcon(QIcon(":/images/system-lock-screen.png"));
  activateAction_->setEnabled(true);
  connect(activateAction_, SIGNAL(triggered()), this, SLOT(activate_()));

  siUnitAction_ = new QAction(tr("&SI"), this);
#if defined(Q_OS_MAC) || defined(Q_OS_UNIX) || (QT_VERSION >= 0x050000) || (_MSC_VER >= 1700)
  siUnitAction_->setStatusTip(QString::fromWCharArray(L"Syst\x00E8me international"));
#else
  siUnitAction_->setStatusTip(QString::fromUtf16(L"Syst\x00E8me international"));
#endif
  siUnitAction_->setEnabled(true);
  siUnitAction_->setCheckable(true);

  enUnitAction_ = new QAction(tr("&EN"), this);
  enUnitAction_->setStatusTip(tr("U.S. Customary Units"));
  enUnitAction_->setEnabled(true);
  enUnitAction_->setCheckable(true);

  engUnitAction_ = new QAction(tr("&Eng"), this);
  engUnitAction_->setStatusTip(tr("Engineering Units"));
  engUnitAction_->setEnabled(true);
  engUnitAction_->setCheckable(true);

  unitGroup_ = new QActionGroup(this);
  unitGroup_->addAction(siUnitAction_);
  unitGroup_->addAction(enUnitAction_);
  unitGroup_->addAction(engUnitAction_);
  QString units = mySettings_->units();
  if (units == "EN") {
    enUnitAction_->setChecked(true);
  } else if (units == "Eng") {
    engUnitAction_->setChecked(true);
  } else {
    siUnitAction_->setChecked(true);
  }
  connect(unitGroup_, SIGNAL(triggered(QAction *)), this, SLOT(setUnit_(QAction *)));

  clearmsgAction_ = new QAction(tr("&Clear"), this);
  connect(clearmsgAction_, SIGNAL(triggered()), this, SLOT(clear_()));

  // actions that should be enabled when a case is loaded
  enableLoadedActions_.push_back(resetAction_);
  enableLoadedActions_.push_back(calculateAction_);
  enableLoadedActions_.push_back(homotopyAction_);
  enableLoadedActions_.push_back(saveAction_);
  enableLoadedActions_.push_back(dumpTxtAction_);
  enableLoadedActions_.push_back(dumpHtmlAction_);
  enableLoadedActions_.push_back(deleteAction_);
  enableLoadedActions_.push_back(odsAction_);
  enableLoadedActions_.push_back(xlsAction_);
  enableLoadedActions_.push_back(expandAction_);
  enableLoadedActions_.push_back(collapseAction_);
  enableLoadedActions_.push_back(rootAction_);
  enableLoadedActions_.push_back(upAction_);
  enableLoadedActions_.push_back(toggleTC_);
  enableLoadedActions_.push_back(sensitivityRestoreAction_);
  enableLoadedActions_.push_back(sensitivityOpenAction_);
  enableLoadedActions_.push_back(sensitivityAnalysisAction_);

  // actions that are normally always enabled but that should be disabled when a case is running
  disableRunningActions_.push_back(newAction_);
  disableRunningActions_.push_back(openAction_);
  disableRunningActions_.push_back(purgeAction_);
  disableRunningActions_.push_back(activateAction_);
  disableRunningActions_.push_back(siUnitAction_);
  disableRunningActions_.push_back(enUnitAction_);
  disableRunningActions_.push_back(engUnitAction_);
  disableRunningActions_.push_back(clearmsgAction_);
  disableRunningActions_.push_back(selectKernelAction_);

  // actions that are normally always disabled but that should be enabled when a case is running
  enableRunningActions_.push_back(stopAction_);

  menuCase_ = menuBar()->addMenu(tr("&Case"));
  menuCase_->addAction(newAction_);
  menuCase_->addAction(openAction_);
  menuCase_->addAction(saveAction_);
  menuCase_->addAction(deleteAction_);
  menuCase_->addAction(purgeAction_);
  menuCase_->addAction(exitAction_);

  menuEdit_ = menuBar()->addMenu(tr("&Edit"));
  menuEdit_->addAction(undoAction_);
  menuEdit_->addAction(redoAction_);
  menuEdit_->addSeparator();
  menuEdit_->addAction(acopyAction_);
  menuEdit_->addAction(aselectallAction_);

  menuRun_ = menuBar()->addMenu(tr("&Run"));
  menuRun_->addAction(resetAction_);
  menuRun_->addAction(calculateAction_);
  menuRun_->addAction(homotopyAction_);
  menuRun_->addAction(stopAction_);

  menuView_ = menuBar()->addMenu(tr("&View"));
  menuView_->addAction(expandAction_);
  menuView_->addAction(collapseAction_);
  menuView_->addAction(rootAction_);
  menuView_->addAction(upAction_);
  menuView_->addSeparator();
  menuView_->addAction(odsAction_);
#ifdef Q_OS_WIN
  menuView_->addAction(xlsAction_);
#endif
  menuView_->addAction(dumpTxtAction_);
  menuView_->addAction(dumpHtmlAction_);

  menuTools_ = menuBar()->addMenu(tr("&Tools"));
  menuTools_->addAction(sensitivityRestoreAction_);
  menuTools_->addAction(sensitivityOpenAction_);
  menuTools_->addAction(sensitivityAnalysisAction_);

  languageDefault_ = new QAction(tr("Default"), this);
  languageDefault_->setIcon(QIcon(":/images/face-smile.png"));
  connect(languageDefault_, SIGNAL(triggered()), this, SLOT(setLanguageDefault()));

  languageAr_ = new QAction(tr("Arabic"), this);
  languageAr_->setIcon(QIcon(":/images/ar.png"));
  connect(languageAr_, SIGNAL(triggered()), this, SLOT(setLanguageAr()));

  languageDe_ = new QAction(tr("German"), this);
  languageDe_->setIcon(QIcon(":/images/de.png"));
  connect(languageDe_, SIGNAL(triggered()), this, SLOT(setLanguageDe()));

  languageEs_ = new QAction(tr("Spanish"), this);
  languageEs_->setIcon(QIcon(":/images/es.png"));
  connect(languageEs_, SIGNAL(triggered()), this, SLOT(setLanguageEs()));

  languageEn_ = new QAction(tr("English"), this);
  languageEn_->setIcon(QIcon(":/images/en.png"));
  connect(languageEn_, SIGNAL(triggered()), this, SLOT(setLanguageEn()));

  languageFr_ = new QAction(tr("French"), this);
  languageFr_->setIcon(QIcon(":/images/fr.png"));
  connect(languageFr_, SIGNAL(triggered()), this, SLOT(setLanguageFr()));

  languageHe_ = new QAction(tr("Hebrew"), this);
  languageHe_->setIcon(QIcon(":/images/he.png"));
  connect(languageHe_, SIGNAL(triggered()), this, SLOT(setLanguageHe()));

  languageIt_ = new QAction(tr("Italian"), this);
  languageIt_->setIcon(QIcon(":/images/it.png"));
  connect(languageIt_, SIGNAL(triggered()), this, SLOT(setLanguageIt()));

  languageJp_ = new QAction(tr("Japanese"), this);
  languageJp_->setIcon(QIcon(":/images/jp.png"));
  connect(languageJp_, SIGNAL(triggered()), this, SLOT(setLanguageJp()));

  languageKo_ = new QAction(tr("Korean"), this);
  languageKo_->setIcon(QIcon(":/images/ko.png"));
  connect(languageKo_, SIGNAL(triggered()), this, SLOT(setLanguageKo()));

  languagePt_ = new QAction(tr("Portuguese"), this);
  languagePt_->setIcon(QIcon(":/images/pt.png"));
  connect(languagePt_, SIGNAL(triggered()), this, SLOT(setLanguagePt()));

  languageRu_ = new QAction(tr("Russian"), this);
  languageRu_->setIcon(QIcon(":/images/ru.png"));
  connect(languageRu_, SIGNAL(triggered()), this, SLOT(setLanguageRu()));

  languageZh_ = new QAction(tr("Chinese (simplified)"), this);
  languageZh_->setIcon(QIcon(":/images/zh.png"));
  connect(languageZh_, SIGNAL(triggered()), this, SLOT(setLanguageZh()));

  menuSettings_ = menuBar()->addMenu(tr("&Settings"));
  menuSettings_->addAction(toggleTC_);
  menuSettings_->addAction(selectKernelAction_);
  subMenuUnits_ = menuSettings_->addMenu(QIcon(":/images/applications-accessories.png"), tr("&Units"));
  subMenuUnits_->setToolTip(tr("Change units of measurements"));
  subMenuUnits_->addAction(siUnitAction_);
  subMenuUnits_->addAction(enUnitAction_);
  subMenuUnits_->addAction(engUnitAction_);
  menuSettings_->addAction(activateAction_);

  subMenuLanguages_ = menuSettings_->addMenu(QIcon(":/images/language-icon.png"), tr("Language"));
  subMenuLanguages_->setToolTip(tr("Change User Interface language"));
  subMenuLanguages_->addAction(languageDefault_);
  subMenuLanguages_->addSeparator();
  subMenuLanguages_->addAction(languageAr_);
  subMenuLanguages_->addAction(languageDe_);
  subMenuLanguages_->addAction(languageEn_);
  subMenuLanguages_->addAction(languageEs_);
  subMenuLanguages_->addAction(languageFr_);
  subMenuLanguages_->addAction(languageHe_);
  subMenuLanguages_->addAction(languageIt_);
  subMenuLanguages_->addAction(languageKo_);
  subMenuLanguages_->addAction(languageJp_);
  subMenuLanguages_->addAction(languagePt_);
  subMenuLanguages_->addAction(languageRu_);
  subMenuLanguages_->addAction(languageZh_);

  menuHelp_ = menuBar()->addMenu(tr("&Help"));
  menuHelp_->addAction(aboutAction_);
  menuHelp_->addAction(aboutQtAction_);

  toolbarMain_ = addToolBar(tr("Main toolbar"));
  toolbarMain_->addAction(newAction_);
  toolbarMain_->addAction(openAction_);
  toolbarMain_->addSeparator();
  toolbarMain_->addAction(calculateAction_);
  toolbarMain_->addAction(stopAction_);
  toolbarMain_->addSeparator();
  toolbarMain_->addAction(expandAction_);
  toolbarMain_->addAction(collapseAction_);
  toolbarMain_->addAction(rootAction_);
  toolbarMain_->addAction(upAction_);

  toolbarMain_->addSeparator();
  toolbarMain_->addAction(odsAction_);
#ifdef Q_OS_WIN
  toolbarMain_->addAction(xlsAction_);
#endif

  vsplitter_ = new QSplitter(Qt::Vertical);
  hsplitter_ = new QSplitter(Qt::Horizontal);

  messages_ = new QTextEdit();
  messages_->setReadOnly(true);
  messages_->addAction(clearmsgAction_);
  //  messages_->setContextMenuPolicy(Qt::ActionsContextMenu);
  messages_->setContextMenuPolicy(Qt::DefaultContextMenu);

#if (QT_VERSION >= 0x050200)
  // http://doc.qt.io/qt-5/qfontdatabase.html#details
  // the enum QFontDatabase::​SystemFont was introduced or modified in Qt 5.2.
  const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
#else
  QFont fixedFont("Monotype");
  fixedFont.setStyleHint(QFont::TypeWriter);
#endif
  messages_->setCurrentFont(fixedFont);

#if defined(Q_OS_MAC)
  messages_->setFontPointSize(12);
#else
  messages_->setFontPointSize(9);
#endif

  if (highlight_)
    messages_->setAcceptRichText(true);
#if defined(Q_OS_WIN) && (!defined(SQLITE))
  tablewInput_ = new WidgetTableDouble(" AND Q.input = true", undoStack_);
  tablewOutput_ = new WidgetTableDouble(" AND Q.output = true", undoStack_);
#else
  tablewInput_ = new WidgetTableDouble(" AND Q.input = \'1\'", undoStack_);
  tablewOutput_ = new WidgetTableDouble(" AND Q.output = \'1\'", undoStack_);
#endif
  tablewInput_->setRw(true);
  tablewOutput_->setRw(false);

  tablewMessages_ = new WidgetTableMessages;

  treew_ = new WidgetTree;
  connect(treew_, SIGNAL(gone(int)), tablewInput_, SLOT(go(int)));
  connect(treew_, SIGNAL(gone(int)), tablewOutput_, SLOT(go(int)));
  connect(treew_, SIGNAL(gone(int)), tablewMessages_, SLOT(go(int)));
  connect(treew_, SIGNAL(gone(int)), this, SLOT(showMessage(int)));
  connect(treew_, SIGNAL(gone(int)), this, SLOT(setUnit_(int)));
  connect(treew_, SIGNAL(showPfd(int)), this, SLOT(showPfd(int)));
  svgw_ = new ScrollAreaSvg();
  connect(svgw_, SIGNAL(select(int)), treew_, SLOT(forceGo(int)));
  connect(tablewInput_, SIGNAL(forceGo(int)), this, SLOT(forceGoTable(int)));
  connect(expandAction_, SIGNAL(triggered()), treew_, SLOT(expandAll()));
  connect(expandAction_, SIGNAL(triggered()), this, SLOT(clearStatusMessage()));
  connect(collapseAction_, SIGNAL(triggered()), treew_, SLOT(collapseAll()));
  connect(collapseAction_, SIGNAL(triggered()), this, SLOT(clearStatusMessage()));
  connect(rootAction_, SIGNAL(triggered()), treew_, SLOT(go()));
  connect(rootAction_, SIGNAL(triggered()), this, SLOT(clearStatusMessage()));
  connect(upAction_, SIGNAL(triggered()), treew_, SLOT(goUp()));
  connect(upAction_, SIGNAL(triggered()), this, SLOT(clearStatusMessage()));
  connect(toggleTC_, SIGNAL(triggered()), this, SLOT(toggleChild()));
  connect(selectKernelAction_, SIGNAL(triggered()), this, SLOT(selectkernel()));

  toggleTC_->setEnabled(true);

  tabs_ = new QTabWidget();
  // @see Tab::index
  tabs_->addTab(tablewInput_, tr("Inputs"));
  tabs_->addTab(tablewOutput_, tr("Outputs"));
  tabs_->addTab(tablewMessages_, tr("Messages"));
  tabs_->addTab(svgw_, "PFD");
  tabs_->setTabEnabled(Tab::pfd, false);

  hsplitter_->addWidget(treew_);
  hsplitter_->addWidget(tabs_);

  vsplitter_->addWidget(hsplitter_);
  vsplitter_->addWidget(messages_);

  connect(menuEdit_, SIGNAL(aboutToShow()), this, SLOT(editShowMenuClicked()));
  connect(menuEdit_, SIGNAL(aboutToHide()), this, SLOT(editHideMenuClicked()));

  //get horizontal header table input
  tblInHorzHeader_ = tablewInput_->view->horizontalHeader();
  tblInHorzHeader_->setContextMenuPolicy(Qt::CustomContextMenu);       //set contextmenu
  connect(tblInHorzHeader_, SIGNAL(customContextMenuRequested(const QPoint &)),    //connect horizontal header table output with contextmenu
          this, SLOT(tablewInputCustomContextMenu(const QPoint &)));

  //get horizontal header table output
  tblOutHorzHeader_ = tablewOutput_->view->horizontalHeader();
  tblOutHorzHeader_->setContextMenuPolicy(Qt::CustomContextMenu);       //set contextmenu
  connect(tblOutHorzHeader_, SIGNAL(customContextMenuRequested(const QPoint &)),    //connect horizontal header table output with contextmenu
          this, SLOT(tablewOutputCustomContextMenu(const QPoint &)));

  //get vertical header table input
  tblInVertHeader_ = tablewInput_->view->verticalHeader();
  tblInVertHeader_->setContextMenuPolicy(Qt::CustomContextMenu);       //set contextmenu
  connect(tblInVertHeader_, SIGNAL(customContextMenuRequested(const QPoint &)),    //connect horizontal header table output with contextmenu
          this, SLOT(tablewInputCustomContextMenu(const QPoint &)));

  //get vertical header table output
  tblOutVertHeader_ = tablewOutput_->view->verticalHeader();
  tblOutVertHeader_->setContextMenuPolicy(Qt::CustomContextMenu);       //set contextmenu
  connect(tblOutVertHeader_, SIGNAL(customContextMenuRequested(const QPoint &)),    //connect horizontal header table output with contextmenu
          this, SLOT(tablewOutputCustomContextMenu(const QPoint &)));

  connect(tablewInput_->view, SIGNAL(customContextMenuRequested(const QPoint &)),    //connect table input with contextmenu
          this, SLOT(tablewInputCustomContextMenu(const QPoint &)));
  connect(tablewOutput_->view, SIGNAL(customContextMenuRequested(const QPoint &)),    //connect table output with contextmenu
          this, SLOT(tablewOutputCustomContextMenu(const QPoint &)));

  connect(tablewInput_->view, SIGNAL(clicked(const QModelIndex &)), this,       //connect table input item clicked with contextmenu
          SLOT(checkInContextMenu(const QModelIndex &)));
  connect(tablewOutput_->view, SIGNAL(clicked(const QModelIndex &)), this,      //connect table output item clicked with contextmenu
          SLOT(checkOutContextMenu(const QModelIndex &)));

  setCentralWidget(vsplitter_);

  h_ = new Hierarchy(typesFile().toStdString());

//  od_ = new DialogOpen(this);
//  connect(od_, SIGNAL(openExisting(const QString &, int)), this, SLOT(openExisting(const QString &, int)));

//  nd_ = new DialogNew(this);
//  buildNewDialog();
//  connect(nd_, SIGNAL(open(const QString &, const QString &, const QString &)), this, SLOT(openNew(const QString &, const QString &, const QString &)));

  process_ = new QProcess(this);
  connect(process_, SIGNAL(readyReadStandardOutput()), this, SLOT(updateLog()));
  connect(process_, SIGNAL(readyReadStandardError()), this, SLOT(updateLogErr()));
  connect(process_, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(finished(int, QProcess::ExitStatus)));
  connect(process_, SIGNAL(started()), this, SLOT(started()));
  connect(process_, SIGNAL(error(QProcess::ProcessError)), this, SLOT(errorProcess(QProcess::ProcessError)));
  loggingProcess_ = NULL;

  setWindowIcon(QIcon(":data/128x128/uipf.png"));

  statusMessage_ = new QLabel(statusBar());
  stateMessage_ = new QLabel(statusBar());
  flag_ = new QLabel(statusBar());
  pix_ = new QPixmap(statusBar()->height() / 2, statusBar()->height() / 2);
  flag_->setPixmap(*pix_);
  flag_->setToolTip(tr("Status indicator"));

  statusBar()->addPermanentWidget(statusMessage_);
  statusBar()->addPermanentWidget(stateMessage_);
  statusBar()->addPermanentWidget(flag_);

  setState(State::empty);
  showMessage(tr("Ready"));
  qDebug() << "Exiting "<< Q_FUNC_INFO;
} // WindowMain::WindowMain

WindowMain::~WindowMain(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  delete mySettings_;
  delete undoStack_;
  delete h_;
} // WindowMain::~WindowMain

void WindowMain::showMessage(const QString &text) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  statusMessage_->setText(text);
  
  QColor c = messages_->textColor();
  messages_->setTextColor(Qt::blue);
  messages_->append(text);
  messages_->setTextColor(c);
  qDebug() << text;
} // WindowMain::showMessage

 void WindowMain::clearStatusMessage(void) {
   qDebug() << "Entering "<< Q_FUNC_INFO;
   showMessage("");
 } // WindowMain::clearStatusMessage
 
void WindowMain::showMessage(int i) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString text = tr("Opened node %1").arg(i);
  showMessage(text);
} // WindowMain::showMessage
 
void WindowMain::started(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  showMessage(tr("The process has been started successfully"));
  setState(State::running);
} // WindowMain::started

void WindowMain::errorProcess(QProcess::ProcessError e) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  switch (e) {
    case QProcess::FailedToStart : showMessage(tr("Impossible to start the kernel")); break;
    case QProcess::Crashed : showMessage(tr("The kernel started successfully but crashed.")); break;
    case QProcess::Timedout : showMessage(tr("Time out")); break;
    case QProcess::WriteError : showMessage(tr("Write error to kernel")); break;
    case QProcess::ReadError : showMessage(tr("Read error to kernel")); break;
    case QProcess::UnknownError : showMessage(tr("Unknown error in kernel")); break;
  } // switch or error code
} // WindowMain::errorProcess

// todo: unify the three run methods
void WindowMain::run(QString command, QString arg) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  messages_->clear();
  QStringList args;
  args << command << arg;
  startKernel(args, FinishAction::load);
} // WindowMain::run

void WindowMain::run(QString command, QString arg1, QString arg2) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  messages_->clear();
  QStringList args;
  args << command << arg1 << arg2;
  startKernel(args, FinishAction::load);
} // WindowMain::run

void WindowMain::run(QString command, QString arg1, QString arg2, QString arg3) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  messages_->clear();
  QStringList args;
  args << command << arg1 << arg2 << arg3;
  startKernel(args, FinishAction::load);
} // WindowMain::run

void WindowMain::updateLog(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (loggingProcess_ == NULL)
    return;
  if (highlight_) {
    QByteArray bytes = loggingProcess_->readAllStandardOutput();
    QStringList lines = QString(bytes).split("\n");
    if (lines.size() != 0) {
      foreach(QString line, lines) {
        if (!line.isEmpty()) {
          if (line.contains(" . Iter")) {
            QString msg("<span style=\"background-color: yellow\">");
            msg += line;
            msg += "</span>";
            messages_->insertHtml(msg);
          } else if (line.contains("solve")) {
            QString msg("<span style=\"background-color: green\">");
            msg += line;
            msg += "</span>";
            messages_->insertHtml(msg);
          } else {
            messages_->insertHtml(line);
          }
          messages_->insertHtml("<br>");
        }
      }
    }
    messages_->ensureCursorVisible();
  } else {
    QByteArray bytes = loggingProcess_->readAllStandardOutput();
    QStringList lines = QString(bytes).split("\n");
    if (lines.size() != 0)
      foreach(QString line, lines)
      if (!line.isEmpty())
        if (line.contains(" * ")) {
          messages_->setTextColor(Qt::black);
          messages_->append(line.trimmed());
        }
    /*
        QString line = loggingProcess_->readAllStandardOutput().trimmed();
        if (!line.isEmpty())
          messages_->append(line);
     */
  }
} // WindowMain::updateLog

void WindowMain::updateLogErr(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (loggingProcess_ == NULL)
    return;
  messages_->setTextColor(Qt::red);
  //QPalette palette = messages_->palette();
  //palette.setColor(QPalette.Active, QPalette.Base, QColor(50, 50, 50));
  //messages_->setPalette(palette);
  //QString msg("<span style=\"background-color: red; color: white\">");
  //msg += loggingProcess_->readAllStandardError();
  //msg += "<\/span>";
  //messages_->insertHtml(msg);
  messages_->append(QString(loggingProcess_->readAllStandardError()));
  messages_->setTextColor(Qt::black);
} // WindowMain::updateLogErr

void WindowMain::finished(int exitCode, QProcess::ExitStatus) {
  qDebug() << "Entering "<< Q_FUNC_INFO;

#if defined(Q_OS_MAC) || defined(Q_OS_UNIX)
  FILE *idfile(NULL);
  QString idfilepath = mySettings_->homePath();
  idfilepath += "/id.txt";
  idfile = fopen(idfilepath.toStdString().c_str(), "rt");
  int rt = fscanf(idfile, "%d\n", &exitCode);
  if ((rt == EOF) || (rt != 1))
    exitCode = -10;
  fclose(idfile);
#else
  // force clearing the ODBC driver cache on Windows
  QSqlDatabase db = QSqlDatabase::database("persistency");
  db.close();
#endif
  loggingProcess_ = NULL;
  showMessage(tr("The process has exited with return code %1").arg(exitCode));
    
  switch (finishAction_.v_) {
  case FinishAction::load:
    // load possibly new case based on kernel return code
    qDebug() << "case FinishAction::load";
    if (exitCode < 0) {
      resetState();
      setState(State::invalid);
    } else {
      if (exitCode == treew_->rootNode()) {
        treew_->refresh();
      } else {
        currentCase_ = tentativeCase_;        
        undoStack_->clear();
        treew_->setRootnode(exitCode);
        treew_->forceGo(exitCode);
      } // refresh or load
      if (treew_->errors() > 0)
        setState(State::errors);
      else if (treew_->warnings() > 0)
        setState(State::warnings);
      else
        setState(State::ok);
    } // successful run
    break;
  case FinishAction::openTxt:
    // open the text file matching the current object
    qDebug() << "case FinishAction::openTxt";
    openTxtDump_();
    resetState();
    break;
  case FinishAction::openHtml:
    // open the HTML file matching the current object
    qDebug() << "case FinishAction::openHtml";
    openHtmlDump_();
    resetState();
    break;
  case FinishAction::success:
    // report successful operation
    qDebug() << "case FinishAction::success";
    showMessage(tr("The operation completed successfully"));
    resetState();
    break;
  case FinishAction::cleanup:
    // clear all data
    qDebug() << "case FinishAction::cleanup";
    treew_->setRootnode(-1); // clear the root node
    clear(false);
    undoStack_->clear(); 
    setState(State::empty);
    break;
  case FinishAction::none:
    qDebug() << "case FinishAction::none";
    showMessage(tr("No action"));
    break;
  default:
    qDebug() << "case default";
    showMessage(tr("Unknown action"));
  } // switch upon FinishAction
 } // WindowMain::finished

void WindowMain::openNew(const QString &type, const QString &tag, const QString &description) {
  qDebug() << "Entering "<< Q_FUNC_INFO << " with type = " << type << ", tag = " << tag << ", description = " << description;
  tentativeCase_ = type.toStdString();
  showMessage(tr("Opening a new object of type %1").arg(type));
  run("newtd", type, tag, description);
} // WindowMain::openNew

void WindowMain::openExisting(const QString &type, int cid) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (treew_->rootNode() == cid) {
    showMessage(tr("Case %1 is already loaded").arg(cid));
  } else {
    currentCase_ = type.toStdString();
    setState(State::running);
    showMessage(tr("Opening the existing case %1").arg(cid));
    treew_->setRootnode(cid);
    treew_->forceGo(cid);
    showMessage(tr("Opened the existing case %1").arg(cid));
    undoStack_->clear();
    if (treew_->errors() > 0)
      setState(State::errors);
    else if (treew_->warnings() > 0)
      setState(State::warnings);
    else
      setState(State::ok);
  }
} // WindowMain::openExisting

// SLOTS

void WindowMain::new_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  nd_ = new DialogNew(this);
  buildNewDialog();
  connect(nd_, SIGNAL(open(const QString &, const QString &, const QString &)), this, SLOT(openNew(const QString &, const QString &, const QString &)));
  nd_->exec();
  return;
} // WindowMain::new_

void WindowMain::open_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  od_ = new DialogOpen(this);
  connect(od_, SIGNAL(openExisting(const QString &, int)), this, SLOT(openExisting(const QString &, int)));
  od_->clear();
  od_->addItem();
  od_->exec();
  return;
} // WindowMain::open_

void WindowMain::save_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  dd_ = new DialogDuplicate(this);
  connect(dd_, SIGNAL(duplicate(const QString &, const QString &)), this, SLOT(duplicate(const QString &, const QString &)));
  dd_->exec();
  return;
} // WindowMain::save_

void WindowMain::duplicate(const QString &tag, const QString &description) {
  qDebug() << "Entering "<< Q_FUNC_INFO << " with tag = " << tag << ", description = " << description;
  showMessage(tr("Duplicating object %1").arg(treew_->rootNode()));
  QStringList args = QStringList() << "duplicate" << QString("%1").arg(treew_->rootNode());
  args << tag;
  args << description;
  startKernel(args, FinishAction::success);
} // WindowMain::save_

void WindowMain::dumpTxt_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  showMessage(tr("Writing TXT file for object %1").arg(treew_->currentNode()));
  QString type;
  //  type.append("\"");
  type.append(currentCase_.c_str());
  //  type.append("\"");
  QString id = QString("%1").arg(treew_->currentNode());
  QStringList args = QStringList() << "txt" << id;
  startKernel(args, FinishAction::openTxt);
} // WindowMain::dumpTxt_

void WindowMain::openTxtDump_(void){
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString id = QString("%1").arg(treew_->currentNode());
  QString dumpFileName("");
  dumpFileName += "" + id + ".txt";

  QString txt = mySettings_->homePath();
  txt += "/" + dumpFileName;
  showMessage(tr("Opening TXT file %1").arg(dumpFileName));
  if (QDesktopServices::openUrl(QUrl::fromLocalFile(txt)))
    showMessage(tr("TXT file %1 opened").arg(txt));
  else
    showMessage(tr("Error opening TXT file %1").arg(txt));
  return;
} // WindowMain::openTxtDump_

// based on: http://stackoverflow.com/questions/3227622/how-to-copy-a-image-file-in-qt
void copyFile(const QString& sourceFile, const QString& destinationDir) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QFileInfo fileInfo(sourceFile);
  if (!fileInfo.exists()) {
    qCritical() << "File " << sourceFile << " not found";
    return;
  }
  QString destinationFile = destinationDir + QDir::separator() + fileInfo.fileName();
  if (!QFile::copy(sourceFile, destinationFile)) {
    qCritical() << "Cannot copy the file " << fileInfo.fileName();
  }
} // copyFile

void WindowMain::dumpHtml_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  {
    QTemporaryFile tempfile;
    tempfile.open();
    tempFileName_ = tempfile.fileName();
  } // force tempfile out of scope so that it is deleted
  QStringList parts = tempFileName_.split("/");
  QString name = parts.value(parts.length() - 1);
  QDir tempdir(QDir::tempPath());
  tempdir.mkdir(name);
  QDir dir(tempFileName_);
  if (!dir.exists()) {
    qCritical() << "Cannot find the directory " << tempFileName_;
  } else {
    showMessage(tr("Writing HTML file for object %1 to directory %2").arg(treew_->currentNode()).arg(tempFileName_));
    QString id = QString("%1").arg(treew_->currentNode());
    QStringList args = QStringList() << "xml" << id << tempFileName_;

    startKernel(args, FinishAction::openHtml);

    copyFile(mySettings_->homePath() + "/body.xhtml", tempFileName_);
    copyFile(mySettings_->homePath() + "/css3tabs.css", tempFileName_);
    copyFile(mySettings_->homePath() + "/css3treetable.css", tempFileName_);
    copyFile(mySettings_->homePath() + "/flowsheet.xsl", tempFileName_);
    copyFile(mySettings_->homePath() + "/frameset.xsl", tempFileName_);
    copyFile(mySettings_->homePath() + "/header.css", tempFileName_);
    copyFile(mySettings_->homePath() + "/header.xhtml", tempFileName_);
    copyFile(mySettings_->homePath() + "/icons.png", tempFileName_);
    copyFile(mySettings_->homePath() + "/index.xhtml", tempFileName_);
    copyFile(mySettings_->homePath() + "/libpf_logo.png", tempFileName_);
    copyFile(mySettings_->homePath() + "/model.css", tempFileName_);
    copyFile(mySettings_->homePath() + "/model.xsl", tempFileName_);
    copyFile(mySettings_->homePath() + "/text.css", tempFileName_);
    copyFile(mySettings_->homePath() + "/toc.css", tempFileName_);
    copyFile(mySettings_->homePath() + "/toc.xml", tempFileName_);
  }
} // WindowMain::dumpHtml_

void WindowMain::openHtmlDump_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString html(tempFileName_);
  html += "/index.xhtml";
  showMessage(tr("Opening HTML file %1").arg(html));
  if (QDesktopServices::openUrl(QUrl::fromLocalFile(html)))
    showMessage(tr("HTML file %1 opened").arg(html));
  else
    showMessage(tr("Error opening HTML file %1").arg(html));
  return;
} // WindowMain::openHtmlDump_

void WindowMain::clear(bool clearmess) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  tablewInput_->clear();
  tablewOutput_->clear();
  tablewMessages_->clear();
  if (clearmess)
    messages_->clear();
  treew_->clear();
  svgw_->clear();
  tabs_->setTabEnabled(Tab::pfd, false);
} // WindowMain::clear

void WindowMain::sensitivityRestore(const QString& sensitivityFilePath, const QString &sensitivityName, const QString& sensitivityType) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // Setup the file path correctly, on an empty file path
  // we assume it's restoring the last ran analysis
  QString filePath;
  bool restoringLastRanAnalysis;
  if (sensitivityFilePath.isEmpty()) {
    filePath = mySettings_->homePath() + "/SensitivityInput.xml";
    restoringLastRanAnalysis = true;
  } else {
    filePath = sensitivityFilePath;
    restoringLastRanAnalysis = false;
  }

  // Setup the file type and name, if the parameters sensitivityName
  // or sensitivityType came in empty we try to fetch them from the
  // file name ourselves
  QString name = sensitivityName;
  QString type = sensitivityType;
  if (!restoringLastRanAnalysis && (sensitivityName.isEmpty() || sensitivityType.isEmpty())) {
    QRegExp rx("SensitivityInput_(.+)_(.+)\\.xml$");
    if (rx.indexIn(filePath) == -1) {
      QMessageBox::critical(this, tr("Error"), QString(tr("Couldn't parse sensitivity input file name '%1'")).arg(filePath.section("/", -1)));
      return;
    }
    if (name.isEmpty())
      name = rx.cap(2);
    if (type.isEmpty()) {
      type = rx.cap(1);
      type.replace("[", "<");
      type.replace("]", ">");
      type.replace("-", "_");
    }
  }

  // Load the sensitivity
  ActionLibpfSensitivity *goAction = new ActionLibpfSensitivity(this);
  tablewInput_->setRw(false);

  DialogSensitivity dlg(treew_->rootNode(), currentCase_.c_str(), goAction, unitSelection(), this);
  if (dlg.loadSensitivity(filePath, name, type))
    dlg.exec();
  else
    QMessageBox::critical(this, tr("Error"), QString(tr("Failed to load sensitivity input file '%1'")).arg(filePath));

  treew_->forceGo(treew_->rootNode());
  tablewInput_->setRw(true);

  delete goAction;
} // WindowMain::sensitivityRestore

void WindowMain::sensitivityOpen(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  DialogSensitivityOpen dlg(QString(treew_->type()), this);
  int r = dlg.exec();
  if (r == QDialog::Accepted)
    sensitivityRestore(dlg.sensitivitiyFileName(), dlg.sensitivityName(), dlg.sensitivityType());
} // WindowMain::sensitivityOpen

Units WindowMain::unitSelection(void) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (siUnitAction_->isChecked()) {
    qWarning() << "SI units";
    return UnitsSI;
  } else if (enUnitAction_->isChecked()) {
    qWarning() << "EN units";
    return UnitsEN;
  } else if (engUnitAction_->isChecked()) {
    qWarning() << "Eng units";
    return UnitsEng;
  } else {
    qWarning() << "Undefined units";
    return UnitsUndefined;
  }
} // WindowMain::unitSelection

void WindowMain::sensitivity_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // ActionGo *goAction = new ActionGo();
  ActionLibpfSensitivity *goAction = new ActionLibpfSensitivity(this);
  tablewInput_->setRw(false);

  DialogSensitivity dlg(treew_->rootNode(), currentCase_.c_str(), goAction, unitSelection(), this);
  dlg.exec();

  treew_->forceGo(treew_->rootNode());
  tablewInput_->setRw(true);

  delete goAction;
} // WindowMain::sensitivity_

#ifdef Q_OS_WIN
// Operating system; dwMajorVersion; dwMinorVersion
// Windows 2000; 5; 0
// Windows XP; 5; 1
// Windows Server 2008, Windows Vista; 6; 0
// Windows Server 2003 R2, Windows Server 2003; 5; 2
// Windows Server 2008 R2, Windows 7; 6; 1
int getWindowsVersion(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  OSVERSIONINFO osvi;

  ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
  osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

  GetVersionEx(&osvi);
  qDebug() << "MajorVersion = " << osvi.dwMajorVersion << " MinorVersion = " << osvi.dwMinorVersion;

  return osvi.dwMajorVersion * 10 + osvi.dwMinorVersion;
} // getWindowsVersion

#endif

#ifdef Q_OS_WIN
// timeout in ms
DWORD launchProcess(QString executable, LPCTSTR parameters, int timeout) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  qDebug() << "launchProcess" << executable;
  SHELLEXECUTEINFO shex;
  memset(&shex, 0, sizeof(SHELLEXECUTEINFO));
  shex.cbSize = sizeof(SHELLEXECUTEINFO);
  shex.fMask = SEE_MASK_NOCLOSEPROCESS;

  if (getWindowsVersion() >= 60)
    shex.lpVerb = L"runas";  // has UAC
  else
    shex.lpVerb = L"open";
  shex.lpFile = reinterpret_cast<LPCWSTR>(executable.utf16());
  shex.lpParameters = parameters;
  shex.lpDirectory = NULL;
  shex.nShow = SW_HIDE;
  // SW_HIDE Hides the window and activates another window.
  // SW_SHOW Activates the window and displays it in its current size and position.
  // SW_SHOWNOACTIVATE Displays a window in its most recent size and position. The active window remains active.

  // Check that we were able to successfully launch the administrator utility application
  if (::ShellExecuteEx(&shex) == 0) {
    return -1;
  }
  // Wait for the application to finish, then check the return code to make sure the utility app succeeded
  DWORD exitCode;
  WaitForSingleObject(shex.hProcess, timeout);
  if (GetExitCodeProcess(shex.hProcess, &exitCode) == FALSE) {
    return -2;
  }
  qDebug() << "Exit code = " << exitCode;
  return exitCode;
} // launchProcess

#endif

void WindowMain::activate_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int code;
  QString kernel = mySettings_->fullKernelName();
  // QProcess procKernel;
  // showMessage(tr("Starting: %1").arg(kernel));

#ifdef Q_OS_WIN
  // Wait for the application to finish, then check the return code to make sure the utility app succeeded
  DWORD exitCode;
  exitCode = launchProcess(kernel, L"activate", 10000);
  if (exitCode == -1) {
    showMessage(tr("Failure 1 during preparation for LIBPF activation"));
    return;
  } else if (exitCode == -2) {
    showMessage(tr("Failure 2 during preparation for LIBPF activation"));
    return;
  } else if (exitCode == -40) {
    // OK, regular license error
  } else if (exitCode == 0) {
    showMessage(tr("LIBPF is already activated !"));
    return;
  } else {
    showMessage(tr("Failure 3 during preparation for LIBPF activation"));
    return;
  }
  QString libpfActivation = QDir::currentPath();
  libpfActivation.append("/libpf_activation.exe");
  showMessage(tr("Starting: %1").arg(libpfActivation));
  code = launchProcess(libpfActivation, NULL, 10000);
  if (code == -1) {
    showMessage(tr("Failure 1 during LIBPF activation"));
    return;
  } else if (code == -2) {
    showMessage(tr("Failure 2 during LIBPF activation"));
    return;
  }
#elif defined(Q_OS_MAC)
  // based on:
  // http://apple.stackexchange.com/questions/23494/what-option-should-i-give-the-sudo-command-to-have-the-password-asked-trough-gra
  // osascript -e "do shell script \"./libpf_activation\" with administrator privileges"
  QProcess procLibpfActivation;
  QString libpfActivation = "do shell script \"";
  libpfActivation.append(mySettings_->kernelPath());
  libpfActivation.append("/libpf_activation");
  libpfActivation.append("\" with administrator privileges");
  QString osascript("osascript");
  QStringList parameters;
  parameters << "-e" << libpfActivation;
  qDebug() << "starting " << osascript << parameters;
  procLibpfActivation.start(osascript, parameters);
  if (!procLibpfActivation.waitForStarted()) {
    showMessage(tr("Failure to start the LIBPF activation"));
    return;
  }
  if (!procLibpfActivation.waitForFinished(10000)) {
    showMessage(tr("Failure during LIBPF activation"));
    return;
  }
  code = procLibpfActivation.exitStatus();
#else
  QProcess procLibpfActivation;
  QString libpfActivation = mySettings_->kernelPath();
  libpfActivation.append("/libpf_activation");
  QFile la(libpfActivation);
  if (!la.exists()) {
    showMessage(tr("The LIBPF activation utility is missing"));
    return;
  }
  QString kdeSu("/usr/lib/x86_64-linux-gnu/libexec/kf5/kdesu");
  QStringList args;
  args << libpfActivation;
  qDebug() << "starting " << kdeSu << args;
  procLibpfActivation.start(kdeSu, args);
  if (!procLibpfActivation.waitForStarted()) {
    showMessage(tr("Failure to start the LIBPF activation as root"));
    return;
  }
  if (!procLibpfActivation.waitForFinished(10000)) {
    showMessage(tr("Failure during LIBPF activation"));
    return;
  }
  code = procLibpfActivation.exitStatus();
  qDebug() << "exit code = " << code;
#endif

  if (code != 0)
    showMessage(tr("Error during LIBPF activation"));
  else
    showMessage(tr("LIBPF activation success !"));
} // WindowMain::activate_

void WindowMain::delete_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int cid = treew_->rootNode();
  showMessage(tr("Deleting object %1").arg(cid));
  QStringList args;
  args << "remove" << QString("%1").arg(cid);
  startKernel(args, FinishAction::cleanup);
} // WindowMain::delete

void WindowMain::homotopy(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (undoStack_->index() == 0) {
    QMessageBox::warning(0, "Impossible to perform a homotopy run", "No data has been changed from previous run");
    return;
  }
  Homotopy homotopy(undoStack_, treew_->rootNode(), currentCase_);
  if (homotopy.size() == 0) {
    QMessageBox::warning(0, "Impossible to perform a homotopy run", "Data changes have no effect");
    return;
  }
  std::string xmlFileName = mySettings_->homePath().toStdString();
  xmlFileName.append("/Homotopy.xml");
  homotopy.writeXmlFile(xmlFileName);
  undoStack_->clear();

  showMessage(tr("Starting homotopy run"));
  QStringList args;
  int id = treew_->rootNode();
  args << "homotopy" << QString("%1").arg(id);
  startKernel(args, FinishAction::load);
} // WindowMain::homotopy

void WindowMain::purge_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QMessageBox::StandardButton rc =
    QMessageBox::question(this, tr("Confirm purge"), tr("Do you really wish to delete all existing cases ?"),
                          QMessageBox::Yes | QMessageBox::No);
  if (rc == QMessageBox::Yes) {
    showMessage(tr("Deleting all existing cases"));
    QStringList args;
    args << "purge";
    startKernel(args, FinishAction::cleanup);
  }
} // WindowMain::purge_

void WindowMain::reset_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  run("new", currentCase_.c_str());
} // WindowMain::reset_

void WindowMain::calculate_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  run("calculate", itoa(treew_->rootNode()).c_str());
} // WindowMain::calculate_

void WindowMain::stop_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  finishAction_ = FinishAction::success;
  process_->kill();
  loggingProcess_ = NULL;
  showMessage(tr("Interrupted calculation"));
} // WindowMain::stop

void WindowMain::clear_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  messages_->clear();
  showMessage(tr("Cleared log window"));
} // WindowMain::clear_

void WindowMain::showPfd(int id) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (id < 0) {
    tabs_->setCurrentIndex(Tab::inputs);
    tabs_->setTabEnabled(Tab::pfd, false);
  } else {
    tabs_->setTabEnabled(Tab::pfd, true);
    QString svgFile = mySettings_->svgFile(id);
    svgw_->openFile(svgFile);
  }
} // WindowMain::showPfd

void WindowMain::about(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QMessageBox::about(this, tr("About uipf"),
                     QString(UIPF_VERSION) + tr("<p>Copyright &copy; 2008-2021 Paolo Greppi simevo s.r.l."
                                                "<br>Based in part on code from Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera, Lahiru Chandima, Shane Shields and Luiz A. Buhnemann (la3280@gmail.com)."
                                                "<br>Language Icon by Onur ") +
#if defined(Q_OS_MAC) || defined(Q_OS_UNIX) || (QT_VERSION >= 0x050000) || (_MSC_VER >= 1700)
                     QString::fromWCharArray(L"M\x00FC\x015Ftak \x00C7obanl\x0131.") +
#else
                     QString::fromUtf16(L"M\x00FC\x015Ftak \x00C7obanl\x0131.") +
#endif
                     tr(".<p>Icons by Tango-icon-theme, Tango Desktop Project."
                        "<br>Based in part on code from the example classes of the Qt Toolkit:"
                        "<br>Copyright &copy; 2011 Nokia Corporation and/or its subsidiary(-ies)."

                        "<p><b>UIPF</b>, the User Interface for Process Flowsheeting provides "
                        "<br>the User Interface to process_ models developed using <b>LIBPF</b>, "
                        "<br>the <b>LIB</b>rary for <b>P</b>rocess <b>F</b>lowsheeting in C++ "
                        "<p>For more informations please visit <a href=\"http://www.libpf.com/\">the LIBPF website</a>."));
} // WindowMain::about

void WindowMain::openOds(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString doods = mySettings_->kernelPath();
  doods += "/doods";
  QString ods = mySettings_->homePath();

  QProcess proc;
  proc.setWorkingDirectory(ods);
  proc.start(doods, QStringList());
  showMessage(tr("Starting: %1").arg(doods));
  if (!proc.waitForFinished(1000)) {
    showMessage(tr("Failure to start external command %1 for ODS file generation").arg(doods));
  } else {
    int code = proc.exitStatus();
    if (code != 0) {
      showMessage(tr("Error writing to OpenOffice spreadsheet %1").arg(ods));
    } else {
      if (ods[0] == '/')
        ods = "file://" + ods + "/streamtable.ods";
      else
        ods = "file:///" + ods + "/streamtable.ods";
      showMessage(tr("Opening OpenOffice spreadsheet %1").arg(ods));
      if (QDesktopServices::openUrl(ods))
        showMessage(tr("OpenOffice spreadsheet %1 opened").arg(ods));
      else
        showMessage(tr("Error opening OpenOffice spreadsheet %1").arg(ods));
    }
  }
} // WindowMain::openOds

#ifdef Q_OS_WIN
void WindowMain::openXls(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  wchar_t *file_and_path;

  QString xls = mySettings_->homePath();
  xls += "/streamtable.xls";

  xls = QDir::toNativeSeparators(xls);

  showMessage(tr("Opening Excel spreadsheet %1").arg(xls));

  file_and_path = new wchar_t[xls.length() + 1];
  xls.toWCharArray(file_and_path);
  file_and_path[xls.length()] = L'\0';
  if (openXlsMain(file_and_path, treew_->rootNode()))
    showMessage(tr("Excel spreadsheet %1 opened").arg(xls));
  else
    showMessage(tr("Error opening Excel spreadsheet %1").arg(xls));
  delete [] file_and_path;
} // WindowMain::openXls

#endif

void WindowMain::selectkernel(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString kp = mySettings_->kernelPath();
  QString directory = QFileDialog::getExistingDirectory(this, tr("Select kernel"), kp, QFileDialog::ShowDirsOnly);
  if (!directory.isEmpty()) {
    // note: on Linux, directory has a trailing slash, kernelPath has not !
    if (directory.right(1) == "/")
      directory = directory.left(directory.length() - 1);
    if ((directory.length() > kp.length()) && (directory.left(kp.length()) == kp)) {
      QStringList aaa = directory.split("/");
      if (aaa.size() > 1)
        mySettings_->setValue("active_kernel", aaa.last());
      showMessage(tr("Set active kernel to %1").arg(aaa.last()));
      delete h_;
      h_ = new Hierarchy(typesFile().toStdString());
      delete nd_;
      nd_ = new DialogNew(this);
      buildNewDialog();
      connect(nd_, SIGNAL(open(const QString &, const QString &, const QString &)), this, SLOT(openNew(const QString &, const QString &, const QString &)));
    } else {
      QMessageBox msgBox(QMessageBox::Warning, tr("Invalid selected kernel"), tr("The kernel can only reside inside the %1 directory.").arg(kp), 0, this);
      msgBox.exec();
      showMessage(tr("Invalid selected kernel"));
    }
  } // if nonempty selection
} // WindowMain::selectkernel

void WindowMain::setLoggingProcess(QProcess* p) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  loggingProcess_ = p;
} // WindowMain::setLoggingProcess

QString WindowMain::typesFile(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString buf = mySettings_->kernelPath();
  buf += "/";
  if (mySettings_->value("active_kernel", "").toString() != "") {
    buf += mySettings_->value("active_kernel").toString();
    buf += "/";
  }
  buf += "types.txt";

  //QMessageBox msgBox(QMessageBox::Information, tr("typesFile"), tr("typesFile =[%1]").arg(buf), 0, this);
  //msgBox.exec();

  return buf;
} // WindowMain::typesFile

void WindowMain::buildNewDialog(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  std::set<std::string> fzz; // FlowSheet<ZeroZero> derived classes
  for (h_->start(); !h_->end(); ) {
    const Type *t(h_->next());
    if (t == NULL) {
      qCritical("NULL Type\n");
      exit(-2);
    }
    //    std::cout << "looking at " << t->name() << std::endl;
    if (h_->isA(t->name(), "FlowSheet<ZeroZero>")) {
      //      std::cout << "ok " <<  t->name() << " is a FlowSheet<ZeroZero>" << std::endl;
      std::pair< std::set<std::string>::iterator, bool > pr;
      pr = fzz.insert(t->name());
      if ((pr.second == true) && (t->name() != "FlowSheet<ZeroZero>")) {
        nd_->addType(t->name().c_str(), t->description().c_str());
      }
    }
  }
  if (nd_->typesCount() == 1)
    nd_->lockType();
} // WindowMain::buildNewDialog

void WindowMain::setLanguage(const char* lang) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (mySettings_->value("locale", "").toString().left(2) != lang) {
    mySettings_->setValue("locale", lang);
    showMessage(tr("Restart to apply language change"));
  }
} // WindowMain::setLanguage

void setAllActions(std::vector<QAction *> &actions, bool enabled) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  for (std::vector<QAction *>::iterator i = actions.begin(); i != actions.end(); ++i)
    (*i)->setEnabled(enabled);
} // setAllActions
    
void WindowMain::setState(State state) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // invalid and running are transient states
  if ( (state_.v_ != State::invalid) && (state_.v_ != State::running) )
    previousState_ = state_;
  state_ = state;
  
  switch (state.v_) {
  case State::empty:
    // no case loaded
    qDebug() << "Setting state to empty";
    stateMessage_->setText(tr("EMPTY"));
    pix_->fill(Qt::white);
    flag_->setPixmap(*pix_);
    setAllActions(enableLoadedActions_, false);
    setAllActions(disableRunningActions_, true);
    setAllActions(enableRunningActions_, false);
    break;
  case State::ok:
    // case loaded without errors and without warnings
    qDebug() << "Setting state to ok";
    stateMessage_->setText(tr("OK"));
    pix_->fill(Qt::green);
    flag_->setPixmap(*pix_);
    setAllActions(enableLoadedActions_, true);
    setAllActions(disableRunningActions_, true);
    setAllActions(enableRunningActions_, false);
    break;
  case State::warnings:
    // case loaded without errors but with warnings 
    qDebug() << "Setting state to warnings";
    stateMessage_->setText(tr("WARNINGS"));
    pix_->fill(Qt::yellow);
    flag_->setPixmap(*pix_);
    setAllActions(enableLoadedActions_, true);
    setAllActions(disableRunningActions_, true);
    setAllActions(enableRunningActions_, false);
    break;
  case State::errors:
    // case loaded with errors
    qDebug() << "Setting state to errors";
    stateMessage_->setText(tr("ERRORS"));    
    pix_->fill(Qt::red);
    flag_->setPixmap(*pix_);
    setAllActions(enableLoadedActions_, true);
    setAllActions(disableRunningActions_, true);
    setAllActions(enableRunningActions_, false);
    break;
  case State::changed:
    // input values changed
    qDebug() << "Setting state to changed";    
    stateMessage_->setText(tr("CHANGED"));
    pix_->fill(Qt::cyan);
    flag_->setPixmap(*pix_);
    setAllActions(enableLoadedActions_, true);
    setAllActions(disableRunningActions_, true);
    setAllActions(enableRunningActions_, false);
    break;
  case State::invalid:
    // invalid return code from kernel
    qDebug() << "Setting state to invalid";
    stateMessage_->setText(tr("INVALID"));
    pix_->fill(Qt::darkRed);
    flag_->setPixmap(*pix_);
    break;
  case State::running:
    // kernel running
    qDebug() << "Setting state to running";
    stateMessage_->setText(tr("RUNNING"));    
    pix_->fill(Qt::blue);
    flag_->setPixmap(*pix_);
    setAllActions(enableLoadedActions_, false);
    setAllActions(disableRunningActions_, false);
    setAllActions(enableRunningActions_, true);
    break;
  case State::undefined:
    stateMessage_->setText(tr("UNDEFINED"));
    pix_->fill(Qt::transparent);
    flag_->setPixmap(*pix_);
  default:
    // perform no action
    ;
  } // switch upon State  
} // WindowMain::setState

void WindowMain::resetState(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  qWarning() << "Resetting State to " << previousState_.v_;
  setState(previousState_);
} // WindowMain::resetState

void WindowMain::startKernel(QStringList args, FinishAction finishAction) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QCoreApplication::processEvents();  
  finishAction_ = finishAction;  
  process_->setWorkingDirectory(mySettings_->homePath());
  qWarning() << "Set kernel working directory to: " << process_->workingDirectory();
  QString kernel = mySettings_->fullKernelName();
  qDebug() << "starting " << kernel << " " << args;
  loggingProcess_ = process_;
  process_->start(kernel, args);
  showMessage(tr("Trying to start: %1 %2").arg(kernel).arg(args.join(" ")));
} // WindowMain::startKernel
  
void WindowMain::editActionStatus(bool value) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  aselectallAction_->setEnabled(value);           //select all action enable/disable
  acopyAction_->setEnabled(value);                //copy action enable/disable
} // WindowMain::editActionStatus

//create table input context menu
void WindowMain::tablewInputCustomContextMenu(const QPoint &) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // get model from tableview
  if (boolGetItemTableView(tablewInput_->view)) {
    QMenu menu(this);
    menu.addAction(acopyAction_);
    menu.addAction(aselectallAction_);
    menu.exec(QCursor::pos());
  }
} // WindowMain::tablewInputCustomContextMenu

//create table output context menu
void WindowMain::tablewOutputCustomContextMenu(const QPoint &) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // get model from tableview
  if (boolGetItemTableView(tablewOutput_->view)) {
    QMenu menu(this);
    menu.addAction(acopyAction_);
    menu.addAction(aselectallAction_);
    menu.exec(QCursor::pos());
  }
} // WindowMain::tablewOutputCustomContextMenu

//copy action signal
void WindowMain::acopy_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString copy_table;
  if (tabs_->currentIndex() == Tab::inputs) { //check if tab input
    qDebug() << "input table";
    if (boolGetItemTableView(tablewInput_->view))       //check selected item
      copy_table = tablewInput_->getTbviewSelectedItem();       //get item selected
  } else if (tabs_->currentIndex() == Tab::outputs) { //if tab output
    qDebug() << "output table";
    if (boolGetItemTableView(tablewOutput_->view))      //check selected item
      copy_table = tablewOutput_->getTbviewSelectedItem();        //get item selected
  }
  QClipboard* clipboard = QApplication::clipboard();   //copy data to clipboard
  if (!copy_table.isEmpty())                            //if string not empty, save string to clipboard
    clipboard->setText(copy_table.trimmed());
} // WindowMain::acopy_

//get QTableView selected item
bool WindowMain::boolGetItemTableView(QTableView* table) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // get model from tableview
  QItemSelectionModel* model = table->selectionModel();
  QModelIndexList list = model->selectedIndexes();

  if (list.size() > 0)  //QTableView have selected data
    return (true);
  else                  //QTableView dont have selected data
    return (false);
} // WindowMain::boolGetItemTableView

//check if item selected at table input
void WindowMain::checkInContextMenu(const QModelIndex &index) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid())                                  //item selected
    acopyAction_->setEnabled(true);                      //enable copy action
  else
    acopyAction_->setEnabled(false);                     //disable copy action
} // WindowMain::checkInContextMenu

//check if item selected at table output
void WindowMain::checkOutContextMenu(const QModelIndex &index) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid())                                  //item selected
    acopyAction_->setEnabled(true);                      //enable copy action
  else
    acopyAction_->setEnabled(false);                     //disable copy action
} // WindowMain::checkOutContextMenu

//select all action
void WindowMain::aselectAll_(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (tabs_->currentIndex() == Tab::inputs)            // check if tab input selected
    tablewInput_->setTbviewSelectAll();               // get selected item
  else if (tabs_->currentIndex() == Tab::outputs)     // check if tab output selected
    tablewOutput_->setTbviewSelectAll();              // get selected item
} // WindowMain::aselectAll_

void WindowMain::editShowMenuClicked(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int idx;

  idx = tabs_->currentIndex();
  if (idx == Tab::inputs) { // table input/output selected
    if (boolGetItemTableView(tablewInput_->view)) // check selected item
      acopyAction_->setEnabled(true); // enable copy action
    else
      acopyAction_->setEnabled(false); // disable copy action
  } else if (idx == Tab::outputs) {
    if (boolGetItemTableView(tablewOutput_->view)) // check selected item
      acopyAction_->setEnabled(true); // enable copy action
    else
      acopyAction_->setEnabled(false); // disable copy action
  } else {
    editActionStatus(false); // disable copy and select all QAction
  }
} // WindowMain::editShowMenuClicked

void WindowMain::editHideMenuClicked(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  //active edit menu to remove  default QAction copy
  //from QTreeView and QTableView
  editActionStatus(true);                       //enable copy and select all QAction
} // WindowMain::editHideMenuClicked

void WindowMain::setLanguageDefault(void) { setLanguage(""); } // WindowMain::setLanguageDefault

void WindowMain::setLanguageAr(void) { setLanguage("ar"); } // WindowMain::setLanguageAr
void WindowMain::setLanguageDe(void) { setLanguage("de"); } // WindowMain::setLanguageDe
void WindowMain::setLanguageEs(void) { setLanguage("es"); } // WindowMain::setLanguageEs
void WindowMain::setLanguageEn(void) { setLanguage("en"); } // WindowMain::setLanguageEn
void WindowMain::setLanguageFr(void) { setLanguage("fr"); } // WindowMain::setLanguageFr
void WindowMain::setLanguageHe(void) { setLanguage("he"); } // WindowMain::setLanguageHe
void WindowMain::setLanguageIt(void) { setLanguage("it"); } // WindowMain::setLanguageIt
void WindowMain::setLanguageJp(void) { setLanguage("jp"); } // WindowMain::setLanguageJp
void WindowMain::setLanguageKo(void) { setLanguage("ko"); } // WindowMain::setLanguageKo
void WindowMain::setLanguagePt(void) { setLanguage("pt"); } // WindowMain::setLanguagePt
void WindowMain::setLanguageRu(void) { setLanguage("ru"); } // WindowMain::setLanguageRu
void WindowMain::setLanguageZh(void) { setLanguage("zh"); } // WindowMain::setLanguageZh

void WindowMain::setUnit_(QAction *action) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (action == siUnitAction_)
    mySettings_->setUnits("SI");
  else if (action == enUnitAction_)
    mySettings_->setUnits("EN");
  else if (action == engUnitAction_)
    mySettings_->setUnits("Eng");
  setUnit_(0);
} // WindowMain::setUnit_

void WindowMain::setUnit_(int /* node */) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  qWarning() << "WindowMain::setUnit_";
  tablewInput_->setUom(unitSelection());
  tablewOutput_->setUom(unitSelection());
} // WindowMain::setUnit_

void WindowMain::undoTextChanged(const QString & text) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  undoAction_->setStatusTip(text);
  tablewInput_->resetUom();
  tablewOutput_->resetUom();
} // WindowMain::undoTextChanged

void WindowMain::redoTextChanged(const QString & text) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  redoAction_->setStatusTip(text);
  tablewInput_->resetUom();
  tablewOutput_->resetUom();
} // WindowMain::redoTextChanged

void WindowMain::dataChanged(bool status) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  qWarning() << "Can undo changed: " << status << " commands in the stack: " << undoStack_->count();
  if (status) {
    setState(State::changed);
  } else {
    resetState();
  }
} // WindowMain::dataChanged

void WindowMain::forceGoTable(int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // jump to node
  treew_->forceGo(node);
  // open tablewInput_ tab
  tabs_->setCurrentIndex(Tab::inputs);
} // WindowMain::forceGoTable

void WindowMain::toggleChild(void) {
  Settings mySettings;
  mySettings.toggleChild();
  setChild(mySettings.child());
  emit treew_->refresh();
  emit clearStatusMessage();
} // WindowMain::toggleChild

void WindowMain::setChild(bool child) {
  tablewInput_->setChild(child);
  tablewOutput_->setChild(child);
} // WindowMain::setChild
