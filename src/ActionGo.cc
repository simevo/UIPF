/** @file ActionGo.cc
    @brief Implementation for the ActionGo class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDebug>
#include <QAbstractItemModel>

#include "ActionGo.h"

ActionGo::ActionGo(void) { } // ActionGo::ActionGo

/// The action resumes at the next call to ActionGo::start()
void ActionGo::stop(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  kill();
  mutex.lock();
  setStopped(true);
  mutex.unlock();
  emit stopped();
} // ActionGo::stop

void ActionGo::setControlledVariable(const VariableControlled *controlledVariable) {
  control_ = controlledVariable;
} // ActionGo::setControlledVariable

/// If you reimplement this method, you should emit the signal ActionGo::computed ()
/// with the parameters (r,false) before inserting data into the model and (r,true) after insertion.
/// The default implementation fills the data model with random results
void ActionGo::compute(int r) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  emit computed(r, executing);
  msleep(50);
  for (int c = 2; c < resultsModel()->columnCount(); ++c) {
    resultsModel()->setData(resultsModel()->index(r, c), qrand());
  }
  emit computed(r, doneOk);
  msleep(50);
} // ActionGo::compute

/// If you reimplement this method, you should emit the signal ActionGo::computed ()
/// with the parameters (r,false) before inserting data into the model and (r,true)
/// after insertion.
void ActionGo::run(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (resultsModel()) {
    while (currentRow() < resultsModel()->rowCount()) {
      mutex.lock();
      if (isStopped()) {
        setStopped(false);
        mutex.unlock();
        return;
      }
      mutex.unlock();
      compute(currentRow());
      incrementCurrentRow();
    }
    emit complete();
  }
} // ActionGo::run

const VariableControlled* ActionGo::controlledVariable(void) const {
  return control_;
} // ActionGo::controlledVariable
