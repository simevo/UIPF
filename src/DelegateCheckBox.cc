/** @file DelegateCheckBox.cc
    @brief Implementation for the DelegateCheckBox class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QUndoStack>
#include <QCheckBox>
#include <QDebug>

#include "DelegateCheckBox.h"
#include "persistency.h"
#include "ModelEditable.h"

DelegateCheckBox::DelegateCheckBox(QObject* parent) : QStyledItemDelegate(parent) { } // DelegateCheckBox::DelegateCheckBox

QWidget* DelegateCheckBox::createEditor(QWidget* parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid() && index.column() == Itbl_I) {
    QCheckBox* editor = new QCheckBox(parent);
    editor->installEventFilter(const_cast<DelegateCheckBox*>(this));
    return editor;
  } else {
    return QStyledItemDelegate::createEditor(parent, option, index);
  }
} // DelegateCheckBox::createEditor

void DelegateCheckBox::setEditorData(QWidget* editor, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid() && index.column() == Itbl_I) {
    QString value = index.model()->data(index, Qt::EditRole).toString();
    QCheckBox* checkBox = static_cast<QCheckBox*>(editor);
    if (value == "1")
      checkBox->setCheckState(Qt::Checked);
    else
      checkBox->setCheckState(Qt::Unchecked);
  } else {
    QStyledItemDelegate::setEditorData(editor, index);
  }
} // DelegateCheckBox::setEditorData

void DelegateCheckBox::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid() && index.column() == Itbl_I) {
    QCheckBox* checkBox = static_cast<QCheckBox*>(editor);
    QString value;
    if (checkBox->checkState() == Qt::Checked)
      value = "1";
    else
      value = "0";
    model->setData(index, value, Qt::EditRole);
  } else {
    QStyledItemDelegate::setModelData(editor, model, index);
  }
} // DelegateCheckBox::setModelData

void DelegateCheckBox::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.isValid() && index.column() == Itbl_I)
    editor->setGeometry(option.rect);
  else
    QStyledItemDelegate::updateEditorGeometry(editor, option, index);
} // DelegateCheckBox::updateEditorGeometry
