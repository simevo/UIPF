/** @file ModelQuantityEditable.cc
    @brief Implementation for the ModelQuantityEditable class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QUndoStack>

#include "ModelQuantityEditable.h"
#include "persistency.h"
#include "CommandDataSetDouble.h"

ModelQuantityEditable::ModelQuantityEditable(const char* filter, QWidget* parent) : ModelEditable(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  extraFilter_ = filter;
} // ModelQuantityEditable::ModelQuantityEditable

QVariant ModelQuantityEditable::data(const QModelIndex & index, int role) const {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  //qDebug() << "ModelQuantityEditable::data" << dspValue_.isValid();
  if (role == Qt::DisplayRole) {
    if ((index.column() == Qtbl_Q) && qValue_.contains(index.row())) {
      return qValue_.value(index.row());
    }
    if ((index.column() == Qtbl_Uom) && uValue_.contains(index.row())) {
      return uValue_.value(index.row());
    }
  }
  // qDebug() << "column = " << index.column();
  return ModelEditable::data(index, role);
}

bool ModelQuantityEditable::setData(const QModelIndex &index, const QVariant &value, int role) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (role == Qt::EditRole) {
    if ((index.column() == Qtbl_Q) || ((index.column() == Qtbl_Uom) && (value != data(index, role)))) {
      undoStack_->push(new CommandDataSetDouble(this, index, value, role, currentNode_));
      return true;
    }
  } else if (role == Qt::DisplayRole) {
    if (index.column() == Qtbl_Q) {
      qValue_.insert(index.row(), value);
    } else if (index.column() == Qtbl_Uom) {
      uValue_.insert(index.row(), value);
    }
    emitDataChanged(index);
    return true;
  }
  return false;
} // ModelQuantityEditable::setData

bool ModelQuantityEditable::setQ(int dbid, double value) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);
#if defined(Q_OS_WIN) && (!defined(SQLITE))
  query.prepare("update Q set [value] = ? where id = ?");
#else
  query.prepare("update Q set value = ? where id = ?");
#endif
  query.addBindValue(value);
  query.addBindValue(dbid);

  bool test = query.exec();

  if (query.lastError().isValid())
    qCritical() << "Error while executing query " << query.executedQuery() << " in " << Q_FUNC_INFO << ": " << query.lastError().type() << query.lastError().text();
  if (!test)
    qCritical() << "Unknown error in " << Q_FUNC_INFO << ": update Q set value = " << value << " where id = " << dbid;
  return test;
} // ModelQuantityEditable::setQ

void ModelQuantityEditable::go(int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);
  if (currentNode_ != node) {
    qCritical() << "ModelQuantityEditable::go" << node;
    qValue_.clear();
    uValue_.clear();
  }
  query.setNumericalPrecisionPolicy(QSql::LowPrecisionDouble);


#if defined(Q_OS_WIN) && (!defined(SQLITE))
  query.exec(QString("select fulltag, range from N where id = %1").arg(node));
#else
  query.exec(QString("select trim(fulltag), range from N where id = %1").arg(node));
#endif
  query.next();
  prefix_ = query.value(0).toString();
  strip_ = prefix_.length();
  qDebug() << "strip = " << strip_;
  range_ = query.value(1).toInt();
  qDebug() << "range = " << range_;

  QString qs;
#if defined SQLITE
  qs = "SELECT distinct substr(N.fulltag||\'.\'||Q.tag,%3+2), Q.value, Q.unit, Q.DESCRIPTION, Q.ID";
#elif defined Q_OS_WIN
  qs = "select distinct right(N.fulltag+\'.\'+Q.tag, len(N.fulltag)+1+len(Q.tag)-%3-1), Q.[value], Q.unit, Q.DESCRIPTION, Q.ID";
#elif defined MYSQL
  qFatal() << "MYSQL not yet supported in "  << Q_FUNC_INFO;
//    qs = "select distinct substring(concat(N.fulltag,\'.\',Q.tag) from (%3+2) ), Q.value, Q.unit, Q.DESCRIPTION, Q.ID";
#elif defined POSTGRESQL
  // Added by StoianE postgres + linux
  qs = "select distinct substring(N.fulltag||\'.\'||Q.tag from (%3+2)), Q.value, Q.unit, Q.DESCRIPTION, Q.ID";
#endif

  qs += " from (Q inner join N on (Q.nid = N.id))";

  if (child_) {
    qDebug() << "only show current node";
    qs += " where N.id = %1 %2 order by Q.ID";
    qs = qs.arg(node).arg(extraFilter_.c_str()).arg(strip_);
  } else {
    qDebug() << "include all descendants";
    qs += " where N.id >= %1 AND N.id < %1 + %4 %2 order by Q.ID";
    qs = qs.arg(node).arg(extraFilter_.c_str()).arg(strip_).arg(range_);
  } // if (child_)

  qDebug() << "Query in ModelQuantityEditable::go = " << qs;
  setQuery(qs, db);
  if (lastError().isValid())
    qCritical() << "Error while executing query in "<< Q_FUNC_INFO << ": " << lastError().type() << lastError().text();
  setHeaders();
  currentNode_ = node;
} // ModelQuantityEditable::go

void ModelQuantityEditable::setHeaders(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  setHeaderData(Qtbl_Tag, Qt::Horizontal, tr("Tag"));
  setHeaderData(Qtbl_Description, Qt::Horizontal, tr("Description"));
  setHeaderData(Qtbl_Q, Qt::Horizontal, tr("Value"));
  setHeaderData(Qtbl_Uom, Qt::Horizontal, tr("Units"));
  setHeaderData(Qtbl_Id, Qt::Horizontal, tr("ID"));

  lHeader_ << "Tag" << "Value" << "Units" << "Description" << "ID"; //list of header
} // ModelQuantityEditable::setHeaders

QStringList ModelQuantityEditable::getHeaders(void) {
  return (lHeader_);
} // ModelQuantityEditable::getHeaders
