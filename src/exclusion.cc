/** @file exclusion.cc
    @brief Implementation of the Windows-specific code to avoid multiple instances of the application

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based on code by Joseph M. Newcomer from: http://www.flounder.com/nomultiples.htm

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <qglobal.h>
#ifdef Q_OS_WIN
#include <windows.h>
#include <wchar.h>
#include "exclusion.h"

// disable annoying MSVC 2005 "This function or variable may be unsafe" warning
#pragma warning(disable : 4996)

/****************************************************************************
*                             createExclusionName
* Inputs:
*       LPCTSTR GUID: The GUID for the exclusion

* Result: CString
*       A name to use for the exclusion mutex
* Effect:
*       Creates the exclusion mutex name
* Notes:
*       The GUID is created by a declaration such as
*               #define UNIQUE L"{44E678F7-DA79-11d3-9FE9-006067718D04}"
****************************************************************************/

#define NAMELENGTH 64

WCHAR buf[NAMELENGTH];

LPCTSTR createExclusionName(LPCTSTR GUID) {
  TCHAR userName[NAMELENGTH];
  DWORD userNameLength = NAMELENGTH;
  TCHAR domainName[NAMELENGTH];
  DWORD domainNameLength = NAMELENGTH;

  if (GetUserName(userName, &userNameLength)) {
    // The NetApi calls are very time consuming
    // This technique gets the domain name via an
    // environment variable
    domainNameLength = ExpandEnvironmentStrings(L"%USERDOMAIN%",
                                                domainName,
                                                NAMELENGTH);
    swprintf(buf, L"%s-%s-%s", GUID, domainName, userName);
  }
  return buf;
} // createExclusionName

#define UNIQUE_GUID L"LIBPF 1.0"

/****************************************************************************
*                         makeInterlock
* Result:
*       Returns true if window exists, false if not.
*
* Effect:
*       Attempts to create the interlock.
****************************************************************************/

bool testUniqueness(void) {
  bool result(false);
  HANDLE exclusion(NULL);

  LPCTSTR name = createExclusionName(UNIQUE_GUID);
  exclusion = CreateMutex(NULL, FALSE, name);
  DWORD err = ::GetLastError();
  if (err != 0) {
    if (err == ERROR_ALREADY_EXISTS) {
      result = true;
    } else { }
  } else { }
  return result;
} // testUniqueness

#endif // Q_OS_WIN
