/** @file Homotopy.cc
    @brief Homotopy class implementation

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QUndoStack>
#include <QString>
#include <QFile>
#include <QXmlStreamWriter>
#include <QDebug>
#include <QRegExp>
 
#include "Homotopy.h"
#include "CommandDataSet.h"

Homotopy::Homotopy(const QUndoStack* undoStack, int caseId, std::string type) : caseId_(caseId), points_(100), type_(type) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  for (int i = 0; i < undoStack->index(); ++i) {
    const CommandDataSet* command = dynamic_cast<const CommandDataSet *>(undoStack->command(i));
    if (command != NULL) {
      qCritical() << command->fullTag() << " old = " << command->oldValue() << " new = " << command->newValue();
      if (command->oldValue() == command->newValue()) {
        // has no effect
      } else {
        VariableMapType::iterator i = variables_.find(command->fullTag());
        if (i == variables_.end()) {
          variables_[command->fullTag()] = std::pair<double, double>(command->oldValue(), command->newValue());
        } else if (i->second.second == command->newValue()) {
          // has no effect
        } else if (i->second.first == command->newValue()) {
          // commands cancel out
          variables_.erase(i);
        } else {
          i->second = std::pair<double, double>(i->second.first, command->newValue());
        }
      } // oldValue != newValue
    } // command is a CommandDataSet
  }
} // Homotopy::Homotopy

int Homotopy::size(void) {
  return variables_.size();
} // Homotopy::size

void Homotopy::writeXmlFile(std::string fileName) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // QString file (fileName);
  QFile xmlFile(fileName.c_str());
  xmlFile.open(QIODevice::WriteOnly);

  QXmlStreamWriter xml(&xmlFile);
  xml.setAutoFormatting(true);
  xml.writeStartDocument();

  xml.writeStartElement("homotopy");
  xml.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
  xml.writeAttribute("xsi:noNamespaceSchemaLocation", "Homotopy_schema.xsd");

  xml.writeTextElement("type", type_.c_str());
  xml.writeTextElement("id", QString("%1").arg(caseId_));
  xml.writeTextElement("points", QString("%1").arg(points_));

  xml.writeStartElement("controlled");

  for (VariableMapType::const_iterator i = variables_.begin(); i != variables_.end(); ++i) {
    xml.writeStartElement("variable");

    xml.writeAttribute("start", QString("%1").arg(i->second.first));
    xml.writeAttribute("end", QString("%1").arg(i->second.second));

    QString fullTag(i->first);
    // strip root object tag
    int firstSeparator = fullTag.indexOf(QRegExp("[\\.:]"));
    if (firstSeparator >= 0) {
      // qDebug() << "**** Variable before " << fullTag;
      fullTag = fullTag.right(fullTag.size() - firstSeparator - 1);
      // qDebug() << "**** Variable after " << fullTag;
    }
    
    xml.writeCharacters(fullTag);

    xml.writeEndElement(); // </variable>
  }
  xml.writeEndElement(); // </controlled>

  xml.writeEndElement(); // </homotopy>

  xml.writeEndDocument();
  xmlFile.close();
} // Homotopy::writeXmlFile
