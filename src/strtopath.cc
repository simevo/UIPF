/** @file strtopath.cc
    @brief strToPath function definition

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from Dmytro Skrypnyk.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QRegExp>
#include <QDebug>

#include "strtopath.h"

const double delta = 0.01;

QPainterPath strToPath(QString str) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QPainterPath path;
  // qDebug() << "Processing string: " << str;
  QRegExp exp("[MCL]?\\s*([\\-\\d.]+)\\s*([\\-\\d.]+)");

  QList<QPointF> curve;
  int offset = 0;
  QChar cmd;

  while ((offset = exp.indexIn(str, offset)) != -1) {
    QString s = exp.cap(0);
    if (s[0].isLetter()) cmd = s[0];
    switch (cmd.toLatin1()) {
    case 'M':
      // qDebug() << "MOVETO " << exp.cap(1) << " -> " <<  exp.cap(2);
      path.moveTo(exp.cap(1).toDouble(), exp.cap(2).toDouble());
      break;
    case 'L':
      // qDebug() << "LINETO " << exp.cap(1) << " -> " <<  exp.cap(2);
      path.lineTo(exp.cap(1).toDouble(), exp.cap(2).toDouble());
      break;
    case 'C':
      // qDebug() << "CURVE " << exp.cap(1) << " -> " <<  exp.cap(2);      
      curve << QPointF(exp.cap(1).toDouble(), exp.cap(2).toDouble());
      if (curve.count() == 3) {
        path.cubicTo(curve[0], curve[1], curve[2]);
        curve.clear();
      }
      break;
    default:
      qCritical() << "unknown command: " << cmd;
    }

    offset += exp.matchedLength();
  }
  QMatrix mb;
  mb.scale(1 + delta, 1 + delta);
  QPainterPath pb = mb.map(path);

  QMatrix ms;
  ms.scale(1 - delta, 1 - delta);
  QPainterPath ps = ms.map(path);

  QPainterPath resPath = pb;
  resPath.connectPath(ps.toReversed());
  resPath.closeSubpath();

  return resPath;
} // strToPath
