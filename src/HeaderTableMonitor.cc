/** @file HeaderTableMonitor.cc
    @brief Implementation for the HeaderTableMonitor class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QSqlTableModel>
#include <QSqlQuery>
#include <QPainter>
#include <QDebug>

#include "persistency.h"
#include "HeaderTableMonitor.h"

/// @param parent the parent widget
HeaderTableMonitor::HeaderTableMonitor(QWidget* parent) :
  QHeaderView(Qt::Vertical, parent) {
#if (QT_VERSION >= 0x050000)
  setSectionsClickable(true);
#else
  setClickable(true);
#endif
  setMinimumWidth(24);
} // HeaderTableMonitor::HeaderTableMonitor

/// Reimplemented from QHeaderView
void HeaderTableMonitor::paintSection(QPainter* painter, const QRect & rect, int /*logicalIndex*/) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  painter->save();
  QIcon icon(":/images/list-remove.png");
  QStyleOptionButton option;
  option.icon = icon;
  option.iconSize = QSize(16, 16);
  option.rect = QRect(rect.x(), rect.y(), 24, 24);
  option.state = QStyle::State_Active | QStyle::State_Enabled;
  style()->drawControl(QStyle::CE_PushButton, &option, painter, this);
  painter->restore();
} // HeaderTableMonitor::paintSection
