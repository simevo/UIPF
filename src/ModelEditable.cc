/** @file ModelEditable.cc
    @brief Implementation for the ModelEditable class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code by Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QUndoStack>
#include <QDebug>

#include "ModelEditable.h"

ModelEditable::ModelEditable(QWidget* parent) : QSqlQueryModel(parent), undoStack_(0), child_(false), rwStatus_(true) { qDebug() << "Entering "<< Q_FUNC_INFO; } // ModelEditable::ModelEditable

Qt::ItemFlags ModelEditable::flags(const QModelIndex &index) const {
//  qDebug() << "Entering "<< Q_FUNC_INFO;
  Qt::ItemFlags flags = QSqlQueryModel::flags(index);
  // qDebug() << "odelEditable::ModelEditable" << rwStatus_ << index.column() << editableColumn();
  if ((index.column() == editableColumn() && rwStatus_) || index.column() == Qtbl_Uom)
    flags |= Qt::ItemIsEditable;
  return flags;
} // ModelEditable::flags

void ModelEditable::setUndoStack(QUndoStack* undoStack) {
  undoStack_ = undoStack;
} // ModelEditable::setUndoStack

void ModelEditable::emitDataChanged(const QModelIndex &index) {
  emit dataChanged(index, index);
} // ModelEditable::emitDataChanged

void ModelEditable::emitForceGo(int node, int row) {
  emit forceGo(node, row);
} // ModelEditable::emitForceGo

void ModelEditable::clear(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  setQuery("", db);
} // ModelEditable::clear

void ModelEditable::setChild(bool child) {
  child_ = child;
} // ModelEditable::setChild

int ModelEditable::id(int row) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QModelIndex primaryKeyIndex = index(row, idColumn());
  return data(primaryKeyIndex).toInt();
} // ModelEditable::id

QString ModelEditable::tag(int row) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QModelIndex tagKeyIndex = index(row, tagColumn());
  return data(tagKeyIndex).toString().trimmed();
} // ModelEditable::tag

QString ModelEditable::description(int row) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QModelIndex descriptionKeyIndex = index(row, descriptionColumn());
  return data(descriptionKeyIndex).toString().trimmed();
} // ModelEditable::description

QString ModelEditable::completeTag(int row) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString p = prefix_;
  QString t = tag(row);
  // workaround for dot being the first character of FullTag
  if (t[0] == '.')
    t.remove(0, 1);
  if (t.indexOf(".") == -1)
    p.append(".").append(t);
  else
    p.append(":").append(t);
  return p;
} // ModelEditable::completeTag

void ModelEditable::rwStatus(bool b) {
  rwStatus_ = b;
}
