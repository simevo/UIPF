/** @file DelegateControl.cc
    @brief Implementation for the DelegateControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <cassert> // for assert
#include <algorithm>
#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlError>
#if (QT_VERSION >= 0x040600)
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#endif

#include "DelegateControl.h"
#include "ModelNameVariableControl.h"
#include "persistency.h"

#include <QDebug>

DelegateControl::DelegateControl(int caseId, Units *u, QObject *parent) :
  QStyledItemDelegate(parent), u_(u) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);
  QString  qs("");
  bool queryCheck(false);

  // get the range
  int range(0);
  qs = QString("SELECT RANGE FROM N WHERE ID=%1;").arg(caseId);
  query.prepare(qs);
  queryCheck = query.exec();
  if (query.lastError().isValid())
      qCritical() <<  "Error while executing query in " << Q_FUNC_INFO << ": " << query.lastError().type() << query.lastError().text();
  if (!queryCheck) qCritical() << "Unknown error in "<< Q_FUNC_INFO << "executing : " << query.lastQuery();
  if (query.next()) {
    range = query.value(0).toInt();
  } else {
    qCritical()<<"Database Error in "<< Q_FUNC_INFO << "executing : " << query.lastQuery();
  }
//  qDebug() << query.lastQuery();
//  qDebug() << "caseId = "<<caseId <<", range = "<<range;

  // clear
  qs = ("");
  query.clear();
  queryCheck = false;
  // get the quantities
#if defined SQLITE
  qs = QString("SELECT distinct Q.ID, N.fulltag||\'.\'||Q.tag, Q.unit, Q.DESCRIPTION, Q.tag, Q.value FROM" \
                " (Q INNER JOIN N ON (Q.nid = N.id)) WHERE Q.nid >= ? AND Q.nid < ? AND Q.input = \'1\' ORDER BY Q.id;");
#elif defined Q_OS_WIN
  qs = QString("SELECT DISTINCT q.id, n.fulltag+\'.\'+Q.tag, q.unit, q.description, q.tag, q.[value] FROM" \
                " (Q INNER JOIN N ON (Q.nid = N.id AND Q.input = true)) WHERE Q.nid >= ? AND Q.nid < ? AND Q.input = true ORDER BY Q.id;");
#else // postgres
  qs = QString("SELECT DISTINCT Q.id, N.fulltag||\'.\'||Q.tag, Q.unit, Q.description, Q.tag, Q.value FROM"
               " (Q INNER JOIN N ON (Q.nid = N.id AND Q.input = true)) WHERE Q.nid >= ? AND Q.nid < ? AND Q.input = true ORDER BY Q.id;");
#endif
  query.prepare(qs);
  query.addBindValue(caseId);
  query.addBindValue(caseId + range);
  qDebug() << query.lastQuery();
  qDebug() << "caseId = "<<caseId <<", range = "<<range;
  queryCheck = query.exec();

  if (query.lastError().isValid())
      qCritical() << "Error while executing query in "<< Q_FUNC_INFO<<": " << query.lastError().text();
  if (!queryCheck) qCritical() << "Unknown error in "<< Q_FUNC_INFO << "executing : " << query.lastQuery();

  comboModel_ = new QSqlQueryModel(this);
  comboModel_->setQuery(query);
  while (comboModel_->canFetchMore())
    comboModel_->fetchMore();  
} // DelegateControl::DelegateControl

QWidget* DelegateControl::createEditor(QWidget* parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.column() == FULLTAG_COL) {
    QComboBox* comboBox = new QComboBox(parent);
    const QAbstractItemModel* model = index.model();
    QString curval = model->data(index).toString();
    ModelNameVariableControl* cvn = new ModelNameVariableControl(curval, comboModel_, 1, model, (QObject *)this);
    comboBox->setModel(cvn);
    comboBox->setModelColumn(0);
    comboBox->setCurrentIndex(comboBox->findText(curval));
    comboBox->setEditable(false);
    connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(commitAndCloseCombo()));
    //connect(comboBox, SIGNAL(editTextChanged(QString)), this, SLOT(commitAndCloseCombo()));
    return comboBox;
  }
  if (index.column() >= LABEL_COL && index.column() <= END_COL) {
    // label, start or end values
    QLineEdit* lineEdit = new QLineEdit(parent);
    if (index.column() >= START_COL) {
      // start and end values must be numbers
      lineEdit->setValidator(new QDoubleValidator(lineEdit));
    }
    // not editable until a variable is chosen
    lineEdit->setReadOnly(true);
    connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(commitAndCloseLineEdit()));
    return lineEdit;
  }
  if (index.column() == NPOINTS_COL) {
    // points
    QSpinBox* spinBox = new QSpinBox(parent);
    spinBox->setMinimum(1);
    spinBox->setMaximum(999);
    connect(spinBox, SIGNAL(editingFinished()), this, SLOT(commitAndCloseSpinBox()));
    return spinBox;
  }
  return QStyledItemDelegate::createEditor(parent, option, index);
} // DelegateControl::createEditor

void DelegateControl::setEditorData(QWidget* editor, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.column() == FULLTAG_COL) {
    QComboBox* comboBox = qobject_cast<QComboBox*>(editor);
    comboBox->setEditText(index.model()->data(index, Qt::DisplayRole).toString().trimmed());
  } else if (index.column() >= LABEL_COL && index.column() <= END_COL) {
    // set the LineEdits to the model data only if a variable has been chosen
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    const QAbstractItemModel* model = index.model();
    QModelIndex varidx = index.sibling(index.row(), FULLTAG_COL);
    if (!model->data(varidx).toString().isEmpty()) {
      lineEdit->setText(model->data(index, Qt::DisplayRole).toString().trimmed());
      lineEdit->setReadOnly(false);
    } else {
      lineEdit->setText("");
    }
  } else if (index.column() == NPOINTS_COL) {
    QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);
    spinBox->setValue(index.model()->data(index, Qt::DisplayRole).toInt());
  } else {
    QStyledItemDelegate::setEditorData(editor, index);
  }
} // DelegateControl::setEditorData

void DelegateControl::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.column() == FULLTAG_COL) {
    QComboBox* comboBox = qobject_cast<QComboBox*>(editor);
    QString currentModelText = index.data(Qt::EditRole).toString();
    QString currentComboBoxText = comboBox->currentText();
    // qDebug() << "currentModelText = " << currentModelText << " currentComboBoxText = " << currentComboBoxText;
    if (currentModelText != currentComboBoxText) {
      model->setData(index, comboBox->model()->data(comboBox->model()->index(comboBox->currentIndex(), 0)));

      // column 0 id/row number
      QModelIndex targetIndex = index.sibling(index.row(), ID_COL);
      model->setData(targetIndex, index.row() + 1);

      QModelIndex sourceIndex = comboModel_->index(comboBox->currentIndex(), 2);
      QString uom = comboModel_->data(sourceIndex).toString().trimmed();
      targetIndex = index.sibling(index.row(), UOM_COL);
      model->setData(targetIndex, uom);

      sourceIndex = comboModel_->index(comboBox->currentIndex(), 3);
      QString description = comboModel_->data(sourceIndex).toString().trimmed();
      targetIndex = index.sibling(index.row(), DESCR_COL);
      model->setData(targetIndex, description);

      // column 4 label/tag
      sourceIndex = comboModel_->index(comboBox->currentIndex(), 4);
      QString tag = comboModel_->data(sourceIndex).toString().trimmed();
      targetIndex = index.sibling(index.row(), LABEL_COL);
      model->setData(targetIndex, tag);

      // column 5, 6 start and end
      sourceIndex = comboModel_->index(comboBox->currentIndex(), 5);
      double value = comboModel_->data(sourceIndex).toDouble();
      targetIndex = index.sibling(index.row(), START_COL);
      model->setData(targetIndex, value);
      targetIndex = index.sibling(index.row(), END_COL);
      model->setData(targetIndex, value);

      // don't reset points, use whatever user has set

      // column 8 (not displayed) dbid
      sourceIndex = comboModel_->index(comboBox->currentIndex(), 0);
      int dbid = comboModel_->data(sourceIndex).toInt();
      targetIndex = index.sibling(index.row(), DBID_COL);
      model->setData(targetIndex, dbid);

      // -> Luiz
      QVariant sValue = model->data(model->index(index.row(), START_COL), Qt::EditRole);
      QVariant eValue = model->data(model->index(index.row(), END_COL), Qt::EditRole);
      QVariant uValue = model->data(model->index(index.row(), UOM_COL), Qt::EditRole);

      const Dimension *dim = unitEngine_.pdimension(std::string(uValue.toString().trimmed().toUtf8()));
      if (dim != NULL) {
        std::string newUnit = dim->unit(*u_);
        QString newUnitQs = QString::fromUtf8(newUnit.c_str());
        QVariant newDisplay = unitEngine_.fromSI(sValue.toDouble(), newUnit);
        model->setData(model->index(index.row(), START_COL), newDisplay, Qt::DisplayRole);
        newDisplay = unitEngine_.fromSI(eValue.toDouble(), newUnit);
        model->setData(model->index(index.row(), END_COL), newDisplay, Qt::DisplayRole);
        model->setData(model->index(index.row(), UOM_COL), newUnitQs, Qt::DisplayRole);
      } else {
        qCritical() << uValue.toString().trimmed().toUtf8() << " has no associated dimension !";
      }
      // <- Luiz
    } // text has changed
  } else if (index.column() == LABEL_COL) {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    model->setData(index, lineEdit->text());
  } else if (index.column() == START_COL || index.column() == END_COL) {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    double value = lineEdit->text().toDouble();
    QModelIndex sibIndex = index.sibling(index.row(), UOM_COL);

    QString siValue = setNewValue(value,model->data(sibIndex, Qt::DisplayRole).toString().trimmed());

    model->setData(index, value, Qt::DisplayRole);
    model->setData(index, siValue, Qt::EditRole);
    //model->setData(index, lineEdit->text().toDouble());
  } else if (index.column() == NPOINTS_COL) {
    QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);
    model->setData(index, spinBox->value());
  } else {
    QStyledItemDelegate::setModelData(editor, model, index);
  }
} // DelegateControl::setModelData

QString DelegateControl::setNewValue(double newValue, QString newUnit) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  UnitArray unit_;
  double newDisplay = unitEngine_.toSI(newValue,std::string(newUnit.toUtf8()), unit_);
  return QString::number(newDisplay);
} // DelegateControl::setNewValue

/// This slot is called every time the data from the cells in the fulltag column is updated
void DelegateControl::commitAndCloseCombo(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QComboBox* comboBox = qobject_cast<QComboBox*>(sender());
  emit commitData(comboBox);
  emit closeEditor(comboBox);
  emit currentIndexChanged();
} // DelegateControl::commitAndCloseCombo

/// This slot is called every time the data from the label, start and end cells are updated
void DelegateControl::commitAndCloseLineEdit(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QLineEdit* lineEdit = qobject_cast<QLineEdit*>(sender());
  emit commitData(lineEdit);
  emit closeEditor(lineEdit);
} // DelegateControl::commitAndCloseLineEdit

void DelegateControl::commitAndCloseSpinBox(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSpinBox* spinBox = qobject_cast<QSpinBox*>(sender());
  emit commitData(spinBox);
  emit closeEditor(spinBox);
} // DelegateControl::commitAndCloseSpinBox

int DelegateControl::hasFullTag(const QString &fullTag) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // qDebug() << "Now checking " << fullTag;
  for (int i = 0; i < comboModel_->rowCount(); ++i) {
    QModelIndex index = comboModel_->index(i, 1);
    // strip root object tag
    QString indexFullTag = index.data().toString();
    int firstSeparator = indexFullTag.indexOf(QRegExp("[\\.:]"));
    if (firstSeparator >= 0) {
      // qDebug() << "Looking at " << indexFullTag << ", has separator @" << firstSeparator;
      indexFullTag = indexFullTag.right(indexFullTag.size() - firstSeparator - 1);
    } else {
      // qDebug() << "Looking at " << indexFullTag << ", has no separator";
    }
    if (indexFullTag == fullTag) {
      // qDebug() << "Found, row = " << index.row();
      return index.row();
    }
  }
  qCritical() << "Fulltag not found !";
  return -1;
} // DelegateControl::hasFullTag
