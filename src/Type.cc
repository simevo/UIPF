/** @file Type.cc
    @brief Implementation for the Type class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include "Type.h"

int Type::idgen_(0);

Type::Type(std::string n, std::string d) : id_(idgen_++), name_(n), description_(d) { } // Type::Type

int Type::id(void) const {
  return id_;
} // Type::id

const std::string &Type::name(void) const {
  return name_;
} // Type::name

const std::string &Type::description(void) const {
  return description_;
} // Type::description
