/** @file Hierarchy.cc
    @brief Define class Hierarchy for reflection

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <cstdlib> // for exit
#include <cstring> // for strlen
#include <algorithm> // for find_if
#include <functional>  // for bind2nd
#include <fstream>
#include <QDebug>

#include "Hierarchy.h"

const int BUF_SIZE = 256;

std::string simple_(const std::string &s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  std::string s_(s);
  std::size_t found = s_.find('<');
  if (found != std::string::npos) {
    //std::cout << "before = " << s << std::endl;
    s_ = s_.substr(0, found);
    //std::cout << "after = " << s << std::endl;
  }
  return s_;
} // simple_

void removeblanks(std::string & s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  std::string::size_type pos = 0;
  bool spacesLeft = true;
  while (spacesLeft) {
    pos = s.find(" ");
    if (pos != std::string::npos)
      s.erase(pos, 1);
    else
      spacesLeft = false;
  }
} // removeblanks

int Hierarchy::id_(std::string n) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  removeblanks(n);
  if (typesmap_.find(n) == typesmap_.end()) {
    qCritical("**************************************** Unknown type %s\n", n.c_str());
    return -1;
  } else {
    return typesmap_[n];
  }
} // Hierarchy::id_

const std::string &Hierarchy::name_(int i) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  static std::string empty("");
  if (rtypesmap_.find(i) == rtypesmap_.end())
    return empty;
  else
    return rtypesmap_[i];
} // Hierarchy::id_

int Hierarchy::insertType(std::string n, std::string d) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  removeblanks(n);
  if (typesmap_.find(n) != typesmap_.end()) {
    qCritical("Type %s found at line %d already exists\n", n.c_str(), lineCount_);
    exit(-6);
  }
  types_.push_back(Type(n, d));
  int id = types_.back().id();
  typesmap_[n] = id;
  rtypesmap_[id] = n;

  std::string sn = simple_(n);
  if (sn != n) {
    generics_[sn].push_back(id);
  } // is a generic

  return id;
} // Hierarchy::insertType

int Hierarchy::closeDown(int ibase, int iderived) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  int inserted = 0;
  // insert into tc (start, finish, pathlength) select start, ID, pathlength+1 from tc where finish = PARENT;
  for (std::set<std::pair<int, int> >::const_iterator p = tc_.begin(); p != tc_.end(); ++p) {
    if (p->second == ibase) {
      inserted += transitiveClosure(p->first, iderived);
    }
  } // for each dependency
  return inserted;
} // Hierarchy::closeDown

int Hierarchy::transitiveClosure(int ibase, int iderived) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  int totalInserted = 0;
  // insert into tc (start, finish, pathlength) select PARENT, ID, 1;
  totalInserted += (tc_.insert(std::pair<int, int>(ibase, iderived)).second ? 1 : 0);

  int inserted;
  for (; (inserted = closeDown(ibase, iderived)) > 0; )
    totalInserted += inserted;
  return totalInserted;
} // Hierarchy::transitiveClosure

void Hierarchy::insertDependency(int ibase, int iderived) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  dependencies_.insert(std::pair<int, int>(ibase, iderived));
  tc_.insert(std::pair<int, int>(ibase, iderived));
} // Hierarchy::insertDependency

void Hierarchy::insertDependency(std::string base, std::string derived) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  insertDependency(id_(base), id_(derived));
} // Hierarchy::insertDependency

void Hierarchy::insertSynonym(std::string newname, std::string source) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  removeblanks(newname);
  removeblanks(source);
  //   std::cout << "Inserting synonym " << newname << " = " << source << std::endl;
  int id = id_(source);
  typesmap_[newname] = id;

  for (std::map<std::string, int>::iterator p = typesmap_.begin(); p != typesmap_.end(); ++p) {
    std::string candidate = p->first;
    std::string::size_type found;
    while ((found = candidate.find(source)) != std::string::npos) {
      candidate.replace(found, source.size(), newname);
      //       std::cout << "also inserting synonym " << candidate << " = " << p->first << std::endl;
      typesmap_[candidate] = p->second;
    } // found
  } // for each
} // Hierarchy::insertSynonym

bool Hierarchy::isA_(int ibase, int iderived) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  return (std::find_if(tc_.begin(), tc_.end(), std::bind2nd(std::equal_to<std::pair<int, int> >(), std::pair<int,int>(ibase, iderived))) != tc_.end());
} // Hierarchy::isA_

bool Hierarchy::isA(std::string c, std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if (s == "Stream") {
    return (!c.compare(0, 6, "Stream"));
  } else if (s == "Phase") {
    return (!c.compare(0, 5, "Phase"));
  } else if ( (s == "Reaction") || (s == "HeatRating") || (s == "RatingColumn") ) {
    // nosy: for the time being, show HeatRatings and RatingColumn as Reactions
    return (!c.compare(0, 8, "Reaction"));
  } else {
    int iderived = id_(c);
    int ibase = id_(s);
    return isA_(ibase, iderived);
  }
} // Hierarchy::isA

bool Hierarchy::isAGeneric(std::string c, std::string s) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  std::map<std::string, std::list<int> >::iterator generic = generics_.find(s);
  if (generic == generics_.end()) {
    qCritical("Type %s is not a generic\n", s.c_str());
    exit(-1);
  }
  int iderived = id_(c);
  std::list<int> ids = generic->second;
  for (std::list<int>::const_iterator i = ids.begin(); i != ids.end(); ++i)
    if (isA_(*i, iderived)) return true;
  return false;
} // Hierarchy::isAGeneric

void Hierarchy::start(void) {
  m_ = types_.begin();
} // Hierarchy::start

const Type* Hierarchy::next(void) {
  //qDebug() << "Entering "<< Q_FUNC_INFO;
  if (m_ != types_.end()) {
    return &(*m_++);
  } else {
    return NULL;
  }
} // Hierarchy::next

bool Hierarchy::end(void) {
  return (m_ == types_.end());
} // Hierarchy::end

Hierarchy::Hierarchy(std::string typesFile) : lineCount_(0) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // manual
  insertType("adjacency_list<boost::vecS,boost::vecS,boost::bidirectionalS,VertexBase*,EdgeBase*,boost::no_property,boost::listS>");
  insertType("edge_desc_impl<boost::bidirectional_tag,unsigned int>");
  insertType("counting_iterator<unsigned int,boost::use_default,boost::use_default>");

  // insert here below the code generated by process_xml.xq / generate_hierarchy.xslt
  // SNIP !

  insertType("AnyItem");
  insertType("Assignment");
  insertType("ChoiOkos");
  insertType("ComponentBase");
  insertType("ComponentBiochem");
  insertType("ComponentCmu");
  insertType("ComponentDippr");
  insertType("ComponentGeneric");
  insertType("ComponentPdippr");
  insertType("ComponentSolid");
  insertType("Compressor");
  insertType("Cut");
  insertType("CutsSolution");
  insertType("Decanter");
  insertType("Dimension");
  insertType("dippr");
  insertType("Direction");
  insertType("Divider");
  insertType("Multiplier");
  insertType("EdgeBase");
  insertType("EosCubicInterface");
  insertType("EosInterface");
  insertType("eosLiquid");
  insertType("eosLiquidDippr");
  insertType("eosSolid");
  insertType("eosSolid_dummy");
  insertType("eosVapor");
  insertType("eosVapor_dummy");
  insertType("eosVaporIdeal");
  insertType("Exchanger");
  insertType("Flash");
  insertType("FlashBase");
  insertType("FlashDrum");
  insertType("FlashDrumBase");
  insertType("FlashMode");
  insertType("FlashVl");
  insertType("FlashVlT");
  insertType("FlashVlTx");
  insertType("FlowPattern");
  insertType("FlowPatternAscii<6,6>");
  insertType("FlowPatternAscii<9,6>");
  insertType("FlowPatternDummy");
  insertType("FlowPatternSpiralRight66");
  insertType("FlowPatternSpiralRight96");
  insertType("FlowSheetBase");
  insertType("FlowSheet<Vertex<1,2>>");
  insertType("FlowSheet<Vertex<1,3>>");
  insertType("GenericActive<double>");
  insertType("GenericQuantity<double>");
  insertType("GroupBkk");
  insertType("heatLiquid");
  insertType("heatSolid");
  insertType("heatSolid_bkk");
  insertType("heatSolid_dummy");
  insertType("heatVapor");
  insertType("heatVaporCmuComponent");
  insertType("heatVaporDippr");
  insertType("heatVapor_dummy");
  insertType("heatVaporVolp");
  insertType("Integer");
  insertType("Jacobian");
  insertType("ManyMany");
  insertType("ManyOne");
  insertType("ManyTwo");
  insertType("MassBalanceMode");
  insertType("Mixer");
  insertType("Model");
  insertType("ModelInterface");
  insertType("MultiExchanger");
  insertType("MultiReaction<2>");
  insertType("MultiReactionElectrochemical");
  insertType("MultiReactionH2Crossover");
  insertType("MultiReactionMcfc");
  insertType("MultiReactionSofc");
  insertType("MultiReactionTransfer");
  insertType("ObjectiveNle");
  insertType("ObjectiveNleAd");
  insertType("ObjectiveNleAdFlowsheet");
  insertType("ObjectiveNleAdVertex");
  insertType("OneMany");
  insertType("ParameterAntoine");
  insertType("ParameterBkk");
  insertType("ParameterChoiOkos");
  insertType("ParameterDippr7");
  insertType("ParameterHenry");
  insertType("ParameterVolp");
  insertType("pdippr");
  insertType("Persistency");
  insertType("Persistent");
  insertType("PersistentInterface");
  insertType("Pointer");
  insertType("Precedence");
  insertType("PressureSwingAbsorption");
  insertType("pure");
  insertType("Registrar");
  insertType("saturation");
  insertType("saturationCmuComponent");
  insertType("saturation_dummy");
  insertType("Scaler");
  insertType("ScalingMode");
  insertType("SequentialAssembly");
  insertType("SmartEnum");
  insertType("SolverNleAuto");
  insertType("SolverNleInterface");
  insertType("SolverNleNumeric");
  insertType("Splitter");
  insertType("String");
  insertType("Task");
  insertType("TaskGraph");
  insertType("TaskList");
  insertType("Terminator");
  insertType("Total");
  insertType("transport");
  insertType("transportCmuComponent");
  insertType("transportDippr");
  insertType("transport_dummy");
  insertType("UnitArrayGen<int>");
  insertType("UnitEngine");
  insertType("Vertex<1,2>");
  insertType("Vertex<1,3>");
  insertType("Vertex<2,2>");
  insertType("VertexBase");
  insertType("ZeroZero");
  insertSynonym("Active", "GenericActive<double>");
  insertSynonym("cpig_parameter", "ParameterDippr7");
  insertSynonym("dnldip_parameter", "ParameterDippr7");
  insertSynonym("Graph", "adjacency_list<boost::vecS,boost::vecS,boost::bidirectionalS,VertexBase*,EdgeBase*,boost::no_property,boost::listS>");
  insertSynonym("GraphEdge", "edge_desc_impl<boost::bidirectional_tag,unsigned int>");
  insertSynonym("kldip_parameter", "ParameterDippr7");
  insertSynonym("kvdip_parameter", "ParameterDippr7");
  insertSynonym("muldip_parameter", "ParameterDippr7");
  insertSynonym("muvdip_parameter", "ParameterDippr7");
  insertSynonym("Quantity", "GenericQuantity<double>");
  insertSynonym("sigdip_parameter", "ParameterDippr7");
  insertSynonym("UnitArray", "UnitArrayGen<int>");
  insertSynonym("vertex_iter", "counting_iterator<unsigned int,boost::use_default,boost::use_default>");
  insertDependency("ChoiOkos", "ComponentBiochem");
  insertDependency("ComponentBase", "ComponentBiochem");
  insertDependency("ComponentBase", "ComponentCmu");
  insertDependency("ComponentBase", "ComponentDippr");
  insertDependency("ComponentBase", "ComponentPdippr");
  insertDependency("ComponentBase", "ComponentSolid");
  insertDependency("ComponentGeneric", "ComponentBiochem");
  insertDependency("ComponentGeneric", "ComponentCmu");
  insertDependency("ComponentGeneric", "ComponentDippr");
  insertDependency("ComponentGeneric", "ComponentPdippr");
  insertDependency("ComponentGeneric", "ComponentSolid");
  insertDependency("dippr", "ComponentDippr");
  insertDependency("EosInterface", "EosCubicInterface");
  insertDependency("eosLiquid", "ChoiOkos");
  insertDependency("eosLiquid", "ComponentGeneric");
  insertDependency("eosLiquid", "ComponentSolid");
  insertDependency("eosLiquidDippr", "dippr");
  insertDependency("eosLiquid", "eosLiquidDippr");
  insertDependency("eosLiquid", "pdippr");
  insertDependency("eosSolid", "ChoiOkos");
  insertDependency("eosSolid", "ComponentGeneric");
  insertDependency("eosSolid_dummy", "ComponentDippr");
  insertDependency("eosSolid_dummy", "ComponentPdippr");
  insertDependency("eosSolid_dummy", "ComponentSolid");
  insertDependency("eosSolid", "eosSolid_dummy");
  insertDependency("eosVapor", "ComponentGeneric");
  insertDependency("eosVapor_dummy", "ComponentBiochem");
  insertDependency("eosVapor_dummy", "ComponentSolid");
  insertDependency("eosVapor", "eosVapor_dummy");
  insertDependency("eosVapor", "eosVaporIdeal");
  insertDependency("eosVaporIdeal", "ComponentCmu");
  insertDependency("eosVaporIdeal", "dippr");
  insertDependency("eosVaporIdeal", "pdippr");
  insertDependency("FlowPatternAscii<6,6>", "FlowPatternSpiralRight66");
  insertDependency("FlowPatternAscii<9,6>", "FlowPatternSpiralRight96");
  insertDependency("FlowPattern", "FlowPatternAscii<6,6>");
  insertDependency("FlowPattern", "FlowPatternAscii<9,6>");
  insertDependency("FlowPattern", "FlowPatternDummy");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<1,2>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<1,3>>");
  insertDependency("FlowSheet<Vertex<1,2>>", "PressureSwingAbsorption");
  insertDependency("FlowSheet<Vertex<1,3>>", "Decanter");
  insertDependency("GenericActive<double>", "GenericQuantity<double>");
  insertDependency("GroupBkk", "heatSolid_bkk");
  insertDependency("heatLiquid", "ChoiOkos");
  insertDependency("heatLiquid", "ComponentGeneric");
  insertDependency("heatLiquid", "dippr");
  insertDependency("heatLiquid", "heatSolid_bkk");
  insertDependency("heatLiquid", "pdippr");
  insertDependency("heatSolid_bkk", "ComponentSolid");
  insertDependency("heatSolid", "ChoiOkos");
  insertDependency("heatSolid", "ComponentGeneric");
  insertDependency("heatSolid_dummy", "ComponentDippr");
  insertDependency("heatSolid_dummy", "ComponentPdippr");
  insertDependency("heatSolid", "heatSolid_bkk");
  insertDependency("heatSolid", "heatSolid_dummy");
  insertDependency("heatVaporCmuComponent", "ComponentCmu");
  insertDependency("heatVapor", "ComponentGeneric");
  insertDependency("heatVaporDippr", "dippr");
  insertDependency("heatVapor_dummy", "ComponentBiochem");
  insertDependency("heatVapor_dummy", "ComponentSolid");
  insertDependency("heatVapor", "heatVaporCmuComponent");
  insertDependency("heatVapor", "heatVaporDippr");
  insertDependency("heatVapor", "heatVapor_dummy");
  insertDependency("heatVapor", "heatVaporVolp");
  insertDependency("heatVapor", "pdippr");
  insertDependency("ManyMany", "MultiExchanger");
  insertDependency("ManyMany", "Terminator");
  insertDependency("ManyMany", "Vertex<1,2>");
  insertDependency("ManyMany", "Vertex<1,3>");
  insertDependency("ManyMany", "Vertex<2,2>");
  insertDependency("ManyOne", "Compressor");
  insertDependency("ManyOne", "FlashDrum");
  insertDependency("ManyOne", "Mixer");
  insertDependency("Model", "Compressor");
  insertDependency("Model", "Decanter");
  insertDependency("Model", "Divider");
  insertDependency("Model", "Multiplier");
  insertDependency("Model", "Exchanger");
  insertDependency("Model", "FlashDrum");
  insertDependency("ModelInterface", "EdgeBase");
  insertDependency("ModelInterface", "FlashDrumBase");
  insertDependency("ModelInterface", "FlowSheetBase");
  insertDependency("ModelInterface", "Model");
  insertDependency("ModelInterface", "VertexBase");
  insertDependency("Model", "Mixer");
  insertDependency("Model", "MultiExchanger");
  insertDependency("Model", "MultiReaction<2>");
  insertDependency("Model", "PressureSwingAbsorption");
  insertDependency("Model", "Splitter");
  insertDependency("Model", "Terminator");
  insertDependency("MultiReaction<2>", "MultiReactionElectrochemical");
  insertDependency("MultiReaction<2>", "MultiReactionH2Crossover");
  insertDependency("MultiReaction<2>", "MultiReactionTransfer");
  insertDependency("MultiReactionElectrochemical", "MultiReactionMcfc");
  insertDependency("MultiReactionElectrochemical", "MultiReactionSofc");
  insertDependency("ObjectiveNleAd", "Flash");
  insertDependency("ObjectiveNleAdFlowsheet", "FlowSheet<Vertex<1,2>>");
  insertDependency("ObjectiveNleAdFlowsheet", "FlowSheet<Vertex<1,3>>");
  insertDependency("ObjectiveNleAd", "ObjectiveNleAdVertex");
  insertDependency("ObjectiveNleAdVertex", "Compressor");
  insertDependency("ObjectiveNleAdVertex", "Exchanger");
  insertDependency("ObjectiveNleAdVertex", "FlashDrumBase");
  insertDependency("ObjectiveNleAdVertex", "MultiExchanger");
  insertDependency("ObjectiveNleAdVertex", "ObjectiveNleAdFlowsheet");
  insertDependency("ObjectiveNle", "ObjectiveNleAd");
  insertDependency("OneMany", "Divider");
  insertDependency("OneMany", "Multiplier");
  insertDependency("OneMany", "Splitter");
  insertDependency("pdippr", "ComponentPdippr");
  insertDependency("Persistent", "AnyItem");
  insertDependency("Persistent", "GenericQuantity<double>");
  insertDependency("Persistent", "Integer");
  insertDependency("PersistentInterface", "ModelInterface");
  insertDependency("PersistentInterface", "Persistent");
  insertDependency("Persistent", "Model");
  insertDependency("Persistent", "Pointer");
  insertDependency("Persistent", "String");
  insertDependency("Precedence", "EdgeBase");
  insertDependency("pure", "ComponentBase");
  insertDependency("pure", "ComponentGeneric");
  insertDependency("pure", "dippr");
  insertDependency("pure", "eosVaporIdeal");
  insertDependency("pure", "heatVaporCmuComponent");
  insertDependency("pure", "pdippr");
  insertDependency("pure", "transport");
  insertDependency("pure", "transportDippr");
  insertDependency("Registrar", "ModelInterface");
  insertDependency("saturationCmuComponent", "ComponentCmu");
  insertDependency("saturation", "ComponentGeneric");
  insertDependency("saturation", "dippr");
  insertDependency("saturation_dummy", "ComponentBiochem");
  insertDependency("saturation_dummy", "ComponentSolid");
  insertDependency("saturation", "pdippr");
  insertDependency("saturation", "saturationCmuComponent");
  insertDependency("saturation", "saturation_dummy");
  insertDependency("SequentialAssembly", "FlowSheetBase");
  insertDependency("SmartEnum", "CutsSolution");
  insertDependency("SmartEnum", "Direction");
  insertDependency("SmartEnum", "FlashMode");
  insertDependency("SmartEnum", "MassBalanceMode");
  insertDependency("SmartEnum", "PhaseIndex");
  insertDependency("SmartEnum", "PhaseType");
  insertDependency("SmartEnum", "ScalingMode");
  insertDependency("SolverNleInterface", "SolverNleAuto");
  insertDependency("SolverNleInterface", "SolverNleNumeric");
  insertDependency("TaskGraph", "FlowSheetBase");
  insertDependency("Task", "VertexBase");
  insertDependency("transportCmuComponent", "ComponentCmu");
  insertDependency("transport", "ComponentGeneric");
  insertDependency("transportDippr", "ComponentDippr");
  insertDependency("transportDippr", "ComponentPdippr");
  insertDependency("transport_dummy", "ComponentBiochem");
  insertDependency("transport_dummy", "ComponentSolid");
  insertDependency("transport", "transportCmuComponent");
  insertDependency("transport", "transportDippr");
  insertDependency("transport", "transport_dummy");
  insertDependency("Vertex<1,2>", "FlowSheet<Vertex<1,2>>");
  insertDependency("Vertex<1,3>", "FlowSheet<Vertex<1,3>>");
  insertDependency("Vertex<2,2>", "Exchanger");
  insertDependency("VertexBase", "ManyMany");
  insertDependency("VertexBase", "ManyOne");
  insertDependency("VertexBase", "ManyTwo");
  insertDependency("VertexBase", "OneMany");
  insertDependency("VertexBase", "ZeroZero");

  // SNIP !
  // insert above the code generated by process_xml.xq / generate_hierarchy.xslt

  // manual

  insertType("Column");
  insertDependency("FlowSheet<Vertex<1,3>>", "Column");

  insertType("MultiStage");

  insertType("FlashSplitter");
  insertDependency("Model", "FlashSplitter");
  insertDependency("FlashDrumBase", "FlashSplitter");
  insertDependency("ManyMany", "FlashSplitter");

  insertType("FlashDegasser");
  insertDependency("Model", "FlashDegasser");
  insertDependency("FlashDrumBase", "FlashDegasser");
  insertDependency("ManyMany", "FlashDegasser");

  insertType("Separator");
  insertDependency("Model", "Separator");
  insertDependency("ManyMany", "Separator");

  // extracted
  insertType("MultiStage2D");

  insertType("FlowSheet<ZeroZero>");
  insertDependency("ZeroZero", "FlowSheet<ZeroZero>");
  insertDependency("FlowSheetBase", "FlowSheet<ZeroZero>");
  insertType("FlowSheet<ManyMany>");
  insertDependency("ManyMany", "FlowSheet<ManyMany>");
  insertDependency("FlowSheetBase", "FlowSheet<ManyMany>");
  insertType("FlowSheet<Vertex<4,3>>");
  insertType("Vertex<4,3>");
  insertDependency("ManyMany", "Vertex<4,3>");
  insertDependency("Vertex<4,3>", "FlowSheet<Vertex<4,3>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<4,3>>");
  insertType("FlowSheet<Vertex<4,2>>");
  insertType("Vertex<4,2>");
  insertDependency("ManyMany", "Vertex<4,2>");
  insertDependency("Vertex<4,2>", "FlowSheet<Vertex<4,2>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<4,2>>");
  insertType("FlowSheet<Vertex<3,2>>");
  insertType("Vertex<3,2>");
  insertDependency("ManyMany", "Vertex<3,2>");
  insertDependency("Vertex<3,2>", "FlowSheet<Vertex<3,2>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<3,2>>");
  insertType("FlowSheet<Vertex<2,2>>");
  insertDependency("Vertex<2,2>", "FlowSheet<Vertex<2,2>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<2,2>>");
  insertType("FlowSheet<Vertex<2,3>>");
  insertType("Vertex<2,3>");
  insertDependency("ManyMany", "Vertex<2,3>");
  insertDependency("Vertex<2,3>", "FlowSheet<Vertex<2,3>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<2,3>>");
  insertType("FlowSheet<Vertex<3,3>>");
  insertType("Vertex<3,3>");
  insertDependency("ManyMany", "Vertex<3,3>");
  insertDependency("Vertex<3,3>", "FlowSheet<Vertex<3,3>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<3,3>>");
  insertType("FlowSheet<Vertex<3,1>>");
  insertType("Vertex<3,1>");
  insertDependency("ManyMany", "Vertex<3,1>");
  insertDependency("Vertex<3,1>", "FlowSheet<Vertex<3,1>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<3,1>>");
  insertType("FlowSheet<Vertex<2,1>>");
  insertType("Vertex<2,1>");
  insertDependency("Vertex<2,1>", "FlowSheet<Vertex<2,1>>");
  insertDependency("FlowSheetBase", "FlowSheet<Vertex<2,1>>");

  insertType("CompressionStage");
  insertType("MultiCompressorIntercooled1");
  insertType("MultiCompressorIntercooled2");
  insertType("MultiCompressorIntercooled3");
  insertDependency("FlowSheet<Vertex<2,2>>", "CompressionStage");
  insertDependency("FlowSheet<Vertex<1,2>>", "MultiCompressorIntercooled1");
  insertDependency("FlowSheet<Vertex<1,2>>", "MultiCompressorIntercooled2");
  insertDependency("FlowSheet<Vertex<1,2>>", "MultiCompressorIntercooled3");
  insertDependency("FlowSheet<Vertex<1,2>>", "MultiStage");
  insertDependency("FlowSheet<Vertex<1,2>>", "MultiStage2D");

  // manual again
  insertType("HeatTransfer");
  insertDependency("FlashDrum", "HeatTransfer");
  insertType("ShellAndTube");
  insertDependency("HeatRating", "ShellAndTube");
  insertType("ShellAndTubeThermosiphon");
  insertDependency("ShellAndTube", "ShellAndTubeThermosiphon");
  insertType("ShellAndTubeHeater");
  insertDependency("ShellAndTube", "ShellAndTubeHeater");
  insertType("ShellAndTubeFallingFilmReboiler");
  insertDependency("ShellAndTube", "ShellAndTubeFallingFilmReboiler");

  insertType("Pipe");
  insertDependency("FlashDrum", "Pipe");
  insertType("Pump");
  insertDependency("FlashDrum", "Pump");

  insertType("Zone");
  insertDependency("FlowSheet<Vertex<2,2>>", "Zone");
  insertType("ReboilerCrossFlow");
  insertDependency("FlowSheet<Vertex<2,2>>", "ReboilerCrossFlow");

  std::ifstream a;
  int i;
  char buf[3][BUF_SIZE];

  a.open(typesFile.c_str());

  if (!a) {
    qCritical("Error in Hierarchy constructor: cannot open file %s\n", typesFile.c_str());
  } else {
    i = 0;
    while (!a.eof()) {
      a.getline(buf[i], BUF_SIZE);
      if (!a.eof()) {
        // std::cout << "Read: " << buf[i] << std::endl;
        if (buf[i][0] != '#') {
          // qDebug("Read: %s\n", buf[i]);
          if (strlen(buf[i]) == 0) {
            i = 0;
          } else if (i == 0) {
            ++i;
          } else if (i == 1) {
            insertType(buf[0], buf[1]);
            ++i;
          } else {
            // std::cout << buf[0] << " is derived from " << buf[i] << std::endl;
            insertDependency(buf[i], buf[0]);
          }
        } // not a comment
        lineCount_++;
      } // | eof
    } // while not eof

    a.close();
  } // if no input stream open error

  std::set<std::pair<int, int> > tc_start = tc_;
  for (std::set<std::pair<int, int> >::iterator p = tc_start.begin(); p != tc_start.end(); ++p) {
    transitiveClosure(p->first, p->second);
  }
  /*
     // optional diagnostic prints
     std::cout << "================================ generics" << std::endl;
     for (std::map<std::string, std::list<int> >::iterator generic = generics_.begin(); generic != generics_.end(); ++generic) {

     std::cout << "================ " << generic->first << ": ";
     for (std::list<int>::const_iterator i=generic->second.begin(); i != generic->second.end(); ++i) {
      if (i != generic->second.begin())
        std::cout << ", ";
      std::cout << name_(*i);
     }
     std::cout << std::endl;

     } // for each generic

     std::cout << std::endl << "================================ EdgeBase derived classes" << std::endl;
     for (start(); !end(); ) {
     const Type *t(next());
     if (t == NULL)  {
      qDebug("NULL Type\n");
      exit(-2);
     }
     if (isA(t->name(), "EdgeBase"))
      std::cout << "================ " << t->name() << std::endl;
     }

     std::cout << std::endl << "================================ VertexBase derived classes" << std::endl;
     for (start(); !end(); ) {
     const Type *t(next());
     if (t == NULL)  {
      qDebug("NULL Type\n");
      exit(-2);
     }
     if (isA(t->name(), "VertexBase"))
      std::cout << "================ " << t->name() << std::endl;
     }

     std::cout << std::endl << "================================ FlowSheet<ZeroZero> derived classes" << std::endl;
     for (start(); !end(); ) {
     const Type *t(next());
     if (t == NULL)  {
      qDebug("NULL Type\n");
      exit(-2);
     }
     if (isA(t->name(), "FlowSheet<ZeroZero>")) {
      std::cout << t->name() << " is a FlowSheet<ZeroZero>" << std::endl;
     }
     }

     std::cout << std::endl << "================================ TRANSITIVE CLOSURE:" << std::endl;
     for (std::set<std::pair<int, int> >::const_iterator p=tc_.begin(); p!=tc_.end(); ++p) {
      std::cout << name_(p->first) << "(" << p->first << ") > " << name_(p->second) << "(" << p->second << ")" << std::endl;
     }
   */
  qDebug() << "Exiting "<< Q_FUNC_INFO;
} // Hierarchy::Hierarchy
