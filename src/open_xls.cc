/** @file open_xls.cc
    @brief Implementation of the Windows-specific code for opening Excel application

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */
#include <qglobal.h>
#ifdef Q_OS_WIN

#include <stdio.h>
#include <windows.h>

#include "open_xls.h"

#ifdef _MSC_VER
// quick and dirty;
// functions have same signature
// but do not behave in the same way when buflen-1 or more characters are sent
// TODO: fix properly
#define snprintf _snprintf
#endif // _MSC_VER

// disable annoying MSVC 2005 "This function or variable may be unsafe" warning
#pragma warning(disable : 4996)

// based on code generated using B2CSE.exe
// Converts Visual Basic Automation Code to Visual C++
// http://support.microsoft.com/?scid=kb%3Ben-us%3B216388&x=15&y=10

HRESULT AutoWrap(int autoType, VARIANT* pvResult, IDispatch* pDisp, LPOLESTR ptName, int cArgs ...) {
  // Begin variable-argument list...
  va_list marker;
  va_start(marker, cArgs);

  if (!pDisp) {
    // MessageBoxA(NULL, "NULL IDispatch passed to AutoWrap()", "Error", 0x10010);
    return -666;
  }
  // Variables used...
  DISPPARAMS dp = {
    NULL, NULL, 0, 0
  };
  DISPID dispidNamed = DISPID_PROPERTYPUT;
  DISPID dispID;
  HRESULT hr;
  char buf[200];
  char szName[200];

  // Convert down to ANSI
  WideCharToMultiByte(CP_ACP, 0, ptName, -1, szName, 256, NULL, NULL);

  // Get DISPID for name passed...
  hr = pDisp->GetIDsOfNames(IID_NULL, &ptName, 1, LOCALE_USER_DEFAULT, &dispID);
  if (FAILED(hr)) {
    snprintf(buf, sizeof(buf), "IDispatch::GetIDsOfNames(\"%s\") failed w/err 0x%08lx", szName, hr);
    // MessageBoxA(NULL, buf, "AutoWrap()", 0x10010);
    return hr;
  }
  // Allocate memory for arguments...
  VARIANT* pArgs = new VARIANT[cArgs + 1];
  // Extract arguments...
  for (int i = 0; i < cArgs; i++) {
    pArgs[i] = va_arg(marker, VARIANT);
  }
  // Build DISPPARAMS
  dp.cArgs = cArgs;
  dp.rgvarg = pArgs;

  // Handle special-case for property-puts!
  if (autoType & DISPATCH_PROPERTYPUT) {
    dp.cNamedArgs = 1;
    dp.rgdispidNamedArgs = &dispidNamed;
  }
  // Make the call!
  hr = pDisp->Invoke(dispID, IID_NULL, LOCALE_SYSTEM_DEFAULT, autoType, &dp, pvResult, NULL, NULL);
  if (FAILED(hr)) {
    snprintf(buf, sizeof(buf), "IDispatch::Invoke(\"%s\"=%08lx) failed w/err 0x%08lx", szName, dispID, hr);
    // MessageBoxA(NULL, buf, "AutoWrap()", 0x10010);
    return hr;
  }
  // End variable-argument section...
  va_end(marker);

  delete [] pArgs;

  return hr;
} // AutoWrap

bool openXlsMain(const wchar_t* fileAndPath, int node) {
  HRESULT hr;
  VARIANT root[64] = {
    0
  };                      // Generic IDispatchs
  VARIANT parm[64] = {
    0
  };                      // Generic Parameters
  VARIANT rVal = {
    0
  };                  // Temporary result holder
  int level = 0; // Current index into root[]

  // Initialize the OLE Library...
  OleInitialize(NULL);

  // Line 1: dim app as object
  VARIANT app = {
    0
  };

  // Line 2: set app = createobject Excel.Application
  {
    CLSID clsid;
    CLSIDFromProgID(L"Excel.Application", &clsid);
    HRESULT hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER | CLSCTX_INPROC_SERVER, IID_IDispatch, reinterpret_cast<void **>(&rVal.pdispVal));
    if (FAILED(hr)) {
      char buf[256];
      snprintf(buf, sizeof(buf), "CoCreateInstance() for \"Excel.Application\" failed. Err=%08lx", hr);
      // ::MessageBoxA(NULL, buf, "Error", 0x10010);
      return false;
    }
    rVal.vt = VT_DISPATCH;
  }
  VariantCopy(&app, &rVal);
  VariantClear(&rVal);

  // Line 3: dim workbooks as object
  VARIANT workbooks = {
    0
  };

  // Line 4: set workbooks = app . Workbooks
  VariantCopy(&root[++level], &app);
  wchar_t s_workbooks[] = L"Workbooks";
  hr = AutoWrap(DISPATCH_PROPERTYGET | DISPATCH_METHOD, &rVal, root[level].pdispVal, s_workbooks, 0);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);
  VariantCopy(&workbooks, &rVal);
  VariantClear(&rVal);

  // Line 5: dim workbook as object
  VARIANT workbook = {
    0
  };

  // Line 6: set workbook = workbooks . Open C:\\Documents and Settings\\Paolo\\Desktop\\M&EB.xls
  VariantCopy(&root[++level], &workbooks);
  parm[0].vt = VT_BSTR;
  parm[0].bstrVal = ::SysAllocString(fileAndPath);
  wchar_t s_open[] = L"Open";
    hr = AutoWrap(DISPATCH_PROPERTYGET | DISPATCH_METHOD, &rVal, root[level].pdispVal, s_open, 1, parm[0]);
  if (FAILED(hr))
    return false;
  VariantClear(&parm[0]);
  VariantClear(&root[level--]);
  VariantCopy(&workbook, &rVal);
  VariantClear(&rVal);

  // Line 7: dim sheets as object
  VARIANT sheets = {
    0
  };

  // Line 8: set sheets = workbook . Worksheets
  VariantCopy(&root[++level], &workbook);
  wchar_t s_worksheets[] = L"Worksheets";
  hr = AutoWrap(DISPATCH_PROPERTYGET | DISPATCH_METHOD, &rVal, root[level].pdispVal, s_worksheets, 0);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);
  VariantCopy(&sheets, &rVal);
  VariantClear(&rVal);

  // Line 9: dim worksheet as object
  VARIANT worksheet = {
    0
  };

  // Line 10: set worksheet = sheets . Item rawdata
  VariantCopy(&root[++level], &sheets);
  parm[0].vt = VT_BSTR;
  parm[0].bstrVal = ::SysAllocString(L"rawdata");
  wchar_t s_item[] = L"Item";
  hr = AutoWrap(DISPATCH_PROPERTYGET | DISPATCH_METHOD, &rVal, root[level].pdispVal, s_item, 1, parm[0]);
  if (FAILED(hr))
    return false;
  VariantClear(&parm[0]);
  VariantClear(&root[level--]);
  VariantCopy(&worksheet, &rVal);
  VariantClear(&rVal);

  // Line 11: worksheet . Activate
  VariantCopy(&root[++level], &worksheet);
  wchar_t s_activate[] = L"Activate";
  hr = AutoWrap(DISPATCH_METHOD, NULL, root[level].pdispVal, s_activate, 0);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);

  // Line 5: dim asheet as object
  VARIANT asheet = {
    0
  };

  // Line 6: set asheet = app . activesheet
  VariantCopy(&root[++level], &app);
  wchar_t s_activesheet[] = L"activesheet";
  hr = AutoWrap(DISPATCH_PROPERTYGET | DISPATCH_METHOD, &rVal, root[level].pdispVal, s_activesheet, 0);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);
  VariantCopy(&asheet, &rVal);
  VariantClear(&rVal);

  // Line 7: dim rng
  VARIANT rng = {
    0
  };

  // Line 8: set rng = asheet . Range A1
  VariantCopy(&root[++level], &asheet);
  parm[0].vt = VT_BSTR;
  parm[0].bstrVal = ::SysAllocString(L"A1");
  wchar_t s_range[] = L"Range";
  hr = AutoWrap(DISPATCH_PROPERTYGET | DISPATCH_METHOD, &rVal, root[level].pdispVal, s_range, 1, parm[0]);
  if (FAILED(hr))
    return false;
  VariantClear(&parm[0]);
  VariantClear(&root[level--]);
  VariantCopy(&rng, &rVal);
  VariantClear(&rVal);

  // Line 9: rng . Select
  VariantCopy(&root[++level], &rng);
  wchar_t s_select[] = L"Select";
  AutoWrap(DISPATCH_METHOD, NULL, root[level].pdispVal, s_select, 0);
  VariantClear(&root[level--]);

  // Line 10: dim acell
  VARIANT acell = {
    0
  };

  // Line 11: set acell = app . ActiveCell
  VariantCopy(&root[++level], &app);
  wchar_t s_activecell[] = L"ActiveCell";
  hr = AutoWrap(DISPATCH_PROPERTYGET | DISPATCH_METHOD, &rVal, root[level].pdispVal, s_activecell, 0);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);
  VariantCopy(&acell, &rVal);
  VariantClear(&rVal);

  // Line 12: acell . Value = 111
  rVal.vt = VT_BSTR;
  wchar_t buf1[256];
  swprintf(buf1, L"%d", node);
  rVal.bstrVal = ::SysAllocString(buf1);
  VariantCopy(&root[++level], &acell);
  wchar_t s_value[] = L"Value";
  hr = AutoWrap(DISPATCH_PROPERTYPUT, NULL, root[level].pdispVal, s_value, 1, rVal);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);
  VariantClear(&rVal);

  // Line 12: app . Run bbb
  VariantCopy(&root[++level], &app);
  parm[0].vt = VT_BSTR;
  parm[0].bstrVal = ::SysAllocString(L"copy_rawdata");
  wchar_t s_run[] = L"Run";
  hr = AutoWrap(DISPATCH_METHOD, NULL, root[level].pdispVal, s_run, 1, parm[0]);
  if (FAILED(hr))
    return false;
  VariantClear(&parm[0]);
  VariantClear(&root[level--]);

  // Line 13: app . Visible = 1
  rVal.vt = VT_I4;
  rVal.lVal = 1;
  VariantCopy(&root[++level], &app);
  wchar_t s_visible[] = L"Visible";
  hr = AutoWrap(DISPATCH_PROPERTYPUT, NULL, root[level].pdispVal, s_visible, 1, rVal);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);
  VariantClear(&rVal);

  // Line 14: app . UserControl = 1
  rVal.vt = VT_I4;
  rVal.lVal = 1;
  VariantCopy(&root[++level], &app);
  wchar_t s_usercontrol[] = L"UserControl";
  hr = AutoWrap(DISPATCH_PROPERTYPUT, NULL, root[level].pdispVal, s_usercontrol, 1, rVal);
  if (FAILED(hr))
    return false;
  VariantClear(&root[level--]);
  VariantClear(&rVal);

  /*
     // Line 15: app . quit
     VariantCopy(&root[++level], &app);
     hr = AutoWrap(DISPATCH_METHOD, NULL, root[level].pdispVal, L"quit", 0);
     if(FAILED(hr))
      return false;
     VariantClear(&root[level--]);

     // Line 16: set app = nothing
     VariantClear(&app);
   */

  // Clearing variables
  VariantClear(&app);
  VariantClear(&workbooks);
  VariantClear(&workbook);
  VariantClear(&sheets);
  VariantClear(&worksheet);

  // Close the OLE Library...
  OleUninitialize();
  return true;
} // openXlsMain

#endif // Q_OS_WIN
