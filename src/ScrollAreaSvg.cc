/** @file ScrollAreaSvg.cc
    @brief Implementation for the ScrollAreaSvg class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Dmytro Skrypnyk.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QScrollBar>
#include <QMouseEvent>
#include <QDebug>

#include "ViewSvgNative.h"
#include "ScrollAreaSvg.h"

ScrollAreaSvg::ScrollAreaSvg() : QScrollArea(), highQualityAntialiasing_(false) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  view_ = new QWidget(this);
  setWidget(view_);
} // ScrollAreaSvg::ScrollAreaSvg

void ScrollAreaSvg::clear(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  data_.clear();
  view_ = new QWidget(this);
  setWidget(view_);
} // ScrollAreaSvg::clear

void ScrollAreaSvg::openFile(const QString &file) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  currentPath_ = file;
  view_ = new ViewSvgNative(currentPath_, this);
  setMouseTracking(true);
  view_->setMouseTracking(true);
  setWidget(view_);
  data_.openXml(currentPath_);
  data_.clear();
  data_.read();
  calcRects((dynamic_cast<ViewSvgNative*>(view_))->doc_);
  view_->show();
} // ScrollAreaSvg::openFile

void ScrollAreaSvg::setHighQualityAntialiasing(bool hq) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  highQualityAntialiasing_ = hq;
} // ScrollAreaSvg::setHighQualityAntialiasing

void ScrollAreaSvg::mousePressEvent(QMouseEvent* event) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  foreach(QString id, rects_.keys()) {
    if (rects_[id].contains(widget()->mapFromParent(event->pos()))) {
      // qDebug() << "rect xlink:href=" << data_.href(id);
      emit select(data_.href(id));
    }
  }

  foreach(QString id, paths_.keys()) {
    QPointF p = widget()->mapFromParent(event->pos());
    QRectF r(p - QPointF(5, 5), p + QPointF(5, 5)); 
    if (paths_[id].intersects(r)) {
      // qDebug() << "path xlink:href=" << data_.href(id);
      emit select(data_.href(id));
    }
  }

  mousePressPos_ = QPoint();
  scrollBarValuesOnMousePress_.rx() = horizontalScrollBar()->value();
  scrollBarValuesOnMousePress_.ry() = verticalScrollBar()->value();
  event->accept();
} // ScrollAreaSvg::mousePressEvent

void ScrollAreaSvg::mouseMoveEvent(QMouseEvent* event) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  bool found = false;
  foreach(QString id, rects_.keys()) {
    if (rects_[id].contains(widget()->mapFromParent(event->pos()))) {
      found = true;
    }
  }
  foreach(QString id, paths_.keys()) {
    QPointF p = widget()->mapFromParent(event->pos());
    QRectF r(p - QPointF(5, 5), p + QPointF(5, 5)); 
    if (paths_[id].intersects(r)) {
      found = true;
    }
  }

  if (found)
    setCursor(Qt::PointingHandCursor);
  else
    unsetCursor();
  if (mousePressPos_.isNull()) {
    event->ignore();
    return;
  }
  horizontalScrollBar()->setValue(scrollBarValuesOnMousePress_.x() - event->pos().x() + mousePressPos_.x());
  verticalScrollBar()->setValue(scrollBarValuesOnMousePress_.y() - event->pos().y() + mousePressPos_.y());
  horizontalScrollBar()->update();
  verticalScrollBar()->update();
  event->accept();
} // ScrollAreaSvg::mouseMoveEvent

void ScrollAreaSvg::mouseReleaseEvent(QMouseEvent* event) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  mousePressPos_ = event->pos();
  // My Problem: I want to write a function which enables me to point with the mouse on any position on the picture and the absolute/real position ON THE PICTURE ist returned. QMouseEvent:os() and QMouseEvent::globalPos() only return the relative position of the pointer in the window.
  // How can I get the absolute position?
  // QPoint picturePos = widget()->mapFromParent(event->pos());
  event->accept();
} // ScrollAreaSvg::mouseReleaseEvent

void ScrollAreaSvg::wheelEvent(QWheelEvent*) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  calcRects((dynamic_cast<ViewSvgNative*>(widget()))->doc_);
} // ScrollAreaSvg::wheelEvent

void ScrollAreaSvg::calcRects(QSvgRenderer* doc) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  rects_.clear();
  paths_.clear();

  qreal dx = (qreal)widget()->width() / (qreal)doc->defaultSize().width();
  qreal dy = (qreal)widget()->height() / (qreal)doc->defaultSize().height();
#if (QT_VERSION >= 0x040200)
  foreach(QString id, data_.elementList()) {
    if (data_.classname(id) == "node") {
      // qDebug() << "Looking for node " << id;
      QRectF rect = doc->boundsOnElement(id);
      QMatrix matrix = doc->matrixForElement(id);
      matrix.scale(dx, dy);
      matrix.translate(20.0, 20.0);
      QRectF r = matrix.mapRect(rect);
      rects_[id] = r.toRect();
    } else {
      // qDebug() << "Looking for edge " << id;
      QPainterPath path = data_.path(id);
      QMatrix matrix = doc->matrixForElement(id);
      matrix.scale(dx, dy);
      matrix.translate(20.0, 20.0);
      QPainterPath mpath = matrix.map(path);
      paths_[id] = mpath;
    }
  }
#endif
} // ScrollAreaSvg::calcRects
