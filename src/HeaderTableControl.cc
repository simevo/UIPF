/** @file HeaderTableControl.cc
    @brief Implementation for the HeaderTableControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDebug>

#include "HeaderTableControl.h"

/// @param parent the parent widget
HeaderTableControl::HeaderTableControl(QWidget* parent) :
  HeaderTableMonitor(parent) {  qDebug() << "Entering "<< Q_FUNC_INFO; } // HeaderTableControl::HeaderTableControl
