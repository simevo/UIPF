/** @file WidgetTableMessages.cc
    @brief Implementation for the WidgetTableMessages class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera and Harish Surana.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QHeaderView>
#include <QHBoxLayout>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>

#include "persistency.h"
#include "WidgetTableMessages.h"
#include "DelegateMessage.h"

WidgetTableMessages::WidgetTableMessages(QWidget* parent) : QWidget(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  model_ = new QSqlTableModel(this);

  model_->setHeaderData(0, Qt::Horizontal, tr("Error/Warning message"));

  view_ = new QTableView;
  view_->setModel(model_);
  view_->setSelectionMode(QAbstractItemView::SingleSelection);
  view_->setSelectionBehavior(QAbstractItemView::SelectRows);
  view_->setEditTriggers(QAbstractItemView::NoEditTriggers);

  DelegateMessage* delegate = new DelegateMessage;
  view_->setItemDelegate(delegate);
  //view_->resizeColumnsToContents();

  QHeaderView* header = view_->horizontalHeader();
  header->setStretchLastSection(true);

  QHBoxLayout* mainLayout = new QHBoxLayout;
  mainLayout->addWidget(view_);
  setLayout(mainLayout);

  setWindowTitle(tr("Messages table"));
} // WidgetTableMessages::WidgetTableMessages

void WidgetTableMessages::go(int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);

  QString qs;
#if defined(Q_OS_WIN) && (!defined(SQLITE))
  qs = "select sv.tag + chr(32) + chr(124) + chr(32) + svv.[value] ";
#else
  qs = "SELECT sv.tag||\' | \'||svv.value ";
#endif
  qs += "from svv inner join sv on sv.id = svv.vid ";
  qs += "where sv.nid = %1";

  qs = qs.arg(node);
  qDebug() << "Query in WidgetTableMessages::go = " << qs;
  model_->setQuery(qs, db);
  if (model_->lastError().isValid())
    qDebug() << "Error while executing query in WidgetTableMessages::go: " << model_->lastError();
  model_->setHeaderData(0, Qt::Horizontal, tr("Error/Warning message"));
} // WidgetTableMessages::go

void WidgetTableMessages::clear(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");

  model_->setQuery("", db);
} // WidgetTableMessages::clear
