/** @file OrderingReflectedGray.cc
    @brief OrderingReflectedGray class implementation

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.
 */

#include <algorithm> // for min/max

#include "OrderingReflectedGray.h"

OrderingReflectedGray::OrderingReflectedGray(const std::vector<int> &m) : Ordering(m) {
  m_.resize(k_ + 1);
  g_.resize(k_ + 1);
  m_[k_] = m[std::max(k_ - 1, 0)];
  g_[k_] = 0;
  u_.resize(k_ + 1);
  for (int i = 0; i < k_; ++i) {
    h_[i] = 0;
    u_[i] = 1;
  }
  u_[k_] = 1;
} // OrderingReflectedGray::OrderingReflectedGray

// Donald E. Knuth
// The Art of Computer Programming
// Volume 4, Fascicle 2
// Generating All Tuples and Permutations, 2005
// ISBN 0-201-85393-0
int OrderingReflectedGray::idFromRound(int round) const {
  toInternalRepresentation_(round, g_);

  int gj = 0;
  for (int j = k_ - 1; j >= 0; --j) {
    if (gj % 2 == 0) // formula (50)
      h_[j] = g_[j];
    else
      h_[j] = m_[j] - 1 - g_[j];
    gj = gj * m_[j] + g_[j]; // formula (49)
  }
  return fromInternalRepresentation_(h_);
} // OrderingReflectedGray::idFromRound

int OrderingReflectedGray::roundFromId(int id) const {
  toInternalRepresentation_(id, g_);

  int gj = 0;
  for (int j = k_ - 1; j >= 0; --j) {
    if (gj % 2 == 0) // formula (51)
      h_[j] = g_[j];
    else
      h_[j] = m_[j] - 1 - g_[j];
    gj += g_[j];  // formula (49)
  }
  return fromInternalRepresentation_(h_);
} // OrderingReflectedGray::roundFromId
