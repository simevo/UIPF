/** @file DelegateMonitor.cc
    @brief Implementation for the DelegateMonitor class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian and Yasantha Samarasekera.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QLineEdit>
#include <QComboBox>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QDebug>

#include "persistency.h"
#include "DelegateMonitor.h"

/// @param caseId the current case ID
/// @param parent the parent object
DelegateMonitor::DelegateMonitor(int caseId, Units *u, QObject* parent) :
  QStyledItemDelegate(parent), u_(u) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QSqlDatabase db = QSqlDatabase::database("persistency");
  QSqlQuery query(db);
  QString  qs("");
  bool queryCheck(false);

  // get the range
  int range(0);
  qs = QString("SELECT RANGE FROM N WHERE ID=%1;").arg(caseId);
  query.prepare(qs);
  queryCheck = query.exec();
  if (query.lastError().isValid())
      qCritical() <<  "Error while executing query in " << Q_FUNC_INFO << ": " << query.lastError().type() << query.lastError().text();
  if (!queryCheck) qCritical() << "Unknown error in "<< Q_FUNC_INFO << "executing : " << query.lastQuery();
  if (query.next()) {
    range = query.value(0).toInt();
  } else {
    qCritical()<<"Database Error in "<< Q_FUNC_INFO << "executing : " << query.lastQuery();
  }
//  qDebug() << query.lastQuery();
//  qDebug() << "caseId = "<<caseId <<", range = "<<range;

  // clear
  qs = ("");
  query.clear();
  queryCheck = false;
  // get the quantities

#if defined SQLITE
  qs = QString("SELECT distinct Q.ID, N.fulltag||\'.\'||Q.tag, Q.unit, Q.DESCRIPTION, Q.tag FROM" \
                " (Q INNER JOIN N ON (Q.nid = N.id)) WHERE Q.nid >= ? AND Q.nid < ? AND Q.output = \'1\' ORDER BY Q.id;");
#elif defined Q_OS_WIN
  qs = QString("SELECT DISTINCT q.id, n.fulltag+\'.\'+Q.tag, q.unit, q.description, q.tag FROM" \
                " (Q INNER JOIN N ON (Q.nid = N.id)) WHERE Q.nid >= ? AND Q.nid < ? AND Q.output = true ORDER BY Q.id;");
#else // postgres
  qs = QString("SELECT DISTINCT Q.id, N.fulltag||\'.\'||Q.tag, Q.unit, Q.description, Q.tag FROM"
               " (Q INNER JOIN N ON (Q.nid = N.id)) WHERE Q.nid >= ? AND Q.nid < ? AND Q.output = true ORDER BY Q.id;");
#endif
  query.prepare(qs);
  query.addBindValue(caseId);
  query.addBindValue(caseId + range);
  qDebug() << query.lastQuery();
  qDebug() << "caseId = "<<caseId <<", range = "<<range;
  queryCheck = query.exec();

  if (query.lastError().isValid())
      qCritical() << "Error while executing query in "<< Q_FUNC_INFO<<": " << query.lastError().text();
  if (!queryCheck) qCritical() << "Unknown error in "<< Q_FUNC_INFO << "executing : " << query.lastQuery();

  comboModel_ = new QSqlQueryModel(this);
  comboModel_->setQuery(query);
  while (comboModel_->canFetchMore())
    comboModel_->fetchMore();  
} // DelegateMonitor::DelegateMonitor

QWidget* DelegateMonitor::createEditor(QWidget* parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.column() == 1) {
    QComboBox* comboBox = new QComboBox(parent);
    comboBox->setModel(comboModel_);
    comboBox->setModelColumn(1);
    comboBox->setCurrentIndex(comboBox->findText(index.data().toString()));
    comboBox->setEditable(false);
    connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(commitAndCloseCombo()));
    //connect(comboBox, SIGNAL(editTextChanged(QString)), this, SLOT(commitAndCloseCombo()));
    return comboBox;
  }
  if (index.column() == 4) {
    QLineEdit* lineEdit = new QLineEdit(parent);
    connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(commitAndCloseLineEdit()));
    return lineEdit;
  }
  return QStyledItemDelegate::createEditor(parent, option, index);
} // DelegateMonitor::createEditor

void DelegateMonitor::setEditorData(QWidget* editor, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.column() == 1) {
    QComboBox* comboBox = qobject_cast<QComboBox*>(editor);
    comboBox->setEditText(index.model()->data(index, Qt::DisplayRole).toString());
  } else if (index.column() == 4) {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    lineEdit->setText(index.model()->data(index, Qt::DisplayRole).toString().trimmed());
  } else {
    QStyledItemDelegate::setEditorData(editor, index);
  }
} // DelegateMonitor::setEditorData

void DelegateMonitor::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (index.column() == 1) {
    QComboBox* comboBox = qobject_cast<QComboBox*>(editor);
    QString currentModelText = index.data(Qt::EditRole).toString();
    QString currentComboBoxText = comboBox->currentText();
    // qDebug() << "currentModelText = " << currentModelText << " currentComboBoxText = " << currentComboBoxText;
    if (currentModelText != currentComboBoxText) {
      model->setData(index, comboBox->model()->data(comboBox->model()->index(comboBox->currentIndex(), 1)));

      QModelIndex targetIndex = index.sibling(index.row(), 0);
      model->setData(targetIndex, index.row() + 1);

      QModelIndex sourceIndex = comboModel_->index(comboBox->currentIndex(), 2);
      QString uom = comboModel_->data(sourceIndex).toString();
      targetIndex = index.sibling(index.row(), 2);
      model->setData(targetIndex, uom);

      // -> Luiz
      std::string newUnit;
      const Dimension *dim = unitEngine_.pdimension(std::string(uom.trimmed().toUtf8()));
      if (dim != NULL) {
        newUnit = dim->unit(*u_);
        QString newUnitQs = QString::fromUtf8(newUnit.c_str());
        model->setData(targetIndex, newUnitQs, Qt::DisplayRole);
      } else {
        qCritical() << uom.trimmed().toUtf8() << " has no associated dimension !";
      }
      // <- Luiz

      sourceIndex = comboModel_->index(comboBox->currentIndex(), 3);
      QString description = comboModel_->data(sourceIndex).toString();
      targetIndex = index.sibling(index.row(), 3);
      model->setData(targetIndex, description);

      sourceIndex = comboModel_->index(comboBox->currentIndex(), 4);
      QString tag = comboModel_->data(sourceIndex).toString();
      targetIndex = index.sibling(index.row(), 4);
      model->setData(targetIndex, tag);

      sourceIndex = comboModel_->index(comboBox->currentIndex(), 0);
      int dbid = comboModel_->data(sourceIndex).toInt();
      targetIndex = index.sibling(index.row(), 5);
      model->setData(targetIndex, dbid);
    } // text has changed
  } else if (index.column() == 4) {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    model->setData(index, lineEdit->text());
  } else {
    QStyledItemDelegate::setModelData(editor, model, index);
  }
} // DelegateMonitor::setModelData

/// This slot is called every time the data from the cells in the second column is updated
void DelegateMonitor::commitAndCloseCombo(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QComboBox* comboBox = qobject_cast<QComboBox*>(sender());
  emit commitData(comboBox);
  emit closeEditor(comboBox);
} // DelegateMonitor::commitAndCloseCombo

/// This slot is called every time the data from the cells in the 5th column is updated
void DelegateMonitor::commitAndCloseLineEdit(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QLineEdit* lineEdit = qobject_cast<QLineEdit*>(sender());
  emit commitData(lineEdit);
  emit closeEditor(lineEdit);
} // DelegateMonitor::commitAndCloseLineEdit

int DelegateMonitor::hasFullTag(const QString &fullTag) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  for (int i = 0; i < comboModel_->rowCount(); ++i) {
    QModelIndex index = comboModel_->index(i, 1);
    // strip root object tag
    QString indexFullTag = index.data().toString();
    int firstSeparator = indexFullTag.indexOf(QRegExp("[\\.:]"));
    if (firstSeparator >= 0)
      indexFullTag = indexFullTag.right(indexFullTag.size() - firstSeparator - 1);
    if (indexFullTag == fullTag)
      return index.row();
  }
  return -1;
} // DelegateMonitor::hasFullTag
