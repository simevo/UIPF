/** @file DialogSensitivitySave.cc
    @brief Implementation for the DialogSaveSensitivityAnalysis class

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#include "DialogSensitivitySave.h"
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QMessageBox>
#include <QFile>
#include <QChar>
#include <QDebug>

#include "ActionLibpfSensitivity.h"
#include "Settings.h"
#include "ValidatorRegExp.h"

DialogSensitivitySave::DialogSensitivitySave(ActionLibpfSensitivity* goAction, const QString & suggestedName, QWidget *parent) :
  QDialog(parent),
  goAction_(goAction),
  gridLayout_(new QGridLayout(this)),
  descriptionLabel_(new QLabel(this)),
  analysisNameEdit_(new QLineEdit(this)),
  okButton_(new QPushButton(this)),
  cancelButton_(new QPushButton(this)),
  settings_(new Settings()) {

  qDebug() << "Entering "<< Q_FUNC_INFO;
  descriptionLabel_->setText(tr("Enter sensitivity name:"));
  descriptionLabel_->setFixedWidth(150);
  descriptionLabel_->setWordWrap(true);
  descriptionLabel_->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
  descriptionLabel_->setBuddy(analysisNameEdit_);

  okButton_->setText(tr("OK"));
  okButton_->setMaximumWidth(45);

  cancelButton_->setText(tr("Cancel"));
  cancelButton_->setMaximumWidth(60);

  // Validator for analysis name
  QString notAllowedChars = QRegExp::escape("<>:\"/\\|?*_.$");
  for (int i = 1; i <= 31; ++i)
    notAllowedChars.append(QRegExp::escape(QChar(i)));
  analysisNameEdit_->setValidator(new ValidatorRegExp(descriptionLabel_, QRegExp("[^" + notAllowedChars + "]{0,49}"), this));
  analysisNameEdit_->setText(suggestedName);

  if (!analysisNameEdit_->text().isEmpty())
    okButton_->setEnabled(true);
  else
    okButton_->setEnabled(false);
  gridLayout_->addWidget(descriptionLabel_, 0, 0, 1, 1);
  gridLayout_->addWidget(analysisNameEdit_, 0, 1, 1, 1);

  QHBoxLayout* horizontalLayout = new QHBoxLayout();
  horizontalLayout->addWidget(okButton_);
  horizontalLayout->addWidget(cancelButton_);
  horizontalLayout->addSpacing(150 - okButton_->width() - cancelButton_->width());

  gridLayout_->addLayout(horizontalLayout, 1, 0, 1, 1);

  connect(okButton_, SIGNAL(clicked()), SLOT(save()));
  connect(cancelButton_, SIGNAL(clicked()), SLOT(reject()));
  connect(analysisNameEdit_, SIGNAL(textChanged(QString)), SLOT(onAnalysisNameEditChange(QString)));

  setFixedSize(500, 100);
  setLayout(gridLayout_);
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
  setWindowTitle(tr("Save sensitivity analysis"));
} // DialogSensitivitySave::DialogSensitivitySave

void DialogSensitivitySave::save() {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString type(goAction_->type().data());
  type.replace("<", "[");
  type.replace(">", "]");
  type.replace("_", "-");

  QString analysisName = analysisNameEdit_->text();
  QString fileName = settings_->homePath() + "/SensitivityInput_" + type + "_" + analysisName + ".xml";
  if (QFile::exists(fileName)) {
    int answer = QMessageBox::question(this, tr("Overwrite?"), QString(tr("An analysis named '%1' already exists. Do you want to overwrite it?")).arg(analysisName), QMessageBox::Yes, QMessageBox::No);
    if (answer == QMessageBox::No)
      return;
  }
  // Save the XML
  goAction_->saveToXml(fileName);
  analysisName_ = analysisName;
  accept();
} // DialogSensitivitySave::save

const QString & DialogSensitivitySave::analysisName() const {
  return analysisName_;
} // DialogSensitivitySave::analysisName

void DialogSensitivitySave::onAnalysisNameEditChange(const QString &text) {
  if (text.size()) {
    okButton_->setEnabled(true);
  } else {
    okButton_->setEnabled(false);
  }
} // DialogSensitivitySave::onAnalysisNameEditChange
