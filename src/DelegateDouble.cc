/** @file DelegateDouble.cc
    @brief Implementation for the DelegateDouble class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QLineEdit>
#include <QDebug>

#include "DelegateDouble.h"
#include "UnitEngine.h"

DelegateDouble::DelegateDouble(QObject* parent) : QStyledItemDelegate(parent) { } // DelegateDouble::DelegateDouble

QWidget* DelegateDouble::createEditor(QWidget* parent, const QStyleOptionViewItem & /* option */, const QModelIndex & /* index */) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QLineEdit* editor = new QLineEdit(parent);
  editor->setValidator(new QDoubleValidator(editor));
  return editor;
} // DelegateDouble::createEditor

void DelegateDouble::setEditorData(QWidget* editor, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString value = index.model()->data(index, Qt::DisplayRole).toString();
  QLineEdit* lineEdit = static_cast<QLineEdit*>(editor);
  lineEdit->setText(value);
} // DelegateDouble::setEditorData

void DelegateDouble::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QLineEdit* lineEdit = static_cast<QLineEdit*>(editor);
  double value = lineEdit->text().toDouble();
  QModelIndex sibIndex = index.sibling(index.row(), index.column() + 1);

  QString siValue = setNewValue(value, model->data(sibIndex, Qt::DisplayRole).toString().trimmed());

  model->setData(index, siValue, Qt::EditRole);
  model->setData(index, value, Qt::DisplayRole);
} // DelegateDouble::setModelData

QString DelegateDouble::setNewValue(double newUnit, QString newValue) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  UnitArray unit_;
  double newDisplay = unitEngine_.toSI(newUnit,std::string(newValue.toUtf8()), unit_);
  return QString::number(newDisplay);
} // DelegateDouble::setNewValue

void DelegateDouble::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem &option, const QModelIndex & /* index */) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  editor->setGeometry(option.rect);
} // DelegateDouble::updateEditorGeometry
