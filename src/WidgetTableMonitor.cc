/** @file WidgetTableMonitor.cc
    @brief Implementation for the WidgetTableMonitor class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Eugen Stoian, Yasantha Samarasekera, Shane Shields
    and Luiz A. Buhnemann (la3280@gmail.com).

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QDebug>

#include "persistency.h"
#include "WidgetTableMonitor.h"
#include "HeaderTableMonitor.h"
#include "ModelDataMonitor.h"
#include "DelegateMonitor.h"
#include "DelegateUnit.h"

/// @param caseId current case ID
/// @param parent parent widget
WidgetTableMonitor::WidgetTableMonitor(int caseId, QWidget* parent) :
  QTableView(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  HeaderTableMonitor* rowHeader = new HeaderTableMonitor(this);
  setVerticalHeader(rowHeader);
  connect(rowHeader, SIGNAL(sectionClicked(int)), this, SLOT(removeRow(int)));
  setModel(new ModelDataMonitor(this));
  connect(model(), SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SIGNAL(edited()));
  connect(model(), SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(resizeToContents()));
  setColumnHidden(5, true);
  DelegateMonitor *dlgtMonitor = new DelegateMonitor(caseId, &u_, this);
//  connect(dlgtMonitor, SIGNAL(closeEditor(QWidget *)), this, SLOT(updateUom()));

  setItemDelegate(dlgtMonitor);
  setItemDelegateForColumn(UOM_COL,new DelegateUnit(QList<int>()));
  setEditTriggers(QAbstractItemView::AllEditTriggers);
  appendRow();
  resizeToContents();
} // WidgetTableMonitor::WidgetTableMonitor

/// Reimplemented from QTableView::resizeEvent()
void WidgetTableMonitor::resizeEvent(QResizeEvent* event) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QTableView::resizeEvent(event);
  resizeToContents();
} // WidgetTableMonitor::resizeEvent

bool WidgetTableMonitor::hasValidData(void) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (model()->rowCount() && model()->data(model()->index(model()->rowCount() - 1, 1)).toString().isEmpty())
    return false;
  return true;
} // WidgetTableMonitor::hasValidData

bool WidgetTableMonitor::hasData(void) const {
  return model()->rowCount();
} // WidgetTableMonitor::hasData

/// @param r the row to be removed
/// If the removal is successfull the WidgetTableMonitor::edited() signal is emited
void WidgetTableMonitor::removeRow(int r) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (model()->removeRow(r)) {
    if (model()->rowCount() == 0)
      appendRow();
    emit edited();
  }
} // WidgetTableMonitor::removeRow

/// The row is appended if and only if the model contains valid data. In this case the WidgetTableMonitor::edited() signal is emited
void WidgetTableMonitor::appendRow(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (hasValidData()) {
    int row = model()->rowCount();
    model()->insertRow(row);
    scrollToBottom();
    // int dy = rowViewportPosition(row);
    // verticalScrollBar()->setValue(dy);
    emit edited();
  }
} // WidgetTableMonitor::appendRow

void WidgetTableMonitor::resizeToContents(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  resizeColumnsToContents();
  int widgetWidth = width() - 40;
  int varWidth = qMax(columnWidth(1), 250);
  setColumnWidth(1, varWidth);
  setColumnWidth(UOM_COL, 100);
  int width = 0;
  for (int i = 0; i < 5; ++i)
    width += columnWidth(i);
  int delta = widgetWidth - width;
  if (delta > 0)
    setColumnWidth(1, varWidth + delta);
} // WidgetTableMonitor::resizeToContents

void WidgetTableMonitor::updateUom() {
  setUom(u_);
} // WidgetTableMonitor::updateUom

void WidgetTableMonitor::setUom(Units u) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  u_ = u;

  for (int indx = 0; indx < verticalHeader()->count(); ++indx) {
    QVariant uValue = model()->data(model()->index(indx, UOM_COL), Qt::EditRole);
    const Dimension *dim = unitEngine_.pdimension(std::string(uValue.toString().trimmed().toUtf8()));
    if (dim != NULL) {
      std::string newUnit = dim->unit(u);
      QString newUnitQs = QString::fromUtf8(newUnit.c_str());
      // qDebug() << "WidgetTableMonitor::setUom" << newUnitQs << verticalHeader()->count();
      model()->setData(model()->index(indx, UOM_COL), newUnitQs, Qt::DisplayRole);
    } else {
      qCritical() << uValue.toString().trimmed().toUtf8() << " has no associated dimension !";
    }
  }
} // WidgetTableControl::setUom
