/** @file WidgetTableControl.cc
    @brief Implementation for the WidgetTableControl class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Yasantha Samarasekera, Shane Shields and Luiz A. Buhnemann (la3280@gmail.com).

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#if (QT_VERSION >= 0x040600)
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#endif

#include <QDebug>

#include "persistency.h"
#include "WidgetTableControl.h"
#include "HeaderTableControl.h"
#include "ModelDataControl.h"
#include "DelegateControl.h"
#include "DelegateUnit.h"

/// @param caseId current case ID
/// @param parent parent widget
WidgetTableControl::WidgetTableControl(int caseId, QWidget* parent) :
  QTableView(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  u_ = UnitsSI;
  HeaderTableControl* rowHeader = new HeaderTableControl(this);
  setVerticalHeader(rowHeader);
  connect(rowHeader, SIGNAL(sectionClicked(int)), this, SLOT(removeRow(int)));
  setModel(new ModelDataControl(this));
  connect(model(), SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SIGNAL(edited()));
  connect(model(), SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(resizeToContents()));
  setColumnHidden(8, true);
  DelegateControl *dlgtControl = new DelegateControl(caseId, &u_, this);
//  connect(dlgtControl, SIGNAL(currentIndexChanged()), this, SLOT(updateUom()));
  setItemDelegate(dlgtControl);
  ufDelegate = new DelegateUnit(QList<int>() << START_COL << END_COL);
  connect(ufDelegate, SIGNAL(currentIndexChanged(int,QObject *)), this, SLOT(indexChanged(int,QObject *)));
  setItemDelegateForColumn(UOM_COL, ufDelegate);
  setEditTriggers(QAbstractItemView::AllEditTriggers);
  appendRow();
  resizeToContents();
} // WidgetTableControl::WidgetTableControl

/// Reimplemented from QTableView::resizeEvent()
void WidgetTableControl::resizeEvent(QResizeEvent* event) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QTableView::resizeEvent(event);
  resizeToContents();
} // WidgetTableControl::resizeEvent

bool WidgetTableControl::hasValidData(void) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int rows = model()->rowCount();
  if (rows && model()->data(model()->index(model()->rowCount() - 1, FULLTAG_COL)).toString().isEmpty())
    return false;
  const ModelDataControl* cdModel = qobject_cast<ModelDataControl*>(model());

  // check valid number of points, start, end
  for (int r = 0; r < rows; ++r) {
    if (!cdModel->rowValid(r))
      return false;
  }
  return true;
} // WidgetTableControl::hasValidData

bool WidgetTableControl::hasData(void) const {
  return model()->rowCount();
} // WidgetTableControl::hasData

/// @param r the row to be removed
/// If the removal is successfull the MonitorTableWidget::edited() signal is emited
void WidgetTableControl::removeRow(int r) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (model()->removeRow(r)) {
    if (model()->rowCount() == 0)
      appendRow();
    emit edited();
  }
} // WidgetTableControl::removeRow

/// The row is appended if and only if the model contains valid data. In this case the WidgetTableControl::edited() signal is emited
void WidgetTableControl::appendRow(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (hasValidData()) {
    int row = model()->rowCount();
    model()->insertRow(row);
    scrollToBottom();
    // int dy = rowViewportPosition(row);
    // verticalScrollBar()->setValue(dy);
    emit edited();
  }
} // WidgetTableControl::appendRow

void WidgetTableControl::resizeToContents(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  resizeColumnsToContents();
  int widgetWidth = width() - 40;
  int varWidth = qMax(columnWidth(1), 250);
  setColumnWidth(1, varWidth);
  setColumnWidth(UOM_COL, 100);
  int width = 0;
  for (int i = 0; i < 8; ++i)
    width += columnWidth(i);
  int delta = widgetWidth - width;
  if (delta > 0)
    setColumnWidth(1, varWidth + delta);
} // WidgetTableControl::resizeToContents

void WidgetTableControl::updateUom(void) {
  setUom(u_);
}

void WidgetTableControl::setUom(Units u) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  u_ = u;

  for (int indx = 0; indx < verticalHeader()->count(); ++indx) {
    QVariant sValue = model()->data(model()->index(indx, START_COL), Qt::EditRole);
    QVariant eValue = model()->data(model()->index(indx, END_COL), Qt::EditRole);
    QVariant uValue = model()->data(model()->index(indx, UOM_COL), Qt::EditRole);

    const Dimension *dim = unitEngine_.pdimension(std::string(uValue.toString().trimmed().toUtf8()));
    if (dim != NULL) {
      std::string newUnit = dim->unit(u);
      QString newUnitQs = QString::fromUtf8(newUnit.c_str());
      // qDebug() << "WidgetTableControl::setUom" << newUnitQs << verticalHeader()->count();
      QVariant newDisplay = unitEngine_.fromSI(sValue.toDouble(), newUnit);
      model()->setData(model()->index(indx, START_COL), newDisplay, Qt::DisplayRole);
      newDisplay = unitEngine_.fromSI(eValue.toDouble(), newUnit);
      model()->setData(model()->index(indx, END_COL), newDisplay, Qt::DisplayRole);
      model()->setData(model()->index(indx, UOM_COL), newUnitQs, Qt::DisplayRole);
    } else {
      qCritical() << uValue.toString().trimmed().toUtf8() << " has no associated dimension !";
    }
  }
} // WidgetTableControl::setUom

void WidgetTableControl::indexChanged(int indx,QObject *objPointer) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int rIndx = currentIndex().row();
  QComboBox *cbPointer = qobject_cast<QComboBox*>(objPointer);
  double siUnit = model()->data(model()->index(rIndx, START_COL), Qt::EditRole).toDouble();
  double siUnit1 = model()->data(model()->index(rIndx, END_COL), Qt::EditRole).toDouble();
  QString newValue = cbPointer->itemText(indx);
  double newDisplay = unitEngine_.fromSI(siUnit, std::string(newValue.trimmed().toUtf8()));

  model()->setData(model()->index(rIndx, START_COL),newDisplay, Qt::DisplayRole);
  newDisplay = unitEngine_.fromSI(siUnit1, std::string(newValue.trimmed().toUtf8()));
  model()->setData(model()->index(rIndx, END_COL),newDisplay, Qt::DisplayRole);
} // WidgetTableControl::indexChanged
