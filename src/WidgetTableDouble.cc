/** @file WidgetTableDouble.cc
    @brief Implementation for the WidgetTableDouble class

    This file is part of UIPF, the User Interface for Process Flowsheeting

    All rights reserved.
    @author (C) Copyright 2008-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Toto Sugito, Eugen Stoian, Yasantha Samarasekera, Harish Surana and Shane Shields.

    Developed for Qt 4.1-4.7 Open Source Edition by Nokia Corporation and/or its subsidiary(-ies).

    Based in part on code from the example classes of the Qt Toolkit.
    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies). All rights reserved.

    This file may be used under the terms of the GNU General Public
    License version 2.0 as published by the Free Software Foundation
    and appearing in the file GPL_LICENSE.txt included in the packaging of
    this file.
 */

#include <QHBoxLayout>
#include <QHeaderView>
#include <QDebug>

#include "persistency.h"
#include "WidgetTableDouble.h"
#include "DelegateDouble.h"
#include "DelegateUnit.h"
#include "ModelQuantityEditable.h"
#include "ViewTable.h"

WidgetTableDouble::WidgetTableDouble(const char* filter, QUndoStack* undoStack, QWidget* parent) : QWidget(parent) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // model_ = new QSqlTableModel(this);
  model_ = new ModelQuantityEditable(filter, this);
  connect(model_, SIGNAL(forceGo(int, int)), this, SLOT(emitForceGo(int, int)));
  model_->setUndoStack(undoStack);

  model_->setHeaders();
  listHeader_ = model_->getHeaders();
  view = new ViewTable(Qtbl_Id);
  view->setModel(model_);
  view->setMouseTracking(true);
  view->setEditTriggers(QAbstractItemView::AllEditTriggers);
  view->setSelectionMode(QAbstractItemView::ContiguousSelection);
  view->setSelectionBehavior(QAbstractItemView::SelectItems);
  view->setContextMenuPolicy(Qt::CustomContextMenu);
  //connect(view, SIGNAL(clicked(const QModelIndex &)), this, SLOT(enteredItem(const QModelIndex &)));

  //view->resizeColumnsToContents();

  QHeaderView* header = view->horizontalHeader();
  header->setStretchLastSection(true);

  QHBoxLayout* mainLayout = new QHBoxLayout;
  mainLayout->addWidget(view);
  setLayout(mainLayout);

  DelegateDouble* delegate = new DelegateDouble;
  view->setItemDelegate(delegate);

  int hIndx = listHeader_.indexOf("Units");

  //Set delegate for units column
  ufDelegate = new DelegateUnit(QList<int>() << Qtbl_Q);
  connect(ufDelegate, SIGNAL(currentIndexChanged(int,QObject *)), this, SLOT(indexChanged(int,QObject *)));
  if (hIndx >= 0) view->setItemDelegateForColumn(hIndx,ufDelegate);
  setWindowTitle(tr("Quantities table"));
} // WidgetTableDouble::WidgetTableDouble

void WidgetTableDouble::emitForceGo(int node, int row) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  // TODO use this simpler command (but needs more testing): model_->go(node);
  emit forceGo(node);

  QModelIndex newIndex = model_->index(row, Qtbl_Q);
  // jump to row
  view->scrollTo(newIndex);
  // blink value cell
  view->selectionModel()->select(newIndex, QItemSelectionModel::Select);
} // WidgetTableDouble::emitForceGo

QString WidgetTableDouble::getTbviewSelectedItem(void) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  QString copyTable;
  QModelIndex previous, last, index;
  QModelIndexList list;
  QString firstHeader;
  int ncol, nsize, dremove;

  QLocale syslocale = QLocale::system();   //get locate setting

  // get model from tableview
  QAbstractItemModel* abmodel = view->model();
  QItemSelectionModel* model = view->selectionModel();
  list = model->selectedIndexes();

  //sorting selected data in tableview
  qSort(list);

  previous = list.first();   //get first item
  last = list.last();        //get last item

  //GET HEADER TEXT
  ncol = listHeader_.count() - 1; //number of header (number of column) without ID
  for (int i = 0; i < list.size(); i++) { //loop over selected data
    index = list.at(i);     //set next index

    if ((index.row() != previous.row()) || (i == list.size() - 1) || (i == ncol - 1)) { //check data in same row
      if (firstHeader == listHeader_.at(index.column())) {
        copyTable.remove(copyTable.count() - 1, 1);        //remove /t character
        copyTable.append("\n");          //add new line
        break;
      } else {
        copyTable.append(listHeader_.at(index.column()));         //save text to string
        copyTable.append("\n");          //add new line
        break;
      }
    } else {
      copyTable.append(listHeader_.at(index.column()));       //save text to string
      if (i == 0)
        firstHeader = listHeader_.at(index.column());
      copyTable.append("\t");
    }
  }
  //GET DATA LIST VALUE
  list = model->selectedIndexes();

  //sorting selected data in tableview
  qSort(list);

  previous = list.first();   //get first item
  last = list.last();        //get last item

  nsize = list.size();

  if (nsize != 1) {
    dremove = 1;
    list.removeFirst();     //remove duplicate item at first data
  } else {
    dremove = 0;
  }
  ncol = 0;
  for (int i = 0; i < list.size(); i++) { //loop over selected data
    QVariant data = abmodel->data(previous);     //get selected data
    QString text = data.toString();     //get text

    index = list.at(i);     //set next index
    if (ncol < listHeader_.count() - 1) { //dont save hidden column
      if ((index.column() - dremove) == 1)
        text = syslocale.toString(text.toDouble());
      copyTable.append(text);       //save text to string
    }
    if (index.row() != previous.row()) {  //check data in same row
      copyTable.append("\n");
      ncol = 0;
    } else {
      if ((ncol < 3) && (nsize != 1))   //check hidden column and if selected one item
        copyTable.append("\t");
      ncol = ncol + 1;
    }
    previous = index;
  }
  if ((ncol < listHeader_.count() - 1) && (nsize != 1)) { //check hidden column and if selected item size=1
    copyTable.append(abmodel->data(list.last()).toString());     //append last data
  }
  return (copyTable);
} // WidgetTableDouble::getTbviewSelectedItem

//table window selected all data
void WidgetTableDouble::setTbviewSelectAll(void) {
  view->selectAll();
} // WidgetTableDouble::setTbviewSelectAll

void WidgetTableDouble::clear(void) {
  model_->clear();
} // WidgetTableDouble::clear

void WidgetTableDouble::go(int node) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  model_->go(node);
  view->setColumnHidden(Qtbl_Id, true);
} // WidgetTableDouble::go

void WidgetTableDouble::setRw(bool b) {
  model_->rwStatus(b);
  /*  if (b)
      view->setEditTriggers(QAbstractItemView::AllEditTriggers);
     else
      view->setEditTriggers(QAbstractItemView::NoEditTriggers);*/
} // WidgetTableDouble::setRw

void WidgetTableDouble::setChild(bool child) {
  model_->setChild(child);
} // WidgetTableDouble::toggleChild

void WidgetTableDouble::resetUom() {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  std::string newUnit;

  for (int indx = 0; indx < view->verticalHeader()->count(); ++indx) {
    QVariant qValue = view->model()->data(view->model()->index(indx,Qtbl_Q),Qt::EditRole);
    newUnit = std::string(view->model()->data(view->model()->index(indx,Qtbl_Uom),Qt::DisplayRole).toString().toUtf8());

    QVariant newDisplay = unitEngine_.fromSI(qValue.toDouble(),newUnit);
    view->model()->setData(view->model()->index(indx,Qtbl_Q),newDisplay,Qt::DisplayRole);
  }
} // WidgetTableDouble::resetUom

void WidgetTableDouble::setUom(Units u) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  std::string newUnit;
  QModelIndex curIndex_ = view->indexAt(QPoint(0,0));
  for (int indx = 0; indx < view->verticalHeader()->count(); ++indx) {
    QVariant qValue = view->model()->data(view->model()->index(indx, Qtbl_Q),Qt::EditRole);
    QVariant uValue = view->model()->data(view->model()->index(indx, Qtbl_Uom),Qt::EditRole);

    const Dimension *dim = unitEngine_.pdimension(std::string(uValue.toString().trimmed().toUtf8()));
    if (dim != NULL) {
      newUnit = dim->unit(u);
      // qDebug() << "WidgetTableDouble::setUom " << indx << qValue.toDouble() << uValue.toString().trimmed() << QString::fromUtf8(newUnit.c_str());
      QVariant newDisplay = unitEngine_.fromSI(qValue.toDouble(), newUnit);
      view->model()->setData(view->model()->index(indx, Qtbl_Q), newDisplay, Qt::DisplayRole);
      view->model()->setData(view->model()->index(indx, Qtbl_Uom), QString::fromUtf8(newUnit.c_str()), Qt::DisplayRole);
    } else {
      qCritical() << uValue.toString().trimmed().toUtf8() << " has no associated dimension !";
    }
  }
  view->scrollTo(curIndex_,QAbstractItemView::PositionAtTop);
} // WidgetTableDouble::setUom

void WidgetTableDouble::indexChanged(int indx, QObject *objPointer) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  int rIndx = view->currentIndex().row();
  QComboBox *cbPointer = qobject_cast<QComboBox*>(objPointer);

  double siUnit = view->model()->data(view->model()->index(rIndx, Qtbl_Q),Qt::EditRole).toDouble();
  std::string value(view->model()->data(view->model()->index(rIndx, Qtbl_Uom), Qt::DisplayRole).toString().toUtf8());
  double enUnit = unitEngine_.fromSI(siUnit, std::string(cbPointer->itemText(indx).toUtf8()));
  QString newDisplay = QString::number(enUnit);

  view->model()->setData(view->model()->index(rIndx, Qtbl_Q),newDisplay, Qt::DisplayRole);
} // WidgetTableDouble::indexChanged

void WidgetTableDouble::enteredItem(const QModelIndex & index) {
  if (index.column() == 2) {
    //qDebug() << index.row() << index.column();
  }
}
