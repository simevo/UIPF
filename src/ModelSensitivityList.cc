/** @file ModelSensitivityList.cc
    @brief Implementation for the ModelSensitivityList class.

    This file is part of LIBPF

    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2021 Paolo Greppi simevo s.r.l.

    Based in part on code from Luiz A. Buhnemann (la3280@gmail.com)
 */

#include "ModelSensitivityList.h"
#include "Settings.h"
#include "Hierarchy.h"
#include <QStandardItemModel>
#include <QStandardItem>
#include <QItemSelectionModel>
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
#include <QDebug>

ModelSensitivityList::ModelSensitivityList(QObject *parent) :
  QSortFilterProxyModel(parent),
  modelSensitivity_(new QStandardItemModel(this)),
  searchXmlPath_(new QDir()) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  setSourceModel(modelSensitivity_);
} // ModelSensitivityList::ModelSensitivityList

ModelSensitivityList::~ModelSensitivityList() {
  delete searchXmlPath_;
} // ModelSensitivityList::~ModelSensitivityList

bool ModelSensitivityList::setSensitivityDirectory(const QString &sensitivityDirectory) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  searchXmlPath_->setPath(sensitivityDirectory);
  if (!searchXmlPath_->isReadable())
    return false;
  loadSensitivityList();
  return true;
} // ModelSensitivityList::setSensitivityDirectory

void ModelSensitivityList::setHighlightedTypeName(const QString &typeName) {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  highlightTypeName_ = typeName;
} // ModelSensitivityList::setHighlightedTypeName

// Used to find out the types supported by the current kernel
// only files from those types are listed on the view
extern Hierarchy* h_;

bool ModelSensitivityList::isTypeSupported(const QString &typeName) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  std::set<std::string> fzz; // FlowSheet<ZeroZero> derived classes
  for (h_->start(); !h_->end(); ) {
    const Type *t(h_->next());
    if (t == NULL) {
      qCritical("NULL Type\n");
      exit(-2);
    }
    if (h_->isA(t->name(), "FlowSheet<ZeroZero>")) {
      std::pair< std::set<std::string>::iterator, bool > pr;
      pr = fzz.insert(t->name());
      if ((pr.second == true) && (t->name() != "FlowSheet<ZeroZero>") && (typeName == QString(t->name().c_str()))) {
        return true;
      }
    }
  }
  return false;
} // ModelSensitivityList::isTypeSupported

void ModelSensitivityList::loadSensitivityList() {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!searchXmlPath_->isReadable())
    return;
  modelSensitivity_->clear();
  QFileInfoList files = searchXmlPath_->entryInfoList(QStringList("SensitivityInput_*_*.xml"), QDir::Files | QDir::Readable, QDir::Time);
  QFileInfo fileInfo;
  QRegExp rx("^SensitivityInput_(.+)_(.+)\\.xml$", Qt::CaseInsensitive);
  rx.setMinimal(false);
  foreach(fileInfo, files) {
    if (rx.indexIn(fileInfo.fileName()) == -1)
      continue;
    QString type = rx.cap(1);
    type.replace("[", "<");
    type.replace("]", ">");
    type.replace("-", "_");
    if (!isTypeSupported(type))
      continue;
    QStandardItem* itemFileName = new QStandardItem();
    itemFileName->setData(fileInfo.absoluteFilePath(), Qt::UserRole + 1);
    itemFileName->setData(rx.cap(2), Qt::DisplayRole);
    modelSensitivity_->insertRow(modelSensitivity_->rowCount(), QList<QStandardItem *>() << new QStandardItem() << itemFileName << new QStandardItem(type));
  }
} // ModelSensitivityList::loadSensitivityList

QVariant ModelSensitivityList::data(const QModelIndex &index, int role) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!index.isValid())
    return QVariant();
  if (role == Qt::BackgroundColorRole) {
    if (this->index(index.row(), COL_TYPE).data().toString() == highlightTypeName_ && !highlightTypeName_.isEmpty())
      return QColor(173, 216, 230, 255); // light blue
    else
      return QColor(255, 255, 255, 255); // white
  }
  return QSortFilterProxyModel::data(index, role);
} // ModelSensitivityList::data

QVariant ModelSensitivityList::headerData(int section, Qt::Orientation orientation, int role) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (orientation == Qt::Horizontal) {
    switch (section) {
    case COL_RADIO:
      return QVariant();

    case COL_NAME:
      return QVariant(tr("Name"));

    case COL_TYPE:
      return QVariant(tr("Type"));
    }
  } else if (orientation == Qt::Vertical) {
    return QSortFilterProxyModel::headerData(section, orientation, role);
  }
  return QVariant();
} // ModelSensitivityList::headerData

Qt::ItemFlags ModelSensitivityList::flags(const QModelIndex &index) const {
  qDebug() << "Entering "<< Q_FUNC_INFO;
  if (!index.isValid())
    return QSortFilterProxyModel::flags(index);
  return QSortFilterProxyModel::flags(index) & ~Qt::ItemIsEditable;
} // ModelSensitivityList::flags
