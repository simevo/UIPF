<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DialogDuplicate</name>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="29"/>
        <source>Tag:</source>
        <translation type="unfinished">Тег:</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="32"/>
        <source>Description:</source>
        <translation type="unfinished">Описание:</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="41"/>
        <source>Ok</source>
        <translation type="unfinished">Да</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="42"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="57"/>
        <source>New case</source>
        <translation type="unfinished">Новый случай</translation>
    </message>
</context>
<context>
    <name>DialogNew</name>
    <message>
        <location filename="../src/DialogNew.cc" line="56"/>
        <source>Tag:</source>
        <translation type="unfinished">Тег:</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="59"/>
        <source>Description:</source>
        <translation type="unfinished">Описание:</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="65"/>
        <source>types</source>
        <translation type="unfinished">типы</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="94"/>
        <source>Ok</source>
        <translation type="unfinished">Да</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="95"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="103"/>
        <source>New case</source>
        <translation type="unfinished">Новый случай</translation>
    </message>
</context>
<context>
    <name>DialogOpen</name>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Tag</source>
        <translation type="unfinished">Тег</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Type</source>
        <translation type="unfinished">Тип</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>ID</source>
        <translation type="unfinished">Идентификатор</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>RANGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="91"/>
        <source>Ok</source>
        <translation type="unfinished">Да</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="92"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="106"/>
        <source>Open case</source>
        <translation type="unfinished">Открыть случай</translation>
    </message>
</context>
<context>
    <name>DialogSensitivity</name>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="56"/>
        <source>Sensitivity analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="71"/>
        <source>Select the variables to manipulate and their ranges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="87"/>
        <source>Multi-dimensional sensitivity ordering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="89"/>
        <source>Lexicographical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="91"/>
        <source>Boustrophedon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="93"/>
        <source>Quasi-spiral</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="98"/>
        <source>Maximum running time (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="103"/>
        <source>Enter 0 to run indefinitely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="124"/>
        <location filename="../src/DialogSensitivity.cc" line="184"/>
        <location filename="../src/DialogSensitivity.cc" line="220"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="127"/>
        <location filename="../src/DialogSensitivity.cc" line="192"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="158"/>
        <source>Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="165"/>
        <source>Select the variables that will be reported for each value of the controlled variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="186"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="190"/>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="207"/>
        <source>Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="222"/>
        <source>Go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="225"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="228"/>
        <source>Copy results to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="230"/>
        <source>Edit sensitivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="248"/>
        <source>Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="371"/>
        <source>ID</source>
        <translation type="unfinished">Идентификатор</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="510"/>
        <location filename="../src/DialogSensitivity.cc" line="534"/>
        <source>Dismiss?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="511"/>
        <location filename="../src/DialogSensitivity.cc" line="535"/>
        <source>Are you sure you want to dismiss the sensitivity?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="513"/>
        <source>All sensitivity settings and results will be lost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="515"/>
        <source>All sensitivity settings will be lost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="536"/>
        <source>All results will be lost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="623"/>
        <source>Controlled variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="625"/>
        <source>Monitored variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="646"/>
        <source>All results have been copied to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogSensitivityOpen</name>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="34"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="34"/>
        <source>The directory &apos;%1&apos; couldn&apos;t be opened for reading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="42"/>
        <source>Open existing multi-dimensional sensitivity study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="71"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="73"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogSensitivitySave</name>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="38"/>
        <source>Enter sensitivity name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="44"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="47"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="78"/>
        <source>Save sensitivity analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="91"/>
        <source>Overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="91"/>
        <source>An analysis named &apos;%1&apos; already exists. Do you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModelDataControl</name>
    <message>
        <location filename="../src/ModelDataControl.cc" line="250"/>
        <source>ID</source>
        <translation type="unfinished">Идентификатор</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="252"/>
        <source>Controlled variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="254"/>
        <source>Units</source>
        <translation type="unfinished">Единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="256"/>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="258"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="260"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="262"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="264"/>
        <source>Points</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModelDataMonitor</name>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="186"/>
        <source>ID</source>
        <translation type="unfinished">Идентификатор</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="188"/>
        <source>Monitored variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="190"/>
        <source>Units</source>
        <translation type="unfinished">Единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="192"/>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="194"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModelQuantityEditable</name>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="150"/>
        <source>Tag</source>
        <translation type="unfinished">Тег</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="151"/>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="152"/>
        <source>Value</source>
        <translation type="unfinished">Значение</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="153"/>
        <source>Units</source>
        <translation type="unfinished">Единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="154"/>
        <source>ID</source>
        <translation type="unfinished">Идентификатор</translation>
    </message>
</context>
<context>
    <name>ModelSensitivityList</name>
    <message>
        <location filename="../src/ModelSensitivityList.cc" line="119"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ModelSensitivityList.cc" line="122"/>
        <source>Type</source>
        <translation type="unfinished">Тип</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/uipf.cc" line="63"/>
        <source>Database Error</source>
        <translation>Ошибка базы данных</translation>
    </message>
    <message>
        <location filename="../src/CommandDataSetDouble.cc" line="45"/>
        <source>Will set again the value of %1 (%2) to %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CommandDataSetDouble.cc" line="55"/>
        <source>Will set the value of %1 (%2) back to %3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ValidatorRegExp</name>
    <message>
        <location filename="../src/ValidatorRegExp.cc" line="34"/>
        <source>The character &apos;%1&apos; is not allowed in the &apos;%2&apos; field.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ValidatorText</name>
    <message>
        <location filename="../src/ValidatorText.cc" line="53"/>
        <source>Character &apos;%1&apos; is numeric therefore it is not allowed as first character in the %2 field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ValidatorText.cc" line="55"/>
        <source>Character &apos;%1&apos; is not alphabetic therefore it is not allowed as first character in the %2 field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ValidatorText.cc" line="67"/>
        <source>Character &apos;%1&apos; is not allowed in the %2 field</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WidgetTableDouble</name>
    <message>
        <location filename="../src/WidgetTableDouble.cc" line="69"/>
        <source>Quantities table</source>
        <translation type="unfinished">Таблица величин</translation>
    </message>
</context>
<context>
    <name>WidgetTableMessages</name>
    <message>
        <location filename="../src/WidgetTableMessages.cc" line="37"/>
        <location filename="../src/WidgetTableMessages.cc" line="78"/>
        <source>Error/Warning message</source>
        <translation type="unfinished">Сообщение об Ошибке/Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/WidgetTableMessages.cc" line="56"/>
        <source>Messages table</source>
        <translation type="unfinished">Таблица сообщений</translation>
    </message>
</context>
<context>
    <name>WidgetTree</name>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Tag</source>
        <translation type="unfinished">Тег</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Type</source>
        <translation type="unfinished">Тип</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>ID</source>
        <translation type="unfinished">Идентификатор</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Parent ID</source>
        <translation type="unfinished">Родительский идентификатор</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="120"/>
        <location filename="../src/WidgetTree.cc" line="129"/>
        <source>Document has large number of elements (&gt;= %1). Expanding all may take lots of time. 
Application could hang.
Are you sure you want to expand them all ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Document has large number of elements (&gt;= %1. Expanding all may take lots of time. 
Application could hang.
Are you sure you want to expand them all ? </source>
        <translation type="obsolete">Документ содержит большое количество элементов(&gt;=%1. Развертывание всех элементов может занять много времени.
Возможно зависание приложения.
Вы уверены, что хотите развернуть все элементы?</translation>
    </message>
</context>
<context>
    <name>WindowMain</name>
    <message>
        <location filename="../src/WindowMain.cc" line="91"/>
        <source>&amp;New</source>
        <translation type="unfinished">&amp;Новый</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="92"/>
        <source>Create a new case</source>
        <translation type="unfinished">Создать новый случай</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="96"/>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Открыть</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="97"/>
        <source>Open an existing case</source>
        <translation type="unfinished">Открыть существующий случай</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="101"/>
        <source>&amp;Save as</source>
        <translation type="unfinished">&amp;Сохранить как</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="102"/>
        <source>Save the current case with a different description</source>
        <translation type="unfinished">Сохранить текущие дела с описанием различных</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="107"/>
        <source>&amp;Delete</source>
        <translation type="unfinished">&amp;Удалить</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="108"/>
        <source>Delete an existing case</source>
        <translation type="unfinished">Удалить существующий случай</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="113"/>
        <source>&amp;Purge</source>
        <translation type="unfinished">&amp;Очистить</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="114"/>
        <source>Delete all existing cases</source>
        <translation type="unfinished">Удалить все существующие случаи</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="118"/>
        <source>&amp;Exit</source>
        <translation type="unfinished">&amp;Выйти</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="119"/>
        <source>Quit the program</source>
        <translation type="unfinished">Выйти из программы</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="128"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="133"/>
        <source>Undo last change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="140"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="144"/>
        <source>Redo last change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="151"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="152"/>
        <source>Copy table region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="159"/>
        <source>&amp;Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="160"/>
        <source>Select entire table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="167"/>
        <source>&amp;Reset</source>
        <translation type="unfinished">&amp;Сбросить</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="168"/>
        <source>Reset the case with its original results</source>
        <translation type="unfinished">Восстановить случай с начальными результатами</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="173"/>
        <source>&amp;Calculate</source>
        <translation type="unfinished">&amp;Рассчитать</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="174"/>
        <source>Launch calculation</source>
        <translation type="unfinished">Запустить вычисления</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="179"/>
        <source>&amp;Homotopy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="180"/>
        <source>Move smoothly to a different solution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="185"/>
        <source>&amp;Stop</source>
        <translation type="unfinished">&amp;Остановить</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="186"/>
        <source>Stop the calculation</source>
        <translation type="unfinished">Остановить вычисления</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="191"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;О программе</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="192"/>
        <source>Show the application&apos;s About box</source>
        <translation type="unfinished">Информация о программе</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="196"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished">О библиотеке &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="197"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation type="unfinished">Информация о библиотеке Qt</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="201"/>
        <source>&amp;ODS stream results</source>
        <translation type="unfinished">&amp;ODS текущие результаты</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="202"/>
        <source>Open stream results for last run in OpenDocument Spreadsheet format</source>
        <translation type="unfinished">Открыть текущие результаты последнего запуска в формате OpenDocument Spreadsheet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="207"/>
        <source>&amp;XLS stream results</source>
        <translation type="unfinished">&amp;XLS текущие результаты</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="208"/>
        <source>Open stream results for last run in Microsoft Excel Spreadsheet format</source>
        <translation type="unfinished">Открыть текущие результаты последнего запуска в формате Microsoft Excel Spreadsheet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="215"/>
        <source>&amp;TXT results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="221"/>
        <source>&amp;HTML results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="222"/>
        <source>Export all results for the current node in HTML format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="227"/>
        <source>&amp;Select kernel</source>
        <translation type="unfinished">&amp;Выбор ядра</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="228"/>
        <source>Select active kernel</source>
        <translation type="unfinished">Выбрать активное ядро</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="232"/>
        <source>&amp;Expand all</source>
        <translation type="unfinished">&amp;Развернуть все</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="233"/>
        <source>Expand all objects</source>
        <translation type="unfinished">Развернуть все объекты</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="237"/>
        <source>&amp;Collapse all</source>
        <translation type="unfinished">&amp;Свернуть все</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="238"/>
        <source>Collapse all objects</source>
        <translation type="unfinished">Свернуть все объекты</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="242"/>
        <source>&amp;Root</source>
        <translation type="unfinished">&amp;Корень </translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="243"/>
        <source>Go to root object</source>
        <translation type="unfinished">Перейти к корневому объекту</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="247"/>
        <source>&amp;Up</source>
        <translation type="unfinished">&amp;Наверх</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="248"/>
        <source>Go up one level</source>
        <translation type="unfinished">Перейти на один уровень выше</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="252"/>
        <source>&amp;Toggle child</source>
        <translation type="unfinished">&amp;Вкл/Выкл подобъекты</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="253"/>
        <source>Toggle visualization of child items</source>
        <translation type="unfinished">Включить/Выключить отображение подобъектов</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="257"/>
        <source>&amp;Sensitivity analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="913"/>
        <source>Duplicating object %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1610"/>
        <source>Trying to start: %1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="792"/>
        <source>The process has exited with return code %1</source>
        <translation type="unfinished">Обработка завершена с кодом %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="833"/>
        <source>The operation completed successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1352"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2008-2021 Paolo Greppi simevo s.r.l.&lt;br&gt;Based in part on code from Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera, Lahiru Chandima, Shane Shields and Luiz A. Buhnemann (la3280@gmail.com).&lt;br&gt;Language Icon by Onur </source>
        <translation type="unfinished">&lt;p&gt;Copyright &amp;copy; 2008-2021 Paolo Greppi simevo s.r.l.&lt;br&gt;Частично основана на исходном коде от Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera, Lahiru Chandima, Shane Shields, Luiz A. Buhnemann (la3280@gmail.com).&lt;br&gt;Language Icon by Onur </translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1381"/>
        <source>Failure to start external command %1 for ODS file generation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="258"/>
        <source>Launch multi-dimensional sensitivity analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="82"/>
        <source>User Interface for Process Flowsheeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="216"/>
        <source>Open a text file with all results for the current node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="263"/>
        <source>&amp;Open multi-dimensional sensitivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="264"/>
        <source>Opens an existing multidimensional sensitivity analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="269"/>
        <source>&amp;Restore last run multi-dimensional sensitivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="270"/>
        <source>Restores the last sensitivity that was executed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="275"/>
        <source>&amp;Activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="276"/>
        <source>Activate LIBPF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="281"/>
        <source>&amp;SI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="290"/>
        <source>&amp;EN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="291"/>
        <source>U.S. Customary Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="295"/>
        <source>&amp;Eng</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="296"/>
        <source>Engineering Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="314"/>
        <source>&amp;Clear</source>
        <translation type="unfinished">&amp;Очистить</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="350"/>
        <source>&amp;Case</source>
        <translation type="unfinished">&amp;Cлучай</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="358"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="365"/>
        <source>&amp;Run</source>
        <translation type="unfinished">&amp;Выполнить</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="371"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Вид</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="384"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="389"/>
        <source>Default</source>
        <translation type="unfinished">По умолчанию</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="393"/>
        <source>Arabic</source>
        <translation type="unfinished">Арабский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="397"/>
        <source>German</source>
        <translation type="unfinished">Немецкий</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="401"/>
        <source>Spanish</source>
        <translation type="unfinished">Испанский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="405"/>
        <source>English</source>
        <translation type="unfinished">Английский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="409"/>
        <source>French</source>
        <translation type="unfinished">Французский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="413"/>
        <source>Hebrew</source>
        <translation type="unfinished">Иврит</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="417"/>
        <source>Italian</source>
        <translation type="unfinished">Итальянский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="421"/>
        <source>Japanese</source>
        <translation type="unfinished">Японский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="425"/>
        <source>Korean</source>
        <translation type="unfinished">Корейский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="429"/>
        <source>Portuguese</source>
        <translation type="unfinished">Португальский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="433"/>
        <source>Russian</source>
        <translation type="unfinished">Русский</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="437"/>
        <source>Chinese (simplified)</source>
        <translation type="unfinished">Китайский (упрощенный)</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="441"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="444"/>
        <source>&amp;Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="445"/>
        <source>Change units of measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="451"/>
        <source>Language</source>
        <translation type="unfinished">Язык</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="452"/>
        <source>Change User Interface language</source>
        <translation type="unfinished">Изменить язык интерфейса пользователя</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="468"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="472"/>
        <source>Main toolbar</source>
        <translation type="unfinished">Главная панель инструментов</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="554"/>
        <source>Inputs</source>
        <translation type="unfinished">Данные</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="555"/>
        <source>Outputs</source>
        <translation type="unfinished">Результаты</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="556"/>
        <source>Messages</source>
        <translation type="unfinished">Сообщения</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="629"/>
        <source>Status indicator</source>
        <translation type="unfinished">Индикатор состояния</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="636"/>
        <source>Ready</source>
        <translation type="unfinished">Готово</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="665"/>
        <source>Opened node %1</source>
        <translation type="unfinished">Oбъекту %1 oткрыт</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="671"/>
        <source>The process has been started successfully</source>
        <translation type="unfinished">Обработка началась успешно</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="857"/>
        <source>Opening a new object of type %1</source>
        <translation type="unfinished">Открытие нового объекта типа %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="864"/>
        <source>Case %1 is already loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="868"/>
        <source>Opening the existing case %1</source>
        <translation type="unfinished">Открытие существующего случая %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="871"/>
        <source>Opened the existing case %1</source>
        <translation type="unfinished">Открыт существующий случай %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1242"/>
        <source>The LIBPF activation utility is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1520"/>
        <source>EMPTY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1530"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1540"/>
        <source>WARNINGS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1550"/>
        <source>ERRORS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1560"/>
        <source>CHANGED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1570"/>
        <source>INVALID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1577"/>
        <source>RUNNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1585"/>
        <source>UNDEFINED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1047"/>
        <location filename="../src/WindowMain.cc" line="1068"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="846"/>
        <source>No action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="850"/>
        <source>Unknown action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="922"/>
        <source>Writing TXT file for object %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="940"/>
        <source>Opening TXT file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="942"/>
        <source>TXT file %1 opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="944"/>
        <source>Error opening TXT file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="977"/>
        <source>Writing HTML file for object %1 to directory %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1005"/>
        <source>Opening HTML file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1007"/>
        <source>HTML file %1 opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1009"/>
        <source>Error opening HTML file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1047"/>
        <source>Couldn&apos;t parse sensitivity input file name &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1068"/>
        <source>Failed to load sensitivity input file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1204"/>
        <location filename="../src/WindowMain.cc" line="1379"/>
        <source>Starting: %1</source>
        <translation type="unfinished">Старт: %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1188"/>
        <source>Failure 1 during preparation for LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="678"/>
        <source>Impossible to start the kernel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="679"/>
        <source>The kernel started successfully but crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="680"/>
        <source>Time out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="681"/>
        <source>Write error to kernel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="682"/>
        <source>Read error to kernel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="683"/>
        <source>Unknown error in kernel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1191"/>
        <source>Failure 2 during preparation for LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1196"/>
        <source>LIBPF is already activated !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1199"/>
        <source>Failure 3 during preparation for LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1207"/>
        <source>Failure 1 during LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1210"/>
        <source>Failure 2 during LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1228"/>
        <location filename="../src/WindowMain.cc" line="1251"/>
        <source>Failure to start the LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1232"/>
        <location filename="../src/WindowMain.cc" line="1255"/>
        <source>Failure during LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1263"/>
        <source>Error during LIBPF activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1265"/>
        <source>LIBPF activation success !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1271"/>
        <source>Deleting object %1</source>
        <translation type="unfinished">Удаление объекта %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1293"/>
        <source>Starting homotopy run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1303"/>
        <source>Confirm purge</source>
        <translation type="unfinished">Подтвердите удаление</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1303"/>
        <source>Do you really wish to delete all existing cases ?</source>
        <translation type="unfinished">Вы действительно хотите удалить все существующие случаи?</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1306"/>
        <source>Deleting all existing cases</source>
        <translation type="unfinished">Удалить все существующие случаи</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1328"/>
        <source>Interrupted calculation</source>
        <translation type="unfinished">Вычисления прерваны</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1334"/>
        <source>Cleared log window</source>
        <translation type="unfinished">Очищен журнал событий</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1351"/>
        <source>About uipf</source>
        <translation type="unfinished">Информация об uipf</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1360"/>
        <source>.&lt;p&gt;Icons by Tango-icon-theme, Tango Desktop Project.&lt;br&gt;Based in part on code from the example classes of the Qt Toolkit:&lt;br&gt;Copyright &amp;copy; 2011 Nokia Corporation and/or its subsidiary(-ies).&lt;p&gt;&lt;b&gt;UIPF&lt;/b&gt;, the User Interface for Process Flowsheeting provides &lt;br&gt;the User Interface to process_ models developed using &lt;b&gt;LIBPF&lt;/b&gt;, &lt;br&gt;the &lt;b&gt;LIB&lt;/b&gt;rary for &lt;b&gt;P&lt;/b&gt;rocess &lt;b&gt;F&lt;/b&gt;lowsheeting in C++ &lt;p&gt;For more informations please visit &lt;a href=&quot;http://www.libpf.com/&quot;&gt;the LIBPF website&lt;/a&gt;.</source>
        <translation type="unfinished">.&lt;p&gt;Icons by Tango-icon-theme, Tango Desktop Project.&lt;br&gt;Частично основана на примерах исходных кодах классов от Qt Toolkit:&lt;br&gt;Copyright &amp;copy; 2011 Nokia Corporation and/or its subsidiary(-ies).&lt;p&gt;&lt;b&gt;UIPF&lt;/b&gt;, User Interface for Process Flowsheeting, предоставляет &lt;br&gt;Пользовательский Интерфейс для моделирования процессов, разработанный с использованием библиотеки &lt;b&gt;LIBPF&lt;/b&gt;, &lt;br&gt;&lt;b&gt;LIB&lt;/b&gt;rary for &lt;b&gt;P&lt;/b&gt;rocess &lt;b&gt;F&lt;/b&gt;lowsheeting, на C++ &lt;p&gt;Более подробная информация на &lt;a href=&quot;http://www.libpf.com/&quot;&gt;вебсайте LIBPF&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1385"/>
        <source>Error writing to OpenOffice spreadsheet %1</source>
        <translation type="unfinished">Ошибка записи в OpenOffice таблицу %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1391"/>
        <source>Opening OpenOffice spreadsheet %1</source>
        <translation type="unfinished">Открытие OpenOffice таблицы %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1393"/>
        <source>OpenOffice spreadsheet %1 opened</source>
        <translation type="unfinished">OpenOffice таблица %1 открыта</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1395"/>
        <source>Error opening OpenOffice spreadsheet %1</source>
        <translation type="unfinished">Ошибка открытия OpenOffice таблицы %1 </translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1410"/>
        <source>Opening Excel spreadsheet %1</source>
        <translation type="unfinished"> Открытие Excel таблицы %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1416"/>
        <source>Excel spreadsheet %1 opened</source>
        <translation type="unfinished">Excel таблица %1 открыта</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1418"/>
        <source>Error opening Excel spreadsheet %1</source>
        <translation type="unfinished"> Ошибка открытия Excel таблицы %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1427"/>
        <source>Select kernel</source>
        <translation type="unfinished">Выбрать ядро</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1436"/>
        <source>Set active kernel to %1</source>
        <translation type="unfinished">Установить активное ядро %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1444"/>
        <location filename="../src/WindowMain.cc" line="1446"/>
        <source>Invalid selected kernel</source>
        <translation type="unfinished">Неверно выбранное ядро</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1444"/>
        <source>The kernel can only reside inside the %1 directory.</source>
        <translation type="unfinished">Ядро может распологаться только  в %1 справочнике.</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1499"/>
        <source>Restart to apply language change</source>
        <translation type="unfinished">Перезагрузка, чтобы применить изменения языка</translation>
    </message>
</context>
</TS>
