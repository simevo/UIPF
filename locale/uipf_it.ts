<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>DialogDuplicate</name>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="29"/>
        <source>Tag:</source>
        <translation>Etichetta:</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="32"/>
        <source>Description:</source>
        <translation>Descrizione:</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="41"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="42"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="57"/>
        <source>New case</source>
        <translation>Nuovo problema</translation>
    </message>
</context>
<context>
    <name>DialogNew</name>
    <message>
        <location filename="../src/DialogNew.cc" line="56"/>
        <source>Tag:</source>
        <translation>Etichetta:</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="59"/>
        <source>Description:</source>
        <translation>Descrizione:</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="65"/>
        <source>types</source>
        <translation>tipi</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="94"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="95"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="103"/>
        <source>New case</source>
        <translation>Nuovo problema</translation>
    </message>
</context>
<context>
    <name>DialogOpen</name>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Tag</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>RANGE</source>
        <translation>INTERVALLO</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="91"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="92"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="106"/>
        <source>Open case</source>
        <translation>Apri problema</translation>
    </message>
</context>
<context>
    <name>DialogSensitivity</name>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="56"/>
        <source>Sensitivity analysis</source>
        <translation>Analisi di sensitività</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="71"/>
        <source>Select the variables to manipulate and their ranges</source>
        <translation>Scegli le variabili da manipolare e i loro limiti</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="87"/>
        <source>Multi-dimensional sensitivity ordering</source>
        <translation>Ordinamento dell&apos;analisi di sensitività multidimensionale</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="89"/>
        <source>Lexicographical</source>
        <translation>Ordinamento naturale</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="91"/>
        <source>Boustrophedon</source>
        <translation>Ordine bustrofedico</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="93"/>
        <source>Quasi-spiral</source>
        <translation>Ordine quasi-spirale</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="98"/>
        <source>Maximum running time (s)</source>
        <translation>Limite di tempo massimo per il calcolo (s)</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="103"/>
        <source>Enter 0 to run indefinitely</source>
        <translation>Indica 0 per non impostare un limite di tempo</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="124"/>
        <location filename="../src/DialogSensitivity.cc" line="184"/>
        <location filename="../src/DialogSensitivity.cc" line="220"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="127"/>
        <location filename="../src/DialogSensitivity.cc" line="192"/>
        <source>Next</source>
        <translation>Avanti</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="158"/>
        <source>Control</source>
        <translation>Controlla</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="165"/>
        <source>Select the variables that will be reported for each value of the controlled variable</source>
        <translation>Seleziona le variabili che verranno tabulate per ogni valore della variabile controllata</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="186"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="190"/>
        <source>Previous</source>
        <translation>Indietro</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="207"/>
        <source>Monitor</source>
        <translation>Osserva</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="222"/>
        <source>Go</source>
        <translation>Vai</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="225"/>
        <source>Stop</source>
        <translation>Ferma</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="228"/>
        <source>Copy results to clipboard</source>
        <translation>Copia i risultati negli appunti</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="230"/>
        <source>Edit sensitivity</source>
        <translation>Modifica la sensitività</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="248"/>
        <source>Results</source>
        <translation>Risultati</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="371"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="510"/>
        <location filename="../src/DialogSensitivity.cc" line="534"/>
        <source>Dismiss?</source>
        <translation>Chiudere?</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="511"/>
        <location filename="../src/DialogSensitivity.cc" line="535"/>
        <source>Are you sure you want to dismiss the sensitivity?</source>
        <translation>Sei sicuro di voler chiudere l&apos;analisi di sensitività ?</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="513"/>
        <source>All sensitivity settings and results will be lost</source>
        <translation>Tutte le impostazioni e i risultati della sensitività saranno persi</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="515"/>
        <source>All sensitivity settings will be lost</source>
        <translation>Tutte le impostazioni della sensitività saranno perse</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="536"/>
        <source>All results will be lost</source>
        <translation>Tutti i risultati saranno persi</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="623"/>
        <source>Controlled variable %1</source>
        <translation>Variabile controllata %1</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="625"/>
        <source>Monitored variable %1</source>
        <translation>Variabile monitorata %1</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="646"/>
        <source>All results have been copied to the clipboard</source>
        <translation>Tutti i risultati sono stati copiati negli appunti</translation>
    </message>
</context>
<context>
    <name>DialogSensitivityOpen</name>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="34"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="34"/>
        <source>The directory &apos;%1&apos; couldn&apos;t be opened for reading.</source>
        <translation>Impossibile aprire in lettura la directory &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="42"/>
        <source>Open existing multi-dimensional sensitivity study</source>
        <translation>Apre un&apos;analisi di sensitività esistente</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="71"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="73"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>DialogSensitivitySave</name>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="38"/>
        <source>Enter sensitivity name:</source>
        <translation>Inserisci il nome della sensitività:</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="44"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="47"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="78"/>
        <source>Save sensitivity analysis</source>
        <translation>Salva l&apos;analisi di sensitività</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="91"/>
        <source>Overwrite?</source>
        <translation>Sovrascrivo?</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="91"/>
        <source>An analysis named &apos;%1&apos; already exists. Do you want to overwrite it?</source>
        <translation>Una sensitività di nome &apos;%1&apos; esiste già. Vuoi sovrascriverla?</translation>
    </message>
</context>
<context>
    <name>ModelDataControl</name>
    <message>
        <location filename="../src/ModelDataControl.cc" line="250"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="252"/>
        <source>Controlled variables</source>
        <translation>Variabili controllate</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="254"/>
        <source>Units</source>
        <translation>Unità di misura</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="256"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="258"/>
        <source>Label</source>
        <translation>Etichetta breve</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="260"/>
        <source>Start</source>
        <translation>Inizio</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="262"/>
        <source>End</source>
        <translation>Fine</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="264"/>
        <source>Points</source>
        <translation>Punti</translation>
    </message>
</context>
<context>
    <name>ModelDataMonitor</name>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="186"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="188"/>
        <source>Monitored variables</source>
        <translation>Variabili osservate</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="190"/>
        <source>Units</source>
        <translation>Unità di misura</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="192"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="194"/>
        <source>Label</source>
        <translation>Etichetta breve</translation>
    </message>
</context>
<context>
    <name>ModelQuantityEditable</name>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="150"/>
        <source>Tag</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="151"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="152"/>
        <source>Value</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="153"/>
        <source>Units</source>
        <translation>Unità di misura</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="154"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
</context>
<context>
    <name>ModelSensitivityList</name>
    <message>
        <location filename="../src/ModelSensitivityList.cc" line="119"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/ModelSensitivityList.cc" line="122"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/uipf.cc" line="63"/>
        <source>Database Error</source>
        <translation>Errore di comunicazione col database</translation>
    </message>
    <message>
        <location filename="../src/CommandDataSetDouble.cc" line="45"/>
        <source>Will set again the value of %1 (%2) to %3</source>
        <translation>Ripristina il valore di %1 (%2) a %3</translation>
    </message>
    <message>
        <location filename="../src/CommandDataSetDouble.cc" line="55"/>
        <source>Will set the value of %1 (%2) back to %3</source>
        <translation>Riporta il valore di %1 (%2) a %3</translation>
    </message>
</context>
<context>
    <name>ValidatorRegExp</name>
    <message>
        <location filename="../src/ValidatorRegExp.cc" line="34"/>
        <source>The character &apos;%1&apos; is not allowed in the &apos;%2&apos; field.</source>
        <translation>Il carattere &apos;%1&apos; non è consentito nel campo %2.</translation>
    </message>
</context>
<context>
    <name>ValidatorText</name>
    <message>
        <location filename="../src/ValidatorText.cc" line="53"/>
        <source>Character &apos;%1&apos; is numeric therefore it is not allowed as first character in the %2 field</source>
        <translation>Il carattere &apos;%1&apos; è numerico quindi non è consentito come carattere iniziale nel campo %2</translation>
    </message>
    <message>
        <location filename="../src/ValidatorText.cc" line="55"/>
        <source>Character &apos;%1&apos; is not alphabetic therefore it is not allowed as first character in the %2 field</source>
        <translation>Il carattere &apos;%1&apos; non è alfabetico quindi non è consentito come carattere iniziale nel campo %2</translation>
    </message>
    <message>
        <location filename="../src/ValidatorText.cc" line="67"/>
        <source>Character &apos;%1&apos; is not allowed in the %2 field</source>
        <translation>Il carattere &apos;%1&apos; non è consentito nel campo %2</translation>
    </message>
</context>
<context>
    <name>WidgetTableDouble</name>
    <message>
        <location filename="../src/WidgetTableDouble.cc" line="69"/>
        <source>Quantities table</source>
        <translation>Tabella delle quantità</translation>
    </message>
</context>
<context>
    <name>WidgetTableMessages</name>
    <message>
        <location filename="../src/WidgetTableMessages.cc" line="37"/>
        <location filename="../src/WidgetTableMessages.cc" line="78"/>
        <source>Error/Warning message</source>
        <translation>Messaggio di errore o avviso</translation>
    </message>
    <message>
        <location filename="../src/WidgetTableMessages.cc" line="56"/>
        <source>Messages table</source>
        <translation>Tabella dei messaggi</translation>
    </message>
</context>
<context>
    <name>WidgetTree</name>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Tag</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Parent ID</source>
        <translation>ID del livello superiore</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="120"/>
        <location filename="../src/WidgetTree.cc" line="129"/>
        <source>Document has large number of elements (&gt;= %1). Expanding all may take lots of time. 
Application could hang.
Are you sure you want to expand them all ?</source>
        <translation>Il documento ha un gran numero di elementi (&gt;= %1.
Espanderli tutti potrebbe richiedere un lungo tempo e l&apos;applicazione potrebbe bloccarsi.
Sei sicuro di volerli espandere ?</translation>
    </message>
    <message>
        <source>Document has large number of elements (&gt;= %1. Expanding all may take lots of time. 
Application could hang.
Are you sure you want to expand them all ? </source>
        <translation type="vanished">Il documento ha un gran numero di elementi (&gt;= %1. Espanderli tutti potrebbe richiedere un lungo tempo e l&apos;applicazione potrebbe bloccarsi. Sei sicuro di volerli espandere ?</translation>
    </message>
</context>
<context>
    <name>WindowMain</name>
    <message>
        <location filename="../src/WindowMain.cc" line="91"/>
        <source>&amp;New</source>
        <translation>&amp;Nuovo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="92"/>
        <source>Create a new case</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="96"/>
        <source>&amp;Open</source>
        <translation>&amp;Apri</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="97"/>
        <source>Open an existing case</source>
        <translation>Apri un problema esistente</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="101"/>
        <source>&amp;Save as</source>
        <translation>&amp;Salva con nome</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="102"/>
        <source>Save the current case with a different description</source>
        <translation>Salva il problema con un&apos;altra descrizione</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="107"/>
        <source>&amp;Delete</source>
        <translation>&amp;Elimina</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="108"/>
        <source>Delete an existing case</source>
        <translation>Elimina un problema</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="113"/>
        <source>&amp;Purge</source>
        <translation>&amp;Svuota</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="114"/>
        <source>Delete all existing cases</source>
        <translation>Elimina tutti i problemi</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="118"/>
        <source>&amp;Exit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="119"/>
        <source>Quit the program</source>
        <translation>Abbandona il programma</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="128"/>
        <source>&amp;Undo</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="133"/>
        <source>Undo last change</source>
        <translation>Annulla l&apos;ultima modifica</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="140"/>
        <source>&amp;Redo</source>
        <translation>&amp;Ripristina</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="144"/>
        <source>Redo last change</source>
        <translation>Ripristina l&apos;ultima modifica</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="151"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copia</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="152"/>
        <source>Copy table region</source>
        <translation>Copia la regione di tabella</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="159"/>
        <source>&amp;Select All</source>
        <translation>&amp;Seleziona Tutto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="160"/>
        <source>Select entire table</source>
        <translation>Seleziona l&apos;intera tabella</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="167"/>
        <source>&amp;Reset</source>
        <translation>&amp;Resetta</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="168"/>
        <source>Reset the case with its original results</source>
        <translation>Riporta il problema alla specifica iniziale</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="173"/>
        <source>&amp;Calculate</source>
        <translation>&amp;Calcola</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="174"/>
        <source>Launch calculation</source>
        <translation>Avvia il calcolo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="179"/>
        <source>&amp;Homotopy</source>
        <translation>&amp;Omotopia</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="180"/>
        <source>Move smoothly to a different solution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="185"/>
        <source>&amp;Stop</source>
        <translation>&amp;Stop</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="186"/>
        <source>Stop the calculation</source>
        <translation>Interrompi il calcolo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="191"/>
        <source>&amp;About</source>
        <translation>&amp;Informazioni su UIPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="192"/>
        <source>Show the application&apos;s About box</source>
        <translation>Mostra informazioni su questo programma</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="196"/>
        <source>About &amp;Qt</source>
        <translation>Informazioni su &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="197"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Mostra informazioni sulla libreria Qt</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="201"/>
        <source>&amp;ODS stream results</source>
        <translation>Risultati per le correnti in formato &amp;ODS</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="202"/>
        <source>Open stream results for last run in OpenDocument Spreadsheet format</source>
        <translation>Apre i risultati per le correnti relativi all&apos;ultima girata in formato OpenDocument Spreadsheet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="207"/>
        <source>&amp;XLS stream results</source>
        <translation>Risultati per le correnti in formato &amp;XLS</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="208"/>
        <source>Open stream results for last run in Microsoft Excel Spreadsheet format</source>
        <translation>Apre i risultati per le correnti relativi all&apos;ultima girata in formato Microsoft Excel Spreadsheet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="215"/>
        <source>&amp;TXT results</source>
        <translation>Risultati in formato &amp;TXT</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="227"/>
        <source>&amp;Select kernel</source>
        <translation>&amp;Seleziona il kernel</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="228"/>
        <source>Select active kernel</source>
        <translation>Seleziona il motore di calcolo (kernel) attivo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="232"/>
        <source>&amp;Expand all</source>
        <translation>&amp;Espandi tutti</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="233"/>
        <source>Expand all objects</source>
        <translation>Espandi tutti gli oggetti e sotto-oggetti</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="237"/>
        <source>&amp;Collapse all</source>
        <translation>&amp;Contrai tutti</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="238"/>
        <source>Collapse all objects</source>
        <translation>Contrai tutti gli oggetti e sotto-oggetti</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="242"/>
        <source>&amp;Root</source>
        <translation>&amp;Radice</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="243"/>
        <source>Go to root object</source>
        <translation>Vai all&apos;oggetto radice</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="247"/>
        <source>&amp;Up</source>
        <translation>&amp;Su</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="248"/>
        <source>Go up one level</source>
        <translation>Vai su di un livello</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="252"/>
        <source>&amp;Toggle child</source>
        <translation>&amp;Commuta figli</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="253"/>
        <source>Toggle visualization of child items</source>
        <translation>Commuta la modalità di visualizzazione dei nodi figlio</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="257"/>
        <source>&amp;Sensitivity analysis</source>
        <translation>Analisi di &amp;sensitività</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="846"/>
        <source>No action</source>
        <translation>Nessuna azione</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="850"/>
        <source>Unknown action</source>
        <translation>Azione sconosciuta</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="913"/>
        <source>Duplicating object %1</source>
        <translation>Duplicazione dell&apos;oggetto %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="922"/>
        <source>Writing TXT file for object %1</source>
        <translation>Scrittura dell&apos;oggetto %1 su file TXT</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="940"/>
        <source>Opening TXT file %1</source>
        <translation>Apertura del file TXT %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="942"/>
        <source>TXT file %1 opened</source>
        <translation>File TXT %1 aperto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="944"/>
        <source>Error opening TXT file %1</source>
        <translation>Errore durante l&apos;apertura del file TXT %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="792"/>
        <source>The process has exited with return code %1</source>
        <translation>Il processo è terminato con codice di uscita %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="833"/>
        <source>The operation completed successfully</source>
        <translation>L&apos;operazione è stata completata con successo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1381"/>
        <source>Failure to start external command %1 for ODS file generation</source>
        <translation>Errore durante la generazione del file ODS col comando %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="216"/>
        <source>Open a text file with all results for the current node</source>
        <translation>Apre un file di testo con tutti i risultati per il nodo corrente</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="221"/>
        <source>&amp;HTML results</source>
        <translation>Risultati in formato &amp;HTML</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="222"/>
        <source>Export all results for the current node in HTML format</source>
        <translation>Esporta tutti i risultati per il nodo corrente in formato HTML</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="258"/>
        <source>Launch multi-dimensional sensitivity analysis</source>
        <translation>Avvia un&apos;analisi di sensitività multi-dimensionale</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="263"/>
        <source>&amp;Open multi-dimensional sensitivity</source>
        <translation>&amp;Apre l&apos;analisi di sensitività</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="264"/>
        <source>Opens an existing multidimensional sensitivity analysis</source>
        <translation>Apre un&apos;analisi di sensitività multi-dimensionale</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="269"/>
        <source>&amp;Restore last run multi-dimensional sensitivity</source>
        <translation>&amp;Ripristina l&apos;ultima analisi di sensitività</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="270"/>
        <source>Restores the last sensitivity that was executed</source>
        <translation>Ripristina l&apos;ultima analisi di sensitività multi-dimensionale eseguita</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="275"/>
        <source>&amp;Activate</source>
        <translation>&amp;Attiva</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="276"/>
        <source>Activate LIBPF</source>
        <translation>Attiva LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="281"/>
        <source>&amp;SI</source>
        <translation>&amp;SI</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="290"/>
        <source>&amp;EN</source>
        <translation>&amp;EN</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="291"/>
        <source>U.S. Customary Units</source>
        <translation>Unità anglosassoni</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="295"/>
        <source>&amp;Eng</source>
        <translation>&amp;Eng</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="296"/>
        <source>Engineering Units</source>
        <translation>Unità ingegneristiche</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="314"/>
        <source>&amp;Clear</source>
        <translation>&amp;Pulisci</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="350"/>
        <source>&amp;Case</source>
        <translation>&amp;Problemi</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="358"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifica</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="365"/>
        <source>&amp;Run</source>
        <translation>&amp;Calcola</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="371"/>
        <source>&amp;View</source>
        <translation>&amp;Vista</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="384"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="389"/>
        <source>Default</source>
        <translation>Predefinita</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="393"/>
        <source>Arabic</source>
        <translation>Arabo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="397"/>
        <source>German</source>
        <translation>Tedesco</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="401"/>
        <source>Spanish</source>
        <translation>Spagnolo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="405"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="409"/>
        <source>French</source>
        <translation>Francese</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="413"/>
        <source>Hebrew</source>
        <translation>Ebraico</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="417"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="421"/>
        <source>Japanese</source>
        <translation>Giapponese</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="425"/>
        <source>Korean</source>
        <translation>Coreano</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="429"/>
        <source>Portuguese</source>
        <translation>Portoghese</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="433"/>
        <source>Russian</source>
        <translation>Russo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="437"/>
        <source>Chinese (simplified)</source>
        <translation>Cinese (semplificato)</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="441"/>
        <source>&amp;Settings</source>
        <translation>&amp;Impostazioni</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="444"/>
        <source>&amp;Units</source>
        <translation>&amp;Unità</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="445"/>
        <source>Change units of measurements</source>
        <translation>Cambia le unità di misura</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="451"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="452"/>
        <source>Change User Interface language</source>
        <translation>Cambia la lingua dell&apos;interfaccia utente</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="468"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="472"/>
        <source>Main toolbar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="554"/>
        <source>Inputs</source>
        <translation>Input</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="555"/>
        <source>Outputs</source>
        <translation>Risultati</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="556"/>
        <source>Messages</source>
        <translation>Messaggi</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="629"/>
        <source>Status indicator</source>
        <translation>Indicatore di stato</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="636"/>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="665"/>
        <source>Opened node %1</source>
        <translation>Il nodo %1 è stato aperto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="671"/>
        <source>The process has been started successfully</source>
        <translation>Il processo è partito correttamente</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="678"/>
        <source>Impossible to start the kernel</source>
        <translation>Impossibile avviare il kernel</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="679"/>
        <source>The kernel started successfully but crashed.</source>
        <translation>Il kernel è stato avviato ma è andato in crash</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="680"/>
        <source>Time out</source>
        <translation>Tempo esaurito</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="681"/>
        <source>Write error to kernel</source>
        <translation>Errore di scrittura al kernel</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="682"/>
        <source>Read error to kernel</source>
        <translation>Errore di lettura dal kernel</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="683"/>
        <source>Unknown error in kernel</source>
        <translation>Errore sconosciuto nell kernel</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="977"/>
        <source>Writing HTML file for object %1 to directory %2</source>
        <translation>Scrittura dell&apos;oggetto %1 su file HTML nella directory %2</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1005"/>
        <source>Opening HTML file %1</source>
        <translation>Apertura del file HTML %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1007"/>
        <source>HTML file %1 opened</source>
        <translation>File HTML %1 aperto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1009"/>
        <source>Error opening HTML file %1</source>
        <translation>Errore durante l&apos;apertura del file HTML %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1242"/>
        <source>The LIBPF activation utility is missing</source>
        <translation>Utility per l&apos;attivazione di LIBPF mancante</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1352"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2008-2021 Paolo Greppi simevo s.r.l.&lt;br&gt;Based in part on code from Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera, Lahiru Chandima, Shane Shields and Luiz A. Buhnemann (la3280@gmail.com).&lt;br&gt;Language Icon by Onur </source>
        <translation>&lt;p&gt;Copyright &amp;copy; 2008-2021 Paolo Greppi simevo s.r.l.&lt;br&gt;Basato anche su codice di Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera, Lahiru Chandima, Shane Shields e Luiz A. Buhnemann (la3280@gmail.com).&lt;br&gt;Language Icon by Onur </translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1610"/>
        <source>Trying to start: %1 %2</source>
        <translation>Provo ad avviare: %1 %2</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="857"/>
        <source>Opening a new object of type %1</source>
        <translation>Apro un nuovo oggetto di tipo %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="864"/>
        <source>Case %1 is already loaded</source>
        <translation>Il problema %1 è già aperto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="868"/>
        <source>Opening the existing case %1</source>
        <translation>Apro il problema esistente %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="871"/>
        <source>Opened the existing case %1</source>
        <translation>Il problema esistente %1 è stato aperto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1520"/>
        <source>EMPTY</source>
        <translation>VUOTO</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1530"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1540"/>
        <source>WARNINGS</source>
        <translation>AVVISI</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1550"/>
        <source>ERRORS</source>
        <translation>ERRORI</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1560"/>
        <source>CHANGED</source>
        <translation>MODIFICHE</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1570"/>
        <source>INVALID</source>
        <translation>NON VALIDO</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1577"/>
        <source>RUNNING</source>
        <translation>GIRA</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1585"/>
        <source>UNDEFINED</source>
        <translation>INDEFINITO</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1047"/>
        <location filename="../src/WindowMain.cc" line="1068"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1047"/>
        <source>Couldn&apos;t parse sensitivity input file name &apos;%1&apos;</source>
        <translation>Impossibile scandire il file d&apos;input della sensitività &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1068"/>
        <source>Failed to load sensitivity input file &apos;%1&apos;</source>
        <translation>Errore nel caricamento del file d&apos;input della sensitività &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1204"/>
        <location filename="../src/WindowMain.cc" line="1379"/>
        <source>Starting: %1</source>
        <translation>Avvio: %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1188"/>
        <source>Failure 1 during preparation for LIBPF activation</source>
        <translation>Malfunzionamento 1 durante la preparazione dell&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="82"/>
        <source>User Interface for Process Flowsheeting</source>
        <translation>Interfaccia utente per la simulazione di processo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1191"/>
        <source>Failure 2 during preparation for LIBPF activation</source>
        <translation>Malfunzionamento 2 durante la preparazione dell&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1196"/>
        <source>LIBPF is already activated !</source>
        <translation>LIBPF è già attivato !</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1199"/>
        <source>Failure 3 during preparation for LIBPF activation</source>
        <translation>Malfunzionamento 3 durante la preparazione dell&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1207"/>
        <source>Failure 1 during LIBPF activation</source>
        <translation>Malfunzionamento 1 durante l&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1210"/>
        <source>Failure 2 during LIBPF activation</source>
        <translation>Malfunzionamento 2 durante l&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1228"/>
        <location filename="../src/WindowMain.cc" line="1251"/>
        <source>Failure to start the LIBPF activation</source>
        <translation>Malfunzionamento durante l&apos;avvio dell&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1232"/>
        <location filename="../src/WindowMain.cc" line="1255"/>
        <source>Failure during LIBPF activation</source>
        <translation>Malfunzionamento durante l&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <source>The LIBPF activation is absent</source>
        <translation type="vanished">L&apos;attivazione LIBPF non è presente</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1263"/>
        <source>Error during LIBPF activation</source>
        <translation>Errore durante l&apos;attivazione LIBPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1265"/>
        <source>LIBPF activation success !</source>
        <translation>LIBPF è attivato !</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1271"/>
        <source>Deleting object %1</source>
        <translation>Cancello l&apos;oggetto %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1293"/>
        <source>Starting homotopy run</source>
        <translation>Avvia l&apos;omotopia</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1303"/>
        <source>Confirm purge</source>
        <translation>Conferma il comando svuota</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1303"/>
        <source>Do you really wish to delete all existing cases ?</source>
        <translation>Vuoi veramente eliminare tutti i problemi ?</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1306"/>
        <source>Deleting all existing cases</source>
        <translation>Cancello tutti i problemi</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1328"/>
        <source>Interrupted calculation</source>
        <translation>Calcolo interrotto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1334"/>
        <source>Cleared log window</source>
        <translation>Pulita la finestra dei log</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1351"/>
        <source>About uipf</source>
        <translation>Informazioni su UIPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1360"/>
        <source>.&lt;p&gt;Icons by Tango-icon-theme, Tango Desktop Project.&lt;br&gt;Based in part on code from the example classes of the Qt Toolkit:&lt;br&gt;Copyright &amp;copy; 2011 Nokia Corporation and/or its subsidiary(-ies).&lt;p&gt;&lt;b&gt;UIPF&lt;/b&gt;, the User Interface for Process Flowsheeting provides &lt;br&gt;the User Interface to process_ models developed using &lt;b&gt;LIBPF&lt;/b&gt;, &lt;br&gt;the &lt;b&gt;LIB&lt;/b&gt;rary for &lt;b&gt;P&lt;/b&gt;rocess &lt;b&gt;F&lt;/b&gt;lowsheeting in C++ &lt;p&gt;For more informations please visit &lt;a href=&quot;http://www.libpf.com/&quot;&gt;the LIBPF website&lt;/a&gt;.</source>
        <translation>.&lt;p&gt;Icons by Tango-icon-theme, Tango Desktop Project.&lt;br&gt;Basato anche su codice dagli esempi del Qt Toolkit:&lt;br&gt;Copyright &amp;copy; 2011 Nokia Corporation and/or its subsidiary(-ies).&lt;p&gt;&lt;b&gt;UIPF&lt;/b&gt;, (User Interface for Process Flowsheeting) fornisce &lt;br&gt;un&apos;interfaccia utente per accedere ai modelli di processo sviluppati con &lt;b&gt;LIBPF&lt;/b&gt;, &lt;br&gt;(&lt;b&gt;LIB&lt;/b&gt;rary for &lt;b&gt;P&lt;/b&gt;rocess &lt;b&gt;F&lt;/b&gt;lowsheeting in C++) &lt;p&gt;Per ulteriori informazioni visita il &lt;a href=&quot;http://www.libpf.com/&quot;&gt;sito web dedicato a LIBPF&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1385"/>
        <source>Error writing to OpenOffice spreadsheet %1</source>
        <translation>Errore durante la scrittura del foglio elettronico OpenOffice %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1391"/>
        <source>Opening OpenOffice spreadsheet %1</source>
        <translation>Apre il foglio elettronico OpenOffice %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1393"/>
        <source>OpenOffice spreadsheet %1 opened</source>
        <translation>Ill foglio elettronico OpenOffice %1 è stato aperto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1395"/>
        <source>Error opening OpenOffice spreadsheet %1</source>
        <translation>Errore durante l&apos;apertura del foglio elettronico OpenOffice %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1410"/>
        <source>Opening Excel spreadsheet %1</source>
        <translation>Apre il foglio elettronico Excel %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1416"/>
        <source>Excel spreadsheet %1 opened</source>
        <translation>Ill foglio elettronico Excel %1 è stato aperto</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1418"/>
        <source>Error opening Excel spreadsheet %1</source>
        <translation>Errore durante l&apos;apertura del foglio elettronico Excel %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1427"/>
        <source>Select kernel</source>
        <translation>Seleziona il kernel</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1436"/>
        <source>Set active kernel to %1</source>
        <translation>Imposta %1 come motore di calcolo (kernel) attivo</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1444"/>
        <location filename="../src/WindowMain.cc" line="1446"/>
        <source>Invalid selected kernel</source>
        <translation>Il motore di calcolo (kernel) selezionato è invalido</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1444"/>
        <source>The kernel can only reside inside the %1 directory.</source>
        <translation>Il motore di calcolo (kernel) può solo trovarsi all&apos;interno della cartella %1.</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1499"/>
        <source>Restart to apply language change</source>
        <translation>Riavvia il programma per rendere efficace il cambiamento di lingua</translation>
    </message>
</context>
</TS>
