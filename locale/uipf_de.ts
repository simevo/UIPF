<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DialogDuplicate</name>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="29"/>
        <source>Tag:</source>
        <translation>Aufschrift:</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="32"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="41"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="42"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/DialogDuplicate.cc" line="57"/>
        <source>New case</source>
        <translation>Neue Simulation</translation>
    </message>
</context>
<context>
    <name>DialogNew</name>
    <message>
        <location filename="../src/DialogNew.cc" line="56"/>
        <source>Tag:</source>
        <translation>Aufschrift:</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="59"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="65"/>
        <source>types</source>
        <translation>Typen</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="94"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="95"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/DialogNew.cc" line="103"/>
        <source>New case</source>
        <translation>Neue Simulation</translation>
    </message>
</context>
<context>
    <name>DialogOpen</name>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Tag</source>
        <translation>Aufschrift</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>ID</source>
        <translation>Kennung</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="69"/>
        <source>RANGE</source>
        <translation>Spanne</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="91"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="92"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/DialogOpen.cc" line="106"/>
        <source>Open case</source>
        <translation>Simulation öffnen</translation>
    </message>
</context>
<context>
    <name>DialogSensitivity</name>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="56"/>
        <source>Sensitivity analysis</source>
        <translation>Sensitivitätsanalyse</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="71"/>
        <source>Select the variables to manipulate and their ranges</source>
        <translation>Wählen Sie die Regelgrößen und ihre Bereiche</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="87"/>
        <source>Multi-dimensional sensitivity ordering</source>
        <translation>Reihenfolge der mehrdimensionale Sensitivitätsanalyse</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="89"/>
        <source>Lexicographical</source>
        <translation>Lexikographisch</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="91"/>
        <source>Boustrophedon</source>
        <translation>Bustrophedon</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="93"/>
        <source>Quasi-spiral</source>
        <translation>Quasi-schneckenförmig</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="98"/>
        <source>Maximum running time (s)</source>
        <translation>Maximale Laufzeit (s)</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="103"/>
        <source>Enter 0 to run indefinitely</source>
        <translation>Null eingeben um die Grenze aufzuheben</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="124"/>
        <location filename="../src/DialogSensitivity.cc" line="184"/>
        <location filename="../src/DialogSensitivity.cc" line="220"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="127"/>
        <location filename="../src/DialogSensitivity.cc" line="192"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="158"/>
        <source>Control</source>
        <translation>Regeln</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="165"/>
        <source>Select the variables that will be reported for each value of the controlled variable</source>
        <translation>Wählen Sie die Variablen, die für jeden Wert der Regelgrößen angezeigt werden müssen</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="186"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="190"/>
        <source>Previous</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="207"/>
        <source>Monitor</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="222"/>
        <source>Go</source>
        <translation>Ausführen</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="225"/>
        <source>Stop</source>
        <translation>Anhalten</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="228"/>
        <source>Copy results to clipboard</source>
        <translation>Ergebnisse in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="230"/>
        <source>Edit sensitivity</source>
        <translation>Sensitivitätsanalyse bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="248"/>
        <source>Results</source>
        <translation>Ergebnisse</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="371"/>
        <source>ID</source>
        <translation>Kennung</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="510"/>
        <location filename="../src/DialogSensitivity.cc" line="534"/>
        <source>Dismiss?</source>
        <translation>Schliessen?</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="511"/>
        <location filename="../src/DialogSensitivity.cc" line="535"/>
        <source>Are you sure you want to dismiss the sensitivity?</source>
        <translation>Sind Sie sicher, dass Sie die Sensitivitätsanalyse schliessen wollen?</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="513"/>
        <source>All sensitivity settings and results will be lost</source>
        <translation>Alle Einstellungen und Ergebnisse der Sensitivitätsanalyse werden gelöscht</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="515"/>
        <source>All sensitivity settings will be lost</source>
        <translation>Alle Einstellungen der Sensitivitätsanalyse werden gelöscht</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="536"/>
        <source>All results will be lost</source>
        <translation>Alle Ergebnisse werden gelöscht</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="623"/>
        <source>Controlled variable %1</source>
        <translation>Regelgröße %1</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="625"/>
        <source>Monitored variable %1</source>
        <translation>Messgröße %1</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivity.cc" line="646"/>
        <source>All results have been copied to the clipboard</source>
        <translation>Alle Ergebnisse sind in die Zwischenablage</translation>
    </message>
</context>
<context>
    <name>DialogSensitivityOpen</name>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="34"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="34"/>
        <source>The directory &apos;%1&apos; couldn&apos;t be opened for reading.</source>
        <translation>Fehler beim Lesen des Verzeichnis &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="42"/>
        <source>Open existing multi-dimensional sensitivity study</source>
        <translation>Öffnet die mehrdimensionale Sensitivitätsanalyse</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="71"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivityOpen.cc" line="73"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>DialogSensitivitySave</name>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="38"/>
        <source>Enter sensitivity name:</source>
        <translation>Name der Sensitivitätsanalyse:</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="44"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="47"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="78"/>
        <source>Save sensitivity analysis</source>
        <translation>Sensitivitätsanalyse speichern</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="91"/>
        <source>Overwrite?</source>
        <translation>Überschreiben?</translation>
    </message>
    <message>
        <location filename="../src/DialogSensitivitySave.cc" line="91"/>
        <source>An analysis named &apos;%1&apos; already exists. Do you want to overwrite it?</source>
        <translation>Eine Sensitivitätsanalyse namens &apos;%1&apos; is schon vorhanden. Wollen Sie sie wirklich überschreiben?</translation>
    </message>
</context>
<context>
    <name>ModelDataControl</name>
    <message>
        <location filename="../src/ModelDataControl.cc" line="250"/>
        <source>ID</source>
        <translation>Kennung</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="252"/>
        <source>Controlled variables</source>
        <translation>Regelgrößen</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="254"/>
        <source>Units</source>
        <translation>Meßeinheit</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="256"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="258"/>
        <source>Label</source>
        <translation>Kurze Bezeichnung</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="260"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="262"/>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../src/ModelDataControl.cc" line="264"/>
        <source>Points</source>
        <translation>Punkte</translation>
    </message>
</context>
<context>
    <name>ModelDataMonitor</name>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="186"/>
        <source>ID</source>
        <translation>Kennung</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="188"/>
        <source>Monitored variables</source>
        <translation>Messgrößen</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="190"/>
        <source>Units</source>
        <translation>Meßeinheit</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="192"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../src/ModelDataMonitor.cc" line="194"/>
        <source>Label</source>
        <translation>Kurze Bezeichnung</translation>
    </message>
</context>
<context>
    <name>ModelQuantityEditable</name>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="150"/>
        <source>Tag</source>
        <translation>Aufschrift</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="151"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="152"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="153"/>
        <source>Units</source>
        <translation>Meßeinheit</translation>
    </message>
    <message>
        <location filename="../src/ModelQuantityEditable.cc" line="154"/>
        <source>ID</source>
        <translation>Kennung</translation>
    </message>
</context>
<context>
    <name>ModelSensitivityList</name>
    <message>
        <location filename="../src/ModelSensitivityList.cc" line="119"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/ModelSensitivityList.cc" line="122"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/uipf.cc" line="63"/>
        <source>Database Error</source>
        <translation>Datenbankfehler</translation>
    </message>
    <message>
        <location filename="../src/CommandDataSetDouble.cc" line="45"/>
        <source>Will set again the value of %1 (%2) to %3</source>
        <translation>Der Wert von %1 (%2) wird wieder zu %3 gesetzt</translation>
    </message>
    <message>
        <location filename="../src/CommandDataSetDouble.cc" line="55"/>
        <source>Will set the value of %1 (%2) back to %3</source>
        <translation>Der Wert von %1 (%2) wird zurück zu %3 gesetzt</translation>
    </message>
</context>
<context>
    <name>ValidatorRegExp</name>
    <message>
        <location filename="../src/ValidatorRegExp.cc" line="34"/>
        <source>The character &apos;%1&apos; is not allowed in the &apos;%2&apos; field.</source>
        <translation>Zeichen &apos;%1&apos; nicht im Feld &apos;%2&apos; erlaubt.</translation>
    </message>
</context>
<context>
    <name>ValidatorText</name>
    <message>
        <location filename="../src/ValidatorText.cc" line="53"/>
        <source>Character &apos;%1&apos; is numeric therefore it is not allowed as first character in the %2 field</source>
        <translation>Zeichen &apos;%1&apos; ist numerisch daher nicht als erstes Zeichen im Feld %2 erlaubt</translation>
    </message>
    <message>
        <location filename="../src/ValidatorText.cc" line="55"/>
        <source>Character &apos;%1&apos; is not alphabetic therefore it is not allowed as first character in the %2 field</source>
        <translation>Zeichen &apos;%1&apos; ist nicht alphabetisch daher nicht als erstes Zeichen im Feld %2 erlaubt</translation>
    </message>
    <message>
        <location filename="../src/ValidatorText.cc" line="67"/>
        <source>Character &apos;%1&apos; is not allowed in the %2 field</source>
        <translation>Zeichen &apos;%1&apos; nicht im Feld %2 erlaubt</translation>
    </message>
</context>
<context>
    <name>WidgetTableDouble</name>
    <message>
        <location filename="../src/WidgetTableDouble.cc" line="69"/>
        <source>Quantities table</source>
        <translation>Größentabelle</translation>
    </message>
</context>
<context>
    <name>WidgetTableMessages</name>
    <message>
        <location filename="../src/WidgetTableMessages.cc" line="37"/>
        <location filename="../src/WidgetTableMessages.cc" line="78"/>
        <source>Error/Warning message</source>
        <translation>Fehler/Warnung</translation>
    </message>
    <message>
        <location filename="../src/WidgetTableMessages.cc" line="56"/>
        <source>Messages table</source>
        <translation>Tabelle der Meldungen</translation>
    </message>
</context>
<context>
    <name>WidgetTree</name>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Tag</source>
        <translation>Aufschrift</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>ID</source>
        <translation>Kennung</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="89"/>
        <source>Parent ID</source>
        <translation>Vorgängerkennung</translation>
    </message>
    <message>
        <location filename="../src/WidgetTree.cc" line="120"/>
        <location filename="../src/WidgetTree.cc" line="129"/>
        <source>Document has large number of elements (&gt;= %1). Expanding all may take lots of time. 
Application could hang.
Are you sure you want to expand them all ?</source>
        <translation>Das Dokument hat eine große Anzahl von Elementen (&gt;= %1).
Die Erweiterung von allen Elementen könnte ziemlich lange dauern. Die Anwendung könnte dabei stehen bleiben.
Wollen Sie wirklich alle Objekte erweitern?</translation>
    </message>
    <message>
        <source>Document has large number of elements (&gt;= %1. Expanding all may take lots of time. 
Application could hang.
Are you sure you want to expand them all ? </source>
        <translation type="obsolete">Das Dokument hat eine große Anzahl von Elementen (&gt;= %1.
Die Erweiterung von allen Elementen könnte ziemlich lange dauern. Die Anwendung könnte dabei stehen bleiben.
Wollen Sie wirklich alle Objekte erweitern?</translation>
    </message>
</context>
<context>
    <name>WindowMain</name>
    <message>
        <location filename="../src/WindowMain.cc" line="91"/>
        <source>&amp;New</source>
        <translation>&amp;Neue</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="92"/>
        <source>Create a new case</source>
        <translation>Neue Simulation erstellen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="96"/>
        <source>&amp;Open</source>
        <translation>&amp;Öffnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="97"/>
        <source>Open an existing case</source>
        <translation>Vorhandene Simulation öffnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="101"/>
        <source>&amp;Save as</source>
        <translation>&amp;Speichern unter</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="102"/>
        <source>Save the current case with a different description</source>
        <translation>Die Simulation mit einer neuen Beschreibung speichern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="107"/>
        <source>&amp;Delete</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="108"/>
        <source>Delete an existing case</source>
        <translation>Vorhandene Simulation löschen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="113"/>
        <source>&amp;Purge</source>
        <translation>&amp;Reinigen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="114"/>
        <source>Delete all existing cases</source>
        <translation>Alle Simulationen löschen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="118"/>
        <source>&amp;Exit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="119"/>
        <source>Quit the program</source>
        <translation>UIPF beenden</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="128"/>
        <source>&amp;Undo</source>
        <translation>&amp;Rückgängig machen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="133"/>
        <source>Undo last change</source>
        <translation>Letzte Änderung rückgängig machen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="140"/>
        <source>&amp;Redo</source>
        <translation>&amp;Wiederholen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="144"/>
        <source>Redo last change</source>
        <translation>Letzte Änderung wiederholen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="151"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="152"/>
        <source>Copy table region</source>
        <translation>Bereich der Tabelle kopieren</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="159"/>
        <source>&amp;Select All</source>
        <translation>&amp;Alles auswählen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="160"/>
        <source>Select entire table</source>
        <translation>Gesamte Tabelle auswählen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="167"/>
        <source>&amp;Reset</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="168"/>
        <source>Reset the case with its original results</source>
        <translation>Die Simulation mit ihrer ursprünglichen Ergebnissen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="173"/>
        <source>&amp;Calculate</source>
        <translation>&amp;Berechnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="174"/>
        <source>Launch calculation</source>
        <translation>Berechnung starten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="179"/>
        <source>&amp;Homotopy</source>
        <translation>&amp;Homotopie</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="180"/>
        <source>Move smoothly to a different solution</source>
        <translation>Stetig zu eine andere Lösung gehen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="185"/>
        <source>&amp;Stop</source>
        <translation>&amp;Anhalten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="186"/>
        <source>Stop the calculation</source>
        <translation>Berechnung anhalten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="191"/>
        <source>&amp;About</source>
        <translation>Über &amp;UIPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="192"/>
        <source>Show the application&apos;s About box</source>
        <translation>Informationen über UIPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="196"/>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="197"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Informationen über die Qt Bibliothek</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="201"/>
        <source>&amp;ODS stream results</source>
        <translation>&amp;ODS Stromergebnisse</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="202"/>
        <source>Open stream results for last run in OpenDocument Spreadsheet format</source>
        <translation>Die Stromergebnisse für den letzten Programmdurchlauf in OpenDocument Tabellenformat öffnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="207"/>
        <source>&amp;XLS stream results</source>
        <translation>&amp;XLS Stromergebnisse</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="208"/>
        <source>Open stream results for last run in Microsoft Excel Spreadsheet format</source>
        <translation>Die Stromergebnisse für den letzten Programmdurchlauf in Microsoft Excel Tabellenformat öffnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="215"/>
        <source>&amp;TXT results</source>
        <translation>&amp;TXT Ergebnisse</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="221"/>
        <source>&amp;HTML results</source>
        <translation>&amp;HTML Ergebnisse</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="222"/>
        <source>Export all results for the current node in HTML format</source>
        <translation>Alle Ergebnisse für den ausgewählten Objekt in HTML Format öffnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="227"/>
        <source>&amp;Select kernel</source>
        <translation>&amp;Rechnerkern auswählen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="228"/>
        <source>Select active kernel</source>
        <translation>Aktiver Rechnerkern auswählen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="232"/>
        <source>&amp;Expand all</source>
        <translation>Alles &amp;erweitern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="233"/>
        <source>Expand all objects</source>
        <translation>Alle Objekte erweitern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="237"/>
        <source>&amp;Collapse all</source>
        <translation>Alles &amp;komprimieren</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="238"/>
        <source>Collapse all objects</source>
        <translation>Alle Objekte komprimieren</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="242"/>
        <source>&amp;Root</source>
        <translation>&amp;Wurzel</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="243"/>
        <source>Go to root object</source>
        <translation>Zum Wurzelobjekt springen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="247"/>
        <source>&amp;Up</source>
        <translation>Nach &amp;oben</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="248"/>
        <source>Go up one level</source>
        <translation>Eine Ebene nach oben gehen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="252"/>
        <source>&amp;Toggle child</source>
        <translation>Abhängigen &amp;ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="253"/>
        <source>Toggle visualization of child items</source>
        <translation>Die Darstellungsweise der abhängigen Objekten ein-/ausblenden</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="257"/>
        <source>&amp;Sensitivity analysis</source>
        <translation>&amp;Sensitivitätsanalyse</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="678"/>
        <source>Impossible to start the kernel</source>
        <translation>Fehler beim Starten vom Rechnerkerns</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="679"/>
        <source>The kernel started successfully but crashed.</source>
        <translation>Der Rechnerkern wurde erfolgreich gestartet, ist aber gestürtzt.</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="680"/>
        <source>Time out</source>
        <translation>Abgelaufen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="681"/>
        <source>Write error to kernel</source>
        <translation>Schreibfehler zum Rechnerkern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="682"/>
        <source>Read error to kernel</source>
        <translation>Lesefehler zum Rechnerkern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="683"/>
        <source>Unknown error in kernel</source>
        <translation>Unbekannter Fehler zum Rechnerkern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="846"/>
        <source>No action</source>
        <translation>Keine Aktion</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="850"/>
        <source>Unknown action</source>
        <translation>Unbekannte Aktion</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="913"/>
        <source>Duplicating object %1</source>
        <translation>Objekt %1 wird kopiert</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="922"/>
        <source>Writing TXT file for object %1</source>
        <translation>Objekt %1 wird geschrieben auf die TXT Datei</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="977"/>
        <source>Writing HTML file for object %1 to directory %2</source>
        <translation>Objekt %1 wird geschrieben auf HTML Dateien im Verzeichins %2</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1005"/>
        <source>Opening HTML file %1</source>
        <translation>HTML Datei %1 wird geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1007"/>
        <source>HTML file %1 opened</source>
        <translation>HTML Datei %1 geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1009"/>
        <source>Error opening HTML file %1</source>
        <translation>Fehler beim Öffnen der HTML Datei %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1242"/>
        <source>The LIBPF activation utility is missing</source>
        <translation>Das Werkzeug zur LIBPF Aktivierung ist  nicht anwesend</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1352"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2008-2021 Paolo Greppi simevo s.r.l.&lt;br&gt;Based in part on code from Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera, Lahiru Chandima, Shane Shields and Luiz A. Buhnemann (la3280@gmail.com).&lt;br&gt;Language Icon by Onur </source>
        <translation>&lt;p&gt;Copyright &amp;copy; 2008-2021 Paolo Greppi simevo s.r.l.&lt;br&gt;Enthält Quellkode von Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera, Lahiru Chandima Shane Shields und Luiz A. Buhnemann (la3280@gmail.com).&lt;br&gt;Language Icon by Onur </translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1610"/>
        <source>Trying to start: %1 %2</source>
        <translation>Versuch %1 %2 zu starten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="940"/>
        <source>Opening TXT file %1</source>
        <translation>TXT Datei %1 wird geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="942"/>
        <source>TXT file %1 opened</source>
        <translation>TXT Datei %1 geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="944"/>
        <source>Error opening TXT file %1</source>
        <translation>Fehler beim Öffnen der TXT Datei %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="792"/>
        <source>The process has exited with return code %1</source>
        <translation>Der Prozess ist beendet mit Exit-Code %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="833"/>
        <source>The operation completed successfully</source>
        <translation>Der Vorgang wurde erfolgreich beendet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1381"/>
        <source>Failure to start external command %1 for ODS file generation</source>
        <translation>Fehler beim Erstellen der ODS Datei mit dem Befehl %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="216"/>
        <source>Open a text file with all results for the current node</source>
        <translation>Alle Ergebnisse für den ausgewählten Objekt in TXT Format öffnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="258"/>
        <source>Launch multi-dimensional sensitivity analysis</source>
        <translation>Mehrdimensionale Sensitivitätsanalyse starten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="263"/>
        <source>&amp;Open multi-dimensional sensitivity</source>
        <translation>Mehrdimensionale Sensitivitätsanalyse &amp;öffnen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="264"/>
        <source>Opens an existing multidimensional sensitivity analysis</source>
        <translation>Öffnet eine schon existierende mehrdimensionale Sensitivitätsanalyse</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="269"/>
        <source>&amp;Restore last run multi-dimensional sensitivity</source>
        <translation>Letzte mehrdimensionale Sensitivitätsanalyse &amp;wiederherstellen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="270"/>
        <source>Restores the last sensitivity that was executed</source>
        <translation>Wiederherstellt die zuletzt ausgeführte mehrdimensionale Sensitivitätsanalyse</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="275"/>
        <source>&amp;Activate</source>
        <translation>&amp;Aktivieren</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="276"/>
        <source>Activate LIBPF</source>
        <translation>LIBPF aktivieren</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="281"/>
        <source>&amp;SI</source>
        <translation>&amp;SI</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="290"/>
        <source>&amp;EN</source>
        <translation>&amp;EN</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="291"/>
        <source>U.S. Customary Units</source>
        <translation>Angloamerikanisches Maßsystem</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="295"/>
        <source>&amp;Eng</source>
        <translation>&amp;Eng</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="296"/>
        <source>Engineering Units</source>
        <translation>Technisches Maßsystem</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="314"/>
        <source>&amp;Clear</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="350"/>
        <source>&amp;Case</source>
        <translation>&amp;Simulation</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="358"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="365"/>
        <source>&amp;Run</source>
        <translation>&amp;Ablauf</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="371"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="384"/>
        <source>&amp;Tools</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="389"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="393"/>
        <source>Arabic</source>
        <translation>Arabisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="397"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="401"/>
        <source>Spanish</source>
        <translation>Spanisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="405"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="409"/>
        <source>French</source>
        <translation>Französisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="413"/>
        <source>Hebrew</source>
        <translation>Hebräisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="417"/>
        <source>Italian</source>
        <translation>Italienisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="421"/>
        <source>Japanese</source>
        <translation>Japanisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="425"/>
        <source>Korean</source>
        <translation>Koreanisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="429"/>
        <source>Portuguese</source>
        <translation>Portugiesisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="433"/>
        <source>Russian</source>
        <translation>Russisch</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="437"/>
        <source>Chinese (simplified)</source>
        <translation>Chinesisch (vereinfacht)</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="441"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="444"/>
        <source>&amp;Units</source>
        <translation>&amp;Maßsystem</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="445"/>
        <source>Change units of measurements</source>
        <translation>Maßsystem ändern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="451"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="452"/>
        <source>Change User Interface language</source>
        <translation>Sprache im User Interface ändern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="468"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="472"/>
        <source>Main toolbar</source>
        <translation>Haupt-Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="554"/>
        <source>Inputs</source>
        <translation>Eingabedaten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="555"/>
        <source>Outputs</source>
        <translation>Ausgabedaten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="556"/>
        <source>Messages</source>
        <translation>Meldungen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="629"/>
        <source>Status indicator</source>
        <translation>Statusanzeige</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="636"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="665"/>
        <source>Opened node %1</source>
        <translation>Knot %1 geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="671"/>
        <source>The process has been started successfully</source>
        <translation>Der Prozess wurde erfolgreich gestartet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="857"/>
        <source>Opening a new object of type %1</source>
        <translation>Eine neue Simulation vom Typ %1 wird ergestellt</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="864"/>
        <source>Case %1 is already loaded</source>
        <translation>Simulation %1 ist bereits geladen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="868"/>
        <source>Opening the existing case %1</source>
        <translation>Vorhandene Simulation %1 wird geladen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="871"/>
        <source>Opened the existing case %1</source>
        <translation>Vorhandene Simulation %1 geladen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1520"/>
        <source>EMPTY</source>
        <translation>LEER</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1530"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1540"/>
        <source>WARNINGS</source>
        <translation>MELDUNGEN</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1550"/>
        <source>ERRORS</source>
        <translation>FEHLER</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1560"/>
        <source>CHANGED</source>
        <translation>VERÄNDERT</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1570"/>
        <source>INVALID</source>
        <translation>UNGÜLTIG</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1577"/>
        <source>RUNNING</source>
        <translation>LÄUFT</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1585"/>
        <source>UNDEFINED</source>
        <translation>UNBESTIMMT</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1047"/>
        <location filename="../src/WindowMain.cc" line="1068"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1047"/>
        <source>Couldn&apos;t parse sensitivity input file name &apos;%1&apos;</source>
        <translation>Konnte nicht die Dateiname &apos;%1&apos; zergliedern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1068"/>
        <source>Failed to load sensitivity input file &apos;%1&apos;</source>
        <translation>Konnte nicht die Sensitivitätsanalyse Datei &apos;%1&apos; laden</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1204"/>
        <location filename="../src/WindowMain.cc" line="1379"/>
        <source>Starting: %1</source>
        <translation>%1 wird gestartet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1188"/>
        <source>Failure 1 during preparation for LIBPF activation</source>
        <translation>Vorbereitung für LIBPF Aktivierung ausgefallen mit Kode 1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="82"/>
        <source>User Interface for Process Flowsheeting</source>
        <translation>Benutzerschnittstelle für Verfahrensimulation</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1191"/>
        <source>Failure 2 during preparation for LIBPF activation</source>
        <translation>Vorbereitung für LIBPF Aktivierung ausgefallen mit Kode 2</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1196"/>
        <source>LIBPF is already activated !</source>
        <translation>LIBPF ist schon aktiviert !</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1199"/>
        <source>Failure 3 during preparation for LIBPF activation</source>
        <translation>Vorbereitung für LIBPF Aktivierung ausgefallen mit Kode 3</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1207"/>
        <source>Failure 1 during LIBPF activation</source>
        <translation>LIBPF Aktivierung ausgefallen mit Kode 1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1210"/>
        <source>Failure 2 during LIBPF activation</source>
        <translation>LIBPF Aktivierung ausgefallen mit Kode 2</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1228"/>
        <location filename="../src/WindowMain.cc" line="1251"/>
        <source>Failure to start the LIBPF activation</source>
        <translation>Fehler beim Starten der LIBPF Aktivierung</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1232"/>
        <location filename="../src/WindowMain.cc" line="1255"/>
        <source>Failure during LIBPF activation</source>
        <translation>LIBPF Aktivierung ausgefallen</translation>
    </message>
    <message>
        <source>The LIBPF activation is absent</source>
        <translation type="vanished">Die LIBPF Aktivierung ist  nicht anwesend</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1263"/>
        <source>Error during LIBPF activation</source>
        <translation>Fehler bei der LIBPF Aktivierung</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1265"/>
        <source>LIBPF activation success !</source>
        <translation>Erfolg bei der LIBPF Aktivierung !</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1271"/>
        <source>Deleting object %1</source>
        <translation>Objekt %1 wird gelöscht</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1293"/>
        <source>Starting homotopy run</source>
        <translation>Homotopie wird gestartet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1303"/>
        <source>Confirm purge</source>
        <translation>Reinigung bestätigen</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1303"/>
        <source>Do you really wish to delete all existing cases ?</source>
        <translation>Sind Sie sicher, dass Sie alle vorhandene Simulationen löschen wollen ?</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1306"/>
        <source>Deleting all existing cases</source>
        <translation>Alle Simulationen werden gelöscht</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1328"/>
        <source>Interrupted calculation</source>
        <translation>Berechnung angehalten</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1334"/>
        <source>Cleared log window</source>
        <translation>Protokollmeldungen gelöscht</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1351"/>
        <source>About uipf</source>
        <translation>Über UIPF</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1360"/>
        <source>.&lt;p&gt;Icons by Tango-icon-theme, Tango Desktop Project.&lt;br&gt;Based in part on code from the example classes of the Qt Toolkit:&lt;br&gt;Copyright &amp;copy; 2011 Nokia Corporation and/or its subsidiary(-ies).&lt;p&gt;&lt;b&gt;UIPF&lt;/b&gt;, the User Interface for Process Flowsheeting provides &lt;br&gt;the User Interface to process_ models developed using &lt;b&gt;LIBPF&lt;/b&gt;, &lt;br&gt;the &lt;b&gt;LIB&lt;/b&gt;rary for &lt;b&gt;P&lt;/b&gt;rocess &lt;b&gt;F&lt;/b&gt;lowsheeting in C++ &lt;p&gt;For more informations please visit &lt;a href=&quot;http://www.libpf.com/&quot;&gt;the LIBPF website&lt;/a&gt;.</source>
        <translation>.&lt;p&gt;Icons by Tango-icon-theme, Tango Desktop Project.&lt;br&gt;Enthält Quellkode aus der Beispiele des Qt Toolkit:&lt;br&gt;Copyright &amp;copy; 2011 Nokia Corporation and/or its subsidiary(-ies).&lt;p&gt;Durch &lt;b&gt;UIPF&lt;/b&gt;, (User Interface for Process Flowsheeting) kann man Verfahrensmodelle entwickelt mit Hilfe von &lt;b&gt;LIBPF&lt;/b&gt; (the &lt;b&gt;LIB&lt;/b&gt;rary for &lt;b&gt;P&lt;/b&gt;rocess &lt;b&gt;F&lt;/b&gt;lowsheeting in C++) steuern. &lt;p&gt;Für mehr Informationen bitte besuchen Sie die &lt;a href=&quot;http://www.libpf.com/&quot;&gt;LIBPF website&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1385"/>
        <source>Error writing to OpenOffice spreadsheet %1</source>
        <translation>Fehler beim Schreiben der OpenOffice Tabelle %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1391"/>
        <source>Opening OpenOffice spreadsheet %1</source>
        <translation>OpenOffice spreadsheet %1 wird geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1393"/>
        <source>OpenOffice spreadsheet %1 opened</source>
        <translation>OpenOffice Tabelle %1 geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1395"/>
        <source>Error opening OpenOffice spreadsheet %1</source>
        <translation>Fehler beim Öffnen der OpenOffice Tabelle %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1410"/>
        <source>Opening Excel spreadsheet %1</source>
        <translation>Excel spreadsheet %1 wird geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1416"/>
        <source>Excel spreadsheet %1 opened</source>
        <translation>Excel Tabelle %1 geöffnet</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1418"/>
        <source>Error opening Excel spreadsheet %1</source>
        <translation>Fehler beim Öffnen der Excel Tabelle %1</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1427"/>
        <source>Select kernel</source>
        <translation>Auswahl des Rechnerkern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1436"/>
        <source>Set active kernel to %1</source>
        <translation>Aktiver Rechnerkern wird auf %1 eingestellt</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1444"/>
        <location filename="../src/WindowMain.cc" line="1446"/>
        <source>Invalid selected kernel</source>
        <translation>Ungültiger Rechnerkern</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1444"/>
        <source>The kernel can only reside inside the %1 directory.</source>
        <translation>Der Rechnerkern darf nur im Verzeichnis %1 gespeichert werden.</translation>
    </message>
    <message>
        <location filename="../src/WindowMain.cc" line="1499"/>
        <source>Restart to apply language change</source>
        <translation>Um die Einstellung zu Übernehmen: Programm neu starten</translation>
    </message>
</context>
</TS>
