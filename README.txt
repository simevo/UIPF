This is UIPF (User Interface for Process Flowsheeting) version 1.1.241.
The UIPF homepage is https://libpf.com/sdk/libpf-ui.html

UIPF, the User Interface for Process Flowsheeting, provides the User Interface to process models developed using LIBPF, the LIBrary for Process Flowsheeting in C++.
For more informations please visit the LIBPF website at https://www.libpf.com

Credits
=======
(C) Copyright 2008-2021 Paolo Greppi simevo s.r.l. - All rights reserved.
Based in part on code from Dmytro Skrypnyk, Eugen Stoian, Harish Surana, Toto Sugito, Yasantha Samarasekera and Shane Shields.
Language Icon by Onur Müstak Çobanli, http://www.languageicon.org
Icons by Tango-icon-theme, Tango Desktop Project.
Based in part on code from the example classes of the Qt Toolkit:
Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).

License
=======
This program may be used under the terms of the GNU General Public License version 2.0 as published by the Free Software Foundation and appearing in the file UIPF_LICENSE.txt or COPYING included in the distribution.

How to download the source:
===========================
- Install git for your platform from http://git-scm.com
- git clone https://gitlab.com/simevo/UIPF.git
this will create a UIPF folder.

How to build:
=============
- install prerequisites for Qt development (C++ compiler etc.)
- get Qt 5 development libraries for your platform from http://www.qt.io
- from the command line issue:
    cd UIPF
    qmake
    make debug release
- from Qt creator open the UIPF.pro file

How to run:
===========
If you install beforehand the demo, then running a freshly compiled uipf executable will read from the registry the right keys and should interface fine with the kernel. 
