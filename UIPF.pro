# enable to compile with LLVM's clang
# QMAKE_CC=clang
# QMAKE_CXX=clang

# enable to compile with color highlighted warnings and errors on linux
# QMAKE_CC=colorgcc
# QMAKE_CXX=colorgcc

CONFIG += debug_and_release
CONFIG += c++11
CONFIG(debug, debug|release) {
  TARGET = uipf_debug
}
# add QT_NO_DEBUG_OUTPUT to turn off qDebug() messages
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
## add QT_NO_WARNING_OUTPUT to turn off qWarning() messages
#CONFIG(release, debug|release):DEFINES += QT_NO_WARNING_OUTPUT

Release:DESTDIR = ../bin/Release.UIPF
Release:OBJECTS_DIR = ../bin/Release.UIPF/.obj
Release:MOC_DIR = ../bin/Release.UIPF/.moc
Release:RCC_DIR = ../bin/Release.UIPF/.rcc
Release:UI_DIR = ../bin/Release.UIPF/.ui

Debug:DESTDIR = ../bin/Debug.UIPF
Debug:OBJECTS_DIR = ../bin/Debug.UIPF/.obj
Debug:MOC_DIR = ../bin/Debug.UIPF/.moc
Debug:RCC_DIR = ../bin/Debug.UIPF/.rcc
Debug:UI_DIR = ../bin/Debug.UIPF/.ui

INCLUDEPATH += include

QT           += sql
QT           += svg
QT           += xml
QT           += xmlpatterns
greaterThan(QT_MAJOR_VERSION, 4) {
  QT += widgets
}

HEADERS     +=   include/ActionBase.h \
  include/ActionGo.h \
  include/ActionLibpf.h \
  include/ActionLibpfSensitivity.h \
  include/CommandDataSetDouble.h \
  include/CommandDataSet.h \
  include/ConvertUTF.h \
  include/DataXml.h \
  include/DelegateCheckBox.h \
  include/DelegateControl.h \
  include/DelegateDouble.h \
  include/DelegateMessage.h \
  include/DelegateMonitor.h \
  include/DelegateRadioButton.h \
  include/DelegateUnit.h \
  include/DialogDuplicate.h \
  include/DialogNew.h \
  include/DialogOpen.h \
  include/DialogSensitivity.h \
  include/DialogSensitivitySave.h \
  include/DialogSensitivityOpen.h \
  include/Dimension.h \
  include/Element.h \
  include/HeaderTableControl.h \
  include/HeaderTableMonitor.h \
  include/Hierarchy.h \
  include/Homotopy.h \
  include/itoa.h \
  include/LineEditValidated.h \
  include/ModelDataControl.h \
  include/ModelDataMonitor.h \
  include/ModelEditable.h \
  include/ModelNameVariableControl.h \
  include/ModelQuantityEditable.h \
  include/ModelSensitivityList.h \
  include/Ordering.h \
  include/OrderingLexicographical.h \
  include/OrderingReflectedGray.h \
  include/OrderingSpiral.h \
  include/persistency.h \
  include/RecordControl.h \
  include/RecordMonitor.h \
  include/ScrollAreaSvg.h \
  include/Settings.h \
  include/strtopath.h \
  include/Type.h \
  include/UnitArray.h \
  include/UnitEngine.h \
  include/UtfConverter.h \
  include/ValidatorText.h \
  include/ValidatorRegExp.h \
  include/VariableControlled.h \
  include/version.h \
  include/ViewSvgNative.h \
  include/ViewTable.h \
  include/WidgetTableControl.h \
  include/WidgetTableDouble.h \
  include/WidgetTableMessages.h \
  include/WidgetTableMonitor.h \
  include/WidgetTree.h \
  include/WindowMain.h

SOURCES     += src/ActionBase.cc \
  src/ActionGo.cc \
  src/ActionLibpf.cc \
  src/ActionLibpfSensitivity.cc \
  src/CommandDataSet.cc \
  src/CommandDataSetDouble.cc \
  src/ConvertUTF.c \
  src/DataXml.cc \
  src/DelegateCheckBox.cc \
  src/DelegateControl.cc \
  src/DelegateDouble.cc \
  src/DelegateMessage.cc \
  src/DelegateMonitor.cc \
  src/DelegateRadioButton.cc \
  src/DelegateUnit.cc \
  src/DialogDuplicate.cc \
  src/DialogNew.cc \
  src/DialogOpen.cc \
  src/DialogSensitivity.cc \
  src/DialogSensitivitySave.cc \
  src/DialogSensitivityOpen.cc \
  src/Dimension.cc \
  src/HeaderTableControl.cc \
  src/HeaderTableMonitor.cc \
  src/Hierarchy.cc \
  src/Homotopy.cc \
  src/itoa.cc \
  src/LineEditValidated.cc \
  src/ModelDataControl.cc \
  src/ModelDataMonitor.cc \
  src/ModelEditable.cc \
  src/ModelNameVariableControl.cc \
  src/ModelQuantityEditable.cc \
  src/ModelSensitivityList.cc \
  src/Ordering.cc \
  src/OrderingLexicographical.cc \
  src/OrderingReflectedGray.cc \
  src/OrderingSpiral.cc \
  src/ScrollAreaSvg.cc \
  src/Settings.cc \
  src/strtopath.cc \
  src/Type.cc \
  src/uipf.cc \
  src/UnitArray.cc \
  src/UnitEngine.cc \
  src/UtfConverter.cc \
  src/ValidatorText.cc \
  src/ValidatorRegExp.cc \
  src/VariableControlled.cc \
  src/ViewSvgNative.cc \
  src/ViewTable.cc \
  src/WidgetTableControl.cc \
  src/WidgetTableDouble.cc \
  src/WidgetTableMessages.cc \
  src/WidgetTableMonitor.cc \
  src/WidgetTree.cc \
  src/WindowMain.cc

win32 {
  SOURCES += src/exclusion.cc
  SOURCES += src/open_xls.cc
  HEADERS += include/exclusion.h
  HEADERS += include/open_xls.h
  RC_FILE = uipf.rc
  LIBS += oleaut32.lib ole32.lib advapi32.lib shell32.lib
  QMAKE_CXXFLAGS += /FS
}
RESOURCES   = uipf.qrc
macx {
  ICON = images/UIPF.icns
  CONFIG += x86_64 
}
TRANSLATIONS = locale/uipf_it.ts locale/uipf_fr.ts locale/uipf_de.ts locale/uipf_pt.ts locale/uipf_es.ts locale/uipf_ru.ts locale/uipf_ar.ts locale/uipf_he.ts locale/uipf_ko.ts locale/uipf_jp.ts locale/uipf_zh.ts

# deploy
DISTFILES += CHANGES \
    COPYING
unix:!mac { 
    isEmpty(PREFIX):PREFIX = /usr
    BINDIR = $$PREFIX/bin
    INSTALLS += target
    target.path = $$BINDIR
    DATADIR = $$PREFIX/share
    PKGDATADIR = $$DATADIR/uipf
    DEFINES += DATADIR=\\\"$$DATADIR\\\" \
        PKGDATADIR=\\\"$$PKGDATADIR\\\"
    INSTALLS += translations \
        desktop \
        iconsvg \
        icon16 \
        icon22 \
        icon32 \
        icon48 \
        icon64 \
        icon128 \
        icon256 \
        icon512
    translations.path = $$PKGDATADIR
    translations.files += $$DESTDIR/locale
    desktop.path = $$DATADIR/applications
    desktop.files += uipf.desktop
    iconsvg.path = $$DATADIR/icons/hicolor/scalable/apps
    iconsvg.files += data/uipf.svg
    icon16.path = $$DATADIR/icons/hicolor/16x16/apps
    icon16.files += data/16x16/uipf.png
    icon22.path = $$DATADIR/icons/hicolor/22x22/apps
    icon22.files += data/22x22/uipf.png
    icon32.path = $$DATADIR/icons/hicolor/32x32/apps
    icon32.files += data/32x32/uipf.png
    icon48.path = $$DATADIR/icons/hicolor/48x48/apps
    icon48.files += data/48x48/uipf.png
    icon64.path = $$DATADIR/icons/hicolor/64x64/apps
    icon64.files += data/64x64/uipf.png
    icon128.path = $$DATADIR/icons/hicolor/128x128/apps
    icon128.files += data/128x128/uipf.png
    icon256.path = $$DATADIR/icons/hicolor/256x256/apps
    icon256.files += data/256x256/uipf.png
    icon512.path = $$DATADIR/icons/hicolor/512x512/apps
    icon512.files += data/512x512/uipf.png
}
